import * as forms from '../library/forms.mjs';

/*
 * Form
 */
customElements.define('se-async-form', class extends HTMLFormElement {
		//
		constructor() {
			super();

			// Get definitions
			this.objSubmitBtn = this.querySelector('[type="submit"]');
			this.objSubmitActionName = ( this.objSubmitBtn && this.objSubmitBtn.tagName.toLowerCase() === 'button' ) ? this.objSubmitBtn.innerHTML : '';
			this.objOutput = this.querySelector('[data-element="response"]');
		}

		//
		connectedCallback() {
			// Check for initial values
			if ( this.dataset.loadurl ) {
				console.warn("is ready to load");
				this.formLoad();
			}

			// Bindings
			this.addEventListener('submit', this.formSubmit.bind(this));
			this.addEventListener('change', this.formChanged.bind(this));
		}

		//
		formChanged(e) {
			//
			if ( this.checkValidity() ) {
				this.submitBtnUpdate('fa-circle-o', 'pulsate');
			} else {
				this.submitBtnUpdate('fa-warning');
			}
		}

		//
		submitBtnUpdate(exData, iconMod) {
			if ( this.objSubmitActionName !== '' ) {
				iconMod = ( typeof iconMod === 'string' ) ? iconMod : '';
				this.objSubmitBtn.innerHTML = '<svg class="icon inline mr '+iconMod+'"><use xlink:href="#'+exData+ '" /></svg>' + this.objSubmitActionName;
			}
		}


		//
		async formLoad() {
			//
			let fResponse = await fetch(this.dataset.loadurl);
			let msg = await fResponse.json();

			//
			for ( let key in msg.d ) {
				if ( !msg.d.hasOwnProperty(key) ) {
					continue;
				}

				console.log("element type", typeof msg.d[key]);

				// type of content in key
				if ( typeof msg.d[key] === 'object' ) {
					// Is an array with multiple objects
					console.log("is object", key);
					this.flattenNameAndAssign(key, msg.d[key]);
				} else {
					this.assignValue(key, msg.d[key]);
				}
			}
		}

		//
		flattenNameAndAssign(name, result) {
			//
			for ( let cSubName in result ) {
				let cName = name + "[" + cSubName + "]";
				//
				if ( typeof result[cSubName] === 'object' ) {
					this.flattenNameAndAssign(cName, result[cSubName]);
				} else {
					this.assignValue(cName, result[cSubName]);
				}
			}
		}

		assignValue(elName, elValue) {
			//
			let cEl = this.elements.namedItem(elName);
			//
			if ( !cEl ) {
				console.trace("Form El Val, undefined element:", elName);
				return;
			}

			if ( cEl.type && cEl.type.toLowerCase() === 'checkbox' ) {
				cEl.checked = (elValue);
			} else {
				cEl.value = elValue;
			}
		}

		//
		async formSubmit(e) {
			e.preventDefault();

			//
			this.submitBtnUpdate('fa-spinner', 'spin');

			//
			let fResponse = await fetch(
				this.action,
				{
					method: this.method,
					body: new FormData(this),
				}
			);

			//
			let msg = await fResponse.json();

			//
			let cStatus = {};

			// Not valid, show error
			if ( msg.s.t ) {
				cStatus = {
					icon:'fa-check',
					style:'notification success',
				}
			} else {
				cStatus = {
					icon:'fa-remove',
					style:'notification alert',
				}
			}

			//
			this.submitBtnUpdate(cStatus.icon);
			if ( this.objOutput !== null ) {
				//
				this.notificationBuild(
					{
						title:msg.s.d,
						content:msg.s.ex,
						...cStatus
					}
				);
			}
		}

		notificationBuild(cOptions) {
			if ( !this.objOutput ) { return; }

			//
			let defaults = {
					icon:null,
					iconMod:null,
					title:null,
					content:'',
					contentHTML:false,
					style:null,
					autoClear:null
				},
				settings = {...defaults, ...cOptions},
				//
				html = '',
				clearInterval;

			//
			if ( settings.icon ) {
				html+= '<div class="icon">';
				if ( settings.icon.indexOf('.') === -1 )
				{
					settings.iconMod = ( typeof settings.iconMod === 'string' ) ? settings.iconMod : '';
					html+= '<svg class="' + settings.iconMod + '"><use xlink:href="#' + settings.icon + '" /></svg>';
				} else {
					console.log("NOTIFICACION, IMAGEN NO PROGRAMADA");
				}
				html+= '</div>';
			}
			// Contenido
			let htmlContent = ( settings.contentHTML ) ? ' html' : '';
			html+= '<div class="text"><div class="title">' + settings.title  + '</div><div class="content'+ htmlContent +'">' + settings.content + '</div></div>';

			// Apply
			this.objOutput.className = settings.style;
			this.objOutput.innerHTML = html;

			//
			if ( typeof settings.autoClear === 'number' && settings.autoClear !== 0 ) {
				clearInterval = setTimeout(() => {
					this.objOutput.se_empty();
				}, settings.autoClear);
			} else {
				clearTimeout(clearInterval);
			}
		}
	},
	{extends: 'form'}
);