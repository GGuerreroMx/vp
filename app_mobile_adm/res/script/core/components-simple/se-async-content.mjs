import {stringPopulate} from "../library/struct.mjs";

// Define the new element
customElements.define('se-async-content', class extends HTMLElement {
	//
	constructor() {
		// Always call super first in constructor
		super();

		//
		this.url = null;
		this.scrolls = 4;
		this.queryFilter = '';
		this.queryOrder = '';

		//
		this.properties = {
			scrollCounter: 0,
			pageCurrent: 0,
			pageTotal: 0,
		};

		//
		this.loadingBtn = this.querySelector(':scope > button.loadMore');


		//
		this.queryData = {
			isNew: true,
			pCur: 0
		};

		//
		this.contentTemplate = this.querySelector('template');
		this.contentContainer = this.querySelector('[data-type="container"]');
	}

	//
	connectedCallback() {
		this.buildFilters();
		this.loadData(true);

		// Button
		this.loadingBtn.addEventListener("click", this.loadBtnClick.bind(this), false);

		// Loading observer
		let observer = new IntersectionObserver(this.observeLoading.bind(this));
		observer.observe(this.loadingBtn);
	}

	// component attributes
	static get observedAttributes() {
		return ['url', 'scrolls', 'filter', 'order'];
	}

	//
	attributeChangedCallback(property, oldValue, newValue) {
		//
		this[property] = newValue;

		//
		switch ( property ) {
			//
			case 'filter':
			case 'order':
				this.buildFilters();
				break;
		}
		//
	}

	//
	loadBtnClick() {
		this.properties.scrollCounter = 0;
		this.loadData(false);
	}

	observeLoading() {
		//
		if ( this.dataset.condition === 'loading' ) {
			return;
		}

		//
		if ( this.properties.scrollCounter < this.scrolls ) {
			this.loadData();
			this.properties.scrollCounter++;
		}
	}

	buildFilters() {
		this.queryFilter = '';
		this.queryOrder = '';

		//
		for ( let keyName in this.dataset ) {
			console.log("current", keyName, keyName.startsWith('where:'), keyName.startsWith('order:'));
			if ( keyName.startsWith('where:') ) {
				let cleanName = keyName.substring(6);
				this.queryFilter+= 'where[' + cleanName + ']=' + encodeURIComponent(this.dataset[keyName]) + '&';
			} else if ( keyName.startsWith('order:') ) {
				let cleanName = keyName.substring(6);
				this.queryOrder+= 'order[' + cleanName + ']=' + encodeURIComponent(this.dataset[keyName]) + '&';
			}
		}
	}

	//
	async loadData(clean = false) {
		if ( this.dataset.condition === 'loading' ) {
			return;
		}

		this.dataset.condition = 'loading';
		//
		if ( !this.url ) {
			console.error("target url not defined.");
			return;
		}

		//
		this.queryData.isNew = false;
		this.properties.pageCurrent++;

		//
		if ( clean ) {
			this.contentContainer.textContent = '';
			this.properties.pageCurrent = 0;
			this.queryData.isNew = true;
		}

		//
		this.queryData.pCur = this.properties.pageCurrent;

		//
		const params = new URLSearchParams(this.queryData);

		//
		let fResponse = await fetch(this.url + '?' + this.queryFilter + this.queryOrder + params.toString());

		let msg = await fResponse.json();

		let templateString = this.contentTemplate.innerHTML;
		for ( let cRow of msg.d ) {
			this.contentContainer.insertAdjacentHTML('beforeend', stringPopulate(templateString, cRow));
		}

		// Set total pages
		if ( this.properties.pageTotal === 0 ) {
			this.properties.pageTotal = msg.p.pTot;
		}

		// Set current status
		this.dataset.condition = ( this.properties.pageCurrent === this.properties.pageTotal ) ? 'complete' : 'loaded';
	}
});
