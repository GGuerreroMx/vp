/*
 * Auto adjusting
 */
customElements.define(
	'se-textarea-autoadjust',
	class extends HTMLTextAreaElement {
		charCounter = true;
		charLimit;
		minHeight = 300;
		counter;

		//
		constructor() {
			super();
		}

		//
		connectedCallback() {
			// styling defautls
			this.style.minWidth = "300px";
			this.style.maxHeight = "70vh";

			// create definitions
			this.charLimit = parseInt(this.attributes['maxlength'].value);
			if ( this.charCounter && this.charLimit ) {
				let curRem = this.value.length;
				// Add HTML
				this.insertAdjacentHTML('afterend','<div class="textAreaCounter"><span data-type="current">' + curRem + '</span><span> / '+this.charLimit+'</span></div>');
				this.counter = this.nextElementSibling.querySelector('[data-type="current"]');
				// Events
				this.addEventListener('keyup', this.reCount.bind(this));
			}

			//
			this.resize();

			// Events
			this.addEventListener('input', this.resize.bind(this));
			this.addEventListener('change', this.resize.bind(this));
		}

		//
		resize() {
			let cs = getComputedStyle(this),
				offset, tSize;
			this.style.height = "0";
			if ( cs.boxSizing === "border-box" ) {
				offset = this.offsetHeight;
			}
			else if ( cs.boxSizing === "content-box" ) {
				offset = -this.clientHeight;
			}
			tSize = this.scrollHeight + offset;
			tSize =  ( tSize > this.minHeight ) ? tSize : this.minHeight;
			this.style.height = tSize + "px";
		}

		//
		reCount() {
			this.counter.innerText = this.value.length;
		}
	},
	{extends: 'textarea'}
);