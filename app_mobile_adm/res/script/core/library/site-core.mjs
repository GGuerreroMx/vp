import * as domManipulator from './dom-manipulation.mjs';

// This framework prioritizes support for custom elements, but safari (webkit) do not want to support the built in ones.
// So in order to allow them a polifill is used, but conditionally, hence this segment of code.

// Custom element built in test
let supportsBuiltInCustomElements = false;
document.createElement('div', {
	get is() {
		supportsBuiltInCustomElements = true;
	}
});

// Not supported, load library
if ( !supportsBuiltInCustomElements ) {
	import('./../component-external/esm-compatibility.mjs')
		.then((module) => {
			console.log("support for custom built in elements loaded.");
		});
}

// Set service worker
export function serviceWorkerSet(fileName) {
	// Service Worker
	if ( !('serviceWorker' in navigator) ) {
		console.warn("Service workers aren\'t supported in this browser");
		return;
	}

	//
	navigator.serviceWorker.register(fileName).then((reg) => {
		console.log("SW Registration:");
		if ( reg.installing ) {
			console.log('SW installing');
		} else if ( reg.waiting ) {
			console.log('SW installed');
		} else if ( reg.active ) {
			console.log('SW active');
		}
	}).catch((error) => {
		console.log("SW REGISTER FAILURE", error);
	});
}

let siteProperties = {};

// Initial setup
export function basicSetup() {
	// Set link click call back
	document.body.se_on('click', 'a[se-nav]', pageNavigationFromLink);

	// History managment
	if ( typeof history.replaceState !== "undefined" ) {
		history.replaceState({
			'function': 'pageInit',
			'target': 'se_middle',
			'url': window.location.pathname + window.location.search
		}, null, null);
	}

	// History change navigation behaviour
	window.onpopstate = pageLoadFromPopState;

	// Load files if exist
	let fileList = document.head.querySelector('meta[name="se:files"]')?.getAttribute('content');
	if ( typeof fileList === 'string' ) {
		let fileArray = fileList.split(';');
		filesLoadAsync(fileArray);
	}
	siteProperties = document.head.querySelector('meta[name="se:properties"]')?.dataset;
}

//<editor-fold desc="Site navigation">

//
function pageNavigationFromLink(event, element) {
	event.preventDefault();

	//
	pageCall(element.href, element.getAttribute('se-nav'), true)
	.catch((error) => {
		console.error("PAGE LOAD ERROR", error)
	});
}

//
function pageLoadFromPopState(event) {
	if ( event.state === null ) {
		return;
	}

	//
	let stateObj = event.state;
	stateObj.target = ( document.body.querySelector('#' + stateObj.target) ) ? stateObj.target : 'se_middle';
	pageCall(stateObj.url, stateObj.target, false);
}


//
async function pageCall(targetUrl, pageLocation, saveToHistory = false) {
	//
	if ( typeof history.pushState === "undefined" ) {
		location.href = targetUrl;
	}

	// Missing target fix?
	pageLocation = (document.body.querySelector('#' + pageLocation)) ? pageLocation : 'se_middle';

	// URL adjustment
	let parser = document.createElement('a');
	parser.href = targetUrl;
	parser.pathname = '/partial/' + pageLocation + parser.pathname;

	//
	let cState = {
		url: targetUrl,
		rUrl: parser.href,
		target: pageLocation,
		function: null
	};

	const fResponse = await fetch(parser.href);

	// Validate content type
	const contentType = fResponse.headers.get('content-type');
	if ( !contentType || !contentType.includes('application/json') ) {
		throw new TypeError("Response is not valid.");
	}

	if ( !fResponse.ok ) {
		const message = `An error has occured: ${fResponse.status}`;
		throw new Error(message);
	}

	// Redirect cases
	if ( fResponse.status === 307 || fResponse.status === 308 ) {
		await pageCall(fResponse.s.d, pageLocation, true);
		throw new Error("redirected");
	}

	const txtData = await fResponse.text();
	let jsonData = {};

	// Fix possible minor php reports
	let fPost = txtData.indexOf("{");
	if ( fPost === -1 ) {
		throw "No json";
	} else if ( fPost !== 0 ) {
		console.error("AJAX JSON WITH ERRORS: \n\n", txtData.trim().substring(0, fPost));
		jsonData = await JSON.parse(txtData.substring(fPost));
	} else {
		jsonData = await JSON.parse(txtData);
	}

	//
	pageLoad(jsonData.d, cState, saveToHistory);
}

//
function pageLoad(pData, stateObj, saveHistory) {
	// Init
	let head = document.getElementsByTagName('head')[0],
		pageTarget = document.getElementById(stateObj.target),
		cUrl = stateObj.url,
		lastHead = head.querySelector('meta[property="og:type"]'),
		rSimple = (elem, array) => {
			return (elem in array && array[elem] !== '') ? array[elem] : '';
		},
		pluginLoad = () => {
			// Auto-plugins
			// se.main.pluginLoad(pageTarget);
			// console.log("load Plugin", pageTarget);
		},
		cEl;

	// Set history
	if ( saveHistory ) {
		history.pushState(stateObj, pData.title, stateObj.url);
	}

	// Change content (page transition api study)
	pageTarget.innerHTML = pData.body;

	// HEAD
	if ( 'url_path' in pData ) {
		cUrl = pData.url_path;
	}
	let cTitle = rSimple('title', pData.head),
		mDesc = rSimple('metaDesc', pData.head),
		mUrl = siteProperties.url + rSimple('url', pData.head),
		metaElems = {
			'meta[name="keywords"]': {content: rSimple('metaWord', pData.head)},
			'meta[name="description"]': {content: mDesc},
			'link[rel="canonical"]': {href: mUrl},
			//
			'meta[property="og:title"]': {content: cTitle},
			'meta[property="og:url"]': {content: mUrl},
			'meta[property="og:image"]': {content: rSimple('img', pData.head.og) || siteProperties.url + siteProperties.image},
			'meta[property="og:description"]': {content: mDesc},
			'meta[property="og:type"]': {content: rSimple('type', pData.head.og)},
			//
			'meta[name="twitter:card"]': {content: rSimple('twType', pData.head.og)},

		},
		cElem, i;
	//
	document.title = (cTitle === '') ? siteProperties.name : cTitle + ' | ' + siteProperties.name;
	//
	for ( const [cTarget, cProps] of Object.entries(metaElems) ) {
		if ( (cElem = head.querySelector(cTarget)) ) {
			for ( const [cName, cValue] of Object.entries(cProps) ) {
				cElem.setAttribute(cName, cValue);
			}
		}
	}

	// Limpiar de página anterior
	head.querySelectorAll('[data-pageOnly="1"]').se_remove();
	// OG No básicos
	if ( 'data' in pData.head.og && pData.head.og.data.length !== 0 ) {
		for ( i = 0; i < pData.head.og.data.length; i++ ) {
			cEl = pData.head.og.data[i];
			let propName = (cEl[0] === 'twitter') ? 'name' : 'property';
			lastHead.insertAdjacentHTML('afterend', '<meta data-ogcustom="1" ' + propName + '="' + cEl[1] + '" content="' + cEl[2] + '"/>');
		}
	}
	// Extra head
	if ( 'headExtra' in pData && pData.head.extra.length !== 0 ) {
		for ( i = 0; i < pData.head.extra.length; i++ ) {
			cEl = pData.head.extra[i];
			lastHead.insertAdjacentHTML('afterend', '<' + cEl.type + ' data-ogcustom="1" ' + cEl.content + '"/>');
		}
	}

	// Dispatch Event
	document.dispatchEvent(new CustomEvent('pageLoadEvent', { detail: { cUrl:stateObj.url } }));

	// Files
	fileLoadAsync(pData, '#jsAjax', pluginLoad);

	// Scroll
	pageTarget.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
}

//</editor-fold>
//
function fileLoadAsync(pData, jsTarget, onLoad) {
	let jsElem = document.body.querySelector(jsTarget),
		jsText = '',
		jsReady = function () {
			window.eval(jsText);
			if ( typeof onLoad === 'function' ) {
				onLoad();
			}
		};

	//
	if ( 'js' in pData ) {
		jsText += pData.js;
	}
	//
	if ( 'mt' in pData ) {
		jsText += pData.mt;
	}

	//
	try {
		jsElem.text = jsText;
	} catch ( e ) {
		try {
			jsElem.innerHTML = jsText;
		} catch ( ee ) {
			console.log(ee);
		}
	}

	// Posible new bug
	if ( 'files' in pData ) {
		// Ext (appends to other files)
		if ( 'ext' in pData.files && pData.files.ext.length !== 0 ) {
			//
			for ( let i = 0; i < pData.files.ext.length; i++ ) {
				let cFile = pData.files.ext[i];
				if ( cFile.endsWith('.js') || cFile.endsWith('.js.gz') || cFile.includes('.js?') ) {
					pData.files.js.push(cFile);
				} else if ( cFile.endsWith('.mjs') || cFile.endsWith('.mjs.gz') || cFile.includes('.mjs?') ) {
					pData.files.mjs.push(cFile);
				} else if ( cFile.endsWith('.css') || cFile.endsWith('.css.gz') || cFile.includes('.css?') ) {
					pData.files.css.push(cFile);
				} else if ( cFile.endsWith('.svg') || cFile.endsWith('.svgz') ) {
					pData.files.svg.push(cFile);
				} else if ( cFile.includes('//fonts.googleapis.com') ) {
					pData.files.css.push(cFile);
				} else {
					console.log("File async not supported (1): ", cFile);
				}
			}
		}

		// CSS
		if ( 'css' in pData.files && pData.files.css.length !== 0 ) {
			fileLoadCSS(pData.files.css);
		}
		// JS
		if ( 'js' in pData.files && pData.files.js.length !== 0 ) {
			fileLoadJS(pData.files.js, false, jsReady);
		} else {
			jsReady();
		}
		// MJS
		if ( 'mjs' in pData.files && pData.files.mjs.length !== 0 ) {
			fileLoadJS(pData.files.mjs, true);
		}
		// SVG
		if ( 'svg' in pData.files && pData.files.svg.length !== 0 ) {
			fileLoadSVG(pData.files.svg);
		}
	} else {
		console.log("NO FILES");
		jsReady();
	}
}


//
function fileLoadJS(fileList, isModule = false, callback = null ) {
	let curJS = document.head.getElementsByTagName('script');

	//
	function loadJSFilePromise(cFileUrl) {
		return new Promise(function(resolve, reject) {
			//
			let compare = fileRealDir(cFileUrl.replace('&amp;', '&'));

			// Search file
			for ( let j = 0, oLen = curJS.length; j < oLen; j++ ) {
				if ( compare === curJS[j].src ) {
					resolve(cFileUrl);
				}
			}
			//
			let nScript = document.createElement('script');
			nScript.src = cFileUrl;
			nScript.async = true;
			if ( isModule ) {
				nScript.type = 'module';
			}
			nScript.onload = () => {
				resolve(cFileUrl);
			};
			nScript.onerror = () => {
				reject(cFileUrl);
			};
			document.head.appendChild(nScript);
		});
	}

	// Create array
	if ( typeof fileList === 'string' ) {
		fileList = [fileList];
	}
	// If not empty
	if ( !fileList ) { return; }


	let promiseData = [];
	fileList.forEach((cFileUrl) => {
		promiseData.push(loadJSFilePromise(cFileUrl));
	});
	Promise.all(promiseData).then(function() {
		console.debug('JS Loaded.');
		if ( typeof callback === 'function' ) {
			callback(null);
		}
	}).catch(function(gfgData) {
		console.log(gfgData + ' failed to load!');
	});
}

//
function fileLoadCSS(file) {
	let curCSS = document.head.getElementsByTagName('link'),
		notExist, compare, cFile;
	// Hacer array
	if ( typeof file === 'string' ) {
		file = [file];
	}

	//
	fileFound: for ( let i = 0, fLen = file.length; i < fLen; i++ ) {
		cFile = file[i];
		compare = fileRealDir(cFile);
		for ( let j = 0, oLen = curCSS.length; j < oLen; j++ ) {
			if ( compare === curCSS[j].href ) {
				continue fileFound;
			}
		}
		//
		let css = document.createElement('link');
		css.rel = "stylesheet";
		css.href = cFile;
		document.head.appendChild(css);
	}
}

//
async function fileLoadSVG(file) {
	//
	if ( typeof file === 'string' ) {
		file = [file];
	} else if ( !Array.isArray(file) ) {
		console.error("Invalid operands", file);
		return;
	}

	for ( let cFile of file ) {
		let response = await fetch( cFile );
		if ( response.status !== 200 ) {
			console.error( 'Looks like there was a problem. Status Code: ' + response.status );
			return;
		}
		let svgData = await response.text();
		document.body.insertAdjacentHTML('beforeend', svgData);
	}
}

//
function fileRealDir(file) {
	let compare = file;
	if ( file.indexOf("//") === 0 ) {
		compare = window.location.protocol + file;
	} else if ( file.indexOf("/") === 0 ) {
		let port = ( window.location.port && parseInt(window.location.port) !== 0 ) ? ':' + window.location.port : '';
		compare = window.location.protocol + '//' + window.location.hostname + port + file;
	}
	return compare;
}

// Initial file load, usually external files (and SVG). doesn not call Modular JS
function filesLoadAsync(files) {
	let i, cFile,
		fjs = [],
		fcss = [],
		fsvg = [];
	//
	for ( i = 0; i < files.length; i++ ) {
		cFile = files[i];
		if ( cFile.endsWith('.js') || cFile.endsWith('.js.gz') || cFile.includes('.js?') ) {
			fjs.push(cFile);
		} else if ( cFile.endsWith('.css') || cFile.endsWith('.css.gz') || cFile.includes('.css?') ) {
			fcss.push(cFile);
		} else if ( cFile.endsWith('.svg') || cFile.endsWith('.svgz') ) {
			fsvg.push(cFile);
		} else if ( cFile.includes('//fonts.googleapis.com') ) {
			fcss.push(cFile);
		} else {
			console.log("File async not supported (2): ", cFile);
		}
	}

	// Enviar
	if ( fjs.length ) {
		fileLoadJS(fjs);
	}
	if ( fcss.length ) {
		fileLoadCSS(fcss);
	}
	if ( fsvg.length ) {
		fileLoadSVG(fsvg);
	}
}
