export function stringPopulate(cString, cData) {
	let value, re;
	for ( let cName in cData ) {
		if ( !cData.hasOwnProperty(cName) ) { continue; }
		value = ( cData[cName] !== null ) ? cData[cName] : '';
		re = new RegExp('!' + cName + ';', "g");
		cString = cString.replace(re, value);
	}

	//
	return cString;
}

export function stringPopulateMany(cString, cData, modFunction = null) {
	let rString = "";

	// Loop each data row
	for ( let rowCur of cData ) {
		// Mod current row
		if ( typeof modFunction === 'function' ) {
			modFunction(rowCur);
		}

		rString+= stringPopulate(cString, rowCur);
	}

	//
	return rString;
}

//
export function htmlFromJson(cObj) {
	let cStruct = '';

	if ( typeof cObj !== 'object' ) {
		console.error("struct from json, error. type not valid", cObj);
	}

	//
	if ( cObj instanceof Array ) {
		for ( const cEl of cObj ) {
			cStruct+= htmlFromJson(cEl);
		}
	} else {
		let iniTag = cObj.tag;

		// Set attributes
		if ( cObj.hasOwnProperty('attr') ) {
			for ( const aName in cObj.attr ) {
				if ( !cObj.attr.hasOwnProperty(aName) ) { continue; }
				iniTag+= ' ' + aName + '="' + cObj.attr[aName] + '"';
			}
		}

		if ( cObj.hasOwnProperty('children') ) {
			let cBody = '';
			for ( const cEl of cObj.children) {
				cBody+= htmlFromJson(cEl);
			}
			//
			cStruct+= `<${ iniTag }>${ cBody }<${ cObj.tag }>`
		} else if ( cObj.hasOwnProperty('content') ) {
			//
			cStruct+= `<${ iniTag }>${ cObj['content'] }<${ cObj.tag }>`
		} else {
			cStruct+= `<${ iniTag } />`
		}
	}

	//
	return cStruct;
}

//
