// Basic appending to object
export function insertContentFromTemplate(mainObject, position, template, content, modFunction) {
	//
	let newHTML = stringPopulate(template, content, modFunction);

	//
	mainObject.insertAdjacentHTML(position, newHTML);
}

// Convert HTML
export function stringPopulate(template, content, modFunction) {
	//
	if ( !Array.isArray(content) ) {
		content = [content];
	}

	if ( template instanceof HTMLElement ) {
		template = template.innerHTML;
	}

	let rString = "", cMod = "",
		cElId;
	for ( let rowCur of content ) {
		//
		cMod = template;

		// Mod current row
		if ( typeof modFunction === 'function' ) {
			modFunction(rowCur);
		}
		// Convert all elements
		for ( cElId in rowCur ) {
			if ( !rowCur.hasOwnProperty(cElId) ) { continue; }
			//
			let cEl = ( rowCur[cElId] !== null ) ? rowCur[cElId] : '',
				re = new RegExp('!' + cElId + ';', "g");
			//
			cMod = cMod.replace(re, cEl);
		}
		rString += cMod;
	}

	//
	return rString;
}

//
function elementUpdate(cEl, cOp) {
	//
	switch ( cOp[0] ) {
		case 'data':
			if ( cOp[2] === null ) {
				delete cEl.dataset[cOp[1]];
			} else {
				cEl.dataset[cOp[1]] = cOp[2];
			}
			break;
		case 'attr':
		case 'attribute':
			if ( cOp[2] === null || cOp[2] === false ) {
				cEl.removeAttribute(cOp[1]);
			} else if( typeof cOp[2] === 'boolean' ) {
				cEl.setAttribute(cOp[1], cOp[1]);
			} else {
				cEl.setAttribute(cOp[1], cOp[2]);
			}
			break;
		case 'class':
			cEl.className = cOp[1];
			break;
		case 'style':
			switch ( typeof(cOp[1]) )
			{
				case 'object':
					for ( let cStyle in cOp[1] ) {
						if ( cOp[1].hasOwnProperty(cStyle) ) {
							cEl.style[cStyle] = ( typeof cOp[1][cStyle] !== 'undefined' ) ? cOp[1][cStyle] : null;
						}
					}
					break;
				case 'string':
					if ( typeof(cOp[2]) === 'undefined' ) {
						cEl.style[cOp[1]];
					} else {
						cEl.style[cOp[1]] = cOp[2];
					}
					break;
				default:
					break;
			}
			break;
		case 'text':
			cEl.textContent = cOp[1];
			break;
		case 'html':
			cEl.innerHTML = cOp[1];
			break;
		//
		case 'func':
			if ( typeof cOp[1] !== 'function' ) {
				console.error("function improperly defined");
				return;
			}

			cOp[1](cEl);
			break;

		//
		default:
			console.error("not valid operation", cOp)
			break;
	}
}

//
export function updateElement(cElement, cOperations) {
	if ( !(cElement instanceof Node ) ) {
		console.error("Invalid element.", cElement);
		return;
	}

	// Many operations
	if ( Array.isArray(cOperations[0]) ) {
		for ( let cOp of cOperations ) {
			elementUpdate(cElement, cOp);
		}
	}
	// Single operation, just process
	else {
		elementUpdate(cElement, cOperations);
	}
}

//
export function updateElementProperties(cElement, cOperations) {
	if ( cElement instanceof NodeList ) {
		cElement.foreach((cEl) => {
			updateElement(cEl, cOperations);
		});
	} else if ( cElement instanceof Node ) {
		updateElement(cElement, cOperations);
	} else {
		console.error("Invalid cElement", cElement);
	}
}

//
export function updateChildren(mainObject, updateList) {
	for ( const [cSelector, cOperations] of Object.entries(updateList) ) {
		let cElement = mainObject.querySelector(cSelector);

		//
		if ( !cElement ) {
			console.error("Child not found", cSelector, mainObject);
			continue;
		}

		//
		updateElement(cElement, cOperations);
	}
}

//
Node.prototype.se_empty = function() {
	this.innerHTML = '';
	return this;
};

//
Node.prototype.se_insertContentFromTemplate = function(position, template, content, modFunction) {
	//
	let newHTML = stringPopulate(template, content, modFunction);

	//
	this.insertAdjacentHTML(position, newHTML);

	//
	return this;
};

//
Node.prototype.se_remove = function() {
	this.parentElement.removeChild(this);
}

//
NodeList.prototype.se_remove = HTMLCollection.prototype.se_remove = function() {
	return se_listOperation(this, (cEl) => {
		if ( cEl && cEl.parentElement ) {
			cEl.parentElement.removeChild(cEl);
		}
	});
};

//
function se_listOperation(tEl, fDef) {
	if ( !tEl.length || typeof fDef !== 'function' ) { return tEl; }
	// Loop elements
	let i = 0;
	for ( let cEl of tEl ) {
		fDef(cEl, i);
		i++;
	}

	return tEl;
}

//
Node.prototype.se_updateElement = function(cOperations) {
	updateElement(this, cOperations);
};

//
Node.prototype.se_updateChild = function (selector, cOperations) {
	updateElement(this.querySelector(selector), cOperations);
};

//
Node.prototype.se_updateChildren = function(selector, cOperations) {
	if ( typeof selector === 'string' ) {
		// Selector is a selector
		let children = this.querySelectorAll(selector);

		if ( !children ) { console.error("empty query", selector, this); return this; }

		children.forEach((cEl) => {
			updateElement(cEl, cOperations);
		});
	} else if ( typeof selector === 'object' ) {
		updateChildren(this, selector)
	} else {
		console.error("Update children failed. Not recognized.");
	}

	return this;
};

// Add event listeners
export function addEventListenerDelegate(cElem, cEvent, cDelegate, cFunc, cContext) {
	cElem.addEventListener(cEvent,
		(e) => {
			let rTarget = e.target.closest(cDelegate, cElem);
			if ( rTarget !== null ) {
				cFunc.call(cContext, e, rTarget);
			}
		},
		false
	);
}

export function addEventListenersToCustomElement(cClass, eventList) {
	// Is not an array of arrays.
	if ( !Array.isArray(eventList[0]) ) {
		eventList = [eventList];
	}

	//
	for ( let cEvent of eventList ) {
		switch ( cEvent.length ) {
			case 2:
				if ( typeof cEvent[0] !== 'string' || typeof cEvent[1] !== 'function' ) {
					console.error("Invalid event attempt in: ", cClass, cEvent);
					continue;
				}
				cClass.addEventListener(cEvent[0], cEvent[1]).bind(cClass);
				break;
			case 3:
				if ( typeof cEvent[0] !== 'string' || typeof cEvent[1] !== 'string' || typeof cEvent[2] !== 'function' ) {
					console.error("Invalid event attempt in: ", cClass, cEvent);
					continue;
				}
				addEventListenerDelegate(cClass, cEvent[0], cEvent[1], cEvent[2], cClass);
				break;
			default:
				console.error("Invalid event attempt in ", cClass, cEvent);
				break;
		}
	}
}

//
Node.prototype.se_on = function(var1, var2, var3, var4 = null) {
	let direct = function (cElem, cEvent, cFunc) {
			cElem.addEventListener(cEvent, cFunc);
		},
		//
		typeVar1 = typeof var1,
		typeVar2 = typeof var2,
		typeVar3 = typeof var3,
		//
		eventList, cEvent,
		cSubEl;

	//
	if ( typeVar1 === 'object' ) {
		//
		if ( typeVar2 === 'object' ) {
			// Multiple events on multiple sub elements
			if ( typeVar3 !== 'function' ) {
				console.error('Element - se_on: Delegate . v1-object, v2-object, v3-not valid', typeVar3);
				return this;
			}

			//
			for ( cEvent of var1 ) {
				for ( cSubEl of var2 ) {
					addEventListenerDelegate(this, cEvent, cSubEl, var3);
				}
			}
		} else if ( typeVar2 === 'string' ) {
			// Multiple events and funcs related with single sub element, posible subDelegation
			for ( cEvent in var1 ) {
				if ( !var1.hasOwnProperty(cEvent) ) { continue; }
				let cFunc = var1[cEvent];
				if ( typeof cFunc !== "function" ) { console.error("invalid function added"); }
				let subDelegation = ( var3 );
				addEventListenerDelegate(this, cEvent, var2, cFunc, subDelegation);
			}
		} else if ( typeVar2 === 'undefined' ) {
			// Multiple events and funcs no sub element
			for ( cEvent in var1 ) {
				if ( !var1.hasOwnProperty(cEvent) ) { continue; }
				let cFunc = var1[cEvent];
				direct(this, cEvent, cFunc);
			}
		} else {
			console.error('Element - se_on: Delegate . v1-object, v2-object, v3-not valid', typeVar3);
		}
	}
	else if ( typeVar1 === 'string' ) {
		// Variable is a string, probably action

		eventList = var1.split(' ');
		if ( typeVar2 === 'function' ) {
			// single/multiple event, single function
			for ( cEvent of eventList ) {
				direct(this, cEvent, var2);
			}
		} else if ( typeVar2 === 'object' ) {
			// single/multiple event, multiple subElements (compatibility with first function)

			// Ignorar si la tercera variable no es una función
			if ( typeVar3 !== 'function' ) {
				console.error('Element - se_on: Delegate . v1-string, v2-object, v3-not valid', typeVar3);
				return this;
			}

			//
			for ( cEvent of eventList ) {
				for ( cSubEl of var2 ) {
					addEventListenerDelegate(this, cEvent, cSubEl, var3, var4);
				}
			}
		} else if ( typeVar2 === 'string' ) {
			// var2 is a type of element, third variable must be a function (or die)
			if ( typeVar3 !== 'function' ) {
				console.error('Element - se_on: Delegate . v1-string, v2-string, v3-not valid');
				return this;
			}

			//
			for ( cEvent of eventList ) {
				addEventListenerDelegate(this, cEvent, var2, var3, var4);
			}
		} else {
			console.log('Element - se_on: Delegate . v1-string, v2-not valid', var2);
		}
	} else {
		console.error('Element - se_on: Method . v1-not valid', var1);
	}
	return this;
};
