//
export function getValue(cEl) {
	// Type
	switch ( cEl.tagName ) {
		//
		case 'INPUT':
			switch ( cEl.type ) {
				//
				case 'number':
					return floatVal(cEl.value);
					break;
				//
				case 'radio':
					return cForm.elements.namedItem(cEl.name).value;
					break;
				//
				case 'checkbox':
					if ( cEl.value === '1' ) {
						return (cEl.checked) ? 1 : 0;
					} else {
						return cEl.value;
					}
					break;
				//
				default:
					return cEl.value;
					break;
			}
			break;
		//
		case 'SELECT':
			console.log("select", cEl);
			if ( cEl.multiple ) {
				console.log("multiple");
				let tResult = [],
					optList = cEl.options, cOpt, j,
					jLen = optList.length;
				//
				for ( j = 0; j < jLen; j++ ) {
					cOpt = optList[j];
					if ( cOpt.selected ) {
						tResult.push(cOpt.value || cOpt.text);
					}
				}
				return tResult;
			} else {
				return cEl.value;
			}
			break;
		//
		default:
			return cEl.value;
			break;
	}
}

//
export function setValue(elem, cVal) {
	if ( elem.tagName === 'INPUT' && elem.type === 'checkbox' ) {
		elem.checked = (parseInt(cVal) === 1 || cVal === true);
	} else if ( elem.tagName === 'INPUT' && elem.type === 'number' && elem.se_data('type') === 'moneyInt' ) {
		elem.value = parseInt(cVal) / 100;
	} else if ( elem.tagName === 'INPUT' && elem.type === 'number' ) {
		elem.value = parseInt(cVal) / 100;
	} else if ( elem.tagName === 'TEXTAREA' && elem.se_data('mode') === 'textareaesp' ) {
		elem.value = cVal;
		let evt = document.createEvent("HTMLEvents");
		evt.initEvent("change", false, true);
		elem.dispatchEvent(evt);
	} else if ( elem.tagName === 'SELECT' && elem.multiple && cVal && cVal instanceof Array ) {
		let i, iLen = cVal.length,
			options = elem.options, cOption,
			j, jLen = options.length;
		//
		for ( i = 0; i < iLen; i++ ) {
			for ( j = 0; j < jLen; j++ ) {
				cOption = elem.options[j];
				if ( cOption.value === cVal[i] ) { cOption.selected = true; }
			}
		}
	} else {
		elem.value = cVal;
	}
}

//
function loadData(cForm, cData) {
	let cName;
	//
	cForm.reset();

	//
	for ( cName in cData ) {
		if ( !cData.hasOwnProperty(cName) ) { continue; }
		//
		let cVal = cData[cName],
			elem = cForm.elements.namedItem(cName);

		//
		if ( !elem ) { continue; }

		//
		setValue(elem, cVal);
	}
}

//
export function serializeFormToJSON(cForm, excludeDisabled) {
	let json = {},
		patterns = {
			"validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)])*$/,
			"key":      /[a-zA-Z0-9_]+|(?=\[])/g,
			"push":     /^$/,
			"fixed":    /^\d+$/,
			"named":    /^[a-zA-Z0-9_]+$/
		},
		elements, len;

	//
	if ( excludeDisabled ) {
		elements = cForm.querySelectorAll('input:enabled, select:enabled, textarea:enabled');
	} else {
		elements = cForm.elements;
	}
	len = elements.length

	//
	for ( let i = 0; i < len; i++ ) {
		let cEl = elements[i];
		// Only process valid elements
		if ( cEl.name && patterns.validate.test(cEl.name) ) {
			let k,
				keys = cEl.name.match(patterns.key),
				merge = getValue(cEl);

			// Loop building
			let cPos = json;
			while ( (k = keys.shift()) !== undefined ) {
				if ( !cPos.hasOwnProperty(k) ) {
					cPos[k] = {};
				}

				// Check if last
				if ( keys.length === 0 ) {
					// Asign value
					cPos[k] = merge;
				} else {
					// Advance pointer
					cPos = cPos[k];
				}
			}
		}
	}

	//
	return json;
}

export function serializeFormToObjectObsolete(cForm, excludeDisabled, extraOps = null) {
	let elements,
		data = {};

	//
	excludeDisabled = ( typeof excludeDisabled === 'undefined') ? false : excludeDisabled;
	if ( excludeDisabled ) {
		elements = cForm.querySelectorAll('input:enabled, select:enabled, textarea:enabled');
	} else {
		elements = cForm.elements;
	}

	//
	if ( !elements ) {
		console.error('form.serializeToObj - Formulario vació');
		return data;
	}

	//
	for ( let i = 0; i < elements.length; i++ ) {
		let cEl = elements[i];
		if ( !cEl.name ) {
			console.warn('form.serializeToObj - Elemento del formulario sin nombre. Saltar', cEl);
			continue;
		}

		data[cEl.name] = getValue(cEl);
	}

	//
	return data;
}

export async function formSubmit(cForm, onComplete, onSuccess, onFail, altResponse = null) {
	let response = await fetch(cForm.action,
	{
			method:cForm.method || 'POST',
			body: new FormData(cForm)
		}
	);

	// Check if output exists
	if ( !(altResponse instanceof HTMLElement ) ) {
		altResponse = cForm.querySelectorAll('output');
	}

	//
	if ( typeof onComplete === 'function' ) {
		onComplete();
	}

	let msg;

	try {
		msg = await response.json();
	} catch ( e ) {
		if ( typeof onFail === 'function' ) {
			onFail();
		}

		console.error("error", e);

		//
		buildNotification(altResponse,
			{
				title:'ERROR (AJAX)',
				content:'No pudo ser procesada la respuesta. Formato no válido.',
				style:'notification alert',
				icon:'fa-remove'
			}
		);
		return;
	}

	//
	if ( msg.s.t === 1 ) {
		if ( altResponse ) {
			buildNotification(altResponse,
				{
					title:( 'd' in msg.s ) ? msg.s.d : 'Enviado',
					content:( 'ex' in msg.s ) ? msg.s.ex : ( 'date' in msg.s ) ? msg.s.date : '',
					style:'notification success',
					icon:'fa-check',
					autoClear:10000
				}
			);
		}
		if ( typeof onSuccess === 'function' ) {
			onSuccess(msg);
		}
	}
	else {
		if ( altResponse ) {
			buildNotification(altResponse,
				{
					title:( 'd' in msg.s ) ? 'ERROR: ' + msg.s.d : 'ERROR: Caso no definido',
					content:( 'ex' in msg.s ) ? msg.s.ex : ( 'date' in msg.s ) ? msg.s.date : '',
					style:'notification alert',
					icon:'fa-remove'
				}
			);
		}
		if ( typeof onFail === 'function' ) {
			onFail();
		}
	}
}

function buildNotification(cEl, cOptions) {
	let defaults = {
			icon:null,
			iconMod:null,
			title:null,
			content:'',
			contentHTML:false,
			style:null,
			autoClear:null
		},
		settings = {...defaults, ...cOptions},
		//
		html = '',
		clearInterval;

	//
	if ( settings.icon ) {
		html+= '<div class="icon">';
		if ( settings.icon.indexOf('.') === -1 )
		{
			settings.iconMod = ( typeof settings.iconMod === 'string' ) ? settings.iconMod : '';
			html+= '<svg class="' + settings.iconMod + '"><use xlink:href="#' + settings.icon + '" /></svg>';
		} else {
			console.log("NOTIFICACION, IMAGEN NO PROGRAMADA");
		}
		html+= '</div>';
	}
	// Contenido
	let htmlContent = ( settings.contentHTML ) ? ' html' : '';
	html+= '<div class="text"><div class="title">' + settings.title  + '</div><div class="content'+ htmlContent +'">' + settings.content + '</div></div>';

	// Apply
	cEl.className = settings.style;
	cEl.innerHTML = html;

	//
	if ( typeof settings.autoClear === 'number' && settings.autoClear !== 0 ) {
		clearInterval = setTimeout(() => {
			cEl.se_empty();
		}, settings.autoClear);
	} else {
		clearTimeout(clearInterval);
	}
}

//
HTMLFormElement.prototype.se_elementValue = function(elName, cVal) {
	let cEl = this.elements.namedItem(elName);
	//
	if ( !cEl ) {
		console.trace("Form El Val, undefined element:", elName);
		return;
	}

	//
	if ( typeof cVal !== 'undefined' ) {
		setValue(cEl, cVal);
	} else {
		return getValue(cEl);
	}
};

//
HTMLFormElement.prototype.se_serializeToJSON = (excludeDisabled) => {
	return serializeFormToJSON(this, excludeDisabled);
};

//
HTMLFormElement.prototype.se_setData = function(cData) {
	 loadData(this, cData);
};
