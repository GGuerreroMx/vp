﻿//
export class swHandler {
	status = false;
	sw_object = undefined;

	constructor() {
	}

	//
	startWorker(method, data = null) {
		//
		if ( !window.Worker ) {
			alert("NO Webworker support... Kill Self");
			return;
		}

		//
		if ( typeof(this.sw_object) !== "undefined" ) {
			console.error("Objeto ya inicializado");
			return;
		}

		console.warn("sw_handler initialize: ", method, data);

		// Define what kind of server

		//
		switch ( method ) {
			//
			case 'ws':
				this.sw_object = new Worker("/admin/res/script/workers/server_ws.js");
				break;
			//
			case 'local':
				this.sw_object = new Worker("/admin/res/script/workers/server_local.js");
				break;
			//
			default:
				return;
		}

		console.log("SW WS - INIT");

		// CB Assign
		this.sw_object.onmessage = this.sw_onMessage;

		// First operation (connect mainly)
		if ( data ) {
			this.sw_postMessage(data);
		}
	}

	//
	disconnect() {
		this.sw_object.terminate();
		this.sw_object = undefined;
	}

	//
	sw_onMessage(e) {
		let msg = e.data;

		//
		console.log("MAIN - SW MESSAGE RECIEVED:", msg);

		//
		switch ( msg.op ) {
			//
			case 'adv':
				pages.match.admin.timer.advanceWork(msg);
				if ( msg.notif ) {
					se_app.notifications.add('Match', {
						body:msg.notif,
						// icon:'icons/chrome_36.png',
						vibrate:[500, 300, 500, 300, 500]
					});
				}
				break;

			//
			case 'field':
				document.body.dispatchEvent(new CustomEvent('swFieldMessage', { bubbles: false, detail:{
					type:msg.data.type,
					data:msg.data.data
				}}));
				break;

			//
			default:
				console.error("Server worker unknown message", e.data);
				break;
		}
	}

	//
	sw_postMessage(data) {
		if ( !this.sw_object ) {
			console.error("no service worker object");
			return;
		}

		//
		this.sw_object.postMessage(data);
	}
}
//