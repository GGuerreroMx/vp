//
export let appState =
	{
		loaded:false,
		settings:{
			notifications:false
		},
		properties:{
			matches:null,
			score:null,
			server:null,
			connection:null,
		},
		status:{
			gameStageId:0,
			eventFieldId:0,
			serverFieldId:0,
			cMatchIndex:0
		},
		fieldData:null,
	};

//
export function appStatusUpdate() {
	// Put the object into storage
	console.log("appState: updating. ", appState);
	localStorage.setItem( 'appState', JSON.stringify(appState));
}

//
export function appStatusGet() {
	// Crear appstates
	let tempAppState = localStorage.getItem( 'appState' );
	//
	if ( tempAppState ) {
		appState = JSON.parse( tempAppState );
	}
}

// Auto get the values
appStatusGet();
