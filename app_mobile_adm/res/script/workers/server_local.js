"use_strict";
//
self.importScripts('/admin/res/script/workers/worker_idb.js', '/admin/res/script/app_idb_struct.js');

// root vars
var server_field_id = null,
	server_field_data = null,
	// Dynamic only
	server_field_dynamic_timer = null,
	server_field_game_type_struct = null,
	server_field_gTime = 0,
	server_field_rTime = 0,
	server_field_sTime = 0;

//
function init() {
	//
	console.log("SW - WS - DB INIT SUCCESS, LOADING.");

	// Listener
	self.addEventListener('message', sw_messageFromMain, false);

	// Check if game is running
	field_dynamic_timeCheck();
}

//<editor-fold desc="Main communication">

//
function sw_messageFromMain(e) {
	let msg = e.data;

	//
	console.log("SW - MSG - IN FROM MAIN", msg);

	//
	switch ( msg.op_type[0] ) {
		// Send File Update
		case 'field':
			//
			switch ( msg.op_type[1] ) {
				//
				case 'command':
					// Avoid random field errors
					if ( server_field_id !== msg.op_data.id ) {
						console.error("SW - FIELD COMMAND FAILED. FIELD NOT DEFINED.", msg.op_data.id, server_field_id, typeof msg.op_data.id, typeof server_field_id);
						return;
					}

					// Local execution of operation
					switch ( server_field_data.serverMode ) {
						//
						case 'dynamic_mobile':
							field_dynamic_command(msg.op_data);
							break;
						//
						case 'dynamic_server':
							field_dynamic_command(msg.op_data);
							break;
						//
						default:
							console.error("No command excecuted, ignoring", server_field_data);
							break;
					}
					break;

				//
				case 'set':
					console.log("SW - FIELD SET: ", msg.op_data.id);
					field_load(parseInt(msg.op_data.id));
					// Load data
					break;

				//
				case 'resync':
					break;

				//
				default:
					console.error("WORKER: Invalid server field/match operation");
					break;
			}
			break;

		//
		default:
			console.error("SW Message, op invalid.", msg);
			break;
	}
}

//
function field_load(fieldId) {
	//
	if ( fieldId === server_field_id ) {
		return;
	}

	//
	server_field_id = fieldId;
	ws_idb.query('server_fields', {
		get:server_field_id,
		onSuccess:(data) => {
			console.log("SW - FIELD LOAD - IDB READ 1 - Field Status", server_field_id, data);
			server_field_data = data;

			// Check if it needs timer and extra information
			switch ( server_field_data.serverMode ) {
				//
				case 'dynamic_mobile':
					// Get game type information
					ws_idb.query('gametypes', {
						get: server_field_data.gtId,
						onSuccess: (data2) => {
							console.log("SW - FIELD LOAD - IDB READ 2 - GameType: ", server_field_data.gtId, data2);
							server_field_game_type_struct = data2;
							field_dynamic_timeCheck();
						}
					});
					break;
			}
		}
	});
}

//</editor-fold>


//<editor-fold desc="Field Dynamic Operations">

//
function field_dynamic_command(cmd) {
	console.log("SW - FIELD Dyn - COMMAND Recieved.", cmd);

	//
	let fieldActions = {d:[]},
		hasUpdate = false;

	//
	switch ( cmd.op ) {
		//
		case 'setup':
			//
			switch ( cmd.data ) {
				// start / pause mechanism
				case 'start':
					//
					if ( server_field_data.data.status.active || server_field_data.data.status.fase !== 0 ) {
						// Game has started, check for pause

						let cTime = (new Date()).getTime();

						// PAUSA (toggle)
						if ( server_field_data.data.status.pause ) {
							// Is paused, unpause

							//
							let timeAdd = cTime - server_field_data.data.status.timePauseStart;
							server_field_data.data.status.timeStart+= timeAdd;
							server_field_data.data.status.timeCur+= timeAdd;
							server_field_sTime+= timeAdd;

							//
							server_field_data.data.status.pause = 0;
							server_field_data.data.status.timePauseStart = 0;

							//
							fieldActions.d = [
								{
									type:'buzzer',
									buzzSide:0,
									buzzType:4
								},
								{
									type:'sound',
									soundFileSide:0,
									soundFileNumber:4
								},
								{
									type:'note',
									note:'CONTINUE'
								},
							];
						}
						else {
							// Is running, pause

							//
							server_field_data.data.status.pause = 1;
							server_field_data.data.status.timePauseStart = cTime;
							//
							fieldActions.d = [
								{
									type:'buzzer',
									buzzSide:0,
									buzzType:2
								},
								{
									type:'sound',
									soundFileSide:0,
									soundFileNumber:2
								},
								{
									type:'note',
									note:'PAUSE'
								},
							];
						}
					}
					else {
						// Game not started, start

						//
						server_field_data.data.status.active = 1;
						server_field_data.data.status.fase = 1;
						server_field_data.data.status.pause = 0;
						server_field_data.data.status.section = 0;
						server_field_data.data.status.timeStart = (new Date()).getTime();
						server_field_data.data.status.timeCur = 0;

						//
						fieldActions.d = [
							{
								type: 'buzzer',
								buzzSide: 0,
								buzzType: 1
							},
							{
								type: 'sound',
								soundFileSide: 0,
								soundFileNumber: 1
							},
							{
								type: 'note',
								note: 'MATCH START'
							},
							{
								type: 'timer',
								timeInt:server_field_game_type_struct.struct.settings.timeBreakInt
							},
						];

						// First game tick
						field_dynamic_timeCheck();
					}

					// Report update
					hasUpdate = true;
					field_dynamic_report('field_status', fieldActions);
					break;

				//
				case 'stop':
					if ( !server_field_data.data.status.active ) {
						console.warn("Field attemp to stop ignored. (game not running)", server_field_data.data.status);
						return;
					}

					//
					server_field_data.data.status.active = 0;
					server_field_data.data.status.pause = 0;
					server_field_data.data.status.fase = 3;

					//
					fieldActions.d = [
						{
							type:'buzzer',
							buzzSide:0,
							buzzType:3
						},
						{
							type:'sound',
							soundFileSide:0,
							soundFileNumber:3
						},
						{
							type:'note',
							note:'MATCH END'
						},
					];

					//
					field_dynamic_triggerCheck(fieldActions);

					//
					server_field_data.data.status.fase = 4; // avoid double end game triggers if called by device action

					//
					field_dynamic_timeCheck();

					//
					hasUpdate = true;
					field_dynamic_report('field_status', fieldActions);
					break;

				//
				case 'reset':
					if ( server_field_data.data.status.active ) {
						console.warn("Field attemp to restart ignored. (game running)", server_field_data.data.status);
						return;
					}

					//
					hasUpdate = true;

					//
					field_dynamic_fieldReset();

					break;

				//
				case 'next':
					//
					if ( server_field_data.data.status.active || server_field_data.type === 'direct' ) {
						console.log("SW - Next Match ignored. Not Tourney.");
						return;
					}

					//
					console.log("SW - --- NEXT MATCH START --- \n");
					// Actualizar información del match a la base de datos, cargar datos del siguiente match, reiniciar elementos

					// Read actuall current match data
					ws_idb.query('game_matches', {
						get:server_field_data.cMatchIndex,
						onSuccess:(cMatchInfo) => {
							console.log("SW - CURRENT GAME DATA: ", server_field_data.data);

							//
							let saveData = {
								sync:0,
								mStatus:4,
								mPoints:server_field_data.data.points,
								mEvents:server_field_data.data.eventsUser,
								t1_points:server_field_data.data.points.t[cMatchInfo.t1_side.toString()].t,
								t1_status:0,
								t2_points:server_field_data.data.points.t[cMatchInfo.t2_side.toString()].t,
								t2_status:0,
							};

							// Validate max points
							saveData.t1_points = ( saveData.t1_points > server_field_game_type_struct.struct.settings.maxPointsGame ) ? server_field_game_type_struct.struct.settings.maxPointsGame : saveData.t1_points;
							saveData.t2_points = ( saveData.t2_points > server_field_game_type_struct.struct.settings.maxPointsGame ) ? server_field_game_type_struct.struct.settings.maxPointsGame : saveData.t2_points;

							// Pick winner
							if ( saveData.t1_points > saveData.t2_points ) {
								saveData.t1_status = 3;
								saveData.t2_status = 1;
							} else if ( saveData.t1_points < saveData.t2_points ) {
								saveData.t1_status = 1;
								saveData.t2_status = 3;
							} else {
								saveData.t1_status = 2;
								saveData.t2_status = 2;
							}

							// Save current information
							ws_idb.update('game_matches', server_field_data.cMatchIndex,
								saveData,
								{
									onSuccess:(data) => {
										// Determinar siguiente match disponible
										ws_idb.query('game_matches', {
											index:'gameFilter',
											keyRange:IDBKeyRange.only([server_field_data.gsId, server_field_data.fId]),
											sort:{
												name:'mOrder',
												direction:'ASC'
											},
											onSuccess:(matches) => {
												let matchAvailable = false,
													selMatch;

												//
												for ( let cMatch of matches ) {
													if ( cMatch.mStatus === 1 ) {
														selMatch = cMatch;
														matchAvailable = true;
														break;
													}
												}

												let nextMatch = {
													cMatchIndex:-1
												};

												//
												if ( matchAvailable ) {
													//
													console.log("SW - NEXT MATCH...", selMatch);

													// Server field information
													server_field_data.cMatchIndex = selMatch.index;
													server_field_data.cMatchOrder = selMatch.mOrder;
													server_field_data.t1_name = selMatch.t1_name;
													server_field_data.t2_name = selMatch.t2_name;

													// Next match information
													nextMatch = {
														index:selMatch.index,
														mOrder:selMatch.mOrder,
														t1_name:server_field_data.t1_name,
														t2_name:server_field_data.t2_name
													};
												}

												// Send information
												field_dynamic_send_postMessage({
													type:'matchChange',
													data:{
														last:saveData,
														next:nextMatch
													}
												});

												//
												field_dynamic_fieldReset();
											},
											onEmpty:() => {
												console.error("No hay partidas en estas condiciones.");
											}
										});
									}
								}
							);
						}
					});
					break;

				//
				default:
					console.error("SETUP: FAIL OP NOT RECOGNIZED", cmd.data);
					break;
			}
			break;

		//
		case 'gameStatus':
			//
			if ( !( server_field_data.data.status.active && server_field_data.data.status.fase === 2) ) {
				console.log("Match status update ignored.(inactive)");
				return;
			}
			if ( server_field_data.data.status.pause ) {
				console.log("Device operation ignored. (paused)");
				return;
			}

			//
			field_dynamic_gameStatusUpdate(cmd.data.index, cmd.data.value);
			break;

		//
		case 'device':
			//
			if ( !server_field_data.data.status.active || server_field_data.data.status.fase !== 2 ) {
				console.log("Device operation ignored. (inactive)");
				return;
			}
			if ( server_field_data.data.status.pause ) {
				console.log("Device operation ignored. (paused)");
				return;
			}

			//
			if ( field_dynamic_deviceUpdate(cmd.data.deviceId, cmd.data.side, cmd.data.status, fieldActions) ) {
				hasUpdate = true;

				// Trigger check
				field_dynamic_triggerCheck(fieldActions);

				// Notificar cambios
				field_dynamic_report('field_status', fieldActions);
			}
			break;

		//
		case 'event_update':
			hasUpdate = true;
			server_field_data.data.eventsUser[cmd.data.index].devUser = cmd.data.devUser;
			field_dynamic_report('field_event_update', cmd.data);
			break;

		//
		default:
			console.error("Operación no reconocida", cmd);
			break;
	}

	//
	if ( hasUpdate ) {
		field_dynamic_saveField();
	}
}

//
function field_dynamic_game_set_section(secNumber, cTime, fieldActions) {
	let cSection = server_field_game_type_struct.struct.properties.sections[secNumber];

	// get current section actions
	fieldActions.d = fieldActions.d.concat(cSection.actions);

	// Start section timer
	server_field_sTime = cTime + ( ( cSection.data.durationInt ) * 1000 );
	server_field_data.data.status.sectionNumber = secNumber;
	server_field_data.data.status.sectionTimeStart = cTime;

	//
	let timeText = SecondsTohhmmss(server_field_rTime);

	//
	console.log("SW - F:%s. - SET SECTION. Time:%s. Name:%s", server_field_id, timeText, cSection.data.title);

	// Add to records
	let eventData = {
		type:'automatic',
		time:server_field_rTime,
		subtype:'sections',
		id:secNumber
	};

	// Notify event action
	server_field_data.data.eventsUser.push(eventData);
	field_dynamic_report('field_event', eventData);

	// Check extra trigger notes
	field_dynamic_triggerCheck(fieldActions);
}

//
function field_dynamic_advance() {
	let hasUpdate = false,
		//
		cTime = (new Date()).getTime(),
		fieldActions = {d:[]},
		timeText;

	//
	if ( !server_field_data.data.status.active || server_field_data.data.status.pause ) {
		return;
	}

	//
	server_field_gTime = Math.round((cTime - server_field_data.data.status.timeStart) / 1000);
	server_field_rTime = server_field_gTime - server_field_game_type_struct.struct.settings.timeBreakInt;

	// console.log("SW - F:%s. - TIME ADV. G:%s. E:%s.", server_field_id, server_field_gTime, server_field_rTime);

	// Start actual game (starts in section 0)
	if ( server_field_data.data.status.fase === 1 && server_field_rTime >= 0 ) {
		hasUpdate = true;
		//
		server_field_data.data.status.fase = 2;
		console.log("SW - F:%s. - SCORE SYSTEM START", server_field_id);

		// Allways start on index
		field_dynamic_game_set_section(0, cTime, fieldActions);
	}

	// Eventos timed list (son removidos según el avance del juego)
	if ( server_field_data.data.eventsTimed.list.length && server_field_data.data.eventsTimed.list[0].rtime <= server_field_gTime ) {
		let cEvent = server_field_data.data.eventsTimed.list.shift(),
			tempProperty = server_field_game_type_struct.struct.properties[cEvent.type][cEvent.index],
			// Event information
			eventData;

		console.log("SW - F:%s. - TIMED EVENT - Type: %s. Name: %s.", server_field_id, cEvent.type, tempProperty.data.title);
		console.log("SW - CONT", cEvent, tempProperty);

		//
		timeText = SecondsTohhmmss(server_field_rTime);

		//
		switch ( cEvent.type ) {
			//
			case 'sections':
				console.error("ya no debería haber este tipo de sección.", server_field_data.data);
				break;
			//
			case 'eTimed':
				hasUpdate = true;

				console.log("SW - Field %s:Match Event. Time:%s. Name:%s.", server_field_id, timeText, tempProperty.data.title);

				// Add event
				eventData = {
					type:'automatic',
					time:server_field_rTime,
					subtype:cEvent.type,
					id:cEvent.index
				};

				// Add event to event list
				server_field_data.data.eventsUser.push(eventData);

				// Quick report (to system)
				field_dynamic_report('field_event', eventData);

				// Process operations
				field_dynamic_actionOperations(tempProperty.actions, null, fieldActions);

				// Check triggers
				field_dynamic_triggerCheck(fieldActions);
				break;
			//
			default:
				console.error("Incorrect event timed.");
				break;
		}
		//
	}

	// Revisión dentro del juego
	if ( server_field_data.data.status.fase === 2 ) {
		// Secciones
		if ( cTime > server_field_sTime ) {
			console.log("SW - F:%s. - SECTION NEW - Time: %s.", server_field_id, server_field_sTime);

			//
			let cSection = server_field_game_type_struct.struct.properties.sections[server_field_data.data.status.sectionNumber];
			if ( cSection.data.endSection === 1 ) {
				// End game with ending section
				field_dynamic_command({
					op:'setup',
					data:'stop'
				});
			}
			else {
				let valid = false;
				// Look for next section
				for ( let i = 0; i < server_field_game_type_struct.struct.properties.sections.length; i++ ) {
					//
					let cSection = server_field_game_type_struct.struct.properties.sections[i];
					if (
						server_field_data.data.status.sectionNumber !== i &&
						server_field_data.data.status.sectionNumber === cSection.data.prevSection &&
						typeof cSection.conditions === 'object' &&
						field_dynamic_conditionCheck(cSection.conditions)
					)
					{
						field_dynamic_game_set_section(i, cTime, fieldActions);
						hasUpdate = true;
						valid = true;
						break;
					}
				}

				//
				if ( !valid ) {
					console.error("juego mal hecho?, no hay posible siguiente sección.");
				}
			}
		}

		// Puntos (se tiene que volver a corroborar que el juego siga activo)
		if ( server_field_data.data.status.fase === 2 ) {
			for ( let period in server_field_data.data.eventsTimed.cyclic ) {
				if ( !server_field_data.data.eventsTimed.cyclic.hasOwnProperty(period) ) {
					continue;
				}
				//
				if ( server_field_rTime % period === 0 ) {
					//
					let cPData = server_field_data.data.eventsTimed.cyclic[period];
					timeText = SecondsTohhmmss(server_field_rTime);

					//
					console.log("SW - F:%s. - POINT CYCLE - Count:%s. Time:%s.", server_field_id, cPData.cycleCount, timeText);

					//
					for ( let cycleDeviceIndex in cPData.devices ) {
						let cycleDevice = cPData.devices[cycleDeviceIndex],
							cDeviceProps = server_field_game_type_struct.struct.properties.devices[cycleDevice.deviceId],
							cDevice = server_field_data.data.devices[cycleDevice.deviceId],
							cPointId = cycleDevice.pointId.toString(),
							teoPoint = server_field_game_type_struct.struct.points.find((cObj) => { return cObj.id === cycleDevice.pointId; });

						// Ignore device
						if ( !cDevice.active ) {
							continue;
						}

						// Point not found?
						if ( !teoPoint ) {
							console.error("PUNTO TEÓRICO NO ENCONTRADO.", teoPoint, cPointId, server_field_game_type_struct.struct.points);
							continue;
						}

						//
						switch ( cDeviceProps.data.devType ) {
							//
							case 'swflag':
								// has not side
								if ( cDevice.side === 0 ) {
									continue;
								}

								// Validate point structure
								if ( cycleDevice.cycleObj.hasOwnProperty(cPData.cycleCount) ) {
									console.warn("Cycle point checking", cycleDevice.cycleObj, cPData.cycleCount, cDevice.side);
									continue;
								}

								// Mark point interval
								cycleDevice.cycleObj[cPData.cycleCount][cDevice.side] = 1;

								// Score Update
								server_field_data.data.points.t[cDevice.side].p[cPointId].c++;
								server_field_data.data.points.t[cDevice.side].p[cPointId].t += teoPoint.points;
								server_field_data.data.points.t[cDevice.side].t += teoPoint.points;
								//
								hasUpdate = true;
								break;
							//
							case 'swopflag':
								// UNTESTED
								if ( cDevice.side !== cDevice.defSide ) {
									continue;
								}

								// Mark point interval
								cycleDevice.cycleObj[cPData.cycleCount][cDevice.side] = 1;

								// Score Update
								server_field_data.data.points.t[cDevice.side].p[cPointId].c++;
								server_field_data.data.points.t[cDevice.side].p[cPointId].t += teoPoint.points;
								server_field_data.data.points.t[cDevice.side].t += teoPoint.points;
								//
								hasUpdate = true;
								break;
							//
							default:
								break;
						}
					}

					// Sumar a la cuenta
					cPData.cycleCount++;
				}
			}
		}
	}

	// reportes

	//
	if ( hasUpdate ) {
		// Save information, process
		field_dynamic_saveField();

		// Send information to server
		field_dynamic_report('field_status', fieldActions);
	}
}

//
function field_dynamic_fieldReset() {
	// Actual local reset by copying data
	server_field_data.data = se.object.copy(server_field_game_type_struct.struct.field);

	// Send reset information to devices
	field_dynamic_report("field_status",
		{
			d:
			[
				{
					type:'buzzer',
					buzzSide:0,
					buzzType:0
				},
				{
					type:'note',
					note:'FIELD READY'
				},
			]
		}
	);

	//
	field_dynamic_saveField();
}

//
function field_dynamic_gameStatusUpdate(gsIndex, gsValue) {
	let cStatusProperties = server_field_game_type_struct.struct.properties.gameStatus[gsIndex],
		cStatusField = server_field_data.data.gameStatus[gsIndex],
		fieldActions = {d:[]};

	//
	cStatusField.value = gsValue;

	// Ejecutar cambios
	if ( typeof cStatusProperties.actions === 'object' ) {
		field_dynamic_actionOperations(cStatusProperties.actions, null, fieldActions);
	}

	// Check triggers
	field_dynamic_triggerCheck(fieldActions);

	// Save changes
	field_dynamic_saveField();

	// Send information to server
	field_dynamic_report('field_status', fieldActions);
}

//
function field_dynamic_deviceUpdate(devIndex, nSide, nStatus, fieldActions) {
	let cDevProperties = server_field_game_type_struct.struct.properties.devices[devIndex],
		cDevField = server_field_data.data.devices[devIndex];

	//
	console.log("SW - DEVICE UPDATE: FASE:%s. ID:%s.", server_field_data.data.status.fase, devIndex, cDevField, cDevProperties);

	//
	switch ( server_field_data.data.status.fase ) {
		// Waiting
		case 0:
			//
			switch ( cDevProperties.data.devType ) {
				//
				case 'base':
					switch ( data.status ) {
						//
						case 1:
							cDevField.status = 1;
							let gameReady = true;
							for ( let cDev of server_field_data.data.devices ) {
								if ( cDev.type === 'base' && !cDev.status ) {
									gameReady = false;
									return false;
								}
							}
							//
							if ( gameReady ) {
								field_dynamic_command({
									op:'setup',
									data:'start'
								});
							} else {
								console.log("Not all teams are ready. Waiting.");

							}
							// field_dynamic_report('device', {id:devIndex});
							break;
						//
						case 0:
							cDevField.status = 0;
							// field_dynamic_report('device', {id:devIndex});
							break;
						//
						default:
							break;
					}
					break;
				//
				case 'swflag':
				case 'opflag':
				case 'opswflag':
				case 'ctflag':
				case 'bomb_loc':
				case 'bomb_bomb':
					console.warn("Device ignored during preparation.");
					break;
				//
				default:
					console.error("Device not recognized.");
					break;
			}
			break;

		// Pre
		case 1:
			//
			switch ( cDevProperties.data.devType ) {
				//
				case 'base':
					console.log("should cancel?.");
					break;
				//
				case 'swflag':
				case 'opflag':
				case 'opswflag':
				case 'ctflag':
				case 'bomb_loc':
				case 'bomb_bomb':
					console.warn("Device ignored during preparation.");
					break;
				//
				default:
					console.error("Device not recognized.");
					break;
			}
			break;

		// Running
		case 2:
			//
			if ( !cDevField.active ) {
				console.log("Request denied. Device inactive.");
				return false;
			}

			//
			switch ( cDevProperties.data.devType ) {
				//
				case 'base':
					console.error("not programed, no operations available");
					return false;
					break;
				//
				case 'swflag':
					// Validation
					if ( cDevField.side === nSide ) {
						console.warn("Swing flag: Request denied. Same side.");
						return false;
					}

					//
					cDevField.side = nSide;
					break;
				//
				case 'opflag':
					// Validation
					if ( cDevField.side === nSide || cDevProperties.data.devSideDef === nSide ) {
						console.log("Opposing flag: Request denied. Same side or trying to return.");
						return false;
					}

					//
					cDevField.side = nSide;
					break;
				//
				case 'opswflag':
					// Validation
					if ( cDevField.side === nSide ) {
						console.warn("Opposing swing flag: Request denied. Same side.");
						return false;
					}

					//
					cDevField.side = nSide;
					break;
				//
				case 'endflag':
					// Validation
					if ( cDevField.side === nSide ) {
						console.warn("End flag: Request denied. Same side.");
						return false;
					}

					//
					cDevField.side = nSide;
					break;
				//
				case 'ctflag':
					// Validation
					if ( cDevField.side === nSide ) {
						console.warn("Capture the flag: Request denied. Same side.");
						return false;
					}

					//
					cDevField.side = nSide;
					break;
				//
				case 'ctspot':
					// Validation
					if ( cDevField.side === nSide || data.status !== 2 ) {
						console.warn("Capture the spot: Request denied. Same side or current status is invalid (not 2)");
						return false;
					}

					//
					cDevField.side = nSide;
					break;
				//
				case 'bomb_loc':
				case 'bomb_bomb':
					console.warn("Bomb ops not developed.");
					return false;
					break;
				//
				default:
					console.error("UKNOWN DEVICE FOR OPERATION");
					return false;
					break;
			}

			// User event add
			let cEvent = {
				type:'device',
				time:server_field_rTime,
				devId:devIndex,
				devSide:nSide,
				devUser:0
			};
			// Add actual event to system
			server_field_data.data.eventsUser.push(cEvent);

			// Report new event
			field_dynamic_report('field_event', cEvent);

			// Get operations
			field_dynamic_actionOperations(cDevProperties.actions, devIndex, fieldActions);

			// Trigger checks and savings are outside this function after true return
			return true;
			break;

		// Waiting reset
		case 3:
			console.log("ingoring requests, game ended");
			break;

		//
		default:
			console.error("Game in unrecognized state.");
			break;
	}

	// Default is not valid.
	return false;
}

//
function field_dynamic_actionOperations(actions, devId = null, fieldActions) {
	//
	console.log("SW - FIELD DYNAMIC - ACTION OPERATION.", actions, devId);

	//
	for ( let cAction of actions ) {
		//
		switch ( cAction.type ) {
			// Enviar a notificaciones directamente
			case 'buzzer':
			case 'sound':
			case 'timer':
			case 'note':
				fieldActions.d.push(cAction);
				break;
			//
			case 'device':
				break;
			//
			case 'devMod':
				let cDevField = server_field_data.data.devices[cAction.devNumber];
				cDevField.side = cAction.devSide;
				cDevField.status = cAction.devStatus;
				cDevField.active = cAction.devActive;
				break;
			//
			case 'statusMod':
				let cFieldStatus = server_field_data.data.gameStatus[cAction.statusNumber];
				cFieldStatus.value = cAction.statusValue;
				break;
			//
			case 'gameEnd':
				// End game with ending section
				field_dynamic_command({
					op:'setup',
					data:'stop'
				});
				break;

			//
			case 'point':
				//
				let cPoint = server_field_game_type_struct.struct.points.find((obj) => { return obj.id === cAction.pointId; }),
					deviceStatus = server_field_data.data.devices[devId];

				//
				if ( !cPoint ) {
					console.error("Punto no definido..", server_field_game_type_struct.struct.points, cAction.pointId);
					continue;
				}

				if ( !deviceStatus ) {
					console.error("Dispositivo no encontrado.", actions, cAction);
					continue;
				}

				// Type 1 added at the end
				if ( cPoint.target !== 1 ) { continue; }

				//
				switch ( cAction.pointType ) {
					//
					case 'simple':
						//
						server_field_data.data.points.t[deviceStatus.side].p[cAction.pointId.toString()].c++;
						server_field_data.data.points.t[deviceStatus.side].p[cAction.pointId.toString()].t += cPoint.points;
						server_field_data.data.points.t[deviceStatus.side].t += cPoint.points;
						break;
					//
					case 'cyclic':
						let missing = true;

						//
						outer_loop: {
							for ( let cIndex in server_field_data.data.eventsTimed.cyclic ) {
								if ( !server_field_data.data.eventsTimed.cyclic.hasOwnProperty(cIndex) ) {
									continue;
								}
								let content = server_field_data.data.eventsTimed.cyclic[cIndex];
								for ( let cDev of content.devices ) {
									if ( cDev.deviceId === devId && cDev.cycleObj[content.cycleCount][deviceStatus.side] ) {
										missing = false;
										break outer_loop;
									}
								}
							}
						}

						//
						if ( !missing ) {
							//
							server_field_data.data.points.t[deviceStatus.side].p[cAction.pointId.toString()].c++;
							server_field_data.data.points.t[deviceStatus.side].p[cAction.pointId.toString()].t += cPoint.points;
							server_field_data.data.points.t[deviceStatus.side].t += cPoint.points;
						}

						break;
					//
					case 'cyclic_exclusive':
						break;
					//
					default:
						break;
				}
				break;

			//
			case 'pointSet':
				let cPointSet = server_field_game_type_struct.struct.points.find((obj) => { return obj.id === cAction.pointSetId; });
				if ( !cPointSet ) {
					console.error("Action operation, punto no válido.", actions);
					continue;
				}

				// For now, only points for teams
				if ( cPointSet.target !== 1 ) { continue; }

				//
				switch ( cAction.pointSetType ) {
					//
					case 'simple':
						//
						server_field_data.data.points.t[cAction.pointSetSide].p[cAction.pointSetId.toString()].c++;
						server_field_data.data.points.t[cAction.pointSetSide].p[cAction.pointSetId.toString()].t += cPointSet.points;
						server_field_data.data.points.t[cAction.pointSetSide].t += cPointSet.points;
						break;
					//
					case 'cyclic':
						console.log("for now not biatch");
						break;
					//
					case 'cyclic_exclusive':
						console.log("for now not biatch");
						break;
					//
					default:
						console.error("pointset invalid pointtype");
						break;
				}
				break;
			//
			default:
				console.error("SW - Field - Action operation not defined.", cAction.type, cAction);
				break;
		}
	}
}

//
function field_dynamic_triggerCheck(fieldActions) {
	//
	for ( let cTrigger of server_field_game_type_struct.struct.properties.eTriggered ) {
		//
		if ( field_dynamic_conditionCheck(cTrigger.conditions) ) {
			field_dynamic_actionOperations(cTrigger.actions, null, fieldActions);
			break;
		}
	}
}

//
function field_dynamic_conditionCheck(conditionList) {
	//
	console.log("SW - F:%s. - CONDITION CHECK", server_field_id);

	//
	for ( let cCondition of conditionList ) {
		//
		switch ( cCondition.type ) {
			//
			case 'section':
				if ( server_field_data.data.status.sectionNumber !== cCondition.sectionNumber ) {
					return false;
				}
				break;

			//
			case 'gameStatus':
				let cStatusValue = server_field_data.data.gameStatus[cCondition.gameStatusNumber].value;
				switch ( cCondition.gameStatusComp ) {
					case '==':
						if ( !( cStatusValue === cCondition.gameStatusValue ) )
						{
							return false;
						}
						break;
					case '!=':
						if ( !( cStatusValue !== cCondition.gameStatusValue ) )
						{
							return false;
						}
						break;
					case '<=':
						if ( !( cStatusValue <= cCondition.gameStatusValue ) )
						{
							return false;
						}
						break;
					case '<':
						if ( !( cStatusValue < cCondition.gameStatusValue ) )
						{
							return false;
						}
						break;
					case '>=':
						if ( !( cStatusValue >= cCondition.gameStatusValue ) )
						{
							return false;
						}
						break;
					case '>':
						if ( !( cStatusValue > cCondition.gameStatusValue ) )
						{
							return false;
						}
						break;
					default:
						console.error("incorrect comparison value");
						break;
				}
				break;

			//
			case 'gameEnd':
				if ( server_field_data.data.status.fase !== 3 ) {
					return false;
				}
				break;

			//
			case 'devStatus':
				let cDev = server_field_data.data.devices[cCondition.devNumber];
				// console.log("SW - F:%s. - SAME SIDE CHECK", server_field_id);
				// console.log("SW - CONT", cDev, cCondition);
				if (
					cDev.side !== cCondition.devSide ||
					cDev.status !== cCondition.devStatus ||
					cDev.active !== cCondition.devActive
				) {
					return false;
				}
				break;

			//
			case 'sameSideCheck':

				for ( let cDevId of cCondition.deviceNumberSide ) {
					if ( server_field_data.data.devices[cDevId].side !== cCondition.sameSideValue ) {
						return false;
					}
				}
				break;

			//
			default:
				console.error("SERVER trigger check type invalid.");
				return false;
				break;
		}
	}
	//
	return true;
}

//
function field_dynamic_saveField() {
	//
	ws_idb.put('server_fields', server_field_data, {
		onSuccess:() => {
			console.log("SW - F:%s. - FIELD SAVED", server_field_id, server_field_data);
			//
			switch ( server_field_data.type ) {
				//
				case 'tournament':
					//
					ws_idb.query('game_matches', {
						get:server_field_data.cMatchIndex,
						onSuccess:(matchDataOrig) => {
							console.log(matchDataOrig);
							let mData = {
								mStatus:1,
								mPoints:server_field_data.data.points,
								mEvents:server_field_data.data.eventsUser,
								t1_points:server_field_data.data.points.t[matchDataOrig.t1_side].t,
								t1_status:0,
								t2_points:server_field_data.data.points.t[matchDataOrig.t2_side].t,
								t2_status:0,
							};

							//
							switch ( server_field_data.data.status.fase ) {
								//
								case 0:
									mData.mStatus = 1;
									break;
								//
								case 1:
								case 2:
									mData.mStatus = 2;
									break;
								// Ending time
								case 3:
								case 4:
									mData.mStatus = 3;
									break;
								//
								default:
									console.error("");
									break;
							}

							// Actualizar match
							ws_idb.update('game_matches', server_field_data.cMatchIndex, mData, {
								onSuccess:(data) => {
									console.log("SW - MATCH DATA SAVED");
								},
								onError:() => {
									console.log("match not updated...");
								}
							});
						}
					});
					break;
				//
				default:
					console.warn('saveField operations not done. Got: ', server_field_data.type);
					break;
			}
			// END IF GAME
		}
	});
}

//
function field_dynamic_report(type, data = {}) {
	let bodyString;
	//
	switch ( type ) {
		//
		case 'field_status':
			//
			field_dynamic_send_postMessage({
				type:'status',
				data:{
					field:{
						active: server_field_data.data.status.active,
						fase: server_field_data.data.status.fase,
						pause: server_field_data.data.status.pause,
						sectionC: server_field_data.data.status.section
					},
					points:{
						'1':server_field_data.data.points.t['1'].t,
						'2':server_field_data.data.points.t['2'].t
					},
					devices:server_field_data.data.devices.map((cObj) => { delete cObj.type; return cObj; }),
					gameStatus:server_field_data.data.gameStatus.map((cObj) => { return cObj.value; }),
					actions:data.d
				}
			});
			break;
		//
		case 'field_event':
			field_dynamic_send_postMessage({
				type:'event',
				data:data
			});
			break;
		//
		case 'field_event_update':
			//
			field_dynamic_send_postMessage({
				type:'event_update',
				data:data
			});
			break;
		//
		default:
			console.error("reporting type not defined", type, data);
			break;
	}
}

//
function field_dynamic_send_postMessage(data) {
	console.log("SW - FIELD DYNAMIC - SEND TO MAIN", data);
	postMessage({
		op:'field',
		id:server_field_id,
		data:data
	});
}

//
function field_dynamic_timeCheck() {
	//
	if ( !server_field_data ) {
		console.warn("SW - FIELD DYNAMIC - NO FIELD INFORMATION, not starting timer.", server_field_id, server_field_data);
		return;
	}

	// Evaluate conditions
	if ( server_field_data.data.status.active ) {
		console.log("SW - FIELD DYNAMIC - TIMER CHECK");
		if ( server_field_dynamic_timer === null ) {
			console.log("SW - FIELD DYNAMIC - TIMER START");
			server_field_dynamic_timer = setInterval(field_dynamic_advance, 1000);
		}
	}
	else {
		console.log("SW - FIELD DYNAMIC - TIMER STOP");
		clearInterval(server_field_dynamic_timer);
		server_field_dynamic_timer = null;
	}
}

//
function SecondsTohhmmss(totalSeconds) {
	let minutes = Math.floor(totalSeconds / 60),
		seconds = totalSeconds - (minutes * 60);

	// round seconds
	seconds = Math.round(seconds * 100) / 100;

	let result = ( minutes < 10 ? "0" + minutes : minutes );
	result += ":" + ( seconds  < 10 ? "0" + seconds : seconds );

	return result;
}

//</editor-fold>

// Proper start
ws_idb = new se_indexedDataBase(app_idb_struct, init);