"use_strict";

var ws_socket,
	server_field_id = null;

//
function init() {
	//
	self.addEventListener('message', ww_messageFromMain, false);
}

//
function ws_open() {
	console.log("WS - Open");
}

//
function ws_close(e) {
	console.log("WS - Closed");
}

//
function ws_message(ev) {
	// Message should be ArrayBuffer, convert to be readable
	let message = [... new Uint8Array(ev.data)],
		cChar;

	console.log('WSS - MSG - IN:', message);

	//
	cChar = String.fromCharCode(message.shift());
	switch ( cChar ) {
		case 'f':
			let fieldId = message.shift();
			cChar = String.fromCharCode(message.shift());
			switch ( cChar ) {
				// Full Status
				case 's':
					// Convert to actual status

					setFieldAction({
						type:'status',
						data:message
					});
					break;
				// Actions
				case 'a':
					break;
				default:
					console.error("WS - Message Invalid. (Field Op)", cChar, message);
					return;
			}
			break;
		case 'm':
			break;
		default:
			console.error("WS - Message Invalid. (Type)", cChar, message);
			return;
	}



	/*
	// Operaciones
	let path = message.destinationName.split('/'),
		actList;

	//
	switch ( path[0] ) {
		//
		case 'server':
			break;
		//
		case 'notifications':
			break;
		//
		case 'fields':
			let fieldId = path[1];
			//
			if ( !fieldId ) {
				console.log("No hay campo.", fieldId);
				return;
			}
			//
			if ( server_field_id !== parseInt(fieldId) ) {
				console.error("Not current field.", msg.op_data.id, server_field_id, typeof msg.op_data.id, typeof server_field_id);
				return;
			}
			//
			switch ( path[2] ) {
				//
				case 'try':
					break;
				//
				case 'actions':
					let data = [];

					actList = message.payloadString.split(';');

					//
					for ( let cAct of actList ) {
						let cActList = cAct.split(':');
						//
						if ( cActList[0] === 'b' ) {
							data.push({
								type:'buzzer',
								buzzSide:parseInt(cActList[1]),
								buzzType:parseInt(cActList[2])
							});
						} else if ( cActList[0] === 's' ) {
							data.push({
								type:'sound',
								soundSide:parseInt(cActList[1]),
								soundFileName:parseInt(cActList[2])
							});
						} else if ( cActList[0] === 't' ) {
							data.push({
								type:'timer',
								timeInt:parseInt(cActList[1])
							});
						} else if ( cActList[0] === 'n' ) {
							data.push({
								type:'note',
								note:cActList[1]
							});
						} else {
							console.error("Action add attemp failed", cAct, cActList);
						}
					}

					setFieldAction({
						type:'actions',
						data:data
					});
					break;
				//
				case 'status':
					actList = message.payloadString.split(';');
					setFieldAction({
						type:'status',
						data:{
							active:actList[0],
							fase:actList[1],
							sectionC:actList[2]
						}
					});
					break;
				//
				case 'notif':
					//
					switch ( path[3] ) {
						//
						case 'points':
							actList = JSON.parse(message.payloadString);
							setFieldAction({
								type:'points',
								data:actList
							});
							break;
						//
						case 'event':
							actList = JSON.parse(message.payloadString);
							setFieldAction({
								type:'event',
								data:actList
							});
							break;
						//
						default:
							console.error("MQTT, CAMPO, Operación no reconocida de notificación", path[3]);
							break;
					}
					break;
				//
				case 'gameStatus':
					//
					switch ( path[4] ) {
						//
						case 'try':

							break;
						//
						case 'set':
							//
							setFieldAction({
								type:'gameStatus',
								data:{
									index:parseInt(path[3]),
									value:parseInt(message.payloadString)
								}
							});
							break;
						//
						default:
							console.error("MQTT, CAMPO, gameStatus. Operación no reconocida", path[4]);
							break;
					}
					break;
				//
				case 'device':
					//
					switch ( path[4] ) {
						//
						case 'try':
							break;
						//
						case 'set':
							actList = message.payloadString.split(';');
							setFieldAction({
								type:'device',
								data:{
									id:path[3],
									active:actList[0],
									side:actList[1],
									status:actList[2],
								}
							});
							break;
						//
						default:
							console.error("MQTT, CAMPO, Dispositivo. Operación no reconocida", path[4]);
							break;
					}
					break;
				//
				default:
					console.error("MQTT, CAMPO, Operación no reconocida", path[2]);
					break;
			}
			break;
		//
		default:
			break;
	}
	*/
}

//
function ws_send(data) {
	console.log("WS - Msg - OUT:", data);
	ws_socket.send(data);
}

//
function ws_error(error) {
	console.log("WS error", error);
}

//
function serverConnect(cData) {
	ws_socket = new WebSocket(cData.url);
	ws_socket.binaryType = "arraybuffer";
	ws_socket.onopen = ws_open;
	ws_socket.onmessage = ws_message;
	ws_socket.onclose = ws_close;
	ws_socket.onerror = ws_error;
}

// Convert instructions to ws message and send
function ww_messageFromMain(e) {
	let msg = e.data;

	//
	console.log("SW_WS - MESSAGE FROM MAIN: ", msg);
	//
	switch ( msg.op_type[0] ) {
		//
		case 'field':
			let newMsg = [];

			//
			newMsg.push('f');
			newMsg.push(msg.op_data.fId);

			//
			switch ( msg.op_type[1] ) {
				//
				case 'command':
					//
					if ( server_field_id !== msg.op_data.fId ) {
						// console.error("Not current field.", msg.op_data.fId, server_field_id, msg.op_data.fId, typeof server_field_id);
						// return;
					}

					//
					newMsg.push('c');

					//
					switch ( msg.op_data.op ) {
						//
						case 'setup':
							newMsg.push('s');
							//
							switch ( msg.op_data.data ) {
								case 'start':
									newMsg.push('i');
									break;
								case 'stop':
									newMsg.push('s');
									break;
								case 'reset':
									newMsg.push('r');
									break;
								case 'next':
									newMsg.push('n');
									break;
								default:
									console.error("SETUP: FAIL OP NOT RECOGNIZED", msg.op_data.data);
									break;
							}
							break;

						//
						case 'gameStatus':
							newMsg.push('g');
							// Index of object
							newMsg.push(msg.op_data.data.index);
							// New tentative value
							newMsg.push(msg.op_data.data.value);
							break;

						//
						case 'device':
							newMsg.push('d');
							// Device Id
							newMsg.push(msg.op_data.data.deviceId);
							// New Side
							newMsg.push(msg.op_data.data.side);
							// Status
							newMsg.push(msg.op_data.data.status);
							break;

						//
						case 'event_update':
							newMsg.push('e');
							break;
						//
						default:
							console.error("WS - ERROR. Field command not valid.", msg);
							break;
					}
					break;

				//
				default:
					console.error("WORKER: Invalid server field/match operation");
					break;
			}

			// Create uint8
			let uint = new Uint8Array(newMsg.length);

			// Convert letters
			for ( let i = 0; i < newMsg.length; i++ ) {
				if ( typeof newMsg[i] === 'number' ) {
					uint[i] = newMsg[i];
				} else {
					uint[i] = newMsg[i].charCodeAt(0);
				}
			}

			// Send
			ws_socket.send(uint);
			break;

		//
		case 'appChange':
			//
			switch ( msg.op_type[1] ) {
				//
				case 'connect':
					serverConnect(msg.op_data);
					break;
				//
				case 'disconnect':
					// mqtt.ops.stop();
					break;
				//
				default:
					console.error("WORKER: Invalid server field operation");
					break;
			}
			break;

		//
		default:
			console.error("SW Message, op invalid.");
			break;
	}
}

//
function setFieldAction(actData) {
	postMessage({
		op:'field',
		id:server_field_id,
		data:actData
	});
}

//
init();
