import { indexeddb } from './core/library/indexeddb.mjs';
import { appState, appStatusUpdate } from './appState.jsm';
import { idbConfig } from './config.jsm';
import { swHandler as swHandlerClass } from './sw_handler.jsm';


async function loadSVG(fileUrl) {
	let response = await fetch( fileUrl );
	if ( response.status !== 200 ) {
		console.error( 'Looks like there was a problem. Status Code: ' + response.status );
		return;
	}
	let svgData = await response.text();
	document.body.insertAdjacentHTML('beforeend', svgData);
}

// Load initial images
await loadSVG( '/admin/res/img/se-icons.svgz' );

export const idbObj = await new indexeddb(idbConfig);


async function appSync(force = false) {
	// Detect if new settings


	//
	if ( !appState.loaded || force) {
		// Reset IDB
		idbObj.clearTables();

		// Request
		let fResponse = await fetch('/admin/ajax/loadInit');
		let msg = await fResponse.json();

		// Update State


		//
		appState.loaded = true;
		//
		appState.fieldData = msg.d.server_fields[0];
		console.log("CURRENT STATUS", appState);
		//
		appState.properties.matches = appState.fieldData.confMatches;
		appState.properties.score = appState.fieldData.confScoring;
		appState.properties.server = appState.fieldData.confServer;
		appState.properties.connection = appState.fieldData.confConnection;

		//
		appStatusUpdate();

		// Update IDB
		idbObj.syncData(msg.d);
	}
}

//
await appSync(true);

export const swHandlerObj = new swHandlerClass();

// Initial operations
if ( appState.properties.score === 'dynamic' ) {
	if ( appState.properties.server === 'server' && appState.properties.connection === 'wifi' ) {
		// Initialize WS Connection
		swHandlerObj.startWorker('ws', {
			'op_type':['appChange', 'connect'],
			'op_data':{
				'url':'wss://' + window.location.hostname + '/admin/ws/',
			}
		});
	} else if ( appState.properties.server === 'mobile' ) {
		// Initialize Web Worker
		swHandlerObj.startWorker('local');
	}
}
