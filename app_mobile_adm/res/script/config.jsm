﻿//
let idbConfig = {
	version: 1,
	name: 'vp_device',
	status: {
		init: false
	},
	reset: false,
	structure: {
		//
		'games': {
			build: { keyPath: "id" },
			indexes: {}
		},
		//
		'game_matches': {
			build: { keyPath: "index", autoIncrement:true },
			indexes: {
				'id': {
					keyPath: 'id',
					params: { unique: false }
				},
				'mOrder': {
					keyPath: 'mOrder',
					params: { unique: false }
				},
				'cOrder': {
					keyPath: ["gId", "fId", "mOrder"],
					params: { unique: true }
				},
				'gameFilter': {
					keyPath: ["gsId", "fId"],
					params: { unique: false }
				},
				'gId': {
					keyPath: 'gId',
					params: { unique: false }
				},
				'ggId': {
					keyPath: 'ggId',
					params: { unique: false }
				},
				'gsId': {
					keyPath: 'gsId',
					params: { unique: false }
				},
				'fId': {
					keyPath: 'fId',
					params: { unique: false }
				},
				'tId': {
					keyPath: 'game_teams',
					params: { unique: false, multiEntry:true }
				},
				'sync': {
					keyPath: 'sync',
					params: { unique: false }
				}
			}
		},
		//
		'gametypes': {
			build: { keyPath: "id" },
			indexes: {}
		},
		//
		'server_fields': {
			build: { keyPath: "id", autoIncrement:true },
			indexes: {}
		}
	}
};

export { idbConfig };