﻿import { appState, appStatusUpdate } from '../appState.jsm';
import { idbObj, swHandlerObj } from '../main.jsm';
import * as objectManipulation from '../core/library/dom-manipulation.mjs';
import * as formManipulation from '../core/library/forms.mjs';
import { translator } from '../translator.jsm';

// Define page custom element
customElements.define('app-page-field', class extends HTMLElement {
	//<editor-fold desc="Elements">
	//
	panelGroup = this.querySelector('div[se-elem="panelGroup"]');
	// Info section
	infoSection = this.panelGroup.querySelector('div[se-elem="info"]');

	//<editor-fold desc="Score Dynamic">

	//<editor-fold desc="Action Tab">
	actionPanel = this.panelGroup.querySelector('div[role="tabpanel"][se-elem="actions"]');
	actionsForm = this.actionPanel.querySelector('form[se-elem="actions"]');
	actionsFormSubmitButton = this.actionsForm.querySelector('button[type="submit"]');
	actionsSide = this.actionsForm.querySelector('div[se-elem="side"]');
	actionsDevices = this.actionsForm.querySelector('div[se-elem="devices"]');
	actionsDevicesTemplate = this.actionsDevices.querySelector('template');
	actionsDevicesContent = this.actionsDevices.querySelector('div[se-elem="content"]');
	pickSideForm = this.actionPanel.querySelector('form[se-elem="pickSide"]');
	game_action_buttons = this.querySelector('div[se-elem="timerAct"]');

	//</editor-fold>

	//<editor-fold desc="Events Tab">
	eventPanel = this.panelGroup.querySelector('div[role="tabpanel"][se-elem="events"]');
	eventsChronicle = this.eventPanel.querySelector('div[se-elem="chronicle"]');
	eventsChronicleTemplate = this.eventsChronicle.querySelector('template');
	eventsChronicleContent = this.eventsChronicle.querySelector('div[se-elem="content"]');
	eventsList = this.eventPanel.querySelector('div[se-elem="ev_list"]');
	eventsListTemplate = this.eventsList.querySelector('template');
	eventsListContent = this.eventsList.querySelector('tbody[se-elem="content"]');
	//
	eventEditDiag = this.querySelector('dialog[se-elem="eventEdit"]');
	eventEditForm = this.eventEditDiag.querySelector('form');

	//</editor-fold>

	//<editor-fold desc="Status Tab">
	statusSection = this.panelGroup.querySelector('div[se-elem="gameStatus"]');
	statusSectionContent = this.statusSection.querySelector('div[se-elem="content"]');
	//</editor-fold>

	//</editor-fold>

	//<editor-fold desc="Data">
	dataSection = this.panelGroup.querySelector('div[se-elem="data"]');
	gameMap = this.dataSection.querySelector('div[se-elem="map"]');
	gameMapTempalte = this.gameMap.querySelector('template');
	gameMapContent = this.gameMap.querySelector('div[se-elem="content"]');
	//
	gameScoreBoard = this.dataSection.querySelector('div[se-elem="scoreboard"]');
	//</editor-fold>

	//<editor-fold desc="Header">
	gameStatus = this.querySelector('div[se-elem="gameStatus"]');
	game_timer = this.gameStatus.querySelector('div[se-elem="timer"]');
	game_timer_progress = this.game_timer.querySelector('div.bar');
	game_timer_text = this.game_timer.querySelector('div.timeText');
	gameStatus_section_c = this.gameStatus.querySelector('span[se-elem="section_current"]');
	game_text = this.gameStatus.querySelector('div[se-elem="text"]');
	game_timertotal_text = this.gameStatus.querySelector('span[se-elem="timeText_total"]');
	//</editor-fold>

	//<editor-fold desc="Passive Match Operations">
	matchScorePanel = this.querySelector('div[se-elem="gameScore"]');
	pickSideForm2 = this.matchScorePanel.querySelector('form[se-elem="pickSide"]');
	scoreForm = this.matchScorePanel.querySelector('form[se-elem="scoreForm"]');
	scoreFormTemplate = this.scoreForm.querySelector('template');
	scoreFormBody = this.scoreForm.querySelector('tbody');
	//</editor-fold>

	//<editor-fold desc="Matches">
	matchesListObj = this.panelGroup.querySelector('div[se-elem="matchesList"]');
	matchTemplate = this.matchesListObj.querySelector('template[se-elem="template"]');
	matchListCont = this.matchesListObj.querySelector('div[se-elem="content"]');
	//</editor-fold>

	//</editor-fold>

	//<editor-fold desc="Variables">
	matchIndex;
	matchData;gameData;
	serverFieldData;
	matchEvents = [];
	//
	gameTypeData;
	fieldStatus = {
		active:0,
		fase:0,
		pause:0
	};
	//</editor-fold>

	//
	constructor() {
		super();
	}

	//
	async connectedCallback() {
		// Initial processing
		//
		this.fieldData = appState.fieldData;

		// Set initial configuration (CSS)
		objectManipulation.updateElement(this, [
			['data', 'propertiesMatches', appState.properties.matches],
			['data', 'propertiesScore', appState.properties.score],
			['data', 'propertiesServer', appState.properties.server],
			['data', 'propertiesConnection', appState.properties.connection],
		]);

		//
		this.infoSection.se_updateChildren({
			'td[data-replace="matchMode"]':['text', this.fieldData.confMatches],
			'td[data-replace="gameMode"]':['text', this.fieldData.gtTitle],
			'td[data-replace="score"]':['text', this.fieldData.confScoring],
			'td[data-replace="server"]':['text', this.fieldData.confServer],
			'td[data-replace="connection"]':['text', this.fieldData.confConnection],
			'td[data-replace="fieldId"]':['text', this.fieldData.id],
		});

		//
		let queries = [
			{
				rName:'gametypes',
				table:'gametypes',
				params: {
					get:appState.fieldData.gtId,
				}
			}
		];

		//
		if ( this.fieldData.confMatches === 'tournament' ) {
			queries.push({
				rName:'game_matches',
				table:'game_matches',
				params: {
					index: 'gameFilter',
					keyRange: IDBKeyRange.only([appState.status.gameStageId, appState.status.eventFieldId]),
					sort:{
						name:'mOrder',
						direction:'ASC'
					}
				}
			});
		}

		// Procesar tipo de juego y jugadores
		let results = await idbObj.queryBatch(queries);

		//
		console.log("MAIN - PAGE:field_dynamic_mobile - Query:gametypedata information", results);
		this.gameTypeData = results['gametypes'];

		//
		switch ( appState.properties.score ) {
			case 'dynamic':
				this.buildPageScoreDynamic()
				break;
			case 'passive':
				this.buildPageScoreDynamic()
				break;
		}


		// Vista general (ver si hay que ajustar.... obvio no está cargando)
		this.updatePage(this.fieldData.data.status.active, this.fieldData.data.status.fase, this.fieldData.data.section, this.gameTypeData.struct.settings.sectionsTotal);

		// Check for more matches and fill
		if ( this.fieldData.confMatches === 'tournament' ) {
			let matchFound = false;

			// Activate team selection
			for ( let cMatch of results.game_matches ) {
				if ( cMatch.index === this.fieldData.cMatchIndex ) {
					this.matchIndex = cMatch.index;
					this.matchData = cMatch;
					matchFound = true;
					break;
				}
			}

			//
			if ( !matchFound ) {
				console.error("el index del match esperado es:", this.fieldData.cMatchIndex, results.game_matches);
				alert("ERROR: no se encontró la partida actual");
				return;
			}

			//
			this.matchListCont.se_insertContentFromTemplate(
				'beforeend',
				this.matchTemplate,
				results.game_matches
			);

			// Update all information about match
			this.field_proc();
		}

		// Bindings
		this.addEventListener('click', this.onClickCallBacks.bind(this));
		document.body.addEventListener('swFieldMessage', this.serverMessageRecieve.bind(this))
	}

	disconnectedCallback() {
		document.body.removeEventListener('swFieldMessage', this.serverMessageRecieve.bind(this))
	}


	//
	buildPageScoreDynamic() {
		// Llenar status
		for ( let i = 0; i < this.gameTypeData.struct.properties.gameStatus.length; i++ ) {
			let cStatus = this.gameTypeData.struct.properties.gameStatus[i].data;

			//
			this.statusSectionContent.se_insertContentFromTemplate(
				'beforeend',
				( cStatus.statusValueType === 'bool' ) ? this.statusSection.querySelector('template[se-elem="boolean"]') : this.statusSection.querySelector('template[se-elem="number"]'),
				{
					index: i,
					title: cStatus.title
				}
			);
		}

		let insDevices = [],
			insActions = [];

		// Llenar dispositivos (mapa y lista)
		for ( let i = 0; i < this.gameTypeData.struct.properties.devices.length; i++ ) {
			let cDev = this.gameTypeData.struct.properties.devices[i].data;

			// Map devices
			insDevices.push({
				id: i,
				title: cDev.title,
				type: cDev.devType,
				mapicon: cDev.mapicon,
				mapx: cDev.mapx,
				mapy: cDev.mapy,
				active: cDev.devActiveDef,
				side: cDev.devSideDef,
				status: cDev.devStatusDef,
			});

			// Acciones

			// Saltar en lista la base
			if ( cDev.devType === 'base' ) { continue; }
			//
			insActions.push({
				id: i,
				title: cDev.title,
				type: cDev.devType,
				icon: cDev.mapicon,
				active: cDev.devActiveDef,
				side: cDev.devSideDef,
				status: cDev.devStatusDef,
			});
		}

		// Insert elements
		this.gameMapContent.se_insertContentFromTemplate('beforeend', this.gameMapTempalte, insDevices);
		this.actionsDevicesContent.se_insertContentFromTemplate('beforeend', this.actionsDevicesTemplate, insActions);

		// ajuste variables
		this.matchTimeTotal = this.gameTypeData.struct.settings.timeGameInt;
		this.eventsChronicle.se_updateChild('.end', ['text', this.secondsToTime(this.matchTimeTotal)]);

		// Poner eventos
		this.matchEvents = this.fieldData.data.eventsUser;
		this.reDrawEvents();

		// Puntos
		this.gameScoreBoard.se_updateChildren({
			'.team.home .score':['text', this.fieldData.data.points.t['1'].t ],
			'.team.visitor .score':['text', this.fieldData.data.points.t['1'].t ],
		});

		// Status
		for ( let cGameStatusIndex in this.fieldData.data.gameStatus ) {
			if ( !this.fieldData.data.gameStatus.hasOwnProperty(cGameStatusIndex) ) { continue; }
			//
			let cGameStatusObj = this.fieldData.data.gameStatus[cGameStatusIndex],
				cGameStatusInput = this.statusSection.querySelector('label[data-index="' + cGameStatusIndex + '"] input');

			//
			switch ( cGameStatusInput.type ) {
				case 'checkbox':
					cGameStatusInput.checked = (cGameStatusObj.value);
					break;
				case 'number':
					cGameStatusInput.value = cGameStatusObj.value;
					break;
				default:
					console.error("Valor incorrecto", cGameStatusInput);
					break;
			}
		}

		// Update device properties
		this.updateFieldDevices(this.fieldData.data.devices);

		// Notify current state to local server
		if ( appState.properties.server === 'mobile' ) {
			console.log("SEND CURRENT SERVER STATUS");
			setTimeout(() => {
				console.log("SEND CURRENT SERVER STATUS");
				swHandlerObj.sw_postMessage({
					'op_type': ['field', 'set'],
					'op_data': {
						id: appState.status.serverFieldId
					}
				});
			}, 1000);
		}

		// Bindings

		// Actions
		this.actionsDevicesContent.addEventListener('click', this.actionDeviceSelect.bind(this));
		this.actionsSide.addEventListener('click', this.actionSideSelect.bind(this));
		this.actionsForm.addEventListener('submit', this.actionFormSubmitAction.bind(this));
		//
		this.pickSideForm.addEventListener('change',this.pickSideAuto.bind(this));
		this.pickSideForm.addEventListener('submit', this.pickSideSubmit.bind(this));
		//
		this.eventEditForm.addEventListener('submit', this.actionUpdate.bind(this));
		this.statusSection.addEventListener('change', this.gameStatusUpdate.bind(this));
	}

	//
	buildPageScorePassive() {
		// Actions
		this.pickSideForm.se_on('change', 'input', this.pickSideAuto);
		this.pickSideForm.se_on('submit', this.pickSideSubmit);
		this.scoreForm.se_on('submit', this.pointsFormSubmit);
	}


	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('[data-onclick]');
		if ( !cBtn ) { return; }

		let operation = cBtn.dataset['onclick'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//
	matchOperation(e, cBtn) {
		e.preventDefault();

		//
		if ( appState.fieldData.confScoring === 'score' ) {
			// Simple timer
			switch ( cBtn.dataset['action'] ) {
				case 'start':
					break;
				case 'stop':
					break;
				case 'reset':
					break;
				case 'next':
					break;
			}
		} else {
			// Online server, send information
			console.warn("match operation", cBtn, cBtn.dataset['action']);
			this.serverMessageSend('setup', cBtn.dataset['action']);
		}
	}

	//
	gameStatusUpdate(e, cEl) {
		e.preventDefault();
		let value;

		//
		switch ( cEl.type ) {
			case 'checkbox':
				value = ( cEl.checked ) ? 1 : 0;
				break;
			case 'number':
				value = parseInt(cEl.value) || 0;
				break;
			default:
				console.error("Game status update invalid...", cType);
				return;
				break;
		}

		//
		this.serverMessageSend('gameStatus', {
			index:parseInt(cEl.dataSet['index']),
			value:value
		});
	}

	//<editor-fold desc="Beeper">

	audioContext = AudioContext && new AudioContext();
	audioBeeps = [
		// Defualts
		[100, 50, 100, 50, 100],                        // 00 - Field Reset
		[300, 150, 300, 150, 300],                      // 01 - Game Start
		[500, 500, 500, 500, 500, 500, 500, 500, 500],  // 02 - Pause
		[500, 500, 500, 500, 3000],                     // 03 - Game Stop
		[2000],                                         // 04 - Insertion
		//
		[450, 100, 450],                                // 05 - ?
		[500, 500, 500],                                // 06 - ?
		[200, 50, 200],                                 // 07 - No
		[2000],                                         // 08 - Single Long
		[500],                                          // 09 - Single short
	]

	//amp:0..100, freq in Hz, ms
	beep(amp, freq, ms) {
		if ( !this.audioContext ) return;
		let osc = this.audioContext.createOscillator(),
			gain = this.audioContext.createGain();
		osc.connect(gain);
		osc.frequency.value = freq;
		gain.connect(this.audioContext.destination);
		gain.gain.value = amp/100;
		osc.start(this.audioContext.currentTime);
		osc.stop(this.audioContext.currentTime + ms / 1000);
	}

	//
	doBeep(beepNum) {
		if ( !this.audioBeeps.hasOwnProperty(beepNum) ) {
			console.error("BEEP ERROR. Requested beep does not exists.", beepNum);
		}
		return false;

		//
		let sound = true,
			tTime = 0;
		for ( let cTime of this.audioBeeps[beepNum] ) {
			//
			if ( sound ) {
				setTimeout(() => { this.beep(10, 900, cTime) }, tTime);
			}
			sound = !sound;
			tTime+= cTime;
		}
	}

	//</editor-fold>


	//<editor-fold desc="Score Passive Operations">
	//
	pickSideAuto(e, cEl) {
		e.preventDefault();
		let cName = cEl.name,
			cVal = intVal(cEl.value);
		console.log("side select operation", cName, cVal);
		//
		switch ( cName ) {
			//
			case 't1_side':
				this.pickSideForm.se_formElVal('t2_side', ( cVal === 2 ) ? 1 : 2);
				break;

			//
			case 't2_side':
				this.pickSideForm.se_formElVal('t1_side', ( cVal === 2 ) ? 1 : 2);
				break;

			//
			default:
				console.error("what, obviously not defined");
				break;
		}
	}

	//
	pickSideSubmit(e) {
		e.preventDefault();
		let eData = se.form.serializeToObj(pickSideForm, false, {'int':['t1_side', 't2_side']});

		//
		console.log("COLOR SUBMITED", eData);

		// All content is updated after saving information
		updateMatch({
			't1_side':eData.t1_side,
			't2_side':eData.t2_side
		});
	}

	//
	pointsFormSubmit(e) {
		e.preventDefault();

		console.log("\nPOINTS SUBMIT\n");

		//
		if ( matchData.t1_side === 0 || matchData.t2_side === 0 ) {
			console.warn("sides not set...? ignoring");
			return;
		}

		//
		let scoreValues = se.form.serializeObject(scoreForm),
			saveData = {},
			cPointsList = {
				// Teams
				t:{
					'1':{
						'p':{},
						't':0
					},
					'2':{
						'p':{},
						't':0
					}
				},
				// Persons, not available
				p:{}
			};

		console.log("SUBMITED:", scoreValues);

		//
		let totalPoints = {
			'1':0,
			'2':0
		};

		// Game points loop, apply max limits and sum
		for ( let cPointsId in gameTypeData.struct.points ) {
			if ( !gameTypeData.struct.points.hasOwnProperty(cPointsId) ) { continue; }

			//
			let cPointData = gameTypeData.struct.points[cPointsId],
				tCount, tTotal;

			// Skip user points
			if ( cPointData.target !== 1 ) { continue; }

			// Do per team
			for ( let i = 1; i < 3; i++ ) {
				let teamIndex = i.toString();

				//
				tCount = scoreValues['point'][teamIndex][cPointsId];
				tCount = ( tCount <= cPointData.pointsMax ) ? tCount : cPointData.pointsMax;
				//
				tTotal = tCount * cPointData.points;
				//
				cPointsList.t[teamIndex].p[cPointsId] = {
					'c':tCount,
					't':tTotal
				};

				//
				totalPoints[teamIndex]+= tTotal;
			}
		}

		// Is there any max limit for points? (as set in game)
		for ( let i = 1; i < 3; i++ ) {
			let teamIndex = i.toString();
			totalPoints[teamIndex] = ( totalPoints[teamIndex] > gameTypeData.struct.settings.maxPointsGame ) ? gameTypeData.struct.settings.maxPointsGame : totalPoints[teamIndex];
			//
			cPointsList.t[teamIndex].t = totalPoints[teamIndex];
		}

		// Set "points side"
		saveData.t1_points = totalPoints[matchData.t1_side.toString()];
		saveData.t2_points = totalPoints[matchData.t2_side.toString()];

		// Set victory side (or draw)
		if ( saveData.t1_points === saveData.t2_points ) {
			saveData.t1_status = 2;
			saveData.t2_status = 2;
		} else {
			saveData.t1_status = ( saveData.t1_points > saveData.t2_points ) ? 3 : 1;
			saveData.t2_status = ( saveData.t1_points < saveData.t2_points ) ? 3 : 1;
		}

		// Add points to save and set match as ended
		saveData.mPoints = cPointsList;
		saveData.mStatus = 4;

		// Match in list update
		let cMatchObj = matchListCont.querySelector('.matchObj[data-index="' + matchIndex + '"]');
		//
		cMatchObj.se_data('status', 4);
		//
		se.element.childrenUpdate(
			cMatchObj,
			{
				'.team.home .score':[
					['text', saveData.t1_points ]
				],
				'.team.home svg[data-mode="win"]':[
					['data', 'status', saveData.t1_status ]
				],
				'.team.visitor .score':[
					['text', saveData.t2_points ]
				],
				'.team.visitor svg[data-mode="win"]':[
					['data', 'status', saveData.t2_status ]
				],
			}
		);

		// Save content
		updateMatch(saveData, false);

		//
		nextGameButton.se_show();
	}
	//</editor-fold>



	//<editor-fold desc="Tournament special actions">


	//
	field_next() {
		// Save game


		// Unsynced matches
		idbObj.query('game_matches', {
			index: 'gameFilter',
			keyRange: IDBKeyRange.only([appState.status.gameStageId, appState.status.eventFieldId]),
			sort:{
				name:'mOrder',
				direction:'ASC'
			},
			onSuccess:(data) => {
				// Loop matches searching for next available
				for ( let cMatch of data ) {
					if ( cMatch.mStatus !== 1 ) { continue; }

					//
					this.matchIndex = cMatch.index;
					this.matchData = cMatch;

					// Update field
					this.updateServerField({
						cMatchIndex:this.matchData.index,
						cMatchOrder:this.matchData.mOrder,
						gtId:this.matchData.gtId,
						t1_name:this.matchData.t1_name,
						t2_name:this.matchData.t2_name,
					});

					//
					this.serverMessageSend('setup', 'next');

					//
					this.field_proc();
					break;
				}
			}
		});
	}


	//
	updateServerField(params) {
		//
		// serverFieldData = se.object.merge(serverFieldData, params);

		//
		idbObj.update('server_fields', appState.status.serverFieldId,
			params,
			{
				onSuccess:(data) => {}
			}
		);
	}

	//
	field_proc() {
		console.debug("FIELD PROC DATA: ", this.serverFieldData, this.matchData, this.gameTypeData);

		//
		this.gameScoreBoard.se_updateChildren({
			'.team.home [se-elem="title"]':['text', this.matchData.t1_name],
			'.team.visitor [se-elem="title"]':['text', this.matchData.t2_name],
			'.team.home':['data', 'side', 0],
			'.team.visitor':['data', 'side', 0],
			'.team.home [se-elem="score"]':['text', '0'],
			'.team.visitor [se-elem="score"]':['text', '0'],
		});


		/*
		Replacing using other method
		//
		se.element.childrenUpdate(infoSection, {
			'td[se-elem="gtTitle"]':[
				['text', serverFieldData.gtTitle]
			],
			'td[se-elem="gsTitle"]':[
				['text', serverFieldData.gsTitle]
			],
			'td[se-elem="fTitle"]':[
				['text', serverFieldData.fTitle]
			],
			'td[se-elem="id"]':[
				['text', matchData.index]
			],
			'td[se-elem="cMatchIndex"]':[
				['text', matchData.mOrder]
			],
		});
		 */

		//
		this.pickSideForm.se_updateChildren({
			'td[se-elem="t1_name"]':[
				['text', this.matchData.t1_name]
			],
			'td[se-elem="t2_name"]':[
				['text', this.matchData.t2_name]
			],
		});

		// Form selector
		this.pickSideForm.reset();

		// Lados definidos?
		if ( this.matchData.t1_side === 0 ) {
			return;
		}

		// Match in list
		this.matchListCont.querySelector('.matchObj[data-index="' + matchIndex + '"]').se_updateChildren(
			{
				'.team.home':[
					['data', 'side', this.matchData.t1_side ]
				],
				'.team.visitor':[
					['data', 'side', this.matchData.t2_side ]
				],
			}
		);
	}

	//
	pickSideAuto(e, cEl) {
		e.preventDefault();
		let cName = cEl.name,
			cVal = parseInt(cEl.value) || 0;

		//
		console.log("side select operation", cName, cVal);

		//
		switch ( cName ) {
			//
			case 't1_side':
				this.pickSideForm.se_elementValue('t2_side', ( cVal === 2 ) ? 1 : 2);
				break;

			//
			case 't2_side':
				this.pickSideForm.se_elementValue('t1_side', ( cVal === 2 ) ? 1 : 2);
				break;

			//
			default:
				console.error("what, obviously not defined");
				break;
		}
	}

	//
	pickSideSubmit(e) {
		e.preventDefault();
		let eData = this.pickSideForm.se_serializeToJSON();

		//
		console.log("COLOR SUBMITED", eData);

		// All content is updated after saving information
		this.updateMatch({
			't1_side':eData.t1_side,
			't2_side':eData.t2_side
		});
	}

	//
	updateMatch(params, updateAll = true) {
		//
		this.matchData = se.object.merge(matchData, { sync:0 }, params);

		console.log("new match information... (local)", matchData);

		//
		idbObj.update('game_matches', matchIndex,
			se.object.merge({ sync:0 }, params),
			{
				onSuccess:(data) => {
					console.log("success data=?", data);
					if ( updateAll ) {
						field_proc();
					}
				}
			}
		);
	}

	//</editor-fold>

	//
	uploadScores() {
		// Get all matches
		idbObj.query('game_matches', {
			index:'sync',
			keyRange:IDBKeyRange.only(0),
			onSuccess:(matches) => {
				let cMatchesUpd = [],
					cMatchesPost = [];

				//
				for ( let tIndex in matches ) {
					if ( !matches.hasOwnProperty(tIndex) ) { continue; }

					let cMatch = matches[tIndex];

					//
					cMatchesUpd.push({index:cMatch.index, sync:1});

					//
					cMatchesPost.push({
						index:cMatch.index,
						t1_side:cMatch.t1_side,
						t1_points:cMatch.t1_points,
						t1_status:cMatch.t1_status,
						t2_side:cMatch.t2_side,
						t2_points:cMatch.t2_points,
						t2_status:cMatch.t2_status,
						mStatus:cMatch.mStatus,
						mPoints:cMatch.mPoints,
						mEvents:cMatch.mEvents,
					});
				}

				//
				se.ajax.json('/admin/ajax/score_mobile/matchesUpload',
					{
						data:JSON.stringify(cMatchesPost)
					},
					{
						onSuccess:() => {
							// Actualizar DB de nuevo de los equipos
							idbObj.updateMany('game_matches', 'index', cMatchesUpd, {
								onSuccess: () => {
									console.log("Matches sincronizados");
								},
								onError: (err) => {
									console.log("Matches no sincronizados", err);
								},
							});

						}
					}
				);
			}
		});
	}



	//<editor-fold desc="Action Add Form">

	//
	actionDeviceSelect(e, cEl) {
		let devId = cEl.dataSet['id'];

		//
		this.actionsDevicesContent.se_updateChildren('div.object', ['attr', 'aria-selected', 'false']);
		cEl.se_attr("aria-selected", "true");

		//
		this.actionsForm.se_updateChildren('div[data-step]', ['func', (cEl) => {
			cEl.setAttribute('aria-hidden', ( cEl.dataset['step'] === '1' ) ? 'false' : 'true');
		}]);

		//
		this.actionsForm.se_updateChildren({
			'.team.home .score':[
				['text', this.fieldData.data.points.t['1'].t ]
			],
			'.team.home svg[data-mode="win"]':[
				['data', 'status', this.fieldData.data.points.t['1'].s ]
			],
			'.team.visitor .score':[
				['text', this.fieldData.data.points.t['1'].t ]
			],
			'.team.visitor svg[data-mode="win"]':[
				['data', 'status', this.fieldData.data.points.t['1'].s ]
			],
			'div[data-step="1"] div.object.device':[
				['data', 'id', cEl.dataSet['id'] ],
				['data', 'type', cEl.dataSet['type'] ],
				['data', 'side', cEl.dataSet['side'] ],
				['data', 'active', cEl.dataSet['active'] ],
				['data', 'status', cEl.dataSet['status'] ],
				['html', cEl.innerHTML ],
			],
			'div[data-step="2"] div.object.device':[
				['data', 'id', cEl.dataSet['id'] ],
				['data', 'type', cEl.dataSet['type'] ],
				['data', 'side', cEl.dataSet['side'] ],
				['data', 'active', cEl.dataSet['active'] ],
				['data', 'status', cEl.dataSet['status'] ],
				['html', cEl.innerHTML ],
			],
		});

		//
		this.actionsForm.se_elementValue('deviceId', devId);
	}

	//
	actionSideSelect(e, cEl) {
		let side = cEl.dataSet['side'];

		//
		this.actionsSide.se_updateChildren('div.object', ['attr', 'aria-selected', 'false']);
		cEl.setAttribute('aria-selected', 'true');

		// Special reset
		this.actionsForm.se_updateChildren('div[data-step]', ['func', (cEl) => {
			cEl.setAttribute('aria-hidden', ( cEl.dataset['step'] === '2' ) ? 'false' : 'true');
		}]);
		//
		this.actionsSide.se_updateChildren('div[data-step="2"] div.object.action', [
			['data', "side", cEl.dataSet['side']],
			['text', cEl.innerText]
		]);

		//
		this.actionsForm.se_elementValue('side', side);
	}

	//
	actionFormReset() {
		this.actionsForm.se_updateChildren('div[data-step]', ['attr', 'aria-hidden', 'true']);
		this.actionsDevicesContent.se_updateChildren('div.object', ['attr', 'aria-selected', 'false']);
		this.actionsSide.se_updateChildren('div.object', ['attr', 'aria-selected', 'false']);
		this.actionsForm.reset();
	}

	//
	actionFormSubmitAction(e) {
		// Bloquear si no esta corriendo juego
		if ( !this.fieldStatus.active || this.fieldStatus.fase === 0 ) {
			console.log("No debería poderse hacer submit ja");
		}

		e.preventDefault();
		let actionData = this.actionsForm.se_serializeToJSON();

		// Proper conversion
		actionData.status = 1;

		//
		this.serverMessageSend('device', actionData);

		// Reset form
		this.actionFormReset();
	}

	//</editor-fold>

	//
	actionUpdate(e) {
		e.preventDefault();
		let eData = this.editEventForm.se_serializeToJSON();

		// Send information to server (update in server), return success
		//
		this.serverMessageSend('event_update', {
			index:eData.id,
			devUser:eData.devUser
		});

		/*
		console.log("PRE\n\n\n", this.matchEvents[eData.id], eData);
		//
		this.matchEvents[eData.id].devUser = parseInt(eData.devUser);
		console.log("post", this.matchEvents[eData.id]);
		*/
		//
		eventEditDiag.close();
	}

	//
	actionDel(actId) {

	}

	//
	updatePage(sActive, sFase, sSectionC, sPaused) {
		objectManipulation.updateElement(this, [
			['data', 'matchActive', sActive],
			['data', 'matchFase', sFase],
			['data', 'matchSectionCurrent', sSectionC],
			['data', 'matchPaused', sPaused],
		]);

		//
		this.fieldStatus.active = parseInt(sActive);
		this.fieldStatus.fase = parseInt(sFase);
		this.fieldStatus.pause = parseInt(sFase);

		// if reset
		if ( !this.fieldStatus.active && !this.fieldStatus.fase ) {
			this.eventsChronicleContent.se_empty();
			this.eventsListContent.se_empty();
			//
			this.gameScoreBoard.se_updateChildren({
				'.team.home .score':['text', '0' ],
				'.team.visitor .score':['text', '0' ],
			});
			//
			this.matchEvents = [];

			// Reset devices
			for ( let i = 0; i < this.gameTypeData.struct.properties.devices.length; i++ ) {
				let cDev = this.gameTypeData.struct.properties.devices[i].data;
				this.gameMapContent.se_updateChild('svg[data-oid="'+i+'"]', [
					['data', 'active', cDev.devActiveDef],
					['data', 'side', cDev.devSideDef],
					['data', 'status', cDev.devStatusDef],
				]);
			}
		}

		//
		this.gameStatus_section_c.textContent = sSectionC;

		// Action buttons disabling
		this.game_action_buttons.se_updateChildren('button', ['attr', 'disabled', true]);
		this.actionsFormSubmitButton.disabled = true;

		// Operations
		if ( this.fieldStatus.active ) {
			this.game_action_buttons.querySelector('button[data-action="stop"]').disabled = false;
			if ( this.fieldStatus.fase === 1 ) {

			} else {
				this.actionsFormSubmitButton.disabled = false;
			}
		} else {
			this.timer_stop();
			if ( this.fieldStatus.fase === 0 ) {
				this.timerText_set(0);
				this.game_text.se_empty();
				this.game_action_buttons.querySelector('button[data-action="start"]').disabled = false;
			} else {
				this.game_action_buttons.querySelector('button[data-action="reset"]').disabled = false;
				this.game_action_buttons.querySelector('button[data-action="next"]').disabled = false;
			}
		}
	}

	//
	serverMessageRecieve(e) {
		let msg = e.detail;
		console.log("MAIN - FIELD PAGE - SW - MSG - IN:", msg);

		//
		switch ( msg.type ) {
			//
			case 'status':
				let fieldStatus = translator.msgToJson(msg.data, this.gameTypeData.struct.properties.devices.length, this.gameTypeData.struct.properties.gameStatus.length);
				console.log("MAIN - FIELD UPDATE - Status", fieldStatus);

				// Status
				this.updatePage(fieldStatus.field.active, fieldStatus.field.fase, fieldStatus.field.section, fieldStatus.field.pause);

				// Puntos
				this.gameScoreBoard.se_updateChildren({
					'.team.home .score':['text', fieldStatus.points['1'].t ],
					'.team.visitor .score':['text', fieldStatus.points['1'].t ],
				});

				// Devices
				this.updateFieldDevices(fieldStatus.devices);

				// gameStatus
				this.updateFieldGameStatus(fieldStatus.gameStatus)

				// actions
				this.updateFieldActions(fieldStatus.actions);

				// TODO: Resend message (bluetooth)
				// se_app.bluetooth.sendMessageDirect(msg.data);
				break;
			//
			case 'event':
				msg.data = msg.data.join();
				console.log("PAGE UPDATE - Event", msg);
				// Agregar a la lista
				this.matchEvents.push(msg.data);
				// Recrear listas
				this.reDrawEvents();
				break;
			//
			case 'matchChange':
				msg.data = msg.data.join();
				//
				console.log("notification of match update");

				// Set last match as finished, update info

				let cMatchObj = matchListCont.querySelector('.matchObj[data-index="' + matchIndex + '"]');
				//
				cMatchObj.se_elementUpdate(['data', 'status', 4]);
				//
				cMatchObj.se_childrenUpdate({
					'.team.home .score':[
						['text', msg.data.last.t1_points ]
					],
					'.team.home svg[data-mode="win"]':[
						['data', 'status', msg.data.last.t1_status ]
					],
					'.team.visitor .score':[
						['text', msg.data.last.t2_points ]
					],
					'.team.visitor svg[data-mode="win"]':[
						['data', 'status', msg.data.last.t2_status ]
					],
				});


				// Next match info
				this.matchIndex = msg.data.next.index;
				this.matchData.t1_name = msg.data.next.t1_name;
				this.matchData.t2_name = msg.data.next.t2_name;

				// Initial defaults
				this.matchData.t1_points = 0;
				this.matchData.t2_points = 0;
				this.matchData.t1_side = 0;
				this.matchData.t2_side = 0;

				// Update match information
				this.field_proc();
				break;
			//
			default:
				console.error("CURRENT OPERATION NOT DEFINED", msg);
				break;
		}
	}

	updateFieldDevices(deviceList) {
		for ( let cDeviceId in deviceList ) {
			if ( !deviceList.hasOwnProperty(cDeviceId) ) { continue; }
			let cDev = deviceList[cDeviceId];

			//
			this.gameMapContent.se_updateChild('svg[data-oid="' + cDeviceId + '"]', [
				['data', 'active', cDev.active],
				['data', 'side', cDev.side],
				['data', 'status', cDev.status],
			]);
		}
	}

	//
	updateFieldGameStatus(gameStatusList) {
		for ( let index of gameStatusList ) {
			if ( !gameStatusList.hasOwnProperty(index) ) { continue; }
			let cValue = gameStatusList[index];

			//
			let cGameStatusInput = this.statusSection.querySelector('label[data-index="'+index+'"] input');

			//
			switch ( cGameStatusInput.type ) {
				case 'checkbox':
					cGameStatusInput.checked = ( cValue );
					break;
				case 'number':
					cGameStatusInput.value = cValue;
					break;
				default:
					console.error("Valor incorrecto", cGameStatusInput);
					break;
			}
		}
	}

	//
	updateFieldActions(actions) {
		for ( let cAction of actions ) {
			switch ( cAction.type ) {
				// Ignorar sonidos
				case 'buzzer':
					if ( cAction.buzzSide === 0 ) {
						this.doBeep(parseInt(cAction.buzzType));
					}
					break;
				//
				case 'sound':
					break;
				//
				case 'timer':
					this.timer_set(parseInt(cAction.timeInt));
					break;
				//
				case 'note':
					this.game_text.textContent = cAction.note;
					break;
				//
				default:
					console.error("Field actions, action not valid", cAction);
					break;
			}
		}
	}

	//
	reDrawEvents() {
		let cronObjs = [],
			listObjs = [];

		//
		for ( let cEventId in this.matchEvents ) {
			//
			if ( !this.matchEvents.hasOwnProperty(cEventId) ) { continue; }
			//
			let cEvent = this.matchEvents[cEventId],
				cSide = 0,
				dataContent;

			// No va a mostrar eventos pre inicio de juego
			if ( cEvent.time < 0 ) { continue; }

			console.log("MAIN - FIELD DYNAMIC - EVENT REDRAWING:", cEvent);

			//
			switch ( cEvent.type ) {
				//
				case 'automatic':
					let cEventTimedInfo = this.gameTypeData.struct.properties[cEvent.subtype][cEvent.id].data;
					console.log(cEventTimedInfo);
					dataContent = `${cEventTimedInfo.title}`;
					break;
				//
				case 'device':
					let gtDevice = this.gameTypeData.struct.properties.devices[cEvent.devId].data;

					//
					dataContent = `
<svg class="device icon inline mr"><use xlink:href="#fa-user" /></svg> ${cEvent.devUser}<br />
<svg class="device icon inline mr" data-side="${cEvent.devSide}"><use xlink:href="#${gtDevice.mapicon}" /></svg> ${gtDevice.title}
`;
					//
					cSide = cEvent.devSide;
					break;
			}

			cronObjs.push({
				id:cEventId,
				type:cEvent.type,
				side:cSide,
				relTime:cEvent.time / this.matchTimeTotal * 100
			});

			listObjs.push({
				id:cEventId,
				type:cEvent.type,
				timeText:this.secondsToTime(cEvent.time),
				dataContent:dataContent
			});
		}


		//
		this.eventsChronicleContent.innerHTML = '';
		objectManipulation.insertContentFromTemplate(
			this.eventsChronicleContent,
			'beforeend',
			this.eventsChronicleTemplate,
			cronObjs
		);

		//
		this.eventsListContent.innerHTML = '';
		objectManipulation.insertContentFromTemplate(
			this.eventsListContent,
			'beforeend',
			this.eventsListTemplate,
			listObjs
		);
	}

	//<editor-fold desc="Timer stuff">

	timerStart = 0;
	timerObj = null;
	timerTargetTime = 0;
	timerTargetTimeTotal = 0;

	//
	timer_set(timeInt) {
		let timeCur = Math.round((new Date()).getTime() / 1000);
		this.timerTargetTime = timeCur + timeInt;
		this.timerTargetTimeTotal = timeInt;
		if ( this.timerStart === 0 ) {
			this.timerStart = timeCur;
			this.timerTotalObj = setInterval(this.timerTotal_advance.bind(this), 1000);
		}
		// Reset section timer (since it may not be exact seconds from other.
		if ( this.timerObj !== null ) {
			clearInterval(this.timerObj);
			this.timerObj = null;
		}
		this.timerObj = setInterval(this.timer_advance.bind(this), 1000);
		//
		this.timerText_set(timeInt);
		this.timerTotalText_set(- this.gameTypeData.struct.settings.timeBreakInt);
	}

	//
	timer_advance() {
		let timeCur = Math.round((new Date()).getTime() / 1000),
			timeDiff = this.timerTargetTime - timeCur;
		// Ejecutar tiempo
		this.timerText_set(timeDiff);
	}

	//
	timerTotal_advance() {
		let timeCur = Math.round((new Date()).getTime() / 1000),
			timeTotal = (timeCur - this.timerStart) - this.gameTypeData.struct.settings.timeBreakInt;

		// Ejecutar tiempo
		this.timerTotalText_set(timeTotal);
	}

	// Actualizar tiempos
	timerText_set(mTime) {
		let perAdvance = 0,
			cClass;
		//
		if ( mTime > 0 ) {
			// game_timer_progress.se_classDel('end').se_classAdd('countdown');
			perAdvance = 100 - ( mTime / this.timerTargetTimeTotal * 100);
			cClass = 'coundown'; // could be 'normal'
		} else {
			cClass = 'end';
		}
		//
		this.game_timer_text.se_updateElement(['text', this.secondsToTime(mTime)]);
		this.game_timer_progress.se_updateElement([
			['style', 'width', perAdvance + '%'],
			['class', cClass]
		]);
	}

	//
	timerTotalText_set(mTime) {
		// console.log("time total update...?", mTime);
		this.game_timertotal_text.se_updateElement(['text', this.secondsToTime(mTime)]);
	}

	//
	timer_stop() {
		this.timerStart = 0;
		if ( this.timerObj ) {
			clearInterval(this.timerObj);
			this.timerObj = null;
		}
		if ( this.timerTotalObj ) {
			clearInterval(this.timerTotalObj);
			this.timerTotalObj = null;
		}
	}

	//
	secondsToTime(seconds) {
		// we will use this to convert seconds in normal time format
		let secs = Math.abs(seconds),
			hr = Math.floor(secs / 3600),
			min = Math.floor((secs - (hr * 3600)) / 60),
			sec = Math.floor(secs - (hr * 3600) - (min * 60)),
			time = '';
		if ( hr !== 0 ) {
			if ( hr < 10 ) {
				time += '0';
			}
			time += hr + ':';
		}
		if ( min !== 0 ) {
			if ( min < 10 ) {
				time += '0';
			}
			time += min + ':';
		} else {
			time += '00:';
		}
		if ( sec < 10 ) {
			time += '0';
		}
		time += sec;
		if ( seconds < 0 ) {
			time = '-' + time;
		}
		return time;
	}

	//</editor-fold>

	//
	serverMessageSend(op, data = {}) {
		// Avoid sending in wrong conditions
		if ( appState.properties.score !== 'dynamic' ) {
			console.warn('SW Message ignored.');
		}

		// Mensaje al worker
		swHandlerObj.sw_postMessage({
			'op_type': ['field', 'command'],
			'op_data': {
				fId:appState.fieldData.id,
				op:op,
				data:data
			}
		});
	}

});
//
