'use strict';
//
class vp_loraUsb {
	//
	serialPort = null;

	// Input
	serialInputDone = null;
	serialReader = null;


	// Output
	serialOutputEncoder = null;
	serialOutputDone = null;
	serialOutputStream = null;

	//
	commandQueue = [];
	commandBlock = false;
	radioBlock = false;

	//
	constructor() {}

	//
	async connect() {
		try {
			// Filter only target devices
			const filters = [
				{usbVendorId: 0x1A86, usbProductId: 0x7523}
			];

			// Actual connection
			this.serialPort = await navigator.serial.requestPort({
				filters: filters
			});

			// Set connection
			await this.serialPort.open({
				baudRate: 57600,
				dataBits: 8,
				stopBits: 1,
				parity: 'none',
				flowControl: 'none'
			});

			// Write setup
			this.serialOutputEncoder = new TextEncoderStream();
			this.serialOutputDone = this.serialOutputEncoder.readable.pipeTo(this.serialPort.writable);
			this.serialOutputStream = this.serialOutputEncoder.writable;

			this.listenToPort();

			//
			console.log("USB Lora connected.");

			// Send initial configuration
			this.sendCommand('sys get ver');
			this.sendCommand('mac pause');
			this.sendCommand('radio set pwr 14');
			this.sendCommand('radio set freq 915000000');
			this.sendCommand('radio set sf sf11');
			this.sendCommand('radio set bw 125');
			this.sendCommand('radio set rxbw 125');
			this.sendCommand('radio set sync 54');
			this.sendCommand('radio set crc off');
			this.sendCommand('radio set prlen 8');
			//

			// this.sendCommand('radio set cr 4/5');
			this.sendCommand('sys set pindig GPIO10 1');
			// this.sendCommand('radio rx 100');
		} catch (e) {
			alert("no fue posible iniciar conexión con el USB");
		}
	}

	async listenToPort() {
		const textDecoder = new TextDecoderStream();
		this.serialInputDone = this.serialPort.readable.pipeTo(textDecoder.writable);
		const reader = textDecoder.readable.getReader();

		let cText = "";

		// Listen to data coming from the serial device.
		while ( true ) {
			const { value, done } = await reader.read();

			if ( value ) {
				cText += value;

				if ( cText.endsWith("\r\n") ) {
					let page_device_lora = $('#page_device_lora'),
						page_server_field = $('#page_server_field');

					// Set to output log if opened
					if ( page_device_lora ) {
						page_device_lora.uwp_page_device_lora.logAdd('in', cText);
					}

					// Set to output log if opened
					if ( page_server_field ) {
						page_device_lora.uwp_page_device_lora.logAdd(cText);
					}

					// Allow the serial port to be closed later.
					console.log("[SERIAL LORA READ] - VALUE:\n" + cText);

					if ( cText.startsWith('radio_rx') ) {
						// Do something with data:
						let msgData = cText.substring(9);

						console.warn("should do something with this lora msg", msgData);
					} else if ( cText.startsWith('radio_tx_ok') || cText.startsWith('radio_err') ) {
						this.radioBlock = false;
						this.commandQueue.push('sys set pindig GPIO10 0');
					} else {
						this.commandBlock = false;
					}

					this.commandProgress();
					cText = '';
				}
			}

			//
			if ( done ) {
				// Allow the serial port to be closed later.
				console.log('[SERIAL LORA READ] - DONE', done);
				reader.releaseLock();
				break;
			}
		}
	}

	sendByteArrayMessage(messageArray) {
		let cString = '';

		//
		for ( let cByte of messageArray ) {
			cString+= cByte.toString(16).padStart(2, '0');
		}
		console.log("Message array", messageArray.length, messageArray, cString, cString.length);

		this.sendMessage(cString);
	}

	sendMessage(message) {
		this.commandQueue.push('sys set pindig GPIO10 1');
		this.commandQueue.push('radio tx ' + message);
		this.commandProgress();
	}

	sendCommand(cmd) {
		this.commandQueue.push(cmd);
		this.commandProgress();
	}

	commandProgress() {
		if ( this.commandBlock || this.radioBlock || !this.commandQueue.length ) {
			return;
		}

		//
		this.writeToStream(this.commandQueue.shift());
	}

	//
	async writeToStream(command) {
		if ( !this.serialOutputStream ) { console.error("No output stream."); return; }

		//
		if ( command.startsWith('radio tx') ) {
			this.radioBlock = true;
		}

		//
		this.commandBlock = true;

		console.log("[SERIAL LORA - WRITE] - VALUE:\n" + command);

		// Log
		let page_device_lora = $('#page_device_lora');
		if ( page_device_lora ) {
			page_device_lora.uwp_page_device_lora.logAdd('out', command);
		}

		// Send
		const writer = this.serialOutputStream.getWriter();
		await writer.write(command + "\r" + "\n");
		writer.releaseLock();
	}

	//
	writeToStream_array(cmds) {
		this.sendMessage(cmds.join(';'));
	}

	//
	async disconnect() {
		// Close reader
		if ( this.serialReader ) {
			await this.serialReader.cancel();
			await this.serialInputDone.catch(() => {});
			this.serialReader = null;
			this.serialInputDone = null;
		}

		// close output
		if ( this.serialOutputStream ) {
			await this.serialOutputStream.getWriter().close();
			await this.serialOutputDone;
			this.serialOutputStream = null;
			this.serialOutputDone = null;
		}

		// close port
		await this.serialPort.close();
		this.serialPort = null;
	}
}