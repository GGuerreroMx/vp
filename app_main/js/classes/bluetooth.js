class bluetooth {
	//
	btDevice = null;
	btGattServer = null;
	btService = null;

	//
	btCharList = {
		'DEVICE': null,
		'GAME': null,
		'FIELD': null,
		'COMS': null,
	};
	//
	btChar_fieldUpdate = null;
	btChar_msgHwIn = null;

	UUID_LIST = {
		'SERVICE': '4fafc201-1fb5-459e-8fcc-c5c9c331914b',
		'DEVICE': 'beb5483e-36e1-4688-b7f5-ea07361b21a8',
		'GAME': 'beb5483e-36e1-4688-b7f5-ea07361b22a8',
		'FIELD': 'beb5483e-36e1-4688-b7f5-ea07361b31a8',
		'COMS': 'beb5483e-36e1-4688-b7f5-ea07361b42a8',
	};


	constructor() {
	}

	async connect(cb) {
		//
		let DEVICE_NAME = 'VP_DEVICE';

		//
		let options = {
			filters: [{name: DEVICE_NAME}],
			optionalServices: [this.UUID_LIST.SERVICE]
		};

		try {
			console.log('Requesting Bluetooth Device with ', options);

			//
			this.btDevice = await navigator.bluetooth.requestDevice(options);

			console.log('> Name:             ' + this.btDevice.name);
			console.log('> Id:               ' + this.btDevice.id);

			// Auto reconnect
			this.btDevice.addEventListener('gattserverdisconnected', await this.onDisconnect);

			// Actual connect operation
			await this.connectReal(cb);
		} catch ( error ) {
			console.error("BLE CONNECT ERROR!\n", error);
		}
	}

	//
	async connectReal(cb) {
		//
		let context = this;

		//
		this.exponentialBackoff(10 /* max retries */, 1 /* seconds delay */,
			async function toTry() {
				//
				console.log('BT - Connecting to GATT Server...');
				console.log('BT - DEVICE: ', context.btDevice);
				context.btGattServer = await context.btDevice.gatt.connect();

				//
				console.log('BT - Getting Service: ');
				console.log('BT - SERVER USED: ', context.btGattServer, context.UUID_LIST.SERVICE);
				context.btService = await context.btGattServer.getPrimaryService(context.UUID_LIST.SERVICE);

				// Save characteristics
				console.log('BT - Setting characteristics: ');
				context.btCharList['DEVICE'] = await context.btService.getCharacteristic(context.UUID_LIST.DEVICE);
				context.btCharList['GAME'] = await context.btService.getCharacteristic(context.UUID_LIST.GAME);
				context.btCharList['FIELD'] = await context.btService.getCharacteristic(context.UUID_LIST.FIELD);
				context.btCharList['COMS'] = await context.btService.getCharacteristic(context.UUID_LIST.COMS);

				// Coms Connect
				console.log('BT - Start notifications cb: ');
				if ( context.btCharList['COMS'].properties.notify ) {
					context.btCharList['COMS'].addEventListener(
						"characteristicvaluechanged",
						context.hardwareMessageIn
					);
					await characteristic.startNotifications();
				}
			},
			function success() {
				console.log('BT connected');
				if ( typeof cb === 'function' ) {
					cb();
				}
			},
			function fail(e) {
				console.log('Failed to reconnect.', e);
			},
		);
	}

	//
	async disconnect() {
		if ( !this.btDevice ) {
			return;
		}
		if ( this.btDevice.gatt.connected ) {
			this.btDevice.gatt.disconnect();
		} else {
		}
	}

	//
	async onDisconnect() {
		console.log('> Bluetooth Device disconnected');
		//
		this.btGattServer = null;
		this.btService = null;
		//
		await this.connectReal();
	}

	//
	async characteristicRead(charName) {
		if ( !(charName in this.btCharList) ) {
			console.error("Char not valid no válido.");
			return;
		}

		// Read current value
		return await this.btCharList[charName].readValue();
	}

	//
	async characteristicWrite(charName, cValue) {
		if ( !(charName in this.btCharList) ) {
			console.error("Char not valid no válido.");
			return;
		}

		console.log("BT - CHAR WRITE: ", charName, cValue);

		// Read current value
		try {
			await this.btCharList[charName].writeValueWithResponse(cValue);

		} catch ( error ) {
			console.error("BT - CHAR WRITE - ERROR: ", error);
		}
	}

	//
	async startComs() {
		let context = this;
		console.log('Getting Characteristic... (DIRECT_COMS)');
		context.btChar_msgHwIn = await context.btService.getCharacteristic(this.UUID_LIST.COMS);

		// Read current value
		await context.btChar_msgHwIn.readValue().then(
			(value) => {
				console.log('msg in Current Value: ' + value, value.byteLength);

				let nValue = context.convertData(value);
				console.log('msg in Currented Value: ' + nValue);
			}
		);

		//
		await context.btChar_msgHwIn.startNotifications();
		context.btChar_msgHwIn.addEventListener('characteristicvaluechanged', context.hardwareMessageIn);
	}

	//
	async exponentialBackoff(max, delay, toTry, success, fail, context) {
		try {
			const result = await toTry();
			success(result);
		} catch ( error ) {
			if ( max === 0 ) {
				return fail();
			}
			console.error("BT - ERROR: ", error);
			console.warn('BT - Retrying in ' + delay + 's... (' + max + ' tries left)');
			setTimeout(
				() => this.exponentialBackoff(--max, delay + 1, toTry, success, fail, context),
				delay * 1000
			);
		}
	}

	//
	convertData(dataView) {
		let nValue = [];

		//
		for ( let i = 0; i < dataView.byteLength; i++ ) {
			nValue.push(this.padHex(dataView.getUint8(i)));
		}
		//
		return nValue;
	}

	//
	padHex(value) {
		return ('00' + value.toString(16).toUpperCase()).slice(-2);
	}

	//
	fieldUpdateChange(event) {
		let fieldData = event.target.value,
			nValue = this.convertData(fieldData);
		console.log('UPDATED VALUE (Length: ' + fieldData.byteLength + ') - ' + nValue);
	}

	//
	async hardwareMessageIn(event) {
		let fieldData = event.target.value;
		// 	nValue = this.convertData(fieldData);
		console.log('UPDATED VALUE (Length: ' + fieldData.byteLength + ') - ', fieldData);

		// General ops


		// Process for logging
		let logPage = $('page_device_bluetooth');
		if ( logPage ) {
			logPage.uwp_page_device_bluetooth.addToLog('in', fieldData);
		}

		/*
		// Online mode
		let gamePage = $('page_device_bluetooth');
		if ( logPage ) {
			logPage.uwp_page_device_bluetooth.addToLog('in', fieldData);
		}

		 */
	}

	//
	sendMessageString(value) {
		if ( !this.btChar_msgHwIn ) {
			return;
		}

		//
		let enc = new TextEncoder(), // always utf-8
			cArray = enc.encode(value);

		console.log("MAIN - BT - Send message string: ", value, cArray);

		//
		this.btChar_msgHwIn.writeValueWithResponse(cArray).then(
			(e) => {
				console.log("MAIN - BT - Sent message: ", value, cArray);
			}
		)
	}

	//
	sendMessageDirect(value) {
		if ( !this.btChar_msgHwIn ) {
			return;
		}

		console.log("MAIN - BT - Send message: ", value);

		//
		this.btChar_msgHwIn.writeValueWithResponse(value).then(
			(e) => {
				console.log("MAIN - BT - Sent message: ", value);
			}
		)
	}
}
