'use strict';
//
se.plugin.vp_chat = function(plugElem, plugSettings) {
	let defaults = {},
		settings = {},
		menu = plugElem.querySelector('div.test_menu'),
		audioLocal = plugElem.querySelector('audio[se-type="local"]'),
		audioLocalMeter = plugElem.querySelector('div.audio_elem[se-elem="local"] meter'),
		audioExternal = plugElem.querySelector('audio[se-type="external"]'),
		audioExternalMeter = plugElem.querySelector('div.audio_elem[se-elem="external"] meter'),
		ws_host = 'wss://'+ window.location.hostname +'/ws',
		peerCon, ws_socket,
		peerConConf = {
			"iceServers":[{"urls":"stun:stun.1.google.com:19302"}]
		},
		//
		cData = {},
		userIdThis, userIdCall,
		//
		chatData = plugElem.querySelector('div.chat_data'),
		chatSystem = plugElem.querySelector('div.chat_sys'),
		chatTemplate = chatSystem.querySelector('template'),
		chatContent = chatSystem.querySelector('div.chat_cont'),
		chatTextArea = chatSystem.querySelector('textarea'),
		msgId = 0;

	//<editor-fold desc="Setup">
	//
	function init() {
		cData.mode = plugElem.se_data('mode');
		switch ( cData.mode ) {
			//
			case 'usr':
				console.log("UKS CHAT USR");
				break;
			//
			case 'adm':
				console.log("UKS CHAT ADM");
				cData.evalResults = plugElem.se_data('interview');
				break;
			//
			default:
				return;
				break;
		}
		// Bindings
		menu.se_on('click', 'button[se-chat-act]', btnAction);
		chatTextArea.se_on('keydown', chat_textarea_actions);
	}
	//
	function btnAction(ev, cBtn) {
		ev.preventDefault();
		switch ( cBtn.se_attr('se-chat-act') ) {
			//
			case 'start':
				startApi();
				handleStatus(1);
				break;
			//
			case 'ready':
				handleReady();
				break;
			//
			case 'close':
				if ( confirm("¿Terminar comunicación?") ) {
					handleClose();
				}
				break;
			//
			default:
				console.log("no válido");
				break;
		}
	}
	//
	function startApi() {
		//
		navigator.mediaDevices.getUserMedia({video:false, audio:true})
			.then(function (stream) {
				let audioLocalContext, audioLocalAnalizer, audioLocalStream,
					javascriptNode;
				audioLocal.srcObject = stream;
				// Analizer
				audioLocalContext = new window.AudioContext();
				audioLocalStream = audioLocalContext.createMediaStreamSource(stream);
				audioLocalAnalizer = audioLocalContext.createAnalyser();
				audioLocalAnalizer.smoothingTimeConstant = 0.3;
				audioLocalAnalizer.fftSize = 1024;
				audioLocalStream.connect(audioLocalAnalizer);
				// New method
				javascriptNode = audioLocalContext.createScriptProcessor(2048, 1, 1);
				javascriptNode.onaudioprocess = function() {
					// get the average, bincount is fftsize / 2
					let array =  new Uint8Array(audioLocalAnalizer.frequencyBinCount);
					audioLocalAnalizer.getByteFrequencyData(array);

					// Get average
					let average, sum = 0, length = array.length, i;
					for ( i = 0; i <length; i++ ) {
						sum+= array[i];
					}
					average = sum / length;
					audioLocalMeter.value = average;
				};
				audioLocalAnalizer.connect(javascriptNode);

				//
				// RTC CONECTION
				peerCon = new RTCPeerConnection(peerConConf);
				peerCon.addStream(stream);
				// Callbacks
				// peerCon.ontrack = pc_track; // Método correcto? según mdn, ignorar hasta que exista el cambio.
				peerCon.onaddstream = pc_track;
				peerCon.onicecandidate = pc_onicecandidate;
				peerCon.onopen = pc_open;
				peerCon.onerror = pc_error;
				// Register
				try {
					ws_socket = new WebSocket(ws_host);
					ws_socket.onopen = ws_open;
					ws_socket.onmessage = ws_message;
					ws_socket.onclose = ws_close;
					ws_socket.onerror = ws_error;
				} catch(ex) {
					alert("Error de comunicación." + ex);
				}
			})
			.catch(function (err) {
					console.log("No audio... why?! ", +err);
				}
			);
	}
	//</editor-fold>

	//<editor-fold desc="Chat">
	//
	function chat_textarea_actions(e) {
		if (!e) { e = window.event; }
		// console.log("KEY PRESS", e);
		// Enter is pressed
		if ( e.keyCode === 13 && !e.shiftKey ) { chat_msg_send(); }
	}
	//
	function chat_msg_send() {
		console.log("enviar");
		let content = chatTextArea.value;
		content = content.trim();
		if ( content ) {
			ws_send({
				type:'chat_msg_send',
				mId:msgId,
				user:userIdCall,
				content:content
			});
			let date = new Date,
				time = '' + date.getHours() + ':' + date.getMinutes();
			//
			chatContent.se_append(se.struct.stringPopulate(chatTemplate.se_html(), {
				'side':1, 'id':msgId, 'content':content, 'status':0, 'time':time
			}));
			//
			chatTextArea.value = '';
			msgId++;
		}
	}
	//
	function chat_msg_recieve(mId, mContent) {
		let date = new Date,
			time = '' + date.getHours() + ':' + date.getMinutes();
		//
		chatContent.se_append(se.struct.stringPopulate(chatTemplate.se_html(), {
			'side':2, 'id':mId, 'content':mContent, 'status':3, 'time':time
		}));
		ws_send({
			type:'chat_msg_confirm',
			mId:mId,
			user:userIdCall
		});
	}
	//
	function chat_msg_status(mId, status) {
		chatContent.querySelector('div.message[data-side="1"][data-id="'+mId+'"]').se_data('status', status);
	}
	//</editor-fold>

	//<editor-fold desc="Websockets">
	//
	function ws_open() {
		console.log("WS - Open");
	}
	//
	function ws_message(ev) {
		let data = JSON.parse(ev.data);
		console.log("WS - Msg:", data);
		switch ( data.type ) {
			case 'register':
				userIdThis = data.user;
				break;
			// Call for start converstation
			case "notready":
				handleStatus(2);
				break;
			//
			case "call":
				handleCreateOffer(data.user);
				break;
			case "offer":
				handleOffer(data.offer, data.user);
				break;
			case "answer":
				handleAnswer(data.answer);
				break;
			//when a remote peer sends an ice candidate to us
			case "candidate":
				handleCandidate(data.candidate);
				break;
			case "leave":
				handleLeave();
				break;
			case "error":
				call_log("WS - ERROR", data.desc);
				break;
			case "chat_msg_status":
				chat_msg_status(data.mId, data.status);
				break;
			case "chat_msg_recieve":
				chat_msg_recieve(data.mId, data.content);
				break;
			//
			case 'setup':
				handleSetup(data);
				break;
			default:
				call_log("WS - Msg: not valid", data);
				break;
		}
	}
	//
	function ws_close(e) {
		console.log("WS - Closed");
		handleStatus(0);
	}
	//
	function ws_send(data) {
		call_log("WS - Sending:", data);
		let rData = JSON.stringify(data);
		ws_socket.send(rData);
	}
	//
	function ws_error(error) {
		console.log("WS error", error);
	}
	//</editor-fold>

	//<editor-fold desc="Handles">
	//
	function handleCreateOffer(user) {
		if ( user === userIdThis ) { alert("system error. same user"); }
		userIdCall = user;
		// create an offer
		peerCon.createOffer(function (offer) {
			call_log("PC - Offer start:", offer);
			ws_send({
				type:"offer",
				offer:offer,
				user:userIdCall
			});
			peerCon.setLocalDescription(offer);
		}, function (error) {
			alert("Error when creating an offer");
		});
	}
	//
	function handleOffer(offer, user) {
		call_log("PC - Offer recieved:", offer);
		userIdCall = user;
		peerCon.setRemoteDescription(new RTCSessionDescription(offer));

		//create an answer to an offer
		peerCon.createAnswer(function (answer) {
			peerCon.setLocalDescription(answer);
			ws_send({
				type:'answer',
				answer: answer,
				user:userIdCall
			});
		}, function (error) {
			alert("Error when creating an answer", error);
		});

	}
	//when we got an answer from a remote user
	function handleAnswer(answer) {
		peerCon.setRemoteDescription(new RTCSessionDescription(answer));
	}
	//when we got an ice candidate from a remote user
	function handleCandidate(candidate) {
		peerCon.addIceCandidate(new RTCIceCandidate(candidate));
	}
	//
	function handleReady() {
		ws_send({
			type:'ready'
		});
	}
	//
	function handleClose() {
		ws_send({
			type:'leave',
			user:userIdCall
		});
		handleLeave();
	}
	//
	function handleCallStart() {
		handleStatus(4);
	}
	//
	function handleLeave() {
		//
		audioExternal.src = null;
		//
		peerCon.close();
		peerCon.onicecandidate = null;
		peerCon.onaddstream = null;
		//
		if ( cData.mode === 'adm' ) {
			setTimeout(handleReady, 5000);
		} else {
			plugElem.se_hide();
			ws_socket.close();
			audioLocal.stop();
		}
	}
	//
	function handleSetup(data) {
		//
		chatData.se_text(data.name);
		handleStatus(3);
		// Enviar examen
		if ( cData.mode === 'adm' ) {
			let evalResEl = $('#' + cData.evalResults);
			if ( evalResEl ) {
				evalResEl.uksUserExamInterview.readExam(data.eId, data.eKey, data.cs);
			}
		} else {
			console.log("exam_interview wrong recepient.");
		}
	}
	//
	function handleStatus(status) {
		plugElem.se_data('status', status);
		switch ( status ) {
			// Desconectado
			case 0:
				chatData.se_text('No conectado');
				plugElem.querySelector('button[se-chat-act="start"]').disabled = false;
				plugElem.querySelector('button[se-chat-act="ready"]').disabled = true;
				plugElem.querySelector('button[se-chat-act="close"]').disabled = true;
				chatTextArea.disabled = true;
				plugElem.querySelector('div.audio_elem[se-elem="local"]').se_data('status', 0);
				plugElem.querySelector('div.audio_elem[se-elem="external"]').se_data('status', 0);
				break;
			// Listo
			case 1:
				plugElem.querySelector('button[se-chat-act="start"]').disabled = true;
				plugElem.querySelector('button[se-chat-act="ready"]').disabled = false;
				plugElem.querySelector('button[se-chat-act="close"]').disabled = true;
				chatData.se_text('Preparando...');
				chatTextArea.disabled = true;
				plugElem.querySelector('div.audio_elem[se-elem="local"]').se_data('status', 1);
				plugElem.querySelector('div.audio_elem[se-elem="external"]').se_data('status', 0);
				break;
			// No answer
			case 2:
			// Conectado
			case 3:
				plugElem.querySelector('button[se-chat-act="start"]').disabled = true;
				plugElem.querySelector('button[se-chat-act="ready"]').disabled = true;
				plugElem.querySelector('button[se-chat-act="close"]').disabled = true;
				chatTextArea.disabled = true;
				plugElem.querySelector('div.audio_elem[se-elem="local"]').se_data('status', 1);
				plugElem.querySelector('div.audio_elem[se-elem="external"]').se_data('status', 0);
				break;
			// Conectado
			case 4:
				plugElem.querySelector('button[se-chat-act="start"]').disabled = true;
				plugElem.querySelector('button[se-chat-act="ready"]').disabled = true;
				plugElem.querySelector('button[se-chat-act="close"]').disabled = false;
				chatTextArea.disabled = false;
				plugElem.querySelector('div.audio_elem[se-elem="local"]').se_data('status', 2);
				plugElem.querySelector('div.audio_elem[se-elem="external"]').se_data('status', 2);
				break;
		}
	}
	//</editor-fold>

	//<editor-fold desc="Peer connection">
	//
	function pc_track(e) {
		let stream = e.stream,
			audioExternalContext, audioExternalAnalizer, audioExternalStream,
			javascriptNode;
		//
		audioExternal.srcObject = stream;
		// Analizer
		audioExternalContext = new window.AudioContext();
		audioExternalStream = audioExternalContext.createMediaStreamSource(stream);
		audioExternalAnalizer = audioExternalContext.createAnalyser();
		audioExternalAnalizer.smoothingTimeConstant = 0.5;
		audioExternalAnalizer.fftSize = 1024;
		audioExternalStream.connect(audioExternalAnalizer);
		// New method
		javascriptNode = audioExternalContext.createScriptProcessor(2048, 1, 1);
		javascriptNode.onaudioprocess = function() {
			// get the average, bincount is fftsize / 2
			let array =  new Uint8Array(audioExternalAnalizer.frequencyBinCount);
			audioExternalAnalizer.getByteFrequencyData(array);

			// Get average
			let average, sum = 0, length = array.length, i;
			for ( i = 0; i <length; i++ ) {
				sum+= array[i];
			}
			average = sum / length;
			audioExternalMeter.value = average;
		};
		audioExternalAnalizer.connect(javascriptNode);
		//
		handleCallStart();
		// audioExternal.src = window.URL.createObjectURL(e.streams[0]); METODO NUEVO, IGNORAR HASTA QUE SEA OBSOLETO
	}
	//
	function pc_onicecandidate(event) {
		if ( event.candidate ) {
			call_log("PC - ICE", event.candidate);
			//
			ws_send({
				type:"candidate",
				candidate:event.candidate,
				user:userIdCall
			});
		}
	}
	//
	function pc_open() {
		console.log("pc opened");
	}
	//
	function pc_error(err) {
		call_log("Got error", err);
	}
	//</editor-fold>

	function call_log(section, obj) {
		// let title = cData.mode.toUpperCase();
		console.log(cData.mode.toUpperCase() + " - " +section, obj);
	}

	//
	init();
	return {};
};
//
