﻿"use strict";

//
var server = {},
	nServer = require('./messenger');

//<editor-fold desc="Service Worker Ops">

//
server.onMessage = function(e) {
	let msg = e.data,
		page_server = $('#page_server'),
		page_server_field = $('#page_server_field');

	//
	console.log("MAIN - SW MESSAGE:", msg);

	//
	switch ( msg.op ) {
		//
		case 'mqtt':
			//
			if ( page_server ) {
				page_server.page_uwp_page_mqtt_message({
					'type':mType,
					'qos':message.qos,
					'retained':message.retained,
					'title':message.destinationName,
					'content':message.payloadString,
					'time':se.time.dateObjectToString(new Date())
				})
			}
			break;
		//
		case 'adv':
			pages.match.admin.timer.advanceWork(msg);
			if ( msg.notif ) {
				app.notifications.add('Match', {
					body:msg.notif,
					// icon:'icons/chrome_36.png',
					vibrate:[500, 300, 500, 300, 500]
				});
			}
			break;
		//
		case 'field':
			if ( page_server_field ) {
				page_server_field.uwp_page_server_field.serverMessageRecieve(msg.id, msg.data);
			}
			break;
		//
		default:
			console.error("Server worker unknown message", e.data);
			break;
	}

	//
	if ( page_server ) {
		page_server.uwp_page_server.sw_message({
			time:se.time.dateObjectToString(new Date()),
			content:e.data
		});
	}
};

//
server.postMessage = nServer.msgToNode;


//</editor-fold>


// Asignar callback
nServer.setCallback(server.onMessage);