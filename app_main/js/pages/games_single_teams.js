﻿"use strict";
se.plugin.uwp_page_games_single_teams = function ( plugElem, plugOptions ) {
	let gameTemplate = plugElem.querySelector('template'),
		gameContent = plugElem.querySelector('[se-elem="content"]'),
		//
		teamAddForm = plugElem.querySelector('form[se-elem="addTeam"]'),
		//
		gameData = null;

	//
	function init() {
		let params = se.url.str_parse(window.location.search),
			filter = {};

		//
		params.gId = intVal(params.gId);

		console.log("PAGE - GAME TEAMS");

		// Determinar la forma (si no es default)
		if ( params.gId ) {
			filter.index = 'gId';
			filter.keyRange = IDBKeyRange.only(params.gId);
			console.log("filter by games");

			//
			app.idb.query('games', {
				get:intVal(params.gId),
				onSuccess:(data) => {
					//
					gameData = data;

					//
					let navigation = plugElem.querySelector('div[se-elem="navigation"][data-mode="teams_game"]');
					//
					navigation.se_attr('aria-hidden', 'false');
					se.uwp.elementTemplateProc(navigation, {
						gId:params.gId,
						gTitle:data.title
					});
				}
			});

			//
			let queries = [
				{
					rName:'game_teams',
					table:'game_teams',
					params: {
						index:'gId',
						keyRange:IDBKeyRange.only(params.gId)
					}
				},
				{
					rName:'game_matches',
					table:'game_matches',
					params: {
						index:'gId',
						keyRange:IDBKeyRange.only(params.gId)
					}
				},
			];

			// Generar json data
			app.idb.queryBatch(queries, {
				onSuccess:(results) => {
					console.log("Query Batch Results", results);

					// Hacer sumas, filtrar e imprimir
					let cGroupedResults = {};
					if ( results.game_teams ) {
						for ( let i = 0; i < results.game_teams.length; i++ ) {
							let cTeam = results.game_teams[i];
							cGroupedResults[cTeam.index.toString()] = {
								'seen': 0,
								'title': cTeam.title,
								'index': cTeam.index,
								'id': cTeam.id,
								'gId': cTeam.gId,
								'place': cTeam.place,
								'points': 0,
								'gW': 0,
								'gD': 0,
								'gL': 0,
								'gT': 0,
								'other': 0
							};
						}
					}

					//
					console.log(cGroupedResults);

					//
					if ( results.game_matches ) {
						for ( let i = 0; i < results.game_matches.length; i++ ) {
							let cMatch = results.game_matches[i],
								cTeam_1 = cGroupedResults[cMatch.t1_index.toString()],
								cTeam_2 = cGroupedResults[cMatch.t2_index.toString()];
							//
							cTeam_1.seen++;
							cTeam_2.seen++;
							cTeam_1.points += cMatch.t1_points;
							cTeam_2.points += cMatch.t2_points;
							//
							switch ( cMatch.t1_status ) {
								case 0:
									break;
								case 1:
									cTeam_1.gL++;
									break;
								case 2:
									cTeam_1.gD++;
									break;
								case 3:
									cTeam_1.gW++;
									break;
							}
							//
							switch ( cMatch.t2_status ) {
								case 0:
									break;
								case 1:
									cTeam_2.gL++;
									break;
								case 2:
									cTeam_2.gD++;
									break;
								case 3:
									cTeam_2.gW++;
									break;
							}

						}
					}

					//
					console.log("Populados", cGroupedResults);
					//
					let i = cGroupedResults.length;
					while ( i-- ) {
						if ( cGroupedResults[i].seen === 0 ) {
							cGroupedResults.splice(i, 1);
						}
					}

					//
					console.log("final", cGroupedResults);

					//
					gameContent.se_empty();
					gameContent.se_append(se.struct.stringPopulateMany(gameTemplate.se_html(), cGroupedResults));
				},
				onError:(err) => {
					console.log("error", err);
				}
			});
		} else {
			plugElem.querySelector('div[se-elem="navigation"][data-mode="teams_all"]').se_attr('aria-hidden', 'false');

			app.idb.query('game_teams', se.object.merge(filter, {
				onSuccess:(data) => {
					gameContent.se_append(se.struct.stringPopulateMany(gameTemplate.se_html(), data));
				}
			}));
		}

		//
		gameContent.se_on('change', 'input', teamInputUpdate);
		teamAddForm.se_on('submit', teamAddSubmit);
	}

	// Function edit team
	function teamInputUpdate(e, cEl) {
		e.preventDefault();
		switch ( cEl.name ) {
			//
			case 'place':
				let id = intVal(cEl.se_closest('tr').se_data('id')),
					updateData = {sync:1, place:intVal(cEl.value)};
				app.idb.update('game_teams', id, updateData, {
					onSuccess:(data) => {
						console.log("Team place updated");
					}
				});
				break;
		}
	}

	//
	function teamAddSubmit(e) {
		e.preventDefault();


		let teamRegInfo = {
			id:0,
			gId:gameData.id,
			title:teamAddForm.se_formElVal('title'),
			place:0,
			gTitle:gameData.title,
			sync:1,
			players_index:[],
			players_list:[],
		};
		// Operation
		app.idb.add('game_teams', teamRegInfo, {
			onSuccess:(e) => {
				let index = e.target.result;

				teamAddForm.reset();

				// Agregar a la tabla
				gameContent.se_append(se.struct.stringPopulate(gameTemplate.se_html(), se.object.merge(teamRegInfo, {
					index:index,
					seen:0,
					gW:0,
					gD:0,
					gL:0,
					points:0
				})));
			}
		});
	}

	//
	init();
	return {};
};
