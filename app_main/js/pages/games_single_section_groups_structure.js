﻿"use strict";
se.plugin.uwp_page_games_single_section_groups_structure = function ( plugElem, plugOptions ) {
	//
	const
		groupTemplate = plugElem.querySelector('template[se-elem="groupObject"]'),
		elementTemplate = plugElem.querySelector('template[se-elem="groupElement"]'),
		fieldTemplate = plugElem.querySelector('template[se-elem="groupField"]'),
		//
		groupsContainer = plugElem.querySelector('div[se-elem="groupList"]'),
		elementsContainer = plugElem.querySelector('div[se-elem="elementList"]'),
		fieldsContainer = plugElem.querySelector('div[se-elem="fieldList"]');

	//
	function init() {
		let params = se.url.str_parse(window.location.search);

		//
		params.gId = intVal(params.gId);
		params.gsId = intVal(params.gsId);

		//
		let queries = [
			{
				rName: 'games',
				table: 'games',
				params: {
					get:params.gId,
				}
			},
			{
				rName: 'game_stages',
				table: 'game_stages',
				params: {
					get:params.gsId
				}
			},
			{
				rName: 'game_groups',
				table: 'game_groups',
				params: {
					index:'gsId',
					keyRange:IDBKeyRange.only(params.gsId)
				}
			},
			{
				rName: 'game_teams',
				table: 'game_teams',
				params: {
					index:'gId',
					keyRange:IDBKeyRange.only(params.gId)
				}
			},
			{
				rName: 'event_fields',
				table: 'event_fields',
				params: {
				}
			},
		];

		// Generar json data
		app.idb.queryBatch(queries, {
			onSuccess: (results) => {
				console.log("PAGE: DATA LOADED", results);

				// Update Page
				let navigation = plugElem.querySelector('div[se-elem="navigation"]');
				se.uwp.elementTemplateProc(navigation, {
					gId:params.gId,
					gsId:params.gsId,
					gTitle:results['games'].title,
					cTitle:results['game_stages'].title,
				});

				// Groups
				for ( let cGroup of results['game_groups'] ) {
					groupsContainer.se_append(se.struct.stringPopulate(groupTemplate.se_html(), cGroup));
				}
				// Elements
				for ( let cElement of results['game_teams'] ) {
					elementsContainer.se_append(se.struct.stringPopulate(elementTemplate.se_html(), cElement));
				}
				// Fields
				for ( let cField of results['event_fields'] ) {
					fieldsContainer.se_append(se.struct.stringPopulate(fieldTemplate.se_html(), cField));
				}

				// Teams in groups
			}
		});
	}

	//
	init();
	return {};
};
