﻿"use strict";
se.plugin.uwp_page_games = function ( plugElem, plugOptions ) {
	let gameTemplate = plugElem.querySelector('template'),
		gameContent = plugElem.querySelector('[se-elem="content"]'),
		//
		temp2 = '';

	//
	function init() {
		//
		gameContent.se_empty();
		app.idb.query('games', {
			onSuccess:(data) => {
				gameContent.se_append(se.struct.stringPopulateMany(gameTemplate.se_html(), data));
			}
		});
	}

	//
	init();
	return {};
};
