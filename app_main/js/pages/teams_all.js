﻿"use strict";
se.plugin.uwp_page_teams_all = function ( plugElem, plugOptions ) {
	let gameTemplate = plugElem.querySelector('template'),
		gameContent = plugElem.querySelector('[se-elem="content"]'),
		//
		temp2 = '';

	//
	function init() {
		let params = se.url.str_parse(window.location.search);

		console.log("PAGE - EVENT TEAMS");

		//
		gameContent.se_empty();
		app.idb.query('game_teams', {
			onSuccess:(data) => {
				gameContent.se_append(se.struct.stringPopulateMany(gameTemplate.se_html(), data));
			}
		});
	}

	//
	init();
	return {};
};
