﻿"use strict";
se.plugin.uwp_page_default = function (plugElem, plugOptions) {
	let elemConnected = plugElem.querySelector('div[se-elem="connected"]'),
		elemDisConnected = plugElem.querySelector('div[se-elem="disconnected"]'),
		infoTable = elemConnected.querySelector('table[se-elem="info"]'),
		keyForm = plugElem.querySelector('form[se-elem="accessKey"]'),
		keyFormResponse = keyForm.querySelector('output[se-elem="response"]'),
		syncButton = plugElem.querySelector('button[se-act="syncGamesFull"]')
	;

	//
    function init() {
		console.log( "Default page", appState.settings );

		updateStatus(false);

		//
	    keyForm.se_formElVal( 'devServer', appState.settings.devServer );
	    keyForm.se_formElVal( 'devServer', appState.settings.devServer );

	    //
	    plugElem.se_on( 'click', 'button[se-act]', btnActions );

	    //
	    keyForm.se_on('submit', connectionTest);
	}

	//
	function unloadApp() {
    	// Reset appstate
		appState = {
			loaded:false,
			settings:{
				setup:false,
				online:false,
				connected:false,
				key:'',
				devServer:appState.settings.devServer,
				mqttLocal:appState.settings.mqttLocal,
				server:null,
				notifications:false
			},
			eventData:{
				id:0,
				title:'',
				pTitle:'',
				dtStart:'',
				dtEnd:'',
				dtStartUnix:'',
				dtEndUnix:''
			}
		};
		// Reiniciar DB
		app.idb.clearTables();

		// Cambiar estilo
		app.stateUpdate();
		updateStatus(true);
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		//
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'unload':
				if ( confirm("¿Desconectar la aplicación?\nTodos los datos serán eliminados.") ) {
					unloadApp();
				}
				break;
			//
			case 'reset':
				if ( confirm("¿Desconectar la aplicación?\nTodos los datos serán eliminados.") ) {
					appReset();
				}
				break;
			//
			case 'syncRegistrations':
				syncRegistrations();
				break;
			//
			case 'syncGamesFull':
				syncGamesFull();
				break;
			//
			default:
				console.log("Boton no programado: ", cAction, cBtn);
				break;
		}
	}

	//
	function appReset() {

	}

	//
	function updateStatus(sendMessage = false) {
    	if ( appState.loaded ) {
    		// Ajustar los textos
		    infoTable.querySelector('td[se-elem="id"]').se_text(appState.eventData.id);
		    infoTable.querySelector('td[se-elem="title"]').se_text(appState.eventData.title);
		    infoTable.querySelector('td[se-elem="pTitle"]').se_text(appState.eventData.pTitle);
		    infoTable.querySelector('td[se-elem="dtStart"]').se_text(appState.eventData.dtStart);
		    infoTable.querySelector('td[se-elem="dtEnd"]').se_text(appState.eventData.dtEnd);
    		//
    		elemConnected.se_show();
    		elemDisConnected.se_hide();
		    // Notificarle al worker
		    if ( sendMessage ) {
			    server.postMessage({
				    'op_type':['appChange', 'connect'],
				    'op_data': {
					    'mqttLocal': appState.settings.mqttLocal
				    }
			    });
		    }
	    } else {
		    elemConnected.se_hide();
		    elemDisConnected.se_show();
		    // Notificarle al worker
		    if ( sendMessage ) {
			    server.postMessage({
				    'op_type':['appChange'],
				    'op_current': 'disconnect'
			    });
		    }
	    }


	}

	//
	function connectionTest(e) {
    	e.preventDefault();

		// Form data
    	let cFormData = se.form.serializeToObj(keyForm);

		console.log("setting form data", appState);

		// Initial conditions read from server
		appState.settings.key = cFormData.eKey;
		appState.settings.devServer = cFormData.devServer;
		appState.settings.mqttLocal = cFormData.mqttLocal;
		appState.settings.server = ( cFormData.devServer ) ? appVariables.server.dev : appVariables.server.live;

		//
		app.request(
			'/setup_download',
			{},
			{
				response: keyFormResponse,
				onSuccess: ( msg ) => {
					appState.loaded = true;
					//
					se.struct.notifAdvanced( keyFormResponse, 'notification success', 'Conexión aprobada', '', 'fa-check' );
					console.log(msg);
					//
					appState.eventData = msg.d.obj;

					// Actualizar aplicación
					app.stateUpdate();
					updateStatus(true);

					//
					app.idb.syncData(msg.d.db);
				},
				onFail: () => {
					se.struct.notifAdvanced( keyFormResponse, 'notification alert', 'Formato incorrecto', 'No parece contener el formato correcto para continuar.', 'fa-remove' );
				},
				onComplete: () => {
					// cBtn.disabled = false;
				},
				onRequest: () => {
					se.struct.notifAdvanced( keyFormResponse, 'notification', 'Enviando solicitud', '', 'fa-spinner', 'spin' );
					// cBtn.disabled = true;
				},
				onProgress: (x) => {
					console.log( "progress", x);
				}
			}
		);
	}

	//
	function syncRegistrations() {
		//
		app.request( '/registration/readAll', {}, {
			onSuccess: ( msg ) => {
				// Seleccionar tabla
				console.log("elementos", msg.d);
				app.idb.rebuild( 'registrations', msg.d, {
					onComplete: function () {
						console.log( "regs synced" );
					}
				} );
			},
			onRequest:() => {

			},
			onFail:() => {

			}
		} );
	}

	//
	function syncGamesFull() {
    	let queries = [
		    {
			    rName:'event_players',
			    table:'event_players',
			    params: {}
		    },
		    {
			    rName:'game_teams',
			    table:'game_teams',
			    params: {
				    index:'sync',
				    keyRange:IDBKeyRange.only(1)
			    }
		    },
		    {
			    rName:'game_matches',
			    table:'game_matches',
			    params: {
				    index:'sync',
				    keyRange:IDBKeyRange.only(1)
			    }
		    }
	    ];

    	// Generar json data
		app.idb.queryBatch(queries, {
			onSuccess:(results) => {
				console.log("Query Batch Results", results);

				// Procesar jugadores
				if ( results.event_players ) {
					let temp_eplayers = [],
						fullEventPlayers = {};
					//
					for ( let ePlayer of results.event_players ) {
						// Guardar como actualización a los que requieran cambios
						if ( ePlayer.sync === 1 ) {
							temp_eplayers.push({
								index: ePlayer.index,
								id: ePlayer.id,
								lregId: ePlayer.lregId,
								eventId: ePlayer.eventId,
								uName: ePlayer.uName
							});
						}
						// Guardar todos como index / id
						let cIndex = "" + ePlayer.index;
						fullEventPlayers[cIndex] = {
							eurId:ePlayer.id,
							lregId:ePlayer.lregId,
							title:ePlayer.uName
						};
					}
					//
					results.event_players = temp_eplayers;
					results.event_players_full = fullEventPlayers;
				}

				//
				if ( results.game_teams ) {
					let temp_teams = [];
					//
					for ( let cTeam of results.game_teams ) {
						// Guardar como actualización a los que requieran cambios
						temp_teams.push({
							index: cTeam.index,
							id: cTeam.id,
							gId: cTeam.gId,
							title: cTeam.title,
							place: cTeam.place,
							players_list: cTeam.players_list
						});
					}
					//
					results.game_teams = temp_teams;
				}

				//
				//
				if ( results.game_matches ) {
					let temp_match = [];
					//
					for ( let cMatch of results.game_matches ) {
						// Guardar como actualización a los que requieran cambios
						temp_match.push({
							index:cMatch.index,
							id:cMatch.id,
							gId:cMatch.gId,
							mStatus:cMatch.mStatus,
							gTimeUnix:cMatch.gTimeUnix,
							t1_id:cMatch.t1_id,
							t1_side:cMatch.t1_side,
							t1_status:cMatch.t1_status,
							t1_points:cMatch.t1_points,
							t2_id:cMatch.t2_id,
							t2_side:cMatch.t2_side,
							t2_status:cMatch.t2_status,
							t2_points:cMatch.t2_points,
							mPoints:cMatch.mPoints,
							mEvents:cMatch.mEvents,
							fId:cMatch.fId,
							ggId:cMatch.ggId,
							gsId:cMatch.gsId,
							teams:cMatch.game_teams
						});
					}
					//
					results.game_matches = temp_match;
				}

				//
				console.log("Attemp to sync: ", results);
				let resultTxt = JSON.stringify(results);
				//
				app.request('/sync', { data:resultTxt }, {
					onSuccess:(msg) => {
						syncButton.querySelector('svg.icon use').se_attr('xlink:href', '#fa-check');
						let cRow;

						// Agregar jugadores de temporada
						if ( msg.d.season_players ) {
							//
							app.idb.put('season_players', msg.d.season_players, {
								onSuccess: () => {
									console.log("Jugadores de evento actualizados.");
								},
								onError: (err) => {
									console.log("Jugadores no actualizados", err);
								},
							});
						}

						// Update event players
						if ( results.event_players && msg.d.event_players ) {
							//
							app.idb.updateMany('event_players', 'index', msg.d.event_players, {
								onSuccess: () => {
									console.log("Jugadores de evento actualizados.");
								},
								onError: (err) => {
									console.log("Jugadores no actualizados", err);
								},
							});
						}

						// Update event teams
						if ( results.game_teams && msg.d.game_teams ) {
							//
							app.idb.updateMany('game_teams', 'index', msg.d.game_teams, {
								onSuccess: () => {
									console.log("Equipos de evento actualizados.");
								},
								onError: (err) => {
									console.log("Equipos no actualizados", err);
								},
							});
						}

						// Update matches
						if ( results.game_matches && msg.d.game_matches ) {
							//
							app.idb.updateMany('game_matches', 'index', msg.d.game_matches, {
								onSuccess: () => {
									console.log("matches locales actualizados");
								},
								onError: (err) => {
									console.log("Matches no actualizados", err);
								},
							});
						}
					},
					onFail:(msg) => {
						//
						console.log("DB Query (delete)", msg);
						syncButton.querySelector('svg.icon use').se_attr('xlink:href', '#fa-remove');
					},
					onRequest:() => {
						syncButton.querySelector('svg.icon').se_classAdd('spin');
					},
					onComplete:() => {
						syncButton.querySelector('svg.icon').se_classDel('spin');
					},
				});
			}
		});
	}

	//
    init();
    return {};
};

