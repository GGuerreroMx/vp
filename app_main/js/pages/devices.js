﻿"use strict";
se.plugin.uwp_page_devices = function ( plugElem, plugOptions ) {
	let devicesTemplate = plugElem.querySelector('template'),
		devicesContent = plugElem.querySelector('[se-elem="content"]'),
		//
		editDeviceDiag = plugElem.querySelector('dialog[se-elem="editDevice"]'),
		editDeviceForm = editDeviceDiag.querySelector('form'),
		inputTypeSetForm = plugElem.querySelector('form[se-elem="inputTypeSet"]'),
		//
		devicesCont = plugElem.querySelector('div[se-elem="devices"]'),
		deviceLog = plugElem.querySelector('div[se-elem="deviceLog"]'),
		deviceSendForm = plugElem.querySelector('form[se-elem="serialInput"]'),
		//
		serialPort, serialReader, serialInputDone, serialOutputDone, serialInputStream, serialOutputStream, serialLineBuffer, serialCommandBuffer, serialCapturing;

	//
	function init() {
		let params = se.url.str_parse(window.location.search);
		//
		devicesContent.se_empty();
		app.idb.query('server_devices', {
			onSuccess:(data) => {
				let template = devicesTemplate.se_html();
				for ( let cRow of data ) {
					device_toTable(cRow);
					devicesContent.se_append(se.struct.stringPopulate(template, cRow));
				}
			}
		});

		// Callbacks
		plugElem.se_on( 'click', 'button[se-act]', btnActions );
		editDeviceForm.se_on('submit', deviceUpdate);
		deviceSendForm.se_on('submit', serialSend);
		inputTypeSetForm.se_on('submit', deviceTypeSubmit);
	}

	//
	function devices_reset() {
		// Confirmation
		if ( !confirm('¿Reiniciar dispositivos?') ) {
			return;
		}

		// Operation
		app.idb.clearTable(
			'server_devices',
			() => {
				console.log("devices deleted???");
				devicesContent.se_empty();
			}
		);
	}

	//
	async function btnActions( e, cBtn ) {
		e.preventDefault();
		//
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'bluetooth_connect':
				await btConnect();
				break;
			//
			case 'bluetooth_disconnect':
				await bluetooth_disconnect();
				break;
			//
			case 'serial_connect':
				if ( serialPort ) {
					await serial_disconnect();
				}
				await serial_connect();
				break;
			//
			case 'serial_sync_first':
				serial_sync_first();
				break;
			//
			case 'serial_sync_props':
				serial_sync_props();
				break;
			//
			case 'serial_test_beep':
				serial_test_beep();
				break;
			//
			case 'serial_test_audio':
				serial_test_audio();
				break;
			//
			case 'serial_disconnect':
				await serial_disconnect();
				break;
			//
			case 'device_edit':
				let id = intVal(cBtn.se_closest('tr').se_data('id'));
				deviceEdit(id);
				break;
			//
			case 'log_clear':
				deviceLog.innerHTML = '';
				break;
			//
			case 'eventUpdCancel':
				editDeviceDiag.close();
				break;
			//
			case 'devices_reset':
				devices_reset();
				break;

			//
			default:
				console.log("Boton no programado: ", cBtn.se_attr( 'se-act' ), cBtn);
				break;
		}
	}

	async function deviceTypeSubmit(e) {
		e.preventDefault();
		let cFormData = se.form.serializeToObj(inputTypeSetForm);

		if ( devicesCont.se_data('mode') === 'bluetooth' ) {
			bluetoothSetType(cFormData);
		} else {
			serialSetType(cFormData);
		}
	}


	//
	async function btConnect() {
		await appVariables.bluetooth.connect(btOnConnect, btOnDisConnect);
	}

	async function bluetooth_disconnect() {
		await appVariables.bluetooth.disconnect();

		devicesCont.se_data('connected', 0);
		deviceLog.se_empty();
	}

	async function btOnConnect() {
		// ui
		devicesCont.se_data('connected', 1);
		devicesCont.se_data('mode', 'bluetooth');

		//
		btSync();
	}

	async function btOnDisConnect() {

		// ui
		devicesCont.se_data('connected', 0);
		deviceLog.se_empty();
	}

	//
	async function btSync() {
		// Read
		const cValue = await appVariables.bluetooth.characteristicRead('DEVICE');

		let cValueArr = Array.from(new Uint8Array(cValue));

		console.warn("BT Device information ini", cValue, cValueArr, cValue.getUint16(0), cValue.getUint16(2));

		//
		const devId = cValue.getUint16(0);
		const devPw = cValue.getUint16(2);

		console.warn("BT DEVICE INFORMATION: ", devId, devPw);

		const tDevice = devicesContent.querySelector('[data-id="'+ devId +'"]');

		// Check if exists
		if ( tDevice && parseInt(tDevice.se_data('pw')) === devPw ) {
			console.log("device is ready, ignore");
			return;
		}

		let deviceInfo = {
			pw:Math.floor(Math.random() * 65535) + 1,
			devMode:0,
			devType:0,
			fieldId:0,
			fieldDevId:0
		};

		//
		app.idb.add('server_devices', deviceInfo, {
			onSuccess:(e) => {
				let index = e.target.result,
					data = se.object.merge(deviceInfo, {id:index});

				// Agregar a la tabla
				devicesContent.se_append(
					se.struct.stringPopulate(
						devicesTemplate.se_html(),
						data
					)
				);


				let tempAppState = localStorage.getItem( 'IoT' ),
					iotData = null;
				//
				if ( !tempAppState ) {
					alert("Initial IOT setup incomplete.");
					return;
				}

				//
				iotData = JSON.parse( tempAppState );

				let cData = [
					index,
					deviceInfo.pw,
					iotData.wifiSSID,
					iotData.wifiPass,
					iotData.hostAddress,
					iotData.serverPort
				];

				//
				let nValue = cData.join(';');

				console.log("SENGING TO BLUETOOTH:", nValue);

				//
				let enc = new TextEncoder(), // always utf-8
					cArray = enc.encode(nValue);

				//
				appVariables.bluetooth.characteristicWrite('DEVICE', cArray);
			}
		});

	}

	//
	function bluetoothSetType(cFormData) {
		let cData = [
			cFormData.fieldId,
			cFormData.deviceTotal,
			cFormData.statusTotal,
			//
			cFormData.conTypePrimary,
			cFormData.conTypeSecondary,
			//
			cFormData.devMode,
			cFormData.fieldDevId,
			cFormData.defSide,
			cFormData.devType,
		];

		//
		let nValue = cData.join(';');

		console.log("SENGING TO BLUETOOTH:", nValue);

		//
		let enc = new TextEncoder(), // always utf-8
			cArray = enc.encode(nValue);

		//
		appVariables.bluetooth.characteristicWrite('GAME', cArray);
	}

	//
	function smallIntToByteArray(long) {
		// we want to represent the input as a 8-bytes char array
		let byteArray = [0, 0];

		for ( let index = 0; index < byteArray.length; index++ ) {
			let byte = long & 0xff;
			byteArray [index] = byte;
			long = (long - byte) / 256;
		}

		return byteArray;
	}

	//
	function byteArrayToSmallInt(/*byte[]*/byteArray) {
		let value = 0;
		for ( let i = byteArray.length - 1; i >= 0; i-- ) {
			value = (value * 256) + byteArray[i];
		}

		return value;
	}


	//
	function deviceEdit(devId) {

		//
		app.idb.query('server_devices', {
			get:devId,
			onSuccess:(data) => {
				//
				se.form.loadData(editDeviceForm, data);
				editDeviceDiag.showModal();
			},
			onEmpty() {
				serial_setNew();
			}
		});
	}

	//
	function deviceUpdate(e) {
		e.preventDefault();
		let data = se.form.serializeToObj(editDeviceForm, false, {'int':['id', 'pw', 'devMode', 'devType', 'fieldId', 'fieldDevid']});

		//
		app.idb.put('server_devices', data, {
			onSuccess:() => {
				if ( data.id === serialDeviceSelId ) {
					//
					let commands = ['setup', 'mode', data.id, data.pw, data.devMode, data.devType, data.fieldId, data.fieldDevId];
					serial_writeToStream_array(commands);
					device_curent_set(data.id);
				}
				editDeviceDiag.close();
			},
			onError:(err) => {
				console.error("No fue posible actualizar el dispositivo.");
			},
		});
	}

	// https://codelabs.developers.google.com/codelabs/web-serial#3

	//<editor-fold desc="Serial operations">
	async function serial_connect() {
		// Filter only target devices
		const filters = [
			{ usbVendorId: 0x10C4, usbProductId: 0xEA60 }
		];

		// Actual connection
		serialPort = await navigator.serial.requestPort({ filters });
		await serialPort.open({ baudRate: 115200, dataBits:8, stopBits:1, parity:'none' });

		// Read setup
		let decoder = new TextDecoderStream();
		serialInputDone = serialPort.readable.pipeTo(decoder.writable);
		serialInputStream = decoder.readable;

		serialReader = serialInputStream.getReader();
		serial_readLoop();

		// Write setup
		const encoder = new TextEncoderStream();
		serialOutputDone = encoder.readable.pipeTo(serialPort.writable);
		serialOutputStream = encoder.writable;

		//
		console.log("Devices connected");

		//
		devicesCont.se_data('connected', 1);
		devicesCont.se_data('mode', 'serial');
	}

	//
	async function serial_readLoop() {
		// Loop foreva
		while ( true ) {
			const { value, done } = await serialReader.read();

			//
			if ( value ) {
				// Direct logging
				deviceLog.textContent += value;

				serialLineBuffer+= value;

				//
				let cIndexOfNewLine = serialLineBuffer.indexOf('\n');
				if ( !serialCapturing ) {
					// Not capturing, clean new lines
					if ( cIndexOfNewLine !== -1 ) {
						serialLineBuffer = serialLineBuffer.slice(cIndexOfNewLine + 1);
					}

					// Check if begin capture
					let cIndexOfNewCommand = serialLineBuffer.indexOf('<!');
					if ( cIndexOfNewCommand !== -1 ) {
						serialLineBuffer = serialLineBuffer.slice(cIndexOfNewCommand);
						serialCapturing = true;
					}

					//
				}

				if ( serialCapturing ) {
					// has command?
					let found = serialLineBuffer.match(/<!([^\/>]+)\/>/);
					if ( found ) {
						serial_recievedCommand(found[1]);
						// clean linebuffer
						serialLineBuffer = serialLineBuffer.slice(serialLineBuffer.indexOf('/>') + 2);
					}
				}

				//
				deviceLog.scrollTop = deviceLog.scrollHeight
			}

			//
			if ( done ) {
				deviceLog.textContent += "End of stream.\n";
				console.log('[readLoop] DONE', done);
				serialReader.releaseLock();
				break;
			}
		}
	}

	//
	function serial_writeToStream(...lines) {
		const writer = serialOutputStream.getWriter();
		lines.forEach((line) => {
			writer.write(line + '\n');
		});
		writer.releaseLock();
	}

	//
	function serial_writeToStream_array(cmds) {
		serial_writeToStream(cmds.join(';') + '\n');
	}

	//
	async function serial_disconnect() {
		// Close reader
		if ( serialReader ) {
			await serialReader.cancel();
			await serialInputDone.catch(() => {});
			serialReader = null;
			serialInputDone = null;
		}

		// close output
		if ( serialOutputStream ) {
			await serialOutputStream.getWriter().close();
			await serialOutputDone;
			serialOutputStream = null;
			serialOutputDone = null;
		}

		// close port
		await serialPort.close();
		serialPort = null;

		// ui
		devicesCont.se_data('connected', 0);
		deviceLog.se_empty();
	}

	//
	function serial_sync_props() {
		console.log("Sending iotProperty");
		//
		app.idb.query('server_devices', {
			get:serialDeviceSelId,
			onSuccess:(data) => {
				let commands = ['setup', 'mode', data.id, data.pw, data.devMode, data.devType, data.fieldId, data.fieldDevId];
				serial_writeToStream_array(commands);
			},
			onEmpty() {
				console.error("no se encontró el dispositiovo... why?");
			}
		});
	}

	//
	function serial_sync_first() {
		// Send first sync information
		serial_writeToStream_array(['setup', 'init']);
	}

	//
	function serial_recievedCommand(msg) {
		let cCommandOps = msg.split(";"),
			commands = [];
		//
		console.log("SERIAL COMMAND: ", msg, cCommandOps);

		//
		switch ( cCommandOps[0] ) {
			//
			case 'ready':
				serial_writeToStream_array(['setup', 'init']);
				break;
			//
			case 'devstatus':
				console.log("DEVICE - PAGE - Checking status");
				const paramList = cCommandOps[1].split('|');
				console.log("PARAM STATUS", paramList);
				let dID = intVal(paramList[0]), dPW = intVal(paramList[1]);

				//
				if ( dID === 0 ) {
					console.log("DEVICE - NOT SET, ADD");
					serial_setNew();
					return;
				}

				//
				app.idb.query('server_devices', {
					get:dID,
					onSuccess:(data) => {
						if ( data.pw === dPW ) {
							// Get data
							let deviceConfig = JSON.parse(localStorage.getItem( 'IoT' ));

							console.log(
								"comparison",
								( intVal(paramList[2]) !== ( deviceConfig.wifiEnabled ? 1 : 0) ),
								( paramList[3] !== deviceConfig.wifiSSID ),
								( paramList[4] !== deviceConfig.wifiPass ),
								( paramList[5] !== deviceConfig.hostAddress ),
								( intVal(paramList[6]) !== deviceConfig.serverPort ),
								( intVal(paramList[7]) !== ( deviceConfig.loraEnabled ? 1 : 0) )
							);


							// compare data for update
							if (
								intVal(paramList[2]) !== ( deviceConfig.wifiEnabled ? 1 : 0 ) ||
								paramList[3] !== deviceConfig.wifiSSID ||
								paramList[4] !== deviceConfig.wifiPass ||
								paramList[5] !== deviceConfig.hostAddress ||
								intVal(paramList[6]) !== deviceConfig.serverPort ||
								intVal(paramList[7]) !== ( deviceConfig.loraEnabled ? 1 : 0 )
							) {
								// Update required
								commands = [
									'setup', 'update',
									deviceConfig.wifiEnabled ? 1 : 0, deviceConfig.wifiSSID, deviceConfig.wifiPass,
									deviceConfig.hostAddress, deviceConfig.serverPort,
									deviceConfig.loraEnabled ? 1 : 0
								];
								serial_writeToStream_array(commands);
							}

							// Define current object
							device_curent_set(data.id);
						} else {
							console.log("DEVICE - DEVICE NOT FOUND, ADD", dID, dPW);
							serial_setNew();
						}
					},
					onEmpty() {
						console.log("DEVICE - DEVICE NOT FOUND, ADD", dID, dPW);
						serial_setNew();
					}
				});
				break;
			//
			case 'gamestatus':
				break;
			//
			case 'error':
				console.error(cCommandOps);
				break;
			//
			case 'log':
				console.log("logging", cCommandOps);
				break;
			//
			default:
				console.error("Mensaje no reconocido: ", msg, cCommandOps);
				break;
		}
	}


	//
	function serialSetType(cFormData) {
		let commands = [
			'setup', 'device',
			intVal(cFormData.devMode), intVal(cFormData.devType),
			intVal(cFormData.fieldId), intVal(cFormData.fieldDevId),
			intVal(cFormData.deviceTotal), intVal(cFormData.statusTotal),
			intVal(cFormData.conTypePrimary), intVal(cFormData.conTypeSecondary),
		];
		serial_writeToStream_array(commands);
	}

	//
	function serialSend(e) {
		e.preventDefault();
		let string = deviceSendForm.se_formElVal('content');
		serial_writeToStream(string);
		deviceSendForm.reset();
	}

	//
	function serial_test_beep() {
		let commands = ['debug', 'beep', 0];
		serial_writeToStream_array(commands);
	}

	//
	function serial_test_audio() {
		let commands = ['debug', 'sound', 0, 1];
		serial_writeToStream_array(commands);
	}

	//</editor-fold>

	//
	function device_curent_set(index) {
		//
		plugElem.querySelector('td[se-elem="serial_id"]').se_text(index);
		// Marcar como seleccionado en la lista control
		devicesContent.querySelectorAll('tr').se_classDel('sel');
		devicesContent.querySelector('tr[data-id="' + index + '"]').se_classAdd('sel');
	}

	//
	function device_toTable(cData) {
		return json_replace(cData, {
			'replace':{
				'devMode':{
					'0':'Neutral',
					'1':'Device',
					'2':'LoraWan'
				},
				'devType':{
					'0':'Not Set',
					'1':'Homebase',
					'2':'opflag',
					'3':'swflag',
					'4':'ctflag',
					'5':'bombsite',
					'6':'bombomb',
					'7':'swflag'
				},
			}
		});
	}

	//
	function json_replace(cJson, cData) {
		for ( let cDEl in cData ) {
			if ( !cData.hasOwnProperty(cDEl) ) { continue; }
			let cDElParams = cData[cDEl];
			//
			switch ( cDEl ) {
				//
				case 'replace':
					for ( let cName in cDElParams ) {
						if ( !cDElParams.hasOwnProperty(cName) ) { continue; }
						let cList = cDElParams[cName];
						for ( let cVal in cList ) {
							if ( !cList.hasOwnProperty(cVal) ) { continue; }
							//
							if ( cJson.hasOwnProperty(cName) && cList.hasOwnProperty(cJson[cName]) ) {
								cJson[cName] = cList[cJson[cName]];
							}
						}
					}
					break;
				//
				default:
					console.error("Caso no programado.");
					break;
			}
		}
	}

	//
	function serial_setNew() {
		let deviceInfo = {
			pw:Math.floor(Math.random() * 65535) + 1,
			devMode:0,
			devType:0,
			fieldId:0,
			fieldDevId:0
		};

		// Operation
		app.idb.add('server_devices', deviceInfo, {
			onSuccess:(e) => {
				let index = e.target.result,
					data = se.object.merge(deviceInfo, {id:index});

				//
				device_toTable(data);
				console.log("datos a guardar", e, data);

				// Agregar a la tabla
				devicesContent.se_append(
					se.struct.stringPopulate(
						devicesTemplate.se_html(),
						data
					)
				);

				let deviceConfig = JSON.parse(localStorage.getItem( 'IoT' ));

				//
				let commands = [
					'setup', 'initial',
					data.id, data.pw,
					deviceConfig.wifiEnabled ? 1 : 0, deviceConfig.wifiSSID, deviceConfig.wifiPass,
					deviceConfig.hostAddress, deviceConfig.serverPort,
					deviceConfig.loraEnabled ? 1 : 0
				];
				serial_writeToStream_array(commands);

				//
				device_curent_set(index);
			}
		});
	}

	//
	init();
	return {};
};
