﻿"use strict";
se.plugin.uwp_page_server = function ( plugElem, plugOptions ) {
	const wsContainer = plugElem.querySelector('div[se-elem="wsContainer"]'),
		wsMsgTemplate = wsContainer.querySelector('template'),
		wsMsgCont = wsContainer.querySelector('[se-elem="content"]'),
		wsFilter = wsContainer.querySelector('[se-elem="messageFilter"]'),
		//
		wsAddForm = wsContainer.querySelector('form[se-elem="wsMessage"]'),
		//
		swContainer = plugElem.querySelector('div[se-elem="swContainer"]'),
		swMsgTemplate = swContainer.querySelector('template'),
		swMsgCont = swContainer.querySelector('[se-elem="content"]'),
		//
		serverFields = plugElem.querySelector('table[se-elem="server_currentFields"]'),
		serverFieldsTemplate = serverFields.querySelector('template'),
		serverFieldsContent = serverFields.querySelector('tbody[se-elem="content"]'),
		//
		serverGameAddForm = plugElem.querySelector('form[se-elem="addGame"]'),
		serverGameTypeAddForm = plugElem.querySelector('form[se-elem="addGameType"]'),
		// QR CODE
		pseudoDialog = plugElem.querySelector('dialog[se-elem="qrCode"]'),
		qrObject = pseudoDialog.querySelector('div[se-elem="qrObject"]');
	//
	let qrGenerator,
		//
		lists,
		//
		currentData,
		nodeHTTP;
	//

	//
	function init() {
		let params = se.url.str_parse(window.location.search);


		console.log( "PAGE - SERVER" );

		// Read current matches data

		//
		let queries = [
			{
				rName: 'gametypes',
				table: 'gametypes',
				params: {}
			},
			{
				rName: 'server_fields',
				table: 'server_fields',
				params: {}
			},
			{
				rName: 'games',
				table: 'games',
				params: {}
			},
			{
				rName: 'game_stages',
				table: 'game_stages',
				params: {}
			},
			{
				rName: 'game_groups',
				table: 'game_groups',
				params: {}
			},
			{
				rName: 'event_fields',
				table: 'event_fields',
				params: {}
			},
		];

		// Generar json data
		app.idb.queryBatch(queries, {
			onSuccess: (results) => {
				let cEl;
				console.log("PAGE SERVER LOAD RESULTS: ", results);

				// copy data
				currentData = results;

				//
				let gameData = new Map();
				results.games.forEach((cRow) => {
					gameData.set(cRow.id, cRow);
				});

				// Category List
				cEl = serverGameAddForm.se_formEl('games');
				for ( let cData of results.game_stages ) {
					let option = document.createElement('option'),
						cGame = gameData.get(cData.gId);
					option.value = cData.id;
					option.text = cGame.title + " - " + cData.title;
					cEl.add(option);
				}

				// Field List
				cEl = serverGameAddForm.se_formEl('fields');
				for ( let cData of results.event_fields ) {
					let option = document.createElement('option');
					option.value = cData.id;
					option.text = cData.title;
					cEl.add(option);
				}

				// GameType List
				cEl = serverGameTypeAddForm.se_formEl('gamesTypes');
				for ( let cData of results.gametypes ) {
					let option = document.createElement('option');
					option.value = cData.id;
					option.text = cData.title;
					cEl.add(option);
				}

				//
				console.log("resultados", results.server_fields);

				// Server Fields
				serverFieldsContent.se_append(se.struct.stringPopulateMany(serverFieldsTemplate.se_html(), results.server_fields));
			}
		});

		//
		nodeHTTP = require('./http');

		// Bindings
		plugElem.se_on('click', 'button[se-act]', playerBtnAction);
		wsFilter.se_on('click', 'button', wsFilterAction);
		wsAddForm.se_on('submit', wsAddMessage);
		//
		//
		qrGenerator = new QRCode(qrObject, {});
		//
		serverGameAddForm.se_on('submit', gameAddSubmit);
		serverGameTypeAddForm.se_on('submit', gameTypeAddSubmit);
	}

	//<editor-fold desc="Game Editor">
	//
	function gameAddSubmit(e) {
		e.preventDefault();

		//
		console.log("----\nAgregar gametype");

		//
		let stageId = parseInt(serverGameTypeAddForm.elements['games'].value),
			serverFieldId = parseInt(serverGameTypeAddForm.elements['serverFieldId'].value),
			confServer = serverGameTypeAddForm.elements['confServer'].value,
			confScoring = serverGameTypeAddForm.elements['confScoring'].value,
			confConnection = serverGameTypeAddForm.elements['confConnection'].value,
			fieldId = parseInt(serverGameTypeAddForm.elements['fields'].value),
			//
			gameData,
			gameStageData,
			fieldData,
			cGameTypeData;

		// Get Game Data
		for ( let cStage of currentData.game_stages ) {
			if ( cStage.id === stageId ) {
				gameStageData = cStage;
				// Loop games
				for ( let cGame of currentData.games ) {
					if ( cGame.id === cStage.gId ) {
						gameData = cGame;
						break;
					}
				}
			}
		}

		// Get Field Data
		for ( let cField of currentData.event_fields ) {
			if ( cField.id === fieldId ) {
				fieldData = cField;
				break;
			}
		}

		// GET GameType Data
		for ( let cGame of currentData.gametypes ) {
			if ( cGame.id === gameData.gtId ) {
				cGameTypeData = cGame;
				break;
			}
		}

		// Revisar si existe un juego en estas condiciones
		app.idb.query('server_fields', {
			index: 'gameFilter',
			keyRange: IDBKeyRange.only([gameStageData.id, fieldData.id]),
			onSuccess:(data) => {
				alert("Ya existe un juego en estas condiciones");
			},
			onEmpty:() => {
				console.log("Condiciones disponibles. Continuar", gameStageData, fieldData);
				//
				app.idb.query('game_matches', {
					index: 'gameFilter',
					keyRange: IDBKeyRange.only([gameStageData.id, fieldData.id]),
					onSuccess:(data) => {
						let matchAvailable = false,
							selMatch;

						//
						for ( let cMatch of data ) {
							if ( cMatch.mStatus === 1 ) {
								selMatch = cMatch;
								matchAvailable = true;
								break;
							}
						}

						//
						if ( !matchAvailable ) {
							alert("Ya no hay partidas pendientes.");
							return;
						}

						//
						if ( serverMode !== 'score_mobile' && ( selMatch.t1_side === 0 || selMatch.t2_side === 0 ) ) {
							alert("La primera partida no tiene lados asignados, asignar antes de crear." + serverMode);
							return;
						}

						console.log("MAIN - MATCH SELECCIONADO", selMatch);

						//
						let newServerField = {
							id:serverFieldId,
							key:se.randomString(16),
							//
							confMatches:'tournament',
							confScoring:confScoring,
							confServer:confServer,
							confConnection:confConnection,
							//
							gtId:cGameTypeData.id,
							gtTitle:cGameTypeData.title,
							//
							gsId:gameStageData.id,
							gsTitle:gameData.title + " - " + gameStageData.title,
							//
							fId:fieldData.id,
							fTitle:fieldData.title,
							//
							cMatchIndex:selMatch.index,
							cMatchOrder:selMatch.mOrder,
							//
							t1_name:( selMatch.t1_side === 1 ) ? selMatch.t1_name : selMatch.t2_name,
							t2_name:( selMatch.t2_side === 2 ) ? selMatch.t2_name : selMatch.t1_name,
							//
							data:cGameTypeData.struct.field,
						};

						//
						console.log("MAIN - SERVER ADD: ", newServerField);

						//
						serverGameAdd(newServerField);
					},
					onEmpty:() => {
						alert("No hay partidas en estas condiciones.");
						console.warn("No hay partidas en las condiciones: ");
					}
				});
			}
		});
		//
	}

	//
	function gameTypeAddSubmit(e) {
		e.preventDefault();
		//
		let gtId = parseInt(serverGameTypeAddForm.elements['gamesTypes'].value),
			serverFieldId = parseInt(serverGameTypeAddForm.elements['serverFieldId'].value),
			confServer = serverGameTypeAddForm.elements['confServer'].value,
			confScoring = serverGameTypeAddForm.elements['confScoring'].value,
			confConnection = serverGameTypeAddForm.elements['confConnection'].value,
			cGameTypeData;

		//
		if ( serverFieldId === 0 ) {
			alert("invalid server id (0)");
		}

		//
		for ( let cGame of currentData.gametypes ) {
			if ( cGame.id === gtId ) {
				cGameTypeData = cGame;
				break;
			}
		}

		//
		let newServerField = {
			id:serverFieldId,
			key:se.randomString(16),
			//
			confScoring:confScoring,
			confServer:confServer,
			confConnection:confConnection,
			confMatches:'free',
			//
			gsId:0,
			gsTitle:'FREE MODE',
			//
			gtId:gtId,
			gtTitle:cGameTypeData.title,
			//
			gId:0,
			gTitle:'none',
			//
			fId:0,
			fTitle:'none',
			//
			cMatchIndex:0,
			cMatchOrder:0,
			//
			t1_name:'Team 01',
			t2_name:'Team 02',
			//
			data:cGameTypeData.struct.field,
		};

		//
		console.log("GAMETYPE FIELD ADD: ", newServerField);

		//
		serverGameAdd(newServerField);
	}

	//
	function serverGameAdd(newGame) {
		console.log("SERVER - GAME ADDING: ", newGame);
		//
		app.idb.add('server_fields', newGame, {
			onSuccess:(e) => {
				console.log("SERVER GAME ADDED:", e);

				// Agregar en tabla
				serverFieldsContent.se_append(se.struct.stringPopulate(serverFieldsTemplate.se_html(), newGame));

				// Enviar a server
				server.postMessage({
					'op_type':['server_field', 'add'],
					'op_data':{
						'id':newGame.id,
						'data':newGame
					}
				});
			}
		});
	}

	//
	function serverGameDel(cBtn) {
		let cRow = cBtn.se_closest('tr'),
			mId = parseInt(cRow.se_data('id'));

		//
		console.log("Removiendo partida del servidor. ID:", mId);

		//
		app.idb.del('server_fields', mId, {
			onSuccess:(e) => {
				console.log("Partida removida:", e);

				//
				cRow.se_remove();

				// Enviar a server
				server.postMessage({
					'op_type':['server_field', 'del'],
					'op_data':{
						'id':mId
					}
				});
			}
		});
	}
	//</editor-fold>
	
	//
	function playerBtnAction(e, cBtn) {
		e.preventDefault();

		//
		switch ( cBtn.se_attr('se-act') ) {
			//
			case 'showQR':
				let cURL = 'https://localhost' + '/admin/?sfId=' + cBtn.se_data('id') + '&sfKey='+ cBtn.se_data('key');
				console.warn("displaying qr", cURL);
				// Show QR
				qrGenerator.makeCode(cURL);
				se.dialog.openModal(pseudoDialog);
				break;

			//
			case 'serverGameRemove':
				if ( confirm('¿Borrar partida?') ) {
					serverGameDel(cBtn);
				}
				break;

			//
			case 'swClear':
				swMsgCont.se_empty();
				break;

			//
			case 'wsClear':
				wsMsgCont.se_empty();
				break;

			//
			default:
				console.log("Evento no definido.");
				break;
		}
		//
	}

	//
	function wsFilterAction(e, cBtn) {
		let type = cBtn.se_attr('se-type');
		console.log("should filter", type);
	}

	//
	function wsAddMessage(e) {
		e.preventDefault();
		let cData = se.form.serializeObject(wsAddForm);
		cData.time = se.time.dateObjectToString(new Date());
		cData.type = 1;
		//
		ws_message(cData);
		//
		nodeHTTP.wss_send('all', cData.head, cData.body);
	}

	//
	function sw_message(message) {
		//
		swMsgCont.se_append(se.struct.stringPopulate(swMsgTemplate.se_html(), message));
		// Auto scroll
		swMsgCont.scrollTop = swMsgCont.scrollHeight;
	}

	//
	function ws_message(message) {
		//
		wsMsgCont.se_append(se.struct.stringPopulate(wsMsgTemplate.se_html(), message));
		// Auto scroll
		wsMsgCont.scrollTop = wsMsgCont.scrollHeight;
	}

	//
	init();
	return {
		sw_message:sw_message,
		ws_message:ws_message
	};
};
//
