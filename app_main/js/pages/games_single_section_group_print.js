﻿"use strict";
se.plugin.uwp_page_games_single_section_group_print = function ( plugElem, plugOptions ) {
	let showMatchesEl = plugElem.querySelector('div[se-elem="show_matches"]'),
		showMatchesTableTemplate = showMatchesEl.querySelector('template'),
		showMatchesTableContent = showMatchesEl.querySelector('tbody[se-elem="content"]'),
		//
		showScoresEl = plugElem.querySelector('div[se-elem="show_scores"]'),
		showScoresTableTemplateMatchScore = showScoresEl.querySelector('template[se-elem="matchScore"]'),
		showScoresTableContent = showScoresEl.querySelector('div[se-elem="content"]'),
		//
		currentStatus = {
			gId:0,
			ggId:0,
			gtId:0
		};

	//
	function init() {
		let params = se.url.str_parse(window.location.search);

		// Parse strings
		params.gId = intVal(params.gId);
		params.gsId = intVal(params.gsId);
		params.ggId = intVal(params.ggId);

		//
		currentStatus.gId = params.gId;
		currentStatus.ggId = params.ggId;

		//
		let queries = [
			{
				rName:'games',
				table:'games',
				params: {
					get:params.gId
				}
			},
			{
				rName:'game_stages',
				table:'game_stages',
				params: {
					get:params.gsId
				}
			},
			{
				rName:'game_groups',
				table:'game_groups',
				params: {
					get:params.ggId
				}
			},

		];

		// Generar json data
		app.idb.queryBatch(queries, {
			onSuccess:(results) => {
				console.log("Query Batch Results", results);
				//
				let navigation = plugElem.querySelector('div[se-elem="navigation"]');
				navigation.se_attr('aria-hidden', 'false');

				//
				currentStatus.gtId = results.games.gtId;

				//
				se.uwp.elementTemplateProc(navigation, {
					gId:params.gId,
					gsId:params.gsId,
					ggId:params.ggId,
					gTitle:results.games.title,
					cTitle:results.game_stages.title,
					ggTitle:results.game_groups.title
				});
			}
		});

		// Bindings
		plugElem.se_on('click', '[se-act]', btnActions);
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		//
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'show':
				switch ( cBtn.se_data( 'show' ) ) {
					//
					case 'game_matches':
						show_matches();
						break;
					//
					case 'scores':
						show_scores();
						break;
					//
					case 'summary':
						show_summary();
						break;
					//
					default:
						console.error("impresión no válida.");
						break;
				}
				break;

			//
			case 'print':
				print();
				break;

			//
			case 'download':
				download();
				break;

			//
			default:
				console.log("Boton no programado", cBtn);
				break;
		}
	}

	//
	function show_matches() {
		//
		plugElem.querySelectorAll('div.printOption').se_data('status', 0);
		plugElem.querySelector('div.printOption[se-elem="show_matches"]').se_data('status', 1);

		//
		app.idb.query('game_matches', {
			index:'ggId',
			keyRange:IDBKeyRange.only(currentStatus.ggId),
			onSuccess:(data) => {
				showMatchesTableContent.se_append(se.struct.stringPopulateMany(showMatchesTableTemplate.se_html(), data));
			}
		});
	}

	//
	function show_scores() {
		//
		plugElem.querySelectorAll('div.printOption').se_data('status', 0);
		plugElem.querySelector('div.printOption[se-elem="show_scores"]').se_data('status', 1);

//
		let queries = [
			{
				rName:'gametypes',
				table:'gametypes',
				params: {
					get:currentStatus.gtId
				}
			},
			{
				rName:'game_matches',
				table:'game_matches',
				params: {
					index:'ggId',
					keyRange:IDBKeyRange.only(currentStatus.ggId),
				}
			},

		];

		// Generar json data
		app.idb.queryBatch(queries, {
			onSuccess:(results) => {
				console.log("scores", results);

				let cRounds = 1,
					cTableUberHead = '',
					cTableNormalHead = '',
					tableBodyRounds = '',
					scoreTableBody = '',
					pointTable = '',
					tableStructure = showScoresTableTemplateMatchScore.se_html();

				// Round adjust
				for ( let i = 0; i < cRounds; i++ ) {
					cTableUberHead+= '<th colspan="4">R ' + (i + 1) + '</th>';
					cTableNormalHead+= "<th colspan=\"2\">Loc</th><th colspan=\"2\">Vis</th>";
					tableBodyRounds+= "<td></td><td></td><td></td><td></td>";
				}

				console.log("MATCHES UPDATES", results.gametypes.struct.points);

				//
				for ( let cPointsIndex in results.gametypes.struct.points ) {
					let cPoints = results.gametypes.struct.points[cPointsIndex];
					//
					scoreTableBody+= se.struct.stringPopulate(
						"<tr><td>!title; (!points; X !pointsMax;)</td>!tableBodyRounds;</tr>",
						{
							title:cPoints.title,
							points:cPoints.points,
							pointsMax:cPoints.pointsMax,
							tableBodyRounds:tableBodyRounds
						}
					);
				}

				//
				pointTable = `
<table class="scoreTable">
<thead>
<tr><th>Puntos</th>${ cTableUberHead }</tr>
<tr><th></th>${ cTableNormalHead }</tr>
</thead>
<tbody>
${ scoreTableBody }
</tbody>
<tfoot>
<tr><td>Total Round:</td>${ tableBodyRounds }</tr>
</tfoot>
</table>`;
				//

				//
				for ( let cRowId in results.game_matches ) {
					//
					let cRow = results.game_matches[cRowId];
					//
					showScoresTableContent.se_append(se.struct.stringPopulate(tableStructure, {
						gsTitle:cRow.gsTitle,
						ggTitle:cRow.ggTitle,
						mOrder:cRow.mOrder,
						gTimeDay:cRow.gTimeDay,
						gTimeHour:cRow.gTimeHour,
						t1_name:cRow.t1_name,
						t2_name:cRow.t2_name,
						pointTable:pointTable
					}));
				}
			}
		});
	}

	//
	function show_summary() {
		//
		plugElem.querySelectorAll('div.printOption').se_data('status', 0);
		plugElem.querySelector('div.printOption[se-elem="show_summary"]').se_data('status', 1);

		//
		app.idb.query('game_matches', {
			index:'ggId',
			keyRange:IDBKeyRange.only(currentStatus.ggId),
			onSuccess:(data) => {
				showMatchesTableContent.se_append(se.struct.stringPopulateMany(showMatchesTableTemplate.se_html(), data));
			}
		});
	}

	//
	function print() {
		win.print({
			autoprint:false
		});
	}

	//
	function download() {

	}

	//
	init();
	return {};
};
