﻿"use strict";
se.plugin.uwp_page_games_match_single = function ( plugElem, plugOptions ) {
	let matchForm = plugElem.querySelector('form[se-elem="match"]'),
		teamsForm = plugElem.querySelector('form[se-elem="teamMod"]'),
		//
		actionChronicle = plugElem.querySelector('div[se-elem="chronicle"]'),
		actionChronicleTemplate = actionChronicle.querySelector('template'),
		actionChronicleContent = actionChronicle.querySelector('div[se-elem="content"]'),
		actionsList = plugElem.querySelector('div[se-elem="ev_list"]'),
		actionsListTemplate = actionsList.querySelector('template'),
		actionsListContent = actionsList.querySelector('tbody[se-elem="content"]'),
		//
		actionsForm = plugElem.querySelector('form[se-elem="actions"]'),
		actionsDevices = actionsForm.querySelector('div[se-elem="devices"]'),
		actionsDevicesTemplate = actionsDevices.querySelector('template'),
		actionsDevicesContent = actionsDevices.querySelector('div[se-elem="content"]'),
		actionsSide = actionsForm.querySelector('div[se-elem="side"]'),
		actionsTimes = actionsForm.querySelector('div[se-elem="time"]'),
		actionsPlayers = actionsForm.querySelector('select[name="userId"]'),
		//
		editEventDiag = plugElem.querySelector('dialog[se-elem="editEvent"]'),
		editEventForm = editEventDiag.querySelector('form'),
		editEventFormTime = editEventForm.querySelector('div[se-elem="time"]'),
		editEventFormPlayers = editEventForm.querySelector('select[name="devUser"]'),
		//
		teamsPointList = plugElem.querySelector('table[se-elem="teamsPoints"]'),
		teamsPointListTemplate = teamsPointList.querySelector('template'),
		teamsPointListContent = teamsPointList.querySelector('tbody[se-elem="content"]'),
		//
		playerPointList = plugElem.querySelector('table[se-elem="playerPoints"]'),
		playerPointListTemplate = playerPointList.querySelector('template'),
		playerPointListContent = playerPointList.querySelector('tbody[se-elem="content"]'),
		//
		matchData, gameData, gameTypeData, playerList = {},
		team_1_data, team_2_data,
		matchEvents = [], matchPoints,
		matchTimeTotal;

	//
	function init() {
		let params = se.url.str_parse(window.location.search),
			gId = intVal(params.gId);

		//
		console.log("PAGE: Games single match init.");

		//
		app.idb.query('games', {
			get:gId,
			onSuccess:(data) => {
				let navigation = plugElem.querySelector('div[se-elem="navigation"]');
				se.uwp.elementTemplateProc(navigation, {
					gId:params.gId,
					gTitle:data.title,
					index:params.index
				});
			}
		});

		// Información del juego
		app.idb.query('game_matches', {
			get: intVal(params.index),
			onSuccess: (data) => {
				console.log("RETURNED matchData", data);
				matchData = data;

				//
				matchEvents = matchData.mEvents;
				matchPoints = matchData.mPoints;

				//
				se.uwp.sectionSetValues(plugElem, data);

				// Multiple sub queries
				let queries = [
					{
						rName:'gameData',
						table:'games',
						params: {
							get: matchData.gId,
						}
					},
					{
						rName:'players_1',
						table:'game_teams',
						params: {
							get:(intVal(matchData.t1_index)),
						}
					},
					{
						rName:'players_2',
						table:'game_teams',
						params: {
							get:(intVal(matchData.t2_index)),
						}
					}
				];

				// Procesar tipo de juego y jugadores
				app.idb.queryBatch(queries, {
					onSuccess:(results) => {
						console.log("RETURNED game/team data", results);

						// Game Data
						gameData = results['gameData'];
						team_1_data = results['players_1'];
						team_2_data = results['players_2'];

						// Player lists
						playersAppend(results['players_1'].id, results['players_1'].title, results['players_1'].players_list);
						playersAppend(results['players_2'].id, results['players_2'].title, results['players_2'].players_list);
						updateSides();

						// Información del tipo de juego
						app.idb.query('gametypes', {
							get: gameData.gtId,
							onSuccess: (data3) => {
								console.log("Gametypedata information", data3);
								gameTypeData = data3;
								// Llenar formularios
								let strTemplate = actionsDevicesTemplate.se_html(),
									cDev;
								for ( let i = 0; i < gameTypeData.struct.properties.devices.length; i++ ) {
									cDev = gameTypeData.struct.properties.devices[i];
									//
									if ( cDev.data.devType === 'base' ) { continue; }
									//
									actionsDevicesContent.se_append(se.struct.stringPopulate(strTemplate, {
										id: i,
										title: cDev.data.title,
										type: cDev.data.devType,
										icon: cDev.data.mapicon
									}));
								}

								// Ajustar puntajes
								teamsPointListContent.se_empty();
								let teamPointsTemplateHTML = teamsPointListTemplate.se_html();
								console.log("agregando puntajes", gameTypeData, gameTypeData.struct, gameTypeData.struct.points);
								for ( let cPointsId in gameTypeData.struct.points ) {
									if ( !gameTypeData.struct.points.hasOwnProperty(cPointsId) ) { continue; }
									let cPointData = gameTypeData.struct.points[cPointsId];
									if ( cPointData.target === 1 ) {
										let cPointsPrint_side_01 = {
												'c':0,
												't':0,
											},
											cPointsPrint_side_02 = cPointsPrint_side_01;

										// Buscar original
										if ( matchData.mPoints && matchData.mPoints.hasOwnProperty('t') ) {
											if ( matchData.mPoints.t['1'].p.hasOwnProperty(cPointsId) ) {
												cPointsPrint_side_01 = matchData.mPoints.t['1'].p[cPointsId];
											}
											if ( matchData.mPoints.t['2'].p.hasOwnProperty(cPointsId) ) {
												cPointsPrint_side_02 = matchData.mPoints.t['2'].p[cPointsId];
											}
										}

										//
										teamsPointListContent.se_append(se.struct.stringPopulate(teamPointsTemplateHTML, {
											'id':cPointData.id,
											'title':cPointData.title,
											'points':cPointData.points,
											'pointsMax':cPointData.pointsMax,
											//
											's_1_c':cPointsPrint_side_01['c'],
											's_1_p':cPointsPrint_side_01['t'],
											//
											's_2_c':cPointsPrint_side_02['c'],
											's_2_p':cPointsPrint_side_02['t'],
										}));
									}
								}

								//
								teamsPointList.querySelector('tfoot td[se-elem="side_1_total"]').se_text(matchData.t1_points);
								teamsPointList.querySelector('tfoot td[se-elem="side_2_total"]').se_text(matchData.t2_points);

								// Ajustar tiempo máximo del objeto
								actionsForm.se_formEl('timeInt').max = gameTypeData.struct.settings.timeGameInt;
								editEventForm.se_formEl('time').max = gameTypeData.struct.settings.timeGameInt;

								// ajuste variables
								matchTimeTotal = gameTypeData.struct.settings.timeGameInt;
								actionChronicle.querySelector('.end').se_text(secondsToTime(matchTimeTotal));

								//
								reDrawEvents();
							}
						});
						//
					},
					onFail:() => {}
				});
				//

			}
		});

		// Get team elements


		// Get action options

		// Bindings
		matchForm.se_on('change', 'input', matchFormUpdate);
		teamsForm.se_on('change', ['select', 'input'], teamsFormUpdate);
		plugElem.se_on('click', '[se-act]', btnActions);

		// Actions
		actionsTimes.se_on('change', 'input', timeUpdate);
		editEventFormTime.se_on('change', 'input', timeUpdateModal);
		actionsDevicesContent.se_on('click', 'div.object', selectDevice);
		actionsSide.se_on('click', 'div.object', selectSide);
		actionsForm.se_on('submit', actionSubmit);
		editEventForm.se_on('submit', actionUpdate);
		teamsPointList.se_on('change', 'input', matchPointsManualUpdate);
	}

	//<editor-fold desc="Event Form">
	//
	function selectDevice(e, cEl) {
		let devId = cEl.se_data('id');
		actionsDevicesContent.querySelectorAll('div.object').se_attr("aria-selected", "false");
		cEl.se_attr("aria-selected", "true");
		actionsForm.se_formElVal('deviceId', devId);
	}

	//
	function selectSide(e, cEl) {
		let side = cEl.se_data('side');
		actionsSide.querySelectorAll('div.object').se_attr("aria-selected", "false");
		cEl.se_attr("aria-selected", "true");
		actionsForm.se_formElVal('side', side);
		console.log(cEl, side);
	}

	//
	function playersAppend(teamId, teamTitle, players) {
		let pad = "000",
			cPlayer,
			cPlayerId, cPlayerIdStr, cPlayerIdPad,
			opt,
			templateStr = playerPointListTemplate.se_html();
		//
		for ( let i = 0; i < players.length; i++ ) {
			cPlayer = players[i];
			//
			cPlayerId = parseInt(cPlayer.eventId);
			cPlayerIdStr = "" + cPlayerId;
			cPlayerIdPad = pad.substring(0, pad.length - cPlayerIdStr.length) + cPlayerIdStr;

			// Action form players
			opt = document.createElement('option');
			opt.se_data('teamId', teamId);
			opt.value = cPlayerId;
			opt.innerHTML = cPlayerIdPad + ' - [' + teamTitle+ '] ' + cPlayer.title;
			// Both
			actionsPlayers.appendChild(opt);
			editEventFormPlayers.appendChild(opt);


			// Player list table
			playerPointListContent.se_append(se.struct.stringPopulate(templateStr, {
				teamId:teamId,
				id:cPlayerId,
				idPad:cPlayerIdPad,
				title:cPlayer.title
			}));

			// Player list object
			playerList[cPlayerId] = cPlayer.uName;
		}
	}

	//
	function updateSides() {
		let t1_index = matchData.t1_index,
			t1_side = matchData.t1_side,
			t2_index = matchData.t2_index,
			t2_side = matchData.t2_side;
		//
		playerPointListContent.querySelectorAll('tr[data-teamindex="'+t1_index+'"]').se_data('side', t1_side);
		playerPointListContent.querySelectorAll('tr[data-teamindex="'+t2_index+'"]').se_data('side', t2_side);
		//
		actionsPlayers.querySelectorAll('option[data-teamindex="'+t1_index+'"]').se_data('side', t1_side);
		actionsPlayers.querySelectorAll('option[data-teamindex="'+t2_index+'"]').se_data('side', t2_side);
		//
		editEventFormPlayers.querySelectorAll('option[data-teamindex="'+t1_index+'"]').se_data('side', t1_side);
		editEventFormPlayers.querySelectorAll('option[data-teamindex="'+t2_index+'"]').se_data('side', t2_side);
		// Flag sides
		teamsPointList.querySelector('th[se-elem="team_01"] svg').se_data('side', t1_side);
		teamsPointList.querySelector('th[se-elem="team_02"] svg').se_data('side', t2_side);
	}

	//
	function timeUpdate(e, cEl) {
		switch ( cEl.name ) {
			//
			case 'timeInt':
				actionsForm.se_formElVal('timeTxt', se.time.secondsToTime(parseInt(cEl.value)));
				break;
			//
			case 'timeTxt':
				if ( cEl.checkValidity() ) {
					actionsForm.se_formElVal('timeInt', se.time.timeToSecs(cEl.value));
				}
				break;
		}
	}
	//
	function timeUpdateModal(e, cEl) {
		//
		switch ( cEl.name ) {
			//
			case 'time':
				editEventForm.se_formElVal('timeTxt', se.time.secondsToTime(parseInt(cEl.value)));
				break;
			//
			case 'timeTxt':
				if ( cEl.checkValidity() ) {
					editEventForm.se_formElVal('timeInt', se.time.timeToSecs(cEl.value));
				}
				break;
		}
	}
	//</editor-fold>

	//
	function secondsToTime(seconds) {
		// we will use this function to convert seconds in normal time format
		let secs = Math.abs(seconds),
			hr = Math.floor(secs / 3600),
			min = Math.floor((secs - (hr * 3600)) / 60),
			sec = Math.floor(secs - (hr * 3600) - (min * 60)),
			time = '';

		//
		if ( hr !== 0 ) {
			if ( hr < 10 ) {
				time += '0';
			}
			time += hr + ':';
		}
		if ( min !== 0 ) {
			if ( min < 10 ) {
				time += '0';
			}
			time += min + ':';
		} else {
			time += '00:';
		}
		if ( sec < 10 ) {
			time += '0';
		}
		time += sec;
		if ( seconds < 0 ) {
			time = '-' + time;
		}
		return time;
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		let id;
		//
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'eventDel':
				if ( confirm('¿Borrar evento?') ) {
					actionDel(id);
				}
				break;
			//
			case 'eventUpd':
				id = cBtn.se_closest('tr').se_index();
				actionEdit(id);
				break;
			//
			case 'eventUpdCancel':
				editEventDiag.close();
				break;
			//
			case 'runGameSimulation':
				runGameSimulation();
				break;
			//
			default:
				console.log("Boton no programado", cBtn);
				break;
		}
	}

	//<editor-fold desc="Real time">

	//
	function actionEdit(evId) {
		let cEventData = matchEvents[evId];
		console.log("event", cEventData);
		if ( cEventData.type !== 'device' ) { return; }

		//
		se.form.loadData(editEventForm,  se.object.merge( {id:evId}, cEventData ) );
		editEventDiag.showModal();
	}

	//
	function actionUpdate(e) {
		e.preventDefault();
		let eData = se.form.serializeToObj(editEventForm, false, {'int':['time', 'devSide', 'devUser']});
		console.log("PRE\n\n\n", matchEvents[eData.id], eData);
		//
		matchEvents[eData.id].time = parseInt(eData.time);
		matchEvents[eData.id].devSide = parseInt(eData.devSide);
		matchEvents[eData.id].devUser = parseInt(eData.devUser);
		console.log("post", matchEvents[eData.id]);
		//
		editEventDiag.close();
		//
		runGameSimulation();
	}

	//
	function reDrawEvents() {
		let chronTemplate = actionChronicleTemplate.se_html(),
			listTemplate = actionsListTemplate.se_html();
		//
		actionChronicleContent.se_empty();
		actionsListContent.se_empty();

		// Re order events by time
		matchEvents.sort((a, b) => { return a.time - b.time; });

		//
		for ( let cEventId in matchEvents ) {
			//
			if ( !matchEvents.hasOwnProperty(cEventId) ) { continue; }
			//
			let cEvent = matchEvents[cEventId],
				cSide = 0,
				dataContent,
				userNumber;

			// No va a mostrar eventos pre inicio de juego
			if ( cEvent.time < 0 ) { continue; }

			console.log("cEvent", cEvent);

			//
			switch ( cEvent.type ) {
				//
				case 'automatic':
					let cEventTimedInfo = gameTypeData.struct.properties[cEvent.subtype][cEvent.id].data;
					console.log(cEventTimedInfo);
					dataContent = `${cEventTimedInfo.title}`;
					userNumber = '-';

					break;
				//
				case 'device':
					let gtDevice = gameTypeData.struct.properties.devices[cEvent.devId].data;

					//
					dataContent = `
<svg class="device icon inline mr" data-side="${cEvent.devSide}"><use xlink:href="#${gtDevice.mapicon}" /></svg> ${gtDevice.title}
`;
					userNumber = cEvent.devUser;
					//
					cSide = cEvent.devSide;
					break;
			}

			//
			actionChronicleContent.se_append(se.struct.stringPopulate(chronTemplate, {
				id:cEventId,
				type:cEvent.type,
				side:cSide,
				relTime:cEvent.time / matchTimeTotal * 100
			}));
			//
			actionsListContent.se_append(se.struct.stringPopulate(listTemplate, {
				id:cEventId,
				type:cEvent.type,
				timeText:secondsToTime(cEvent.time),
				dataContent:dataContent,
				userNumber:userNumber
			}));
		}
	}

	//
	function actionSubmit(e) {
		e.preventDefault();
		let actionData = se.form.serializeToObj(actionsForm, false,
			{'int':['timeInt', 'deviceId', 'side', 'userId']}
		);

		// Process
		matchEvents.push({
			type:'device',
			time:actionData.timeInt,
			devId:actionData.deviceId,
			devSide:actionData.side,
			devUser:actionData.userId
		});

		// Reset form
		actionsDevicesContent.querySelectorAll('div.object').se_attr("aria-selected", "false");
		actionsSide.querySelectorAll('div.object').se_attr("aria-selected", "false");
		actionsForm.reset();

		//
		runGameSimulation();
	}

	//
	function matchFormUpdate(e, cEl) {
		//
		let updateData = se.object.merge({sync:1}, se.form.serializeToObj(matchForm, false, {'int':['mStatus']}));
		matchData = se.object.merge(matchData, updateData);
		app.idb.update('game_matches', matchData.index, updateData, {
			onSuccess:(data) => {
				console.log("Match General status updated");
			}
		});
	}

	//
	function teamsFormUpdate(e, cEl) {
		e.preventDefault();

		let cName = cEl.name, cVal = intVal(cEl.value), otherValue;

		// Adjust other
		switch ( cName ) {
			//
			case 't1_side':
				teamsForm.se_formElVal('t2_side', ( cVal === 2 ) ? 1 : 2);
				break;
			//
			case 't2_side':
				teamsForm.se_formElVal('t1_side', ( cVal === 2 ) ? 1 : 2);
				break;
			//
			case 't1_status':
					 if ( cVal === 0 ) { otherValue = 0; }
				else if ( cVal === 2 ) { otherValue = 2; }
				else if ( cVal === 1 ) { otherValue = 3; }
				else if ( cVal === 3 ) { otherValue = 1; }
				teamsForm.se_formElVal('t2_status', otherValue);
				break;
			//
			case 't2_status':
					 if ( cVal === 0 ) { otherValue = 0; }
				else if ( cVal === 2 ) { otherValue = 2; }
				else if ( cVal === 1 ) { otherValue = 3; }
				else if ( cVal === 3 ) { otherValue = 1; }
				teamsForm.se_formElVal('t1_status', otherValue);
				break;
			//
			default:
				console.error("Unknown varaible");
				break;
		}

		//
		let updateData = se.object.merge({sync:1}, se.form.serializeToObj(teamsForm, false, {'int':['t1_side', 't1_status', 't2_side', 't2_status']}));
		console.log(updateData);
		matchData = se.object.merge(matchData, updateData);
		updateSides();
		app.idb.update('game_matches', matchData.index, updateData, {
			onSuccess:(data) => {
				console.log("Match Elements status updated");
			}
		});
	}

	//
	function runGameSimulation() {
		let cField = se.object.copy(gameTypeData.struct.field),
			cEvents = [],
			cEvent,
			cIndex,
			valid = true,
			userPoints = {};

		// Copy match points structure
		matchData.mPoints = se.object.copy(cField.points);

		//
		console.log("---\nSIMULATION START\n----");

		// Reset individual points
		for ( let cPlayerId in playerList ) {
			if ( !playerList.hasOwnProperty(cPlayerId) ) { continue; }
			userPoints[cPlayerId] = 0;
		}

		//
		function actionOperation(actionList, cEvent) {
			//
			for ( let cAction of actionList ) {
				switch ( cAction.type ) {
					// Ignoring non realtime activities
					case 'buzzer':
					case 'sound':
					case 'respawn':
					case 'note':
					case 'timer':
					case 'timeAlternate':
						break;
					// Actuable actions
					case 'devMod':
						break;
					case 'point':
						let pointData = gameTypeData.struct.points.find(({id}) => id === cAction.pointId),
							cDeviceField = cField.devices[cEvent.devId];

						//
						// console.log("cEvent", cEvent);
						// console.log("action", cAction);
						// console.log("point", pointData);
						// console.log("device", cDeviceField);
						// console.log("end...\n\n\n");

						// Ignorar punto
						if ( !pointData ) { console.error("no hay punto, revisar"); continue; }

						// Ignorar punto, ya que sólo se maneja para equipo (arreglar)
						if ( pointData.target !== 1 ) { continue; }

						// Determinar punto objetivo
						switch ( pointData.target ) {
							// Equipo general
							case 1:
								switch ( cAction.pointType ) {
									//
									case 'simple':
										//
										// Puntuación de grupo
										let sideIndex = cDeviceField.side.toString(),
											pointsId = cAction.pointId.toString();

										console.warn("simple point add check", matchData.mPoints);


										// Match Data
										matchData.mPoints.t[sideIndex].p[pointsId].c++;
										matchData.mPoints.t[sideIndex].p[pointsId].t+= pointData.points;
										matchData.mPoints.t[sideIndex].t+= pointData.points;
										/*
										//
										cField.points.partial[cAction.pointId][cDeviceField.side]['c']++;
										cField.points.partial[cAction.pointId][cDeviceField.side]['t'] += pointData.points;
										cField.points.total[cDeviceField.side] += pointData.points;

										 */
										break;
									//
									case 'cyclic':
										let missing = true,
											cycleEventIndex,
											tempCycleIndex,
											tempCycleObj,
											tempDeviceIndex,
											tempDeviceObj;

										//
										for ( tempCycleIndex in cField.eventsTimed.cyclic ) {
											if ( !cField.eventsTimed.cyclic.hasOwnProperty(tempCycleIndex) ) {
												continue;
											}
											tempCycleObj = cField.eventsTimed.cyclic[tempCycleIndex];
											//
											for ( tempDeviceIndex in tempCycleObj ) {
												if (!tempCycleObj.hasOwnProperty(tempCycleIndex)) {
													continue;
												}
												tempDeviceObj = tempCycleObj[tempCycleIndex];

												// Validation
												if ( tempDeviceObj.devId === devId && tempDeviceObj.cycleObj[tempCycleObj.cycleCount][cDeviceField.side] ) {
													cycleEventIndex = tempCycleIndex;
													missing = false;
													break;
												}
											}

											//
											if (!missing) {
												break;
											}
										}

										// No esta registrado el punto, adicionar
										if ( missing ) {
											// Puntuación de grupo
											let sideIndex = cDeviceField.side.toString(),
												pointsId = cAction.pointId.toString();
											matchData.mPoints.t[sideIndex].p[pointsId].c++;
											matchData.mPoints.t[sideIndex].p[pointsId].t+= pointData.points;
											matchData.mPoints.t[sideIndex].t+= pointData.points;

											// console.log(cDeviceField, cField.points.partial[cAction.pointId][cDeviceField.side]);
											// Points update
											/*
											cField.points.partial[cAction.pointId][cDeviceField.side]['c']++;
											cField.points.partial[cAction.pointId][cDeviceField.side]['t'] += pointData.points;
											cField.points.total[cDeviceField.side] += pointData.points;

											 */
										}
										break;
									// Do not include switch
									case 'cyclic_exclusive':
										break;
									//
									default:
										console.log("");
										break;
								}
								break;
							// División (big games only)
							case 2:
								break;
							// Jugador individual
							case 3:
								console.log("objetivo un jugador individual", userPoints[cEvent.devUser], cAction);
								// Puntos para un usuario
								userPoints[cEvent.devUser] += pointData.points;
								console.log("FIN", userPoints);
								break;
						}
						//
						break;
					case 'gameEnd':
						break;
					default:
						console.log("Operación de acciones... caso no reconocido..", cAction.type, cAction);
						break;
				}
			}
		}

		//
		function eTriggerOps() {

		}

		//<editor-fold desc="Load events">
		// Agregar eventos de usuarios
		for ( cEvent of matchEvents ) {
			if ( cEvent.type === 'device' ) {
				cEvents.push(cEvent);
			}
		}

		// Agregar eventos tradicionales
		for ( cIndex in cField.eventsTimed.list ) {
			if ( !cField.eventsTimed.list.hasOwnProperty(cIndex) ) { continue; }
			cEvent = cField.eventsTimed.list[cIndex];
			// Avoid preparations
			if ( cEvent.time < 0 ) { continue; }
			// Count max events
			cEvents.push({
				time: cEvent.time,
				type: 'list',
				index: cIndex
			});
		}

		// Agregar eventos de tiempo cíclicos
		for ( cIndex in cField.eventsTimed.cyclic ) {
			if ( !cField.eventsTimed.cyclic.hasOwnProperty(cIndex) ) { continue; }
			// Count max events
			let maxEvents = gameTypeData.struct.settings.timeGameInt / cIndex;
			// Create markers only
			for ( let i = 0; i < maxEvents; i++ ) {
				cEvents.push({
					time: i * cIndex,
					type: 'cyclic',
					index: cIndex
				});
			}
		}
		//</editor-fold>

		// Ordenar tiempos
		cEvents = se.object.sort(cEvents, 'time', 'ASC');

		console.log("Game Events", cEvents);

		// Ejecutar simulación de puntos.
		for ( cEvent of cEvents ) {
			let timeEventData, eventData;
			switch ( cEvent.type ) {
				// Eventos automáticos
				case 'list':
					timeEventData = cField.eventsTimed.list[cEvent.index];
					eventData = gameTypeData.struct.properties[timeEventData.type][timeEventData.index];
					//
					actionOperation(eventData.actions);
					break;

				// Eventos cíclicos (puntajes principalmente)
				case 'cyclic':
					timeEventData = cField.eventsTimed.cyclic[cEvent.index];
					if ( timeEventData.devices ) {
						// console.log("GAMEDATA", gameTypeData);
						// console.log("CFIELD", cField);
						for ( let cDevObjIndex in timeEventData.devices ) {
							if ( !timeEventData.devices.hasOwnProperty(cDevObjIndex) ) { continue; }
							let cDevObj = timeEventData.devices[cDevObjIndex],
								cDeviceField = cField.devices[cDevObj.deviceId],
								cDeviceOriginal = gameTypeData.struct.properties.devices[cDevObj.deviceId];

							// Determinar si esta activo, sino saltarlo
							if ( cDeviceField.active === 0 ) { continue; }

							//
							// console.log("Cycle, cDevice", cDevObj, cDeviceField, cDeviceOriginal);

							//
							switch ( cDeviceField.type ) {
								//
								case 'swflag':
									if ( cDeviceField.side !== 0 ) {
										// Puntuación de grupo
										let sideIndex = cDeviceField.side.toString(),
											pointsId = cDevObj.pointId.toString(),
											pointObject = gameTypeData.struct.points.find(({id}) => id === cDevObj.pointId);

										//
										matchData.mPoints.t[sideIndex].p[pointsId].c++;
										matchData.mPoints.t[sideIndex].p[pointsId].t+= pointObject.points;
										matchData.mPoints.t[sideIndex].t+= pointObject.points;

										// Actualizar el status
										// console.log(cField.eventsTimed.cyclic[cEvent.index].devices[cDevObjIndex].cycleObj, cField.eventsTimed.cyclic[cEvent.index].cycleCount);
										cField.eventsTimed.cyclic[cEvent.index].devices[cDevObjIndex].cycleObj[cField.eventsTimed.cyclic[cEvent.index].cycleCount][cDeviceField.side] = 1;

										//
										/*
										cField.points.partial[cDevObj.pointId][cDeviceField.side]['c']++;
										cField.points.partial[cDevObj.pointId][cDeviceField.side]['t']+= gameTypeData.struct.points[cDevObj.pointId].points;
										cField.points.total[cDeviceField.side] += gameTypeData.struct.points[cDevObj.pointId].points;
										 */
									}
									break;
								//
								case 'opswflag':
									if ( cDeviceField.side !== cDeviceOriginal.data.devSideDef ) {
										// Puntuación de grupo
										let sideIndex = cDeviceField.side.toString(),
											pointsId = cDevObj.pointId.toString();
										matchData.mPoints.t[sideIndex].p[pointsId].c++;
										matchData.mPoints.t[sideIndex].p[pointsId].t+= gameTypeData.struct.points[cDevObj.pointId].points;
										matchData.mPoints.t[sideIndex].t+= gameTypeData.struct.points[cDevObj.pointId].points;

										// Actualizar el status
										cField.eventsTimed.cyclic[cEvent.index].devices[cDevObjIndex].cycleObj[cField.eventsTimed.cyclic[cEvent.index].cycleCount][cDeviceField.side] = 1;

										//
										cField.points.partial[cDevObj.pointId][cDeviceField.side]['c']++;
										cField.points.partial[cDevObj.pointId][cDeviceField.side]['t']+= gameTypeData.struct.points[cDevObj.pointId].points;
										cField.points.total[cDeviceField.side] += gameTypeData.struct.points[cDevObj.pointId].points;
									}
									break;
								//
								case '':
									break;
								//
								default:
									console.log("cyclo no reconocido...", cDeviceField);
									break;
							}
						}

						//
						cField.eventsTimed.cyclic[cEvent.index].cycleCount++;
					}
					break;

				// Acción sobre dispositivo
				case 'device':
					let deviceGral = gameTypeData.struct.properties.devices[cEvent.devId],
						deviceField = cField.devices[cEvent.devId];

					// console.log("ACCIÓN USUARIO");
					// console.log("EVENTO\n\n", cEvent);
					// console.log("DIS. GENERAL \n\n", deviceGral);
					// console.log("DIS. CAMPO\n\n", deviceField);
					// console.log("FIN\n\n");

					if ( deviceField.active === 0 ) {
						console.error("movimiento prohibido, esta desactivado el objeto.", deviceField, cEvent);
						return;
					}

					//
					switch ( deviceGral.data.devType ) {
						case 'base':
							break;
						//
						case 'ctflag':
							if ( deviceField.side !== cEvent.devSide ) {
								cField.devices[cEvent.devId].side = cEvent.devSide;
							} else {
								console.error("movimiento prohibido, CT Flag", deviceField, cEvent);
								return;
							}
							break;
						//
						case 'ctspot':
							if ( deviceField.active !== cEvent.devActive ) {
								cField.devices[cEvent.devId].side = cEvent.devSide;
							} else {
								console.error("movimiento prohibido, SWFLAG mismo lado", deviceField, cEvent);
								return;
							}
							break;
						//
						case 'swflag':
							if ( deviceField.side !== cEvent.devSide ) {
								cField.devices[cEvent.devId].side = cEvent.devSide;
							} else {
								console.error("movimiento prohibido, SWFLAG mismo lado", deviceField, cEvent);
								return;
							}
							break;
						//
						case 'opflag':
							if ( deviceField.side !== cEvent.devSide && deviceField.side === deviceGral.data.devSideDef ) {
								cField.devices[cEvent.devId].side = cEvent.devSide;
							} else {
								console.error("movimiento prohibido, OP FLAG", deviceGral, deviceField, cEvent);
								return;
							}
							break;
						//
						case 'opswflag':
							if ( deviceField.side !== cEvent.devSide  ) {
								cField.devices[cEvent.devId].side = cEvent.devSide;
							} else {
								console.error("movimiento prohibido, opswflag", deviceField, cEvent);
								return;
							}
							break;
						case 'bomb_loc':
							break;
						case 'bomb_bomb':
							break;
						//
						default:
							console.error("Dispositivo no reconocido");
							return;
							break;
					}

					// No hubo return, aprobar
					actionOperation(deviceGral.actions, cEvent);
					eTriggerOps();
					break;

				//
				default:
					console.log("ERROR DE CÓDIGO... NO PUEDE SER ESTE EL MUNDO EN EL QUE VIVIMOS...!");
					break;
			}
		}

		// Print (even if invalid)
		reDrawEvents();

		// Update user points
		matchData.mPoints.u = userPoints;
		printUserPoints();

		//
		// console.log("END status");
		// console.log("Field", cField);
		// console.log("MatchData", matchData);
		// console.log("\n\n");

		//
		console.log("---\nSIMULATION END\n----");

		// Save if valid
		if ( valid ) {
			let saveData = {
					mEvents:matchEvents,
					mPoints:se.object.copy(matchData.mPoints),
					sync:1,
					mStatus:4
				},
				updateScore = false;

			//
			if ( matchData.t1_side !== 0 && matchData.t2_side !== 0 ) {
				saveData.t1_points = matchData.mPoints.t[matchData.t1_side].t;
				saveData.t2_points = matchData.mPoints.t[matchData.t2_side].t;
				//
				if ( saveData.t1_points === saveData.t2_points ) {
					saveData.t1_status = 2;
					saveData.t2_status = 2;
				} else {
					saveData.t1_status = ( saveData.t1_points > saveData.t2_points ) ? 3 : 1;
					saveData.t2_status = ( saveData.t1_points < saveData.t2_points ) ? 3 : 1;
				}
				//
				updateScore = true;
			}

			matchPoints = se.object.copy(matchData.mPoints);
			matchPointsUpdate();

			//
			app.idb.update('game_matches', matchData.index, saveData, {
				onSuccess: (data) => {
					console.log("MATCH DATA UPDATED");
					//
					matchForm.se_formElVal('mStatus', 4);
					// Actualizar
					if ( updateScore ) {
						teamsForm.querySelector('td[se-elem-replace="t1_points"]').se_text(saveData.t1_points);
						teamsForm.querySelector('td[se-elem-replace="t2_points"]').se_text(saveData.t2_points);
						teamsForm.se_formElVal('t1_status', saveData.t1_status);
						teamsForm.se_formElVal('t2_status', saveData.t2_status);
					}
				}
			});
		}
	}

	//</editor-fold>

	//
	function matchPointsManualUpdate() {
		matchData.mPoints.t = {
			'1':{
				'p':{},
				't':0
			},
			'2':{
				'p':{},
				't':0
			}
		};
		let totalPoints = {
			'1':0,
			'2':0
		};

		//
		for ( let cPointData of gameTypeData.struct.points ) {
			// Only team points
			if ( cPointData.target !== 1 ) { continue; }

			//
			let cRow = teamsPointListContent.querySelector('tr[data-id="' + cPointData.id + '"]'),
				tCount, tTotal, tInput;

			// Hacer doble
			for ( let i = 1; i < 3; i++ ) {
				let teamIndex = i.toString();
				tInput = cRow.querySelector('input[data-side="' + i + '"]');
				tCount = intVal(tInput.value);
				tCount = ( tCount <= cPointData.pointsMax ) ? tCount : cPointData.pointsMax;
				//
				tTotal = tCount * cPointData.points;
				//
				tInput.value = tCount;
				cRow.querySelector('td[data-side="' + i + '"]').se_text(tTotal);
				//
				matchData.mPoints.t[teamIndex].p[cPointData.id] = {
					'c':tCount,
					't':tTotal
				};
				matchData.mPoints.t[teamIndex].t+= tTotal;
				//
				totalPoints[teamIndex]+= tTotal;
			}
		}

		//
		for ( let i = 1; i < 3; i++ ) {
			let teamIndex = i.toString();
			totalPoints[teamIndex] = ( totalPoints[teamIndex] > gameTypeData.struct.settings.maxPointsGame ) ? gameTypeData.struct.settings.maxPointsGame : totalPoints[teamIndex];
			//
			matchData.mPoints.t[teamIndex].t = totalPoints[teamIndex];
			//
			teamsPointList.querySelector('tfoot td[se-elem="side_' + teamIndex + '_total"]').se_text(totalPoints[teamIndex]);
		}

		//
		let saveData = {
				mPoints:se.object.copy(matchData.mPoints),
				mStatus:4,
				sync:1
			};

		//
		if ( matchData.t1_side !== 0 && matchData.t2_side !== 0 ) {
			// Real point designation according to side
			saveData.t1_points = matchData.mPoints.t[matchData.t1_side].t;
			saveData.t2_points = matchData.mPoints.t[matchData.t2_side].t;

			//
			if ( saveData.t1_points === saveData.t2_points ) {
				saveData.t1_status = 2;
				saveData.t2_status = 2;
			} else {
				saveData.t1_status = ( saveData.t1_points > saveData.t2_points ) ? 3 : 1;
				saveData.t2_status = ( saveData.t1_points < saveData.t2_points ) ? 3 : 1;
			}

			//
			app.idb.update('game_matches', matchData.index, saveData, {
				onSuccess: (data) => {
					console.log("MATCH DATA UPDATED");
					//
					matchForm.se_formElVal('mStatus', 4);
					// Actualizar
					teamsForm.querySelector('td[se-elem-replace="t1_points"]').se_text(saveData.t1_points);
					teamsForm.querySelector('td[se-elem-replace="t2_points"]').se_text(saveData.t2_points);
					teamsForm.se_formElVal('t1_status', saveData.t1_status);
					teamsForm.se_formElVal('t2_status', saveData.t2_status);
				}
			});
		}


	}

	//
	function matchPointsUpdate() {

		//
		for ( let i = 1; i < 3; i++ ) {
			let teamIndex = i.toString();

			//
			for ( let cPointsId in matchPoints.t[teamIndex].p ) {
				if ( !matchPoints.t[teamIndex].p.hasOwnProperty(cPointsId) ) { continue; }

				//
				let cPointData = matchPoints.t[teamIndex].p[cPointsId],
					cRow = teamsPointListContent.querySelector('tr[data-id="' + cPointsId + '"]');

				//
				cRow.querySelector('input[data-side="' + i + '"]').value = cPointData.c;
				cRow.querySelector('td[data-side="' + i + '"]').se_text(cPointData.t);
			}
		}

		// Update totals
	}

	//
	function printUserPoints() {
		if ( matchData.mPoints ) {
			for ( let cUserId in matchData.mPoints.u ) {
				if ( !matchData.mPoints.hasOwnProperty(cUserId) ) { continue; }
				let uPoints = matchData.mPoints.u[cUserId];
				playerPointListContent.querySelector('tr[data-id="' + cUserId + '"] td[se-elem="points"]').se_text(uPoints);
			}
		}
	}

	//
	function actionDel(actId) {
		matchEvents.splice(actId, 1);
		console.log(matchEvents);
		runGameSimulation();
	}

	//
	init();
	return {};
};
