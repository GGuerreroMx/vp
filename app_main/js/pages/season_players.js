﻿"use strict";
se.plugin.uwp_page_season_players = function ( plugElem, plugOptions ) {
	let table = plugElem.querySelector( 'table[se-elem="registered"]' ),
		tableTemplate = table.querySelector( 'template' ),
		tableContent = table.querySelector( 'tbody' ),
		//
		temp2 = '';

	//
	function init() {
		console.log("PAGE - Season players:");

		//
		app.idb.query('season_players', {
			index:'oType',
			keyRange:IDBKeyRange.only(3),
			onSuccess: function ( data ) {
				let cIndex, cData,
					updatesString = tableTemplate.se_html();
				// Empty
				tableContent.se_empty();
				// Sort
				// data = se.object.sort( data, 'dtAddUnix', 'DESC' );
				// Print
				for ( cIndex in data ) {
					if ( !data.hasOwnProperty( cIndex ) ) { continue; }
					// Asignar y procesar
					cData = data[cIndex];

					// Print
					tableContent.se_append( se.struct.stringPopulate( updatesString, cData ) );
				}
			},
			onEmpty: function () {
				console.log( "No content available." );
			}
		} );
		//
		plugElem.se_on( 'click', 'button[se-act]', btnActions );
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		switch ( cBtn.se_attr( 'se-act' ) ) {
			// Mostrar todos los resultados
			case 'showAll':
				showAll();
				break;
			// Ocultar resultados
			case 'hideAll':
				hideAll();
				break;
			//
			default:
				console.log( "Boton no programado", cBtn );
				break;
		}
	}

	//
	function hideAll() {
		// Empty
		tableContent.se_empty();
	}

	//
	function showAll() {

	}

	//
	init();
	return {};
};
