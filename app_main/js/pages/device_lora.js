﻿"use strict";
se.plugin.uwp_page_device_lora = function ( plugElem, plugOptions ) {
	let deviceLog = plugElem.querySelector('div[se-elem="deviceLog"]'),
		deviceSendForm = plugElem.querySelector('form[se-elem="serialInput"]'),
		//
		device,
		testInterval;

	//
	function init() {
		console.log("usb lora init");
		let params = se.url.str_parse(window.location.search);

		//
		plugElem.se_on( 'click', 'button[se-act]', btnActions );
		deviceSendForm.se_on('submit', serialSend);
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		//
		const cAction = cBtn.se_attr( 'se-act' );
		switch ( cAction ) {
			//
			case 'serial_reload':
				app.loraUsb.connect();
				break;
			//
			case 'serial_test_start':
				sendTestMessages();
				testInterval = setInterval(sendTestMessages, 4000);
				break;
			//
			case 'serial_test_stop':
				clearInterval(testInterval);
				break;
			//
			case 'log_clear':
				deviceLog.innerHTML = '';
				break;
			//
			default:
				console.error("Boton no programado: ", cAction, cBtn);
				break;
		}
	}
	// Debugging WS
	//


let testMessages = [
		[102, 1, 115, 129, 0, 0, 0, 0, 0, 9, 6, 133, 134, 132, 132, 132, 0, 0, 0, 98, 1, 115, 1, 110, 77, 65, 84, 67, 72, 32, 83, 84, 65, 82, 84, 32, 116, 6, 0],
		[102, 1, 115, 129, 0, 0, 0, 0, 0, 9, 6, 133, 134, 132, 132, 132, 0, 0, 0, 98, 9, 115, 5, 110, 83, 99, 111, 117, 116, 32, 32, 32, 32, 32, 32, 32],
		[102, 1, 115, 130, 0, 0, 0, 0, 0, 9, 6, 133, 134, 132, 132, 132, 0, 0, 0, 115, 4, 98, 4, 110, 80, 114, 105, 109, 101, 114, 97, 32, 32, 32, 32, 32, 116, 30, 0],
		[102, 1, 115, 130, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 132, 132, 0, 0, 0],
		[102, 1, 115, 130, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 132, 132, 0, 0, 0, 115, 6],
		[102, 1, 115, 130, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 133, 132, 0, 0, 0],
		[102, 1, 115, 130, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 133, 132, 0, 0, 0, 115, 4, 110, 83, 101, 103, 117, 110, 100, 97, 32, 32, 32, 32, 32, 98, 4, 116, 30, 0],
		[102, 1, 115, 4, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 133, 132, 0, 0, 0, 98, 3, 115, 3, 110, 77, 65, 84, 67, 72, 32, 69, 78, 68, 32, 32, 32],
		[102, 1, 115, 0, 0, 0, 0, 0, 0, 9, 6, 133, 134, 132, 132, 132, 0, 0, 0, 98, 0, 110, 70, 73, 69, 76, 68, 32, 82, 69, 65, 68, 89, 32],
	],
	testCurrent = 0;

	//
	function sendTestMessages() {
		if ( testCurrent >= testMessages.length ) { testCurrent = 0; }

		console.warn("SENDING TEST MESSAGE TO FIELD 1.");

		app.loraUsb.sendByteArrayMessage(testMessages[testCurrent])
		testCurrent++;
	}

	//
	function serialSend(e) {
		e.preventDefault();
		let string = deviceSendForm.se_formElVal('content');
		app.loraUsb.writeToStream(string);
		deviceSendForm.reset();
	}

	//
	function logAdd(type, text) {
		deviceLog.textContent+= ( type === 'in') ? '↓' : '↑';
		deviceLog.textContent+= "\t" + text + "\n";
		deviceLog.scrollTop = deviceLog.scrollHeight;
	}

	//
	init();
	return {
		logAdd:logAdd
	};
};
