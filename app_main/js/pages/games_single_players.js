﻿"use strict";
se.plugin.uwp_page_games_single_players = function ( plugElem, plugOptions ) {
	let gameTemplate = plugElem.querySelector('template'),
		gameContent = plugElem.querySelector('[se-elem="content"]'),
		//
		temp2 = '';

	//
	function init() {
		let params = se.url.str_parse(window.location.search);
		console.log("PAGE: Games single players init.");
		//
		gameContent.se_empty();
		app.idb.query('players', {
			index:'gId',
			keyRange:IDBKeyRange.only(intVal(params.gId)),
			onSuccess:(data) => {
				gameContent.se_append(se.struct.stringPopulateMany(gameTemplate.se_html(), data));
			}
		});

		// Bindings
		gameContent.se_on('change', 'input', playerUpdate);
	}

	//
	function playerUpdate(e, cEl) {
		let playerId = parseInt(cEl.se_closest('tr').se_data('id'));

		//
		app.idb.update('players', playerId, { eventId:parseInt(cEl.value), sync:1 }, {
			onSuccess:(data) => {
				console.log("datos jugador actualizados.");
			}
		});
	}

	//
	init();
	return {};
};
