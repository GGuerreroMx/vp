﻿"use strict";
se.plugin.uwp_page_server_field = function ( plugElem, plugOptions ) {
	let actionsForm = plugElem.querySelector('form[se-elem="actions"]'),
		actionsFormSubmit = actionsForm.querySelector('button[type="submit"]'),
		actionsCreator = plugElem.querySelector('div[se-elem="actionsCreator"]'),
		actionsObjectives = actionsForm.querySelector('div[se-elem="objective"]'),
		actionsSide = actionsForm.querySelector('div[se-elem="side"]'),
		actionsStatus = actionsForm.querySelector('div[se-elem="status"]'),
		//
		gameTextData = plugElem.querySelector('div[se-elem="data"]'),
		//
		statusSection = plugElem.querySelector('div[se-elem="gameStatus"]'),
		statusSectionContent = statusSection.querySelector('div[se-elem="content"]'),
		//
		actionsDevices = actionsForm.querySelector('div[se-elem="devices"]'),
		actionsDevicesTemplate = actionsDevices.querySelector('template'),
		actionsDevicesContent = actionsDevices.querySelector('div[se-elem="content"]'),
		actionsTimes = actionsForm.querySelector('div[se-elem="time"]'),
		actionsPlayers = actionsForm.querySelector('select[name="userId"]'),
		//
		editEventDiag = plugElem.querySelector('dialog[se-elem="editEvent"]'),
		editEventForm = editEventDiag.querySelector('form'),
		//
		actionChronicle = plugElem.querySelector('div[se-elem="chronicle"]'),
		actionChronicleTemplate = actionChronicle.querySelector('template'),
		actionChronicleContent = actionChronicle.querySelector('div[se-elem="content"]'),
		actionsList = plugElem.querySelector('div[se-elem="ev_list"]'),
		actionsListTemplate = actionsList.querySelector('template'),
		actionsListContent = actionsList.querySelector('tbody[se-elem="content"]'),
		//
		gameMap = plugElem.querySelector('div[se-elem="map"]'),
		gameMapTempalte = gameMap.querySelector('template'),
		gameMapContent = gameMap.querySelector('div[se-elem="content"]'),
		//
		playerPointList = plugElem.querySelector('table[se-elem="playerPoints"]'),
		playerPointListTemplate = playerPointList.querySelector('template'),
		playerPointListContent = playerPointList.querySelector('tbody[se-elem="content"]'),
		//
		game_status = plugElem.querySelector('div[se-elem="status"]'),
		game_text = game_status.querySelector('div[se-elem="text"]'),
		game_status_section_c = game_status.querySelector('span[se-elem="section_current"]'),
		game_timertotal_text = game_status.querySelector('span[se-elem="timeText_total"]'),
		//
		game_timer = plugElem.querySelector('div[se-elem="timer"]'),
		game_timer_progress = game_timer.querySelector('div.bar'),
		game_timer_text = game_timer.querySelector('div.timeText'),
		//
		game_score = plugElem.querySelector('div[se-elem="scoreboard"]'),
		//
		game_action_buttons = plugElem.querySelector('div[se-elem="timerAct"]'),
		//
		actionsObj = plugElem.querySelector('table[se-elem="actions"]'),

		//
		matchData, gameData, playerList = {},
		team_1_data, team_2_data,
		matchEvents = [],
		matchTimeTotal,
		//
		timerObj = null, timerTotalObj = null,
		timeStart = 0,
		timerTargetTime, timerTargetTimeTotal,
		timePauseIni = 0,
		//
		fieldData, gameTypeData,
		fieldId,
		fieldStatus = {
			active:0,
			fase:0,
			pause:0
		},
		//
		audioContext = AudioContext && new AudioContext(),
		audioBeeps = [
			// Defualts
			[100, 50, 100, 50, 100],                        // 00 - Field Reset
			[300, 150, 300, 150, 300],                      // 01 - Game Start
			[500, 500, 500, 500, 500, 500, 500, 500, 500],  // 02 - Pause
			[500, 500, 500, 500, 3000],                     // 03 - Game Stop
			[2000],                                         // 04 - Insertion
			//
			[450, 100, 450],                                // 05 - ?
			[500, 500, 500],                                // 06 - ?
			[200, 50, 200],                                 // 07 - No
			[2000],                                         // 08 - Single Long
			[500],                                          // 09 - Single short
		];

	//
	function init() {
		let params = se.url.str_parse(window.location.search);
		//
		fieldId = intVal(params.id);

		console.log( "PAGE - SERVER FIELDS" );

		// Información del juego
		app.idb.query('server_fields', {
			get: fieldId,
			onSuccess:(data) => {
				console.log("MAIN - SERVER FIELD - SERVER FIELD DATA", data);

				fieldData = data;

				// Navegación
				let navigation = plugElem.querySelector('div[se-elem="navigation"]');
				se.uwp.elementTemplateProc(navigation, {
					id:params.id
				});

				// Status inicial
				plugElem.se_data('type', fieldData.type);

				//
				se.uwp.sectionSetValues(plugElem, data);

				//
				let queries = [
					{
						rName:'gametypes',
						table:'gametypes',
						params: {
							get:fieldData.gtId,
						}
					}
				];

				// Procesar tipo de juego y jugadores
				app.idb.queryBatch(queries, {
					onSuccess:(results) => {
						//
						console.log("MAIN - SERVER FIELD - GAME TYPES", results);
						gameTypeData = results['gametypes'];

						// Llenar status
						for ( let i = 0; i < gameTypeData.struct.properties.gameStatus.length; i++ ) {
							let cStatus = gameTypeData.struct.properties.gameStatus[i].data;

							//
							if ( cStatus.statusValueType === 'bool' ) {
								//
								statusSectionContent.se_append(se.struct.stringPopulate(statusSection.querySelector('template[se-elem="boolean"]').se_html(), {
									index: i,
									title: cStatus.title
								}));
							} else {
								//
								statusSectionContent.se_append(se.struct.stringPopulate(statusSection.querySelector('template[se-elem="number"]').se_html(), {
									index: i,
									title: cStatus.title
								}));
							}
						}

						// Llenar mapa y formulario de acciones
						for ( let i = 0; i < gameTypeData.struct.properties.devices.length; i++ ) {
							let cDev = gameTypeData.struct.properties.devices[i].data;
							// Mapa
							gameMapContent.se_append(se.struct.stringPopulate(gameMapTempalte.se_html(), {
								id: i,
								title: cDev.title,
								type: cDev.devType,
								mapicon: cDev.mapicon,
								mapx: cDev.mapx,
								mapy: cDev.mapy,
								active: cDev.devActiveDef,
								side: cDev.devSideDef,
								status: cDev.devStatusDef,
							}));

							// Saltar en lista la base
							if ( cDev.devType === 'base' ) { continue; }
							//
							actionsDevicesContent.se_append(se.struct.stringPopulate(actionsDevicesTemplate.se_html(), {
								id: i,
								title: cDev.title,
								type: cDev.devType,
								icon: cDev.mapicon
							}));
						}

						// Actualizaciones según el match

						// ajuste variables
						matchTimeTotal = gameTypeData.struct.settings.timeGameInt;
						actionChronicle.querySelector('.end').se_text(secondsToTime(matchTimeTotal));

						// Poner eventos
						matchEvents = fieldData.data.eventsUser;
						reDrawEvents();

						// Puntos
						game_score.querySelector('div[se-score="1"]').se_text(fieldData.data.points.t['1'].t);
						game_score.querySelector('div[se-score="2"]').se_text(fieldData.data.points.t['2'].t);

						// Status
						for ( let cStatusIndex in fieldData.data.status ) {
							if ( !fieldData.data.gameStatus.hasOwnProperty(cStatusIndex) ) { continue; }
							//
							let cStatusObj = fieldData.data.gameStatus[cStatusIndex],
								cStatusInput = statusSection.querySelector('label[data-index="'+cStatusIndex+'"] input');

							//
							switch ( cStatusInput.type ) {
								case 'checkbox':
									cStatusInput.checked = ( cStatusObj.value );
									break;
								case 'number':
									cStatusInput.value = cStatusObj.value;
									break;
								default:
									console.error("Valor incorrecto", cStatusInput);
									break;
							}
						}

						// Actualizar dispositivos
						for ( let cDeviceIndex in fieldData.data.devices ) {
							if ( !fieldData.data.devices.hasOwnProperty(cDeviceIndex) ) { continue; }
							let cDev = fieldData.data.devices[cDeviceIndex];
							//
							updateDevice(cDeviceIndex, cDev.active, cDev.side, cDev.status);
						}

						// Vista general
						updatePage(fieldData.data.status.active, fieldData.data.status.fase, fieldData.data.section, gameTypeData.struct.settings.sectionsTotal);
					},
					onFail:() => {}
				});

			}
		});

		// Bindings
		plugElem.se_on('click', '[se-act]', btnActions);

		// Actions
		actionsDevicesContent.se_on('click', 'div.object', selectDevice);
		actionsSide.se_on('click', 'div.object', selectSide);
		actionsStatus.se_on('click', 'div.object', selectStatus);
		actionsForm.se_on('submit', actionSubmit);
		//
		editEventForm.se_on('submit', actionUpdate);
		statusSection.se_on('change', 'input', gameStatusUpdate);
	}

	//
	function gameStatusUpdate(e, cEl) {
		e.preventDefault();
		let value;

		//
		switch ( cEl.type ) {
			case 'checkbox':
				value = ( cEl.checked ) ? 1 : 0;
				break;
			case 'number':
				value = intVal(cEl.value);
				break;
			default:
				console.error("Game status update invalid...", cType);
				return;
				break;
		}

		//
		serverMessageSend(['gameStatus', intVal(cEl.se_data('index')), value]);
	}

	//<editor-fold desc="Event Form">
	//
	function selectDevice(e, cEl) {
		let devId = cEl.se_data('id'),
			status = intVal(cEl.se_data('status')),
			side = intVal(cEl.se_data('side')),
			active = intVal(cEl.se_data('active'));
		//
		if ( !active ) {
			return;
		}
		actionsDevicesContent.querySelectorAll('div.object').se_attr("aria-selected", "false");
		cEl.se_attr("aria-selected", "true");
		actionsForm.se_formElVal('deviceId', devId);

		// Reset
		actionsForm.se_formElVal('side', side);
		actionsForm.se_formElVal('status', status);
		actionsSide.querySelectorAll('div.object').se_attr("aria-selected", "false");
		actionsStatus.querySelectorAll('div.object').se_attr("aria-selected", "false");

		//
		switch ( cEl.se_data('type') ) {
			//
			case 'opflag':
			case 'swflag':
			case 'opswflag':
			case 'ctflag':
			case 'endflag':
				actionsStatus.se_hide();
				//
				actionsSide.se_show();
				actionsSide.querySelectorAll('.object').se_attr('aria-hidden', 'false');
				actionsSide.querySelectorAll('.object[data-side="' + side + '"]').se_attr('aria-hidden', 'true');
				break;
			//
			case 'ctspot':
				actionsSide.se_hide();
				//
				actionsStatus.se_show();
				actionsStatus.querySelectorAll('.object').se_attr('aria-hidden', 'false');
				actionsStatus.querySelectorAll('.object[data-status="' + status + '"]').se_attr('aria-hidden', 'true');
				break;
			//
			default:
				console.error("MAIN - SERVER FIELD - SELECTED DEVICE NOT PROGRAMMED", cEl.se_data('type'), cEl);
				break;
		}
	}

	//
	function selectSide(e, cEl) {
		let side = cEl.se_data('side');
		actionsSide.querySelectorAll('div.object').se_attr("aria-selected", "false");
		cEl.se_attr("aria-selected", "true");
		actionsForm.se_formElVal('side', side);
		console.log(cEl, side);
	}

	//
	function selectStatus(e, cEl) {
		let status = cEl.se_data('status');
		actionsStatus.querySelectorAll('div.object').se_attr("aria-selected", "false");
		cEl.se_attr("aria-selected", "true");
		actionsForm.se_formElVal('status', status);
		console.log(cEl, status);
	}

	//
	function playersAppend(teamId, teamTitle, players) {
		let pad = "000",
			cPlayer,
			cPlayerId, cPlayerIdStr, cPlayerIdPad,
			opt,
			templateStr = playerPointListTemplate.se_html();
		//
		for ( let i = 0; i < players.length; i++ ) {
			cPlayer = players[i];
			//
			cPlayerId = parseInt(cPlayer.eventId);
			cPlayerIdStr = "" + cPlayerId;
			cPlayerIdPad = pad.substring(0, pad.length - cPlayerIdStr.length) + cPlayerIdStr;

			// Action form players
			opt = document.createElement('option');
			opt.se_data('teamId', teamId);
			opt.value = cPlayerId;
			opt.innerHTML = cPlayerIdPad + ' - [' + teamTitle+ '] ' + cPlayer.title;
			actionsPlayers.appendChild(opt);

			// Player list table
			playerPointListContent.se_append(se.struct.stringPopulate(templateStr, {
				teamId:teamId,
				id:cPlayerId,
				idPad:cPlayerIdPad,
				title:cPlayer.title
			}));

			// Player list object
			playerList[cPlayerId] = cPlayer.uName;
		}
	}

	//
	function updateSides() {
		let t1_index = matchData.t1_index,
			t1_side = matchData.t1_side,
			t2_index = matchData.t2_index,
			t2_side = matchData.t2_side;
		//
		actionsPlayers.querySelectorAll('option[data-teamindex="'+t1_index+'"]').se_data('side', t1_side);
		playerPointListContent.querySelectorAll('tr[data-teamindex="'+t1_index+'"]').se_data('side', t1_side);
		actionsPlayers.querySelectorAll('option[data-teamindex="'+t2_index+'"]').se_data('side', t2_side);
		playerPointListContent.querySelectorAll('tr[data-teamindex="'+t2_index+'"]').se_data('side', t2_side);
	}

	//</editor-fold>

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		let id;
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'm_start':
				if ( fieldStatus.fase === 0 || confirm("¿Habilitar pausa?\nSolo usar en casos excepcionales.") ) {
					serverMessageSend(['setup', 'start']);
				}
				break;
			//
			case 'm_stop':
				if ( confirm("¿Cerrar manualmente la partida?\nSolo usar en casos excepcionales.") ) {
					serverMessageSend(['setup', 'stop']);
				}
				break;
			//
			case 'm_reset':
				if ( fieldData.type === 'live' || confirm("¿Reiniciar?\nProvocará que se borren todos los puntos.\nSolo usar en casos excepcionales.") ) {
					serverMessageSend(['setup', 'reset']);
				}
				break;
			//
			case 'm_next':
				serverMessageSend(['setup', 'next']);
				break;
			//
			case 'actionEdit':
				id = cBtn.se_closest('tr').se_data('index');
				actionEdit(id);
				break;
			//
			case 'actionDel':
				id = cBtn.se_closest('tr').se_index();
				actionDel(id);
				break;
			//
			default:
				console.log("Boton no programado", cBtn);
				break;
		}
	}

	//
	function actionSubmit(e) {
		// Bloquear si no esta corriendo juego
		if ( !fieldStatus.active || fieldStatus.fase === 0 ) {
			console.log("No debería poderse hacer submit ja");
		}


		e.preventDefault();
		let actionData = se.form.serializeToObj(actionsForm, false, {'int':['deviceId', 'side', 'status']});
		// Proper conversion
		actionData.userId = 0;

		//
		serverMessageSend(['device', actionData.deviceId, actionData.side, actionData.status]);

		// Reset form
		actionsDevicesContent.querySelectorAll('div.object').se_attr("aria-selected", "false");
		actionsSide.querySelectorAll('div.object').se_attr("aria-selected", "false");
		actionsStatus.querySelectorAll('div.object').se_attr("aria-selected", "false");
		actionsForm.reset();
		//
		actionsStatus.se_hide();
		actionsSide.se_hide();
	}

	//
	function actionEdit(evId) {
		let cEventData = matchEvents[evId];
		console.log("event", cEventData);
		if ( cEventData.type !== 'device' ) { return; }

		//
		se.form.loadData(editEventForm,  se.object.merge( {id:evId}, cEventData ) );
		editEventDiag.showModal();
	}

	//
	function actionUpdate(e) {
		e.preventDefault();
		let eData = se.form.serializeToObj(editEventForm, false, {'int':['id', 'devUser']});

		// Send information to server (update in server), return success
		//
		serverMessageSend(['event_update', eData.id, eData.devUser]);

		/*
		console.log("PRE\n\n\n", matchEvents[eData.id], eData);
		//
		matchEvents[eData.id].devUser = parseInt(eData.devUser);
		console.log("post", matchEvents[eData.id]);
		*/
		//
		editEventDiag.close();
	}

	//
	function reRunGameSimulation() {
		let cEvents = [],
			cEvent,
			cIndex,
			valid = true,
			matchPoints = {};

		// Set enviroment
		plugElem.se_data('active', fieldData.data.status.active);
		plugElem.se_data('section', fieldData.data.status.sectionC);

		/*
		// Determinar si hay contador
		if ( fieldData.data.status.active ) {

		}

		// Reset everything
		for ( let cPlayerId in playerList ) {
			if ( !playerList.hasOwnProperty(cPlayerId) ) { continue; }
			matchPoints[cPlayerId] = 0;
		}

		//
		function actionOperation(actionList, cEvent) {
			//
			for ( let cAction of actionList ) {
				switch ( cAction.type ) {
					// Ignoring non realtime activities
					case 'buzzer':
					case 'sound':
					case 'respawn':
					case 'note':
					case 'timer':
					case 'timeAlternate':
						break;
					// Actuable actions
					case 'devMod':
						break;
					case 'point':
						let pointData = gameTypeData.struct.points[cAction.pointId],
							cDeviceField = fieldData.devices[cEvent.deviceId];
						// console.log("cEvent", cEvent);
						// console.log("action", cAction);
						// console.log("point", pointData);
						// console.log("device", cDeviceField);
						// console.log("end...\n\n\n");
						// Ignorar punto,
						// if ( pointData.target !== 1 ) { continue; }

						// Determinar punto objetivo
						switch ( pointData.target ) {
							// Equipo general
							case 1:
								switch ( cAction.pointType ) {
									//
									case 'simple':
										fieldData.points.partial[cAction.pointId][cDeviceField.side]['c']++;
										fieldData.points.partial[cAction.pointId][cDeviceField.side]['t'] += gameTypeData.struct.points[cAction.pointId].points;
										fieldData.points.total[cDeviceField.side] += gameTypeData.struct.points[cAction.pointId].points;
										break;
									//
									case 'cyclic':
										let missing = true,
											cycleEventIndex,
											tempCycleIndex,
											tempCycleObj,
											tempDeviceIndex,
											tempDeviceObj;

										//
										for ( tempCycleIndex in fieldData.eventsTimed.cyclic ) {
											if ( !fieldData.eventsTimed.cyclic.hasOwnProperty(tempCycleIndex) ) {
												continue;
											}
											tempCycleObj = fieldData.eventsTimed.cyclic[tempCycleIndex];
											//
											for ( tempDeviceIndex in tempCycleObj ) {
												if (!tempCycleObj.hasOwnProperty(tempCycleIndex)) {
													continue;
												}
												tempDeviceObj = tempCycleObj[tempCycleIndex];

												// Validation
												if ( tempDeviceObj.deviceId === devId && tempDeviceObj.cycleObj[tempCycleObj.cycleCount][cDeviceField.side] ) {
													cycleEventIndex = tempCycleIndex;
													missing = false;
													break;
												}
											}

											//
											if (!missing) {
												break;
											}
										}

										//
										if ( missing ) {
											// console.log(cDeviceField, fieldData.points.partial[cAction.pointId][cDeviceField.side]);
											// Points update
											fieldData.points.partial[cAction.pointId][cDeviceField.side]['c']++;
											fieldData.points.partial[cAction.pointId][cDeviceField.side]['t'] += gameTypeData.struct.points[cAction.pointId].points;
											fieldData.points.total[cDeviceField.side] += gameTypeData.struct.points[cAction.pointId].points;
										}
										break;
									// Do not include switch
									case 'cyclic_exclusive':
										break;
									//
									default:
										console.log("");
										break;
								}
								break;
							// División (big games only
							case 2:
								break;
							// Jugador individual
							case 3:
								console.log("objetivo un jugador individual", matchPoints[cEvent.userId], cAction);
								// Puntos para un usuario
								matchPoints[cEvent.userId] += gameTypeData.struct.points[cAction.pointId].points;
								console.log("FIN", matchPoints);
								break;
						}
						//
						break;
					case 'gameEnd':
						break;
					default:
						console.log("Operación de acciones... caso no reconocido..", cAction.type, cAction);
						break;
				}
			}
		}

		//
		function eTriggerOps() {

		}

		//<editor-fold desc="Load events">
		// Agregar eventos de usuarios
		for ( cEvent of matchEvents ) {
			cEvents.push({
				time:cEvent.timeInt,
				type:'user',
				deviceId:cEvent.deviceId,
				side:cEvent.side,
				userId:cEvent.userId
			});
		}

		// Agregar eventos tradicionales
		for ( cIndex in fieldData.eventsTimed.list ) {
			if ( !fieldData.eventsTimed.list.hasOwnProperty(cIndex) ) { continue; }
			cEvent = fieldData.eventsTimed.list[cIndex];
			// Avoid preparations
			if ( cEvent.time < 0 ) { continue; }
			// Count max events
			cEvents.push({
				time: cEvent.time,
				type: 'list',
				index: cIndex
			});
		}

		// Agregar eventos de tiempo cíclicos
		for ( cIndex in fieldData.eventsTimed.cyclic ) {
			if ( !fieldData.eventsTimed.cyclic.hasOwnProperty(cIndex) ) { continue; }
			// Count max events
			let maxEvents = gameTypeData.struct.settings.timeGameInt / cIndex;
			// Create markers only
			for ( let i = 0; i < maxEvents; i++ ) {
				cEvents.push({
					time: i * cIndex,
					type: 'cyclic',
					index: cIndex
				});
			}
		}
		//</editor-fold>

		// Ordenar tiempos
		cEvents = se.object.sort(cEvents, 'time', 'ASC');

		console.log("Game Events", cEvents);

		// Ejecutar simulación de puntos.
		for ( cEvent of cEvents ) {
			let timeEventData, eventData;
			switch ( cEvent.type ) {
				// Eventos automáticos
				case 'list':
					timeEventData = fieldData.eventsTimed.list[cEvent.index];
					eventData = gameTypeData.struct.properties[timeEventData.type][timeEventData.index];
					//
					actionOperation(eventData.actions);
					break;

				// Eventos cíclicos (puntajes principalmente)
				case 'cyclic':
					timeEventData = fieldData.eventsTimed.cyclic[cEvent.index];
					if ( timeEventData.devices ) {
						// console.log("GAMEDATA", gameTypeData);
						// console.log("CFIELD", fieldData);
						for ( let cDevObjIndex in timeEventData.devices ) {
							if ( !timeEventData.devices.hasOwnProperty(cDevObjIndex) ) { continue; }
							let cDevObj = timeEventData.devices[cDevObjIndex],
								cDeviceField = fieldData.devices[cDevObj.deviceId],
								cDeviceOriginal = gameTypeData.struct.properties.devices[cDevObj.deviceId];

							// Determinar si esta activo, sino saltarlo
							if ( cDeviceField.active === 0 ) { continue; }

							//
							// console.log("Cycle, cDevice", cDevObj, cDeviceField, cDeviceOriginal);

							//
							switch ( cDeviceField.type ) {
								case 'swflag':
									if ( cDeviceField.side !== 0 ) {
										// Actualizar el status
										// console.log(fieldData.eventsTimed.cyclic[cEvent.index].devices[cDevObjIndex].cycleObj, fieldData.eventsTimed.cyclic[cEvent.index].cycleCount);
										fieldData.eventsTimed.cyclic[cEvent.index].devices[cDevObjIndex].cycleObj[fieldData.eventsTimed.cyclic[cEvent.index].cycleCount][cDeviceField.side] = 1;

										//
										fieldData.points.partial[cDevObj.pointId][cDeviceField.side]['c']++;
										fieldData.points.partial[cDevObj.pointId][cDeviceField.side]['t']+= gameTypeData.struct.points[cDevObj.pointId].points;
										fieldData.points.total[cDeviceField.side] += gameTypeData.struct.points[cDevObj.pointId].points;
									}
									break;
								case 'opswflag':
									if ( cDeviceField.side !== cDeviceOriginal.data.devSideDef ) {
										// Actualizar el status
										fieldData.eventsTimed.cyclic[cEvent.index].devices[cDevObjIndex].cycleObj[fieldData.eventsTimed.cyclic[cEvent.index].cycleCount][cDeviceField.side] = 1;

										//
										fieldData.points.partial[cDevObj.pointId][cDeviceField.side]['c']++;
										fieldData.points.partial[cDevObj.pointId][cDeviceField.side]['t']+= gameTypeData.struct.points[cDevObj.pointId].points;
										fieldData.points.total[cDeviceField.side] += gameTypeData.struct.points[cDevObj.pointId].points;
									}
									break;
								case '':
									break;
								default:
									console.log("cyclo no reconocido...", cDeviceField);
									break;
							}
						}

						//
						fieldData.eventsTimed.cyclic[cEvent.index].cycleCount++;
					}
					break;

				// Acción sobre dispositivo
				case 'user':
					let deviceGral = gameTypeData.struct.properties.devices[cEvent.deviceId],
						deviceField = fieldData.devices[cEvent.deviceId];

					// console.log("ACCIÓN USUARIO");
					// console.log("EVENTO\n\n", cEvent);
					// console.log("DIS. GENERAL \n\n", deviceGral);
					// console.log("DIS. CAMPO\n\n", deviceField);
					// console.log("FIN\n\n");

					if ( deviceField.active === 0 ) {
						console.log("movimiento prohibido, esta desactivado el objeto.", deviceField, cEvent);
						return;
					}

					//
					switch ( deviceGral.data.devType ) {
						case 'base':
							break;
						//
						case 'swflag':
							if ( deviceField.side !== cEvent.side ) {
								fieldData.devices[cEvent.deviceId].side = cEvent.side;
							} else {
								console.log("movimiento prohibido, SWFLAG mismo lado", deviceField, cEvent);
								return;
							}
							break;
						//
						case 'opflag':
							if ( deviceField.side !== cEvent.side && deviceField.side === deviceGral.data.devSideDef ) {
								fieldData.devices[cEvent.deviceId].side = cEvent.side;
							} else {
								console.log("movimiento prohibido, OP FLAG", deviceField, cEvent);
								return;
							}
							break;
						//
						case 'opswflag':
							if ( deviceField.side !== cEvent.side  ) {
								fieldData.devices[cEvent.deviceId].side = cEvent.side;
							} else {
								console.log("movimiento prohibido, opswflag", deviceField, cEvent);
								return;
							}
							break;
						//
						case 'ctflag':
							if ( deviceField.side !== cEvent.side ) {
								fieldData.devices[cEvent.deviceId].side = cEvent.side;
							} else {
								console.log("movimiento prohibido, ctf", deviceField, cEvent);
								return;
							}
							break;
						case 'bomb_loc':
							break;
						case 'bomb_bomb':
							break;
					}

					// No hubo return, aprobar
					actionOperation(deviceGral.actions, cEvent);
					eTriggerOps();
					break;

				//
				default:
					console.log("ERROR DE CÓDIGO... NO PUEDE SER ESTE EL MUNDO EN EL QUE VIVIMOS...!");
					break;
			}
		}

		// Print (even if invalid)
		matchPointsPrint();
		matchEventsPrint();

		// Update user points
		matchData.mPoints = matchPoints;
		printUserPoints();

		//
		// console.log("END status");
		// console.log("Field", fieldData);
		// console.log("MatchData", matchData);
		// console.log("\n\n");

		// Save if valid
		if ( valid ) {
			let saveData = {
					mEvents:matchEvents,
					mPoints:matchPoints
				},
				updateScore = false;

			//
			if ( matchData.t1_side !== 0 && matchData.t2_side !== 0 ) {
				saveData.t1_points = fieldData.points.total[matchData.t1_side];
				saveData.t2_points = fieldData.points.total[matchData.t2_side];
				//
				if ( saveData.t1_points === saveData.t2_points ) {
					saveData.t1_status = 2;
					saveData.t2_status = 2;
				} else {
					saveData.t1_status = ( saveData.t1_points > saveData.t2_points ) ? 3 : 1;
					saveData.t2_status = ( saveData.t1_points < saveData.t2_points ) ? 3 : 1;
				}
				//
				updateScore = true;
			}

			//
			app.idb.update('game_matches', matchData.index, saveData, {
				onSuccess: (data) => {
					console.log("MATCH DATA UPDATED");
					// Actualizar
					if ( updateScore ) {
						teamsForm.querySelector('td[se-elem-replace="t1_points"]').se_text(saveData.t1_points);
						teamsForm.querySelector('td[se-elem-replace="t2_points"]').se_text(saveData.t2_points);
						teamsForm.se_formElVal('t1_status', saveData.t1_status);
						teamsForm.se_formElVal('t2_status', saveData.t2_status);
					}
				}
			});
		}
		*/
	}

	//
	function actionDel(actId) {
		matchEvents.splice(actId, 1);
		console.log(matchEvents);
		reRunGameSimulation();
	}

	//
	function updatePage(sActive, sFase, sSectionC, sPause) {
		//
		plugElem.se_data('active', sActive);
		plugElem.se_data('fase', sFase);
		plugElem.se_data('sectionC', sSectionC);
		plugElem.se_data('pause', sPause);

		//
		game_status_section_c.se_text(sSectionC);

		// Operations
		if ( sActive ) {
			game_action_buttons.querySelector('button[se-act="m_start"]').disabled = false;
			game_action_buttons.querySelector('button[se-act="m_stop"]').disabled = false;
			game_action_buttons.querySelector('button[se-act="m_reset"]').disabled = true;
			game_action_buttons.querySelector('button[se-act="m_next"]').disabled = true;
			if ( sFase === 1 ) {
				actionsFormSubmit.disabled = true;
			} else {
				actionsFormSubmit.disabled = false;
			}

			if ( sPause ) {
				game_action_buttons.querySelector('button[se-act="m_start"] use').se_attr('xlink:href', '#fa-play');
			} else {
				game_action_buttons.querySelector('button[se-act="m_start"] use').se_attr('xlink:href', '#fa-pause');
			}

			// Start general timer
			if ( fieldStatus.active !== sActive ) {
				if ( timeStart === 0 ) {
					// First start
					timeStart = (new Date()).getTime();
					clearInterval(timerTotalObj);
					timerTotalObj = null;
					timerTotalObj = setInterval(timerTotal_advance, 1000);
					console.log("TIMER STARTED", timeStart);
				}
			} else {
				// Was running
				if ( fieldStatus.pause !== sPause ) {
					let timeCur = (new Date()).getTime();
					if ( sPause ) {
						clearInterval(timerObj);
						timerObj = null;
						clearInterval(timerTotalObj);
						timerTotalObj = null;

						timePauseIni = timeCur;
					} else {
						let addTime = timeCur - timePauseIni;

						timeStart += addTime;
						timerTargetTime += addTime;

						// Reinciar objetos contadores de tiempo
						timerObj = setInterval(timer_advance, 1000);
						timerTotalObj = setInterval(timerTotal_advance, 1000);

						// Reset pause
						timePauseIni = 0;
					}
				}
			}
		} else {
			actionsFormSubmit.disabled = true;
			game_action_buttons.querySelector('button[se-act="m_start"] use').se_attr('xlink:href', '#fa-play');
			game_action_buttons.querySelector('button[se-act="m_stop"]').disabled = true;

			timer_stop();
			if ( sFase === 0 ) {
				// Its a reset
				timerText_set(0);
				game_text.se_text('');
				actionChronicleContent.se_empty();
				actionsListContent.se_empty();
				//
				game_score.querySelector('[se-score="1"]').se_text(0);
				game_score.querySelector('[se-score="2"]').se_text(0);
				//
				matchEvents = [];

				for ( let i = 0; i < gameTypeData.struct.properties.devices.length; i++ ) {
					let cDev = gameTypeData.struct.properties.devices[i].data;

					//
					updateDevice(i, cDev.devActiveDef, cDev.devSideDef, cDev.devStatusDef);
				}

				//
				game_action_buttons.querySelector('button[se-act="m_start"]').disabled = false;
				game_action_buttons.querySelector('button[se-act="m_reset"]').disabled = true;
				game_action_buttons.querySelector('button[se-act="m_next"]').disabled = true;
			} else {
				// Not reseted
				game_action_buttons.querySelector('button[se-act="m_start"]').disabled = true;
				game_action_buttons.querySelector('button[se-act="m_reset"]').disabled = false;
				game_action_buttons.querySelector('button[se-act="m_next"]').disabled = false;
			}
		}

		// Guardar status
		fieldStatus.active = sActive;
		fieldStatus.fase = sFase;
		fieldStatus.pause = sPause;

		console.log("MAIN - SERVER FIELD - STATUS CHANGE", fieldStatus);
	}

	//
	function serverMessageRecieve(rFieldId, msg) {
		// Analize current field
		if ( fieldId !== rFieldId ) {
			console.log("Not this field. %s - %s", fieldId, rFieldId, typeof fieldId, typeof rFieldId);
			return;
		}

		//
		switch ( msg.type ) {
			//
			case 'status':
				console.log("PAGE UPDATE - Status", msg);

				// Status
				updatePage(msg.data.field.active, msg.data.field.fase, msg.data.field.sectionC, msg.data.field.pause);

				// Points
				game_score.querySelector('[se-score="1"]').se_text(msg.data.points['1'].t);
				game_score.querySelector('[se-score="2"]').se_text(msg.data.points['2'].t);

				// Devices
				for ( let cDevId in msg.data.devices ) {
					if ( !msg.data.devices.hasOwnProperty(cDevId) ) { return; }
					let cDevProp = msg.data.devices[cDevId];

					gameMapContent.querySelector('svg[data-oid="'+cDevId+'"]').se_data('active', cDevProp.active).se_data('side', cDevProp.side).se_data('status', cDevProp.status);
				}

				// gameStatus
				for ( let cGameStatusId in msg.data.gameStatus ) {
					if ( !msg.data.gameStatus.hasOwnProperty(cGameStatusId) ) { break; }
					let cGameStatus = msg.data.gameStatus[cGameStatusId];

					//
					let cGameStatusInput = statusSection.querySelector('label[data-index="'+cGameStatusId+'"] input');

					//
					switch ( cGameStatusInput.type ) {
						case 'checkbox':
							cGameStatusInput.checked = ( msg.data.value );
							break;
						case 'number':
							cGameStatusInput.value = msg.data.value;
							break;
						default:
							console.error("Valor incorrecto", cGameStatusInput);
							break;
					}
				}

				// Actions
				if ( msg.data.actions ) {
					for ( let cAction of msg.data.actions ) {
						switch ( cAction.type ) {
							// Ignorar sonidos
							case 'buzzer':
								if ( cAction.buzzSide === 0 ) {
									doBeep(parseInt(cAction.buzzType));
								}
								break;
							//
							case 'sound':
								break;
							//
							case 'timer':
								timer_set(parseInt(cAction.timeInt));
								break;
							//
							case 'note':
								game_text.se_text(cAction.note);
								break;
							//
							default:
								console.error("Field actions, action not valid", cAction);
								break;
						}
					}
				}
				break;

			//
			case 'event':
				console.log("PAGE UPDATE - Event", msg);
				// Agregar a la lista
				matchEvents.push(msg.data);
				// Recrear listas
				reDrawEvents();
				break;
			//
			case 'event_update':
				console.log("PAGE UPDATE - Event FIX", msg);
				matchEvents[msg.data.index].devSide = msg.data.devSide;
				// Recrear listas
				reDrawEvents();
				break;

			//
			case 'info':
				console.log("match current info", msg.data);
				game_score.querySelector('[se-team="1"]').se_text(msg.data.t1_name);
				game_score.querySelector('[se-team="2"]').se_text(msg.data.t2_name);
				gameTextData.querySelector('[se-elem-replace="cMatchIndex"]').se_text(msg.data.index);
				gameTextData.querySelector('[se-elem-replace="cMatchOrder"]').se_text(msg.data.mOrder);
				break;
			//
			case 'alert':
				console.log("Notify an error: ", msg.data);
				alert(msg.data);
				break;
			//
			default:
				console.log("error, not defined operation in main server app");
				break;
		}
	}

	//
	function updateDevice(devId, cActive, cSide, cStatus) {
		gameMapContent.querySelector('svg[data-oid="' + devId + '"]').se_data('active', cActive).se_data('side', cSide).se_data('status', cStatus);
		let cActDevice = actionsDevicesContent.querySelector('div.device[data-id="' + devId + '"]');
		if ( cActDevice ) {
			cActDevice.se_data('active', cActive).se_data('side', cSide).se_data('status', cStatus);
		}
	}

	//
	function reDrawEvents() {
		let chronTemplate = actionChronicleTemplate.se_html(),
			listTemplate = actionsListTemplate.se_html();
		//
		actionChronicleContent.se_empty();
		actionsListContent.se_empty();

		//
		for ( let cEventId in matchEvents ) {
			//
			if ( !matchEvents.hasOwnProperty(cEventId) ) { continue; }
			//
			let cEvent = matchEvents[cEventId],
				cSide = 0,
				dataContent,
				userIndex = '';

			// No va a mostrar eventos pre inicio de juego
			if ( cEvent.time < 0 ) { continue; }

			// console.log("MAIN - SERVER FIELD - cEvent", cEvent);

			//
			switch ( cEvent.type ) {
				//
				case 'automatic':
					let cEventTimedInfo = gameTypeData.struct.properties[cEvent.subtype][cEvent.id].data;
					// console.log("MAIN - SERVER FIELD - ADD Automatic event", cEventTimedInfo);
					dataContent = `${cEventTimedInfo.title}`;
					break;
				//
				case 'device':
					let gtDevice = gameTypeData.struct.properties.devices[cEvent.devId].data;

					//
					dataContent = `
<svg class="device icon inline mr" data-side="${cEvent.devSide}"><use xlink:href="#${gtDevice.mapicon}" /></svg> ${gtDevice.title}
`;
					//
					userIndex = cEvent.devUser;
					//
					cSide = cEvent.devSide;
					break;
			}

			//
			actionChronicleContent.se_append(se.struct.stringPopulate(chronTemplate, {
				id:cEventId,
				type:cEvent.type,
				side:cSide,
				relTime:cEvent.time / matchTimeTotal * 100
			}));

			//
			actionsListContent.se_append(se.struct.stringPopulate(listTemplate, {
				id:cEventId,
				type:cEvent.type,
				timeText:secondsToTime(cEvent.time),
				dataContent:dataContent,
				userIndex:userIndex
			}));
		}
	}


	//<editor-fold desc="Beeper">

	//amp:0..100, freq in Hz, ms
	function beep(amp, freq, ms) {
		if ( !audioContext ) return;
		let osc = audioContext.createOscillator(),
			gain = audioContext.createGain();
		osc.connect(gain);
		osc.frequency.value = freq;
		gain.connect(audioContext.destination);
		gain.gain.value = amp/100;
		osc.start(audioContext.currentTime);
		osc.stop(audioContext.currentTime + ms / 1000);
	}

	//
	function doBeep(beepNum) {
		if ( !audioBeeps.hasOwnProperty(beepNum) ) {
			console.error("requested beep does not exists");
		}

		//
		let sound = true,
			tTime = 0;
		for ( let cTime of audioBeeps[beepNum] ) {
			//
			if ( sound ) {
				setTimeout(() => { beep(10, 900, cTime) }, tTime);
			}
			sound = !sound;
			tTime+= cTime;
		}
	}

	//</editor-fold>

	//<editor-fold desc="Timer stuff">

	//
	function timer_set(timeInt) {
		let timeCur = (new Date()).getTime();
		timerTargetTime = timeCur + ( timeInt * 1000 );
		timerTargetTimeTotal = ( timeInt * 1000 );
		// Reset section timer (since it may not be exact seconds from other).
		if ( timerObj !== null ) {
			clearInterval(timerObj);
			timerObj = null;
		}
		timerObj = setInterval(timer_advance, 1000);
		//
		timerText_set(timeInt);
		timerTotalText_set(- gameTypeData.struct.settings.timeBreakInt);
	}

	//
	function timer_advance() {
		let timeCur = (new Date()).getTime(),
			timeDiff = Math.round( (timerTargetTime - timeCur) / 1000 );
		// Ejecutar tiempo
		timerText_set(timeDiff);
	}

	//
	function timerTotal_advance() {
		let timeCur = (new Date()).getTime(),
			timeTotal = Math.round( (timeCur - timeStart - ( gameTypeData.struct.settings.timeBreakInt * 1000 ) ) / 1000);
		// Ejecutar tiempo
		timerTotalText_set(timeTotal);
	}

	// Actualizar tiempos
	function timerText_set(mTime) {
		let perAdvance = 0,
			cClass;
		//
		if ( mTime > 0 ) {
			// game_timer_progress.se_classDel('end').se_classAdd('countdown');
			perAdvance = 100 - ( mTime / timerTargetTimeTotal * 100000); // 1 / 1000 (ms) * 100 (%) => 100000
			cClass = 'coundown'; // could be 'normal'
		} else {
			cClass = 'end';
		}
		//
		game_timer_text.se_text(secondsToTime(mTime));
		game_timer_progress.se_css('width', perAdvance + '%').se_attr('class', cClass);
	}

	//
	function timerTotalText_set(mTime) {
		// console.log("time total update...?", mTime);
		game_timertotal_text.se_text(secondsToTime(mTime));
	}

	//
	function timer_stop() {
		timeStart = 0;
		clearInterval(timerObj);
		timerObj = null;
		clearInterval(timerTotalObj);
		timerTotalObj = null;
	}

	//
	function secondsToTime(seconds) {
		// we will use this function to convert seconds in normal time format
		let secs = Math.abs(seconds),
			hr = Math.floor(secs / 3600),
			min = Math.floor((secs - (hr * 3600)) / 60),
			sec = Math.floor(secs - (hr * 3600) - (min * 60)),
			time = '';
		if ( hr !== 0 ) {
			if ( hr < 10 ) {
				time += '0';
			}
			time += hr + ':';
		}
		if ( min !== 0 ) {
			if ( min < 10 ) {
				time += '0';
			}
			time += min + ':';
		} else {
			time += '00:';
		}
		if ( sec < 10 ) {
			time += '0';
		}
		time += sec;
		if ( seconds < 0 ) {
			time = '-' + time;
		}
		return time;
	}

	//</editor-fold>

	//
	function serverMessageSend(data) {
		console.log("MP - Server Field %d. Sending data to server.", fieldId, data);
		// Enviar a server
		server.postMessage({
			'op_type':['field', 'command'],
			'op_data':[fieldId, ...data]
		});
	}

	//
	init();
	return {
		serverMessageRecieve:serverMessageRecieve
	};
};
