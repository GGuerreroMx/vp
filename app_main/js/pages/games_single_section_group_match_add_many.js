﻿"use strict";
se.plugin.uwp_page_games_single_section_group_match_add_many = function ( plugElem, plugOptions ) {
	let matchesCurrent = plugElem.querySelector('div[se-elem="matches_current"]'),
		matchesCurrentTemplate = matchesCurrent.querySelector('template'),
		matchesCurrentContent = matchesCurrent.querySelector('tbody[se-elem="content"]'),
		//
		objectListActive = plugElem.querySelector('div[se-elem="object_list_active"]'),
		objectListActiveTemplate = objectListActive.querySelector('template'),
		objectListActiveContent = objectListActive.querySelector('tbody[se-elem="content"]'),
		//
		objectListInactive = plugElem.querySelector('div[se-elem="object_list_inactive"]'),
		objectListInactiveTemplate = objectListInactive.querySelector('template'),
		objectListInactiveContent = objectListInactive.querySelector('tbody[se-elem="content"]'),
		//
		fieldsList = plugElem.querySelector('div[se-elem="fields"]'),
		fieldsListTemplate = fieldsList.querySelector('template'),
		fieldsListContent = fieldsList.querySelector('div[se-elem="content"]'),
		//
		matchCount = plugElem.querySelector('input'),
		//
		matchCreateOutput = plugElem.querySelector('output[se-elem="matchCreateOutput"]'),
		//
		matchesCandidate = plugElem.querySelector('div[se-elem="matchesCandidates"]'),
		matchesCandidateTemplate = matchesCandidate.querySelector('template'),
		matchesCandidateContent = matchesCandidate.querySelector('tbody[se-elem="content"]'),
		//
		matchCreateForm = plugElem.querySelector('form[se-elem="matchCreate"]'),
		matchPerTeamInput = matchCreateForm.se_formEl('matchsPerTeam'),
		//
		matchCreateResults = plugElem.querySelector('table[se-elem="results"]'),
		//
		gameInformation = undefined,
		matchCreateInfo = {
			gId:0,
			gTitle:'',
			ggId:0,
			ggTitle:'',
			gsId:0,
			gsTitle:'',
			gtId:0,
		},
		matchCreateParameters = {
			startGroupTimeUnix:0,
			teamsCount:0,
			fieldsCount:0
		},
		//
		teamListActive = [],
		teamListInactive = [],
		fieldList = [],
		virtualMatches = [];

	//
	function init() {
		let params = se.url.str_parse(window.location.search);

		// Parse strings
		params.gId = intVal(params.gId);
		params.gsId = intVal(params.gsId);
		params.ggId = intVal(params.ggId);

		console.log("PAGE: Games matches create init.");

		// Get information
		let queries = [
			{
				rName:'games',
				table:'games',
				params:{
					get:params.gId
				}
			},
			{
				rName:'game_stages',
				table:'game_stages',
				params: {
					get:params.gsId
				}
			},
			{
				rName:'game_groups',
				table:'game_groups',
				params: {
					get:params.ggId
				}
			},
			{
				rName:'game_matches',
				table:'game_matches',
				params:{
					index:'ggId',
					keyRange:IDBKeyRange.only(params.ggId)
				}
			},
			{
				rName:'event_fields',
				table:'event_fields',
				params:{}
			},
			{
				rName:'game_teams',
				table:'game_teams',
				params:{
					index:'gId',
					keyRange:IDBKeyRange.only(params.gId)
				}
			}
		];

		// Procesar tipo de juego y jugadores
		app.idb.queryBatch(queries, {
			onSuccess:(results) => {
				console.log("Game information query:", results);
				gameInformation = results;

				// Navegación
				let navigation = plugElem.querySelector('div[se-elem="navigation"]');
				se.uwp.elementTemplateProc(navigation, {
					gId:params.gId,
					gsId:params.gsId,
					ggId:params.ggId,
					gTitle:results.games.title,
					cTitle:results.game_stages.title,
					ggTitle:results.game_groups.title
				});

				// Obtener información del grupo seleccionado

				// Defaults for all matches
				matchCreateInfo.gId = gameInformation.games.id;
				matchCreateInfo.gTitle = gameInformation.games.title;
				matchCreateInfo.gsId = gameInformation.game_stages.id;
				matchCreateInfo.gsTitle = gameInformation.game_stages.title;
				matchCreateInfo.ggId = gameInformation.game_groups.id;
				matchCreateInfo.ggTitle = gameInformation.game_groups.title;
				matchCreateInfo.gtId = gameInformation.games.gtId;

				//
				console.log("CURRENT DEFAULTS ARE", matchCreateInfo);
				console.log("INFORMATION ABOUT THE GAME", gameInformation.games);

				//
				matchCreateParameters.startGroupTimeUnix = gameInformation.game_groups.dtStartUNIX;
				let isoStr = new Date(gameInformation.game_groups.dtStartUNIX * 1000).toISOString();
				matchCreateForm.se_formElVal('groupTimeStart', isoStr.substring(0, isoStr.length-1));


				// Tabla de contenido

				// Equipos
				for ( let cTeam of gameInformation.game_teams ) {
					let nTeam = {
						id:cTeam.id,
						index:cTeam.index,
						title:cTeam.title,
						checked:'checked',
						isError:0,
						gCount:0,
						sel:true
					};

					//
					teamListActive.push(nTeam);
				}

				//
				matchCreateForm.se_formElVal('matchsPerTeam', gameInformation.game_teams.length - 1 );

				// Campos
				for ( let cField of gameInformation.event_fields ) {
					// if ( !gameInformation.event_fields.hasOwnProperty( cIndex ) ) { continue; }

					let nField = {
						id:cField.id,
						title:cField.title,
						checked:'checked',
						sel:true
					};

					// Agregar a la lista
					fieldList.push(nField);

					// Print
					fieldsListContent.se_append( se.struct.stringPopulate( fieldsListTemplate.se_html(), nField ) );
				}

				// Juegos existentes
				if ( gameInformation.game_matches ) {
					matchesCurrentContent.se_append(se.struct.stringPopulateMany(matchesCurrentTemplate.se_html(), gameInformation.game_matches));
				}

				// Determinar tiempo para partida... tal vez despsués jaja
				matchCreateForm.se_formElVal('minutesBetweenMatches', 11);

				// Información práctica de la jugada
				matchRecreation();
			},
			onFail:() => {

			}
		});

		// Bindings
		plugElem.se_on('click', 'button[se-act]', playerBtnAction);
		matchPerTeamInput.se_on('change', matchRecreation);

		//
		matchesCandidateContent.se_on({
			'dragstart':(event, cElem) => {
				//
				let pEl = cElem.se_closest('tr');
				//
				event.dataTransfer.dropEffect = 'copy';
				event.dataTransfer.setData('order', pEl.se_index());
				event.dataTransfer.setData('name', cElem.se_text());
			}
		}, '.matchOrder');

		//
		matchesCandidateContent.se_on({
			'dragover':(event, cElem) => {
				event.preventDefault();
				let pEl = cElem.se_closest('tr'),
					cOrder = pEl.se_index(),
					oOrder = intVal(event.dataTransfer.getData('order'));
				//
				if ( oOrder === cOrder ) { return; }
				//
				pEl.se_classAdd('gSel');

			},
			'dragleave':(event, cElem) => {
				event.preventDefault();
				let pEl = cElem.se_closest('tr');
				pEl.se_classDel('gSel');
			},
			'drop':(event, cElem) => {
				console.log("DROPED BIATCH", cElem);
				event.preventDefault();
				let pEl = cElem.se_closest('tr'),
					cOrder = pEl.se_index(),
					oOrder = intVal(event.dataTransfer.getData('order'));
				//
				if ( oOrder === cOrder ) { return; }
				//
				matchSwitch(oOrder, cOrder);
			}
		}, '.matchOrder');

		//
		objectListActive.se_on({
			'dragstart':(event, cElem) => {
				//
				let pEl = cElem.se_closest('tr');
				//
				event.dataTransfer.dropEffect = 'copy';
				event.dataTransfer.setData('order', pEl.se_index());
				event.dataTransfer.setData('name', cElem.se_text());

			},
			'mouseover':(e, cEl) => {
				let objIndex = cEl.se_closest('tr').se_data('index');
				matchesCandidate.querySelectorAll('td[data-index="' + objIndex + '"]').se_classAdd('highlight');
			},
			'mouseout':() => {
				matchesCandidate.querySelectorAll('td[data-index].highlight').se_classDel('highlight');
			},
			'click':(e, cEl) => {
				let objIndex = cEl.se_closest('tr').se_data('index'),
					isSel = cEl.se_data('sel');
				if ( isSel === '1' ) {
					matchesCandidate.querySelectorAll('td[data-index="' + objIndex + '"]').se_classAdd('highlight_02');
					cEl.se_classAdd('highlight_02');
					cEl.se_data('sel', '');
				} else {
					matchesCandidate.querySelectorAll('td[data-index="' + objIndex + '"]').se_classDel('highlight_02');
					cEl.se_classDel('highlight_02');
					cEl.se_data('sel', '1');
				}
			}
		}, '.objGroup');

		//
		objectListActive.se_on({
			'dragover':(event, cElem) => {
				event.preventDefault();
				let pEl = cElem.se_closest('tr'),
					cOrder = pEl.se_index(),
					oOrder = intVal(event.dataTransfer.getData('order'));
				//
				if ( oOrder === cOrder ) { return; }
				//
				pEl.se_classAdd('gSel');

			},
			'dragleave':(event, cElem) => {
				event.preventDefault();
				let pEl = cElem.se_closest('tr');
				pEl.se_classDel('gSel');
			},
			'drop':(event, cElem) => {
				event.preventDefault();
				let pEl = cElem.se_closest('tr'),
					cOrder = pEl.se_index(),
					oOrder = intVal(event.dataTransfer.getData('order'));
				//
				if ( oOrder === cOrder ) { return; }
				//
				objSwitch(oOrder, cOrder);
			},
			'click':() => {}
		}, '.objGroupNumber');
	}

	//
	function teamAdd(cBtn) {
		let cElement = cBtn.se_closest('tr'),
			cIndex = cElement.se_index(),
			cRow = teamListInactive.splice(cIndex, 1)[0];

		//
		teamListActive.push(cRow);

		//
		matchRecreation();
	}

	//
	function teamRemove(cBtn) {
		let cElement = cBtn.se_closest('tr'),
			cIndex = cElement.se_index(),
			cRow = teamListActive.splice(cIndex, 1)[0];

		//
		teamListInactive.push(cRow);

		//
		matchRecreation();
	}

	//
	function changeTeamStatus(e, cEl) {
		let cElement = cEl.se_closest('label'),
			cIndex = cElement.se_index(),
			cStatus = cEl.checked,
			cTeamElem = teamListActive[cIndex],
			nCount = 0;

		//
		if ( cStatus ) {
			cTeamElem.sel = true;
			cTeamElem.checked = 'checked';
		} else {
			cTeamElem.sel = false;
			cTeamElem.checked = '';
		}

		//
		for ( let cObj of teamList ) {
			if ( cObj.sel ) {
				nCount++;
			}
		}

		// matchCreateForm.se_formElVal('matchsPerTeam', nCount - 1);
		matchRecreation();
	}

	//
	function playerBtnAction(e, cBtn) {
		e.preventDefault();

		//
		switch ( cBtn.se_attr('se-act') ) {
			//
			case 'objRandomize':
				objRandomize();
				break;

			//
			case 'matchesVirtualCreate':
				createMatchesFromVirtual();
				break;

			//
			case 'matchDel':
				matchDelete(cBtn);
				break;

			//
			case 'matchCandidateDelete':
				matchCandidateDelete(cBtn);
				break;

			//
			case 'team_add':
				teamAdd(cBtn);
				break;

			//
			case 'team_remove':
				teamRemove(cBtn);
				break;

			//
			default:
				console.log("Botón: Acción no definida.", cBtn.se_attr('se-act'), cBtn);
				break;
		}
		//
	}

	//
	function matchDelete(cBtn) {
		let cRow = cBtn.se_closest('tr'),
			cIndex = intVal(cRow.se_data('index')),
			gsId = cRow.se_data('id'),
			cStatus = cRow.se_data('status');
		//
		if ( cStatus !== '1' ) {
			alert("Ya no es posible borrar la partida. (ya realizada)");
			return;
		}
		//
		if ( gsId !== '0' ) {
			alert("Ya no es posible borrar la partida. (Creada en la base de datos.)");
			return;
		}

		console.log("deleting", cIndex);

		//
		app.idb.del('game_matches', cIndex, {
			onSuccess:(msg) => {
				cRow.se_remove();
			}
		});
	}

	//
	function matchSwitch(i, j) {
		let temp = virtualMatches[i];
		virtualMatches[i] = virtualMatches[j];
		virtualMatches[j] = temp;

		// Re print
		printVirtualMatches();
	}

	//
	function matchCandidateDelete(cBtn) {
		let cRow = cBtn.se_closest('tr'),
			cIndex = cRow.se_index();

	}

	//
	function objRandomize() {
		let i, j, temp;

		// Actual randomice
		for ( i = teamListActive.length - 1; i > 0; i-- ) {
			j = Math.floor(Math.random() * (i + 1));
			temp = teamListActive[i];
			teamListActive[i] = teamListActive[j];
			teamListActive[j] = temp;
		}

		// Re create matches
		matchRecreation();
	}

	//
	function objSwitch(i, j) {
		let temp = teamListActive[i];
		teamListActive[i] = teamListActive[j];
		teamListActive[j] = temp;

		// Re create matches
		matchRecreation();
	}

	//
	function objReprint() {
		let cIndex = 0, tObj;

		console.log(teamListActive);

		//
		objectListActiveContent.se_empty();
		//
		for ( let cObj of teamListActive ) {
			tObj = se.object.merge(cObj, { index:cIndex + 1, cOrder:cIndex });
			// Print
			objectListActiveContent.se_append( se.struct.stringPopulate( objectListActiveTemplate.se_html(), tObj) );
			cIndex++;
		}

		//
		objectListInactiveContent.se_empty();
		//
		for ( let cObj of teamListInactive ) {
			tObj = se.object.merge(cObj, { index:0, cOrder:0, gCount:0 });
			// Print
			objectListInactiveContent.se_append( se.struct.stringPopulate( objectListInactiveTemplate.se_html(), tObj) );
		}
	}

	//
	function getEffectiveTeams() {
		let nTeams = [];
		//
		for ( let cObj of teamList ) {
			if ( cObj.sel ) {
				nTeams.push(cObj);
			}
		}
		return nTeams;
	}

	//
	function getEffectiveFields() {
		let nFields = [];
		//
		for ( let cObj of fieldList ) {
			if ( cObj.sel ) {
				nFields.push(cObj);
			}
		}
		return nFields;
	}

	//
	function matchRecreation() {
		console.log("recreate match");
		// Cleanup
		matchCreateOutput.se_empty();
		matchesCandidateContent.se_empty();
		objReprint();

		let totTeams = teamListActive.length,
			matchPerTeam = parseInt(matchCreateForm.se_formElVal('matchsPerTeam')),
			teoMatches = totTeams * matchPerTeam / 2;

		console.log("MATCH CREATION INIT");

		// Análisis
		matchCreateResults.querySelector('td[se-elem="res_teomatch"]').se_text(teoMatches);

		//
		if ( teoMatches % 1 !== 0 ) {
			matchCreateResults.querySelector('td[se-elem="res_totalmatches"]').se_text(0);
			matchCreateResults.querySelector('td[se-elem="res_totaltime"]').se_text('N/D');
			matchCreateResults.querySelector('td[se-elem="res_matchmethod"]').se_text('none');
			se.struct.notifAdvanced(matchCreateOutput, 'notification alert', 'ERROR', 'La cantidad de partidas no cuadra. Impar con impar (equipos/partidas).', 'fa-remove');
			return;
		}

		console.log("INITIAL CHECKUP: OK");

		//
		if ( matchPerTeam >= (totTeams - 1) ) {
			matchCreateRoundRobin(matchPerTeam);
		} else {
			matchCreateCircle(matchPerTeam);
		}

		// matchesSetProperties();
		printVirtualMatches();
		objReprint();

	}

	//
	function matchCreateRoundRobin(matchPerTeam) {
		let teams = JSON.parse(JSON.stringify(teamListActive)),     // Deep copy array
			totTeams = teams.length,
			//
			teamsMod, altTotTeams, teamSingle,
			parts, half,
			curPartM, team1, team2,
			teamMatchCount = {},
			oddTeams = false,
			i;

		//
		console.log("ROUND ROBIN CREATE");
		matchCreateResults.querySelector('td[se-elem="res_matchmethod"]').se_text('round robin');

		// Create counter
		for ( let i = 0; i < totTeams; i++ ) {
			teamMatchCount[teams[i].index] = 0;
		}

		// Impar and close to robin or more
		if ( totTeams % 2 !== 0 )
		{
			console.log("ADDED BYE");
			totTeams++;
			matchPerTeam++;
			oddTeams = true;
			teams.push({id:0, index:0, title:''});
		}

		parts = matchPerTeam;
		half = totTeams / 2;

		// Copiar y remover primer resultado
		teamsMod = teams.slice(0);
		teamSingle = teamsMod.shift();
		altTotTeams = teamsMod.length;

		// Limpiar
		virtualMatches = [];

		// Ciclo
		for ( let curPart = 0; curPart < parts; curPart++ ) {
			console.log("loopy loop", curPart, parts);
			//
			curPartM = curPart + 1;
			let cInit = true;
			//
			for ( let cPointer = 1; cPointer < half; cPointer++ ) {
				//
				team1 = ( curPart + cPointer ) % altTotTeams;
				team2 = ( curPart + totTeams - cPointer ) % altTotTeams;
				// Si no es bye, agregar
				if ( !( teamsMod[team1].index === 0 || teamsMod[team2].index === 0 ) )
				{
					// Agregar
					virtualMatches.push({
						part:curPartM,
						init:cInit,
						team1:{
							num:team1 + 2,
							id:teamsMod[team1].id,
							index:teamsMod[team1].index,
							title:teamsMod[team1].title
						},
						team2:{
							num:team2 + 2,
							id:teamsMod[team2].id,
							index:teamsMod[team2].index,
							title:teamsMod[team2].title
						},
						t1:team1 + 2,
						t2:team2 + 2
					});

					//
					teamMatchCount[teamsMod[team1].index]++;
					teamMatchCount[teamsMod[team2].index]++;

					//
					cInit = false;
				}
			}

			// Agregar el round final
			team1 = ( curPart % altTotTeams ) + half;
			if ( team1 >= altTotTeams ) { team1-= altTotTeams; }

			// Ignorar bye
			if ( teamsMod[team1].index !== 0 ) {
				// Agregar último match
				virtualMatches.push({
					part:curPartM,
					init:false,
					team1:{
						num:team1 + 2,
						id:teamsMod[team1].id,
						index:teamsMod[team1].index,
						title:teamsMod[team1].title
					},
					team2:{
						num:1,
						id:teamSingle.id,
						index:teamSingle.index,
						title:teamSingle.title
					},
					t1:team1 + 2,
					t2:1
				});

				//
				teamMatchCount[teamsMod[team1].index]++;
				teamMatchCount[teamSingle.index]++;
			}
		}

		// Revision (excedent games)
		let excedent = false,
			exTeams = [], cTeam,
			teamLen = 0;

		// Fix matchperteam
		if ( oddTeams ) {
			matchPerTeam--;
			console.log("odd teams, removing extra match per team (even fix)", matchPerTeam);
		}
		// matchPerTeam--;

		// Check excess
		for ( cTeam in teamMatchCount ) {
			if ( !teamMatchCount.hasOwnProperty(cTeam) ) { continue; }
			let cGames = teamMatchCount[cTeam];
			if ( cGames > matchPerTeam ) {
				exTeams.push(intVal(cTeam));
				excedent = true;
			}
			teamLen++;
		}

		//
		console.log("matches", virtualMatches);

		// Validation
		console.log("VALIDATION. Excedents:", excedent, ". Match per team: ", matchPerTeam, ". Total Matches", virtualMatches.length, ". Teams with excedent: ", exTeams);

		if ( excedent ) {
			let attempts = 0,
				cMatches = virtualMatches.length;
			console.log("CLEANING ATTEMPT. MaxTrys", attempts, teamLen);
			while ( excedent && attempts <= teamLen ) {
				// Try to remove
				for ( i = 0; i < cMatches; i++ ) {
					let cMatch = virtualMatches[i];
					team1 = cMatch.team1.index;
					team2 = cMatch.team2.index;
					let index,
						t1Index = exTeams.indexOf(team1),
						t2Index = exTeams.indexOf(team2);
					// console.log("Match removal attempt:", i, cMatches, "T1", t1Index, "T2", t2Index);
					// Check
					if ( t1Index > -1 && t2Index > -1 ) {
						console.log("Removing: ", i);
						cMatches--;
						// Remove match from object
						virtualMatches.splice(i, 1);
						// Remove elements from exTeams
						index = exTeams.indexOf(team1);
						exTeams.splice(index,1);
						index = exTeams.indexOf(team2);
						exTeams.splice(index,1);
						break;
					}
				}
				if ( exTeams.length === 0 ) {
					excedent = false;
				}
				// console.log("attempts", attempts, teamLen, excedent);
				attempts++;
			}
		}

		//
		console.debug("final matches are", virtualMatches);
		console.debug("match count", teamMatchCount);
		console.debug("teamListActive", teamListActive);

		// Actualizar contador de juegos actuales
		for ( let cTeamIndexBix in teamListActive ) {
			if ( !teamListActive.hasOwnProperty(cTeamIndexBix) ) { continue; }
			//
			let cTeamIndex = teamListActive[cTeamIndexBix].index;
			//
			teamListActive[cTeamIndexBix].gCount = teamMatchCount[cTeamIndex];
			teamListActive[cTeamIndexBix].isError = ( teamMatchCount[cTeamIndex] !== matchPerTeam ) ? '1' : '0';
		}

		// Actualizar resultados finales
		matchCreateResults.querySelector('td[se-elem="res_totalmatches"]').se_text(virtualMatches.length);
		matchCreateResults.querySelector('td[se-elem="res_totaltime"]').se_text(se.time.toHHMMSS(virtualMatches.length * parseInt(matchCreateForm.se_formElVal('minutesBetweenMatches')) * 60));
	}

	//
	function matchCreateCircle(matchPerTeam) {
		let teamsMod = JSON.parse(JSON.stringify(teamListActive)),     // Deep copy array
			totTeams = teamsMod.length,
			totGames = totTeams * matchPerTeam / 2,
			periods = Math.floor(matchPerTeam / 2),
			//
			half = Math.round(totTeams / 2),
			teamMatchCount = {},
			evenTeams = ( totTeams % 2 === 0 ),
			evenMatches = ( matchPerTeam % 2 === 0 ),
			cInit;

		//
		console.log("CIRCLE METHOD");
		matchCreateResults.querySelector('td[se-elem="res_matchmethod"]').se_text('circle method');

		// Limpiar
		virtualMatches = [];

		// Create counter
		for ( let i = 0; i < totTeams; i++ ) {
			teamMatchCount[teamsMod[i].index] = 0;
		}

		//
		function addMatch(cPart, cInit, team1, team2) {
			// Agregar
			virtualMatches.push({
				part:cPart + 1,
				init:cInit,
				team1:{
					num:team1 + 1,
					id:teamsMod[team1].id,
					index:teamsMod[team1].index,
					title:teamsMod[team1].title
				},
				team2:{
					num:team2 + 1,
					id:teamsMod[team2].id,
					index:teamsMod[team2].index,
					title:teamsMod[team2].title
				},
				t1:team1 + 1,
				t2:team2 + 1
			});

			//
			teamMatchCount[teamsMod[team1].index]++;
			teamMatchCount[teamsMod[team2].index]++;
		}

		//
		for ( let i = 0; i < periods; i++ ) {
			cInit = true;
			for ( let j = 0; j < totTeams; j++ ) {
				let team1 = j,
					team2 = half + j + i;
				team2 = ( team2 >= totTeams ) ? team2 - totTeams : team2;

				//
				addMatch(i, cInit, team1, team2);
				cInit = false;
			}
		}

		// Even teams and uneven matches, create last round
		if ( evenTeams && !evenMatches ) {
			cInit = true;
			for ( let i = 0; i < half; i++ ) {
				let team1 = i * 2,
					team2 = ((i + 1) * 2) - 1;

				//
				addMatch(periods, cInit, team1, team2);
				cInit = false;
			}
		}

		//
		console.debug("final matches are", virtualMatches);
		console.debug("match count", teamMatchCount);
		console.debug("teamListActive", teamListActive);

		// Actualizar contador de juegos actuales
		for ( let cTeamIndexBix in teamListActive ) {
			if ( !teamListActive.hasOwnProperty(cTeamIndexBix) ) { continue; }
			//
			let cTeamIndex = teamListActive[cTeamIndexBix].index;
			//
			teamListActive[cTeamIndexBix].gCount = teamMatchCount[cTeamIndex];
			teamListActive[cTeamIndexBix].isError = ( teamMatchCount[cTeamIndex] !== matchPerTeam ) ? '1' : '0';
		}

		// Actualizar resultados finales
		matchCreateResults.querySelector('td[se-elem="res_totalmatches"]').se_text(virtualMatches.length);
		matchCreateResults.querySelector('td[se-elem="res_totaltime"]').se_text(se.time.toHHMMSS(virtualMatches.length * parseInt(matchCreateForm.se_formElVal('minutesBetweenMatches')) * 60));
	}

	//
	function printVirtualMatches() {
		let teamplateHTML = matchesCandidateTemplate.se_html(),
			index = 0;
		//
		matchesCandidateContent.se_empty();
		for ( let cMatch of virtualMatches ) {
			// Print
			matchesCandidateContent.se_append( se.struct.stringPopulate(teamplateHTML, {
				index:index,
				type:( cMatch.init ) ? 'init' : '',
				part:cMatch.part,
				cOrder:index + 1,
				t1Num:cMatch.team1.num,
				t1Id:cMatch.team1.id,
				t1Index:cMatch.team1.index,
				t1Title:cMatch.team1.title,
				t2Num:cMatch.team2.num,
				t2Id:cMatch.team2.id,
				t2Index:cMatch.team2.index,
				t2Title:cMatch.team2.title,
			}) );

			//
			index++;
		}
		//
	}

	//
	function createMatchesFromVirtual() {
		let nMatches = [],
			nextMatchStart = new Date(matchCreateForm.se_formElVal('groupTimeStart')).getTime() / 1000,
			secondsBetweenMatches = parseInt(matchCreateForm.se_formElVal('minutesBetweenMatches')) * 60,
			fields = getEffectiveFields(),
			totFields = fields.length,
			index = 1;

		//
		if ( !Number.isInteger(nextMatchStart) ) {
			alert("Fecha no válida.");
			return;
		}

		//
		if ( !fields.length ) {
			alert("No hay campos seleccionados.");
			return;
		}

		// Crear objetos
		for ( let cMatch of virtualMatches ) {
			nMatches.push(matchObjectCreate(index, nextMatchStart, cMatch.team1, cMatch.team2, fields[0]));
			//
			nextMatchStart+= secondsBetweenMatches;
			index++;
		}

		// Operación SQL
		console.log("Crear partidas", nMatches);
		app.idb.add('game_matches', nMatches, {
			onSuccess:(e) => {
				matchesCandidateContent.se_empty();
				console.log("SUCCESS CREATING THE MATCHES!!!");
				//
				app.idb.query( 'game_matches', {
					index:'ggId',
					keyRange:IDBKeyRange.only(matchCreateInfo.ggId),
					onSuccess: ( data ) => {
						let updatesString = matchesCurrentTemplate.se_html();
						// Empty
						matchesCurrentContent.se_empty();
						for ( let cMatch of data ) {
							// Print
							matchesCurrentContent.se_append( se.struct.stringPopulate( updatesString, cMatch ) );
						}
					},
					onEmpty: () => {
						console.log( "No content available." );
					}
				} );
			},
			onError:(e) => {
				console.error("no se guardaron los resultados.", e);
			}
		});
	}

	//
	function matchObjectCreate(cOrder, timeStartUnix, team1, team2, field) {
		let newMatchDefaults = {
				id:0,
				mStatus:1,
				gTimeEst:null,
				t1_side:0,
				t1_status:0,
				t1_points:0,
				t2_side:0,
				t2_status:0,
				t2_points:0,
				mPoints:[],
				mEvents:[],
				sync:1
			},
			// Date operations
			monthIndex = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
			timeStart = new Date(timeStartUnix * 1000),
			// Current match information
			curMatchData = {
				mOrder:cOrder,
				gTimeDay:`${ ('0'+timeStart.getDate()).slice(-2) } ${monthIndex[timeStart.getMonth()]}`,
				gTimeHour:`${ ('0'+timeStart.getHours()).slice(-2) }:${ ('0'+timeStart.getMinutes()).slice(-2) }`,
				gTimeUnix:timeStartUnix,
				t1_id:team1.id,
				t1_index:team1.index,
				t1_name:team1.title,
				t2_id:team2.id,
				t2_index:team2.index,
				t2_name:team2.title,
				fId:field.id,
				fTitle:field.title,
				teams:[team1.index, team2.index]
			};
		//
		return se.object.merge(newMatchDefaults, matchCreateInfo, curMatchData);
	}

	//
	init();
	return {};
};
