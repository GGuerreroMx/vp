﻿"use strict";
se.plugin.uwp_page_registration = function ( plugElem, plugOptions ) {
	let table = plugElem.querySelector( 'table[se-elem="registered"]' ),
		tableTemplate = table.querySelector( 'template' ),
		tableContent = table.querySelector( 'tbody' ),
		//
		actForm = plugElem.querySelector( 'form[se-elem="registerCheck"]' ),
		actFormResponse = actForm.querySelector('output[se-elem="response"]'),
		//
		temp2 = '';

	//
	function init() {
		//
		console.log("PAGE - Registrations:");
		app.idb.query('event_players', {
			onSuccess:( data ) => {
				console.log( "Activity updates - print", data );
				let cIndex, cData,
					updatesString = tableTemplate.se_html();
				// Empty
				tableContent.se_empty();
				// Sort
				// data = se.object.sort( data, 'dtAddUnix', 'DESC' );
				// Print
				for ( cIndex in data ) {
					if ( !data.hasOwnProperty( cIndex ) ) { continue; }
					// Asignar y procesar
					cData = regDataProcess(data[cIndex]);

					// Print
					tableContent.se_append( se.struct.stringPopulate( updatesString, cData ) );
				}
			},
			onEmpty:() => {
				console.log( "No content available." );
			}
		} );
		//
		plugElem.se_on( 'click', 'button[se-act]', btnActions );
		actForm.se_on('keyup', 'input', formOnEnter);
	}

	//
	function formOnEnter(e, cInput) {
		e.preventDefault();
        if ( e.keyCode === 13 ) {
        	switch ( cInput.name ) {
        		//
				case 'shortId':
                    let shortIdStr = cInput.value;
                    //
                    if ( shortIdStr.length !== 12 ) {
                    	console.log(shortIdStr.length);
                        se.struct.notifAdvanced( actFormResponse, 'notification alert', 'Formato incorrecto', 'No parece contener el formato correcto para continuar.', 'fa-remove' );
                        return;
                    }
                    //
                    app.idb.query('registrations', {
                        index: 'shortId',
                        keyRange: IDBKeyRange.only( shortIdStr ),
                        onSuccess: function ( data ) {
                            // Empty
                            tableContent.se_empty();
                            // Proc
                            let cData = regDataProcess( data[0] );
                            // Print
                            tableContent.se_append( se.struct.stringPopulate( tableTemplate.se_html(), cData ) );
                            //
                            let nextInput = actForm.se_formEl('eventId');
                            nextInput.focus();
                            nextInput.select();
                        },
                        onComplete: function () {
                        }
                    } );
					break;
				//
				case 'eventId':
                    // Actualizar propiedad
                    app.idb.query( 'registrations', {
                        index: 'shortId',
                        keyRange: IDBKeyRange.only( shortIdStr ),
                        onSuccess: function ( data ) {
                            // Actualizar
                            console.log("proceso de asginar id pendiente");
                        },
                        onComplete: function () {
                        }
                    } );
					break;
			}
        }
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'connectionTest':
				connectionTest();
				break;
			// Mostrar todos los resultados
			case 'showAll':
				showAll();
				break;
			// Ocultar resultados
			case 'hideAll':
				hideAll();
				break;
			//
			default:
				console.log( "Boton no programado", cBtn );
				break;
		}
	}

	//
	function regDataProcess( cData ) {
		let games = [],
			count = 0;
		//
		for ( let indexName in cData ) {
			if ( !cData.hasOwnProperty(indexName) ) { continue; }
			//
			if ( indexName.startsWith('game_') ) {
				games.push(count + ':' +  cData[indexName]);
            }
            count++;
		}
		//
		cData['games'] = games.join('<br />');
		//
		return cData;
	}

	//
	function hideAll() {
		// Empty
		tableContent.se_empty();
	}

	//
	function showAll() {

	}

	//
	init();
	return {};
};
