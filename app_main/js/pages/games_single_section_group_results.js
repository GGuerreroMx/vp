﻿"use strict";
se.plugin.uwp_page_games_single_section_group_results = function ( plugElem, plugOptions ) {
	//
	const gameTemplate = plugElem.querySelector('template'),
		gameContent = plugElem.querySelector('[se-elem="content"]');

	//
	function init() {
		let params = se.url.str_parse(window.location.search);

		// Parse strings
		params.gId = intVal(params.gId);
		params.gsId = intVal(params.gsId);
		params.ggId = intVal(params.ggId);

		//
		console.log("PAGE: Games group results.");

		//
		let queries = [
			{
				rName:'games',
				table:'games',
				params: {
					get:params.gId
				}
			},
			{
				rName:'game_stages',
				table:'game_stages',
				params: {
					get:params.gsId
				}
			},
			{
				rName:'game_groups',
				table:'game_groups',
				params: {
					get:params.ggId
				}
			},
		];

		// Generar json data
		app.idb.queryBatch(queries, {
			onSuccess:(results) => {
				console.log("Query Batch Results", results);
				//
				let navigation = plugElem.querySelector('div[se-elem="navigation"]');
				navigation.se_attr('aria-hidden', 'false');

				//
				se.uwp.elementTemplateProc(navigation, {
					gId:params.gId,
					gsId:params.gsId,
					ggId:params.ggId,
					gTitle:results.games.title,
					cTitle:results.game_stages.title,
					ggTitle:results.game_groups.title
				});
			}
		});

		//
		queries = [
			{
				rName:'game_teams',
				table:'game_teams',
				params: {
					index:'gId',
					keyRange:IDBKeyRange.only(gId)
				}
			},
			{
				rName:'game_matches',
				table:'game_matches',
				params: {
					index:'ggId',
					keyRange:IDBKeyRange.only(ggId)
				}
			},

		];

		// Generar json data
		app.idb.queryBatch(queries, {
			onSuccess:(results) => {
				console.log("Query Batch Results", results);

				// Hacer sumas, filtrar e imprimir
				let cGroupedResults = {};
				for ( let i = 0; i < results.game_teams.length; i++ ) {
					let cTeam = results.game_teams[i];
					cGroupedResults[cTeam.index.toString()] = {
						'seen':0,
						'title':cTeam.title,
						'id':cTeam.id,
						'index':cTeam.index,
						'gId':cTeam.gId,
						'points':0,
						'gW':0,
						'gD':0,
						'gL':0,
						'other':0
					};
				}
				//
				console.log("resultados agrupados", cGroupedResults);

				//
				if ( results.game_matches ) {
					for ( let i = 0; i < results.game_matches.length; i++ ) {
						console.log("current cMatch for analysis", results.game_matches[i]);
						let cMatch = results.game_matches[i],
							cTeam_1 = cGroupedResults[cMatch.t1_index.toString()],
							cTeam_2 = cGroupedResults[cMatch.t2_index.toString()];
						//
						cTeam_1.seen++;
						cTeam_2.seen++;
						cTeam_1.points += cMatch.t1_points;
						cTeam_2.points += cMatch.t2_points;
						//
						switch ( cMatch.t1_status ) {
							case 0:
								break;
							case 1:
								cTeam_1.gL++;
								break;
							case 2:
								cTeam_1.gD++;
								break;
							case 3:
								cTeam_1.gW++;
								break;
						}
						//
						switch ( cMatch.t2_status ) {
							case 0:
								break;
							case 1:
								cTeam_2.gL++;
								break;
							case 2:
								cTeam_2.gD++;
								break;
							case 3:
								cTeam_2.gW++;
								break;
						}

					}
				}

				//
				//
				console.log("Populados", cGroupedResults);
				//
				let i = cGroupedResults.length;
				while ( i-- ) {
					if ( cGroupedResults[i].seen === 0 ) {
						cGroupedResults.splice(i, 1);
					}
				}
				//
				console.log("final", cGroupedResults);
				//
				gameContent.se_empty();
				gameContent.se_append(se.struct.stringPopulateMany(gameTemplate.se_html(), cGroupedResults));
			}

		});

	}

	//
	init();
	return {};
};
