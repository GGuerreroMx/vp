﻿"use strict";
se.plugin.uwp_page_games_team_players = function ( plugElem, plugOptions ) {
	let gameTemplate = plugElem.querySelector('template'),
		gameContent = plugElem.querySelector('[se-elem="content"]'),
		//
		playerAddForm = plugElem.querySelector('form[se-elem="addPlayer"]'),
		playerNameInput = playerAddForm.se_formEl('title'),
		playerAddFormResponse = playerAddForm.querySelector('output[se-elem="response"]'),
		playerAddFormBtn = playerAddForm.querySelector('button[type="submit"]'),
		//
		dataList = $('#event_players'),
		//
		season_players = [],
		event_players = [],
		teamData = {};

	//
	function init() {
		let params = se.url.str_parse(window.location.search),
			gId = intVal(params.gId),
			tIndex = intVal(params.tIndex);

		console.log("PAGE - GAME TEAM PLAYERS");

		//
		if ( gId ) {
			console.log("Filtrado de juego");
			//
			let queries = [
				{
					rName:'game_teams',
					table:'game_teams',
					params: {
						get:tIndex
					}
				},
				{
					rName:'games',
					table:'games',
					params: {
						get:gId
					}
				}
			];

			// Generar json data
			app.idb.queryBatch(queries, {
				onSuccess:(results) => {
					console.log("Query Batch Results", results);

					//
					let navigation = plugElem.querySelector('div[se-elem="navigation"][data-mode="games_team"]');
					navigation.se_attr('aria-hidden', 'false');
					//
					se.uwp.elementTemplateProc(navigation, {
						gId:gId,
						tIndex:tIndex,
						gTitle:results.games.title,
						tTitle:results.game_teams.title
					});

					//
					teamData = results.game_teams;
					gameContent.se_append(se.struct.stringPopulateMany(gameTemplate.se_html(), teamData.players_list));
				}
			});
		} else {
			//
			console.log("Filtrado por equipo individual");
			//
			gameContent.se_empty();
			app.idb.query('game_teams', {
				get: (intVal(params.tIndex)),
				onSuccess: (data) => {
					console.log("consultado el equipo, los datos son", data);

					//
					let navigation = plugElem.querySelector('div[se-elem="navigation"][data-mode="team_single"]');
					navigation.se_attr('aria-hidden', 'false');

					//
					se.uwp.elementTemplateProc(navigation, {
						tIndex:params.tIndex,
						tTitle:data.title
					});

					// Popular jugadores actuales
					teamData = data;
					gameContent.se_append(se.struct.stringPopulateMany(gameTemplate.se_html(), teamData.players_list));
				}
			});
		}

		// Preparar datalist
		app.idb.query('season_players', {
			onSuccess:(data) => {
				console.log("Judagores de la temporada: ", data);
				season_players = data;
				data.forEach((item) => {
					let option = document.createElement('option');
					option.se_data('id', item.id);
					option.value = item.title;
					dataList.appendChild(option);
				});
			}
		});
		app.idb.query('event_players', {
			onSuccess:(data) => {
				console.log("Judagores en el evento: ", data);
				event_players = data;
			}
		});

		// Bindings
		playerAddForm.se_on('submit', playerAddSubmit);
		playerNameInput.se_on('change', playerAddCheck);
		gameContent.se_on('change', 'input', eventIdUpdate);
		gameContent.se_on('click', 'button[se-act]', playerBtnAction);
	}

	//
	function playerBtnAction(e, cBtn) {
		e.preventDefault();

		let cIndex = intVal(cBtn.se_closest('tr').se_data('index'));

		//
		switch ( cBtn.se_attr('se-act') ) {
			case 'unlink':
				team_playerUnlink(cIndex);
				break;
			default:
				console.log("Evento no definido.");
				break;
		}
	}

	//
	function playerAddCheck(e) {
		e.preventDefault();
		let cName = playerNameInput.value,
			playerExist = false,
			playerInTeam = false,
			selPlayer;

		// Determinar si en evento
		for ( let cPlayer of event_players ) {
			if ( cName === cPlayer.uName ) {
				selPlayer = cPlayer;
				console.log("player found", selPlayer);
				playerAddForm.se_formElVal('index', selPlayer.index);
				playerAddForm.se_formElVal('lregId', selPlayer.lregId);
				playerAddForm.se_formElVal('eurId', selPlayer.id);
				playerAddForm.se_formElVal('eventId', selPlayer.eventId);
				playerExist = true;
				break;
			}
		}

		// Determinar estado del nombre
		if ( playerExist ) {
			// Determinar si ya esta en el equipo
			for ( let cIndex of teamData.players_index ) {
				if ( cIndex === selPlayer.index ) {
					playerInTeam = true;
					break;
				}
			}

			//
			if ( playerInTeam ) {
				// Borrar datos de usuario anterior
				playerAddForm.se_formElVal('index', '');
				playerAddForm.se_formElVal('lregId', '');
				playerAddForm.se_formElVal('eurId', '');
				playerAddForm.se_formElVal('eventId', '');
				playerAddForm.se_formElVal('action', 'none');
				playerAddFormBtn.disabled = true;
				playerAddFormBtn.querySelector('span').se_text('No disponible');
				playerAddFormResponse.se_text('El jugador ya esta en el presente equipo');
			} else {
				playerAddForm.se_formElVal('action', 'link');
				playerAddFormBtn.querySelector('span').se_text('Vincular');
				playerAddFormBtn.disabled = false;
			}
		} else {
			// Borrar datos de usuario anterior
			playerAddForm.se_formElVal('index', '');
			playerAddForm.se_formElVal('lregId', '');
			playerAddForm.se_formElVal('eurId', '');
			playerAddForm.se_formElVal('eventId', '');
			playerAddForm.se_formElVal('action', 'create');

			// Determinar si esta en la lista de temporada
			for ( let cPlayer of season_players ) {
				if ( cName === cPlayer.title ) {
					playerAddForm.se_formElVal('lregId', cPlayer.id);
					break;
				}
			}

			//
			playerAddFormBtn.querySelector('span').se_text('Crear');
			playerAddFormBtn.disabled = false;
		}
		//
	}

	//
	function playerAddSubmit(e) {
		e.preventDefault();

		// Determinar operación
		switch ( playerAddForm.se_formElVal('action') ) {
			//
			case 'create':
				console.log("User add from scratch");
				let playerRegInfo = {
					id:0,
					lregId:intVal(playerAddForm.se_formElVal('lregId')),
					eventId:0,
					uName:playerAddForm.se_formElVal('title'),
					dtAdd:null,
					dtMod:null,
					passId:0,
					regId:0,
					shortId:'',
					sync:1,
					tName:null
				};
				// Operation
				app.idb.add('event_players', playerRegInfo, {
					onSuccess:(e) => {
						let index = e.target.result;
						team_playerLink({
							index:index,
							eventId:0,
							title:playerAddForm.se_formElVal('title'),
							lregId:0,
							eurId:0,
							egoId:0
						});
					}
				});
				break;
			//
			case 'link':
				console.log("User link");
				team_playerLink({
					index:intVal(playerAddForm.se_formElVal('index')),
					eventId:intVal(playerAddForm.se_formElVal('eventId')),
					title:playerAddForm.se_formElVal('title'),
					lregId:intVal(playerAddForm.se_formElVal('lregId')),
					eurId:intVal(playerAddForm.se_formElVal('eurId')),
					egoId:0
				});
				break;
			//
			default:
				console.log("Acción no definida", playerAddForm);
				break;
		}
	}

	//
	function eventIdUpdate(e, cEl) {
		e.preventDefault();
		let nEventId = intVal(cEl.value),
			uIndex = intVal(cEl.se_closest('tr').se_data('index'));

		app.event_user_updateId(uIndex, nEventId, {
			onIDUsed:() => {
				cEl.value = 0;
			},
			onSuccess:() => {
				console.log("my body is ready");
			}
		});
	}

	//
	function team_playerLink(pData) {
		// Update properties
		teamData.players_index.push(pData.index);
		teamData.players_list.push(pData);
		teamData.sync = 1;

		console.log("New teamData", teamData);

		// Execution
		team_dbUpdate();
	}

	//
	function team_playerUnlink(index) {
		// Update properties
		for ( let tIndex in teamData.players_index ) {
			if ( !teamData.players_index.hasOwnProperty(tIndex) ) { continue; }
			if ( teamData.players_index[tIndex] === index ) {
				teamData.players_index.splice(tIndex, 1);
			}
		}

		//
		for ( let tIndex in teamData.players_list ) {
			if ( !teamData.players_list.hasOwnProperty(tIndex) ) { continue; }
			if ( teamData.players_list[tIndex].index === index ) {
				teamData.players_list.splice(tIndex, 1);
			}
		}

		//
		teamData.sync = 1;

		// Execution
		team_dbUpdate();
	}

	//
	function team_dbUpdate() {
		app.idb.put('game_teams', teamData, {
			onSuccess:(e) => {
				console.log("Equipo actualizado: ", e);
				gameContent.se_empty();
				gameContent.se_append(se.struct.stringPopulateMany(gameTemplate.se_html(), teamData.players_list));

				//
				playerAddForm.se_formElVal('index', '');
				playerAddForm.se_formElVal('lregId', '');
				playerAddForm.se_formElVal('eurId', '');
				playerAddForm.se_formElVal('eventId', '');
				playerAddForm.se_formElVal('title', '');
				playerAddForm.se_formElVal('action', '');
				playerAddForm.se_formEl('title').focus();
				playerAddFormBtn.disabled = true;
			}
		});
	}

	//
	init();
	return {};
};
