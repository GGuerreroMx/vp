﻿"use strict";

//
se.plugin.uwp_page_games_single_section_group_match_add_single = function ( plugElem, plugOptions ) {
	let matchAddForm = plugElem.querySelector('form[se-elem="addMatch"]'),
		matchAddFormResponse = matchAddForm.querySelector('output[se-elem="response"]'),
		matchAddFormSubmit = matchAddForm.querySelector('button[type="submit"]'),
		matchAddFormGroup = matchAddForm.se_formEl('ggId'),
		matchAddFormTime = matchAddForm.se_formEl('timeStart'),
		//
		gameMatches = plugElem.querySelector('div[se-elem="matches"]'),
		gameTemplate = gameMatches.querySelector('template'),
		gameContent = gameMatches.querySelector('[se-elem="content"]'),
		//
		gameInformation = {},
		matchCreateInfo = {
			gId:0,
			gTitle:'',
			ggId:0,
			ggTitle:'',
			gsId:0,
			gsTitle:''
		},
		currentStatus = {
			gId:0,
			ggId:0
		};

	//
	function init() {
		let params = se.url.str_parse(window.location.search);

		// Parse strings
		params.gId = intVal(params.gId);
		params.gsId = intVal(params.gsId);
		params.ggId = intVal(params.ggId);
		//
		currentStatus.gId = params.gId;
		currentStatus.ggId = params.ggId;

		console.log("PAGE:Games matches add init.");

		// Get information
		let queries = [
			{
				rName:'games',
				table:'games',
				params:{
					get:params.gId
				}
			},
			{
				rName:'game_stages',
				table:'game_stages',
				params: {
					get:params.gsId
				}
			},
			{
				rName:'game_groups',
				table:'game_groups',
				params: {
					get:params.ggId
				}
			},
			{
				rName:'game_matches',
				table:'game_matches',
				params:{
					index:'ggId',
					keyRange:IDBKeyRange.only(params.ggId)
				}
			},
			{
				rName:'event_fields',
				table:'event_fields',
				params:{}
			},
			{
				rName:'game_teams',
				table:'game_teams',
				params:{
					index:'gId',
					keyRange:IDBKeyRange.only(params.gId)
				}
			}
		];

		// Procesar tipo de juego y jugadores
		app.idb.queryBatch(queries, {
			onSuccess:(results) => {
				console.log("Game information query:", results);
				gameInformation = results;

				// Navegación
				let navigation = plugElem.querySelector('div[se-elem="navigation"]');
				se.uwp.elementTemplateProc(navigation, {
					gId:params.gId,
					gsId:params.gsId,
					ggId:params.ggId,
					gTitle:results.games.title,
					cTitle:results.game_stages.title,
					ggTitle:results.game_groups.title
				});

				// Obtener información del grupo seleccionado
				for ( let cCat of gameInformation.games.structure ) {
					for ( let cGroup of cCat.game_groups ) {
						console.log("checking all groups", cGroup.id, currentStatus.ggId);
						if ( cGroup.id === currentStatus.ggId ) {
							console.log("GROUP FOUND");
							// Defaults for all matches
							matchCreateInfo.gId = gameInformation.games.id;
							matchCreateInfo.gTitle = gameInformation.games.title;
							matchCreateInfo.gsId = cCat.data.id;
							matchCreateInfo.gsTitle = cCat.data.title;
							matchCreateInfo.ggId = cGroup.id;
							matchCreateInfo.ggTitle = cGroup.title;
							//
							matchAddFormTime.calendarInput.setUnix(cGroup.dtStartUNIX);
							break;
						}
					}
				}

				//
				gameContent.se_append(se.struct.stringPopulateMany(gameTemplate.se_html(), results.game_matches));

				//
				addOptions('fId', results.event_fields);
				addOptions('tId_1', results.game_teams);
				addOptions('tId_2', results.game_teams);
			},
			onFail:() => {

			}
		});

		// Bindings
		matchAddForm.se_on('submit', matchCreate);
	}

	//
	function timeUpdate() {
		console.log("game group id", ggId);
		for ( let cCat of gameInformation.games.structure ) {
			for ( let cGroup of cCat.game_groups ) {
				if ( cGroup.id === currentStatus.ggId ) {
					matchAddFormTime.calendarInput.setUnix(cGroup.dtStartUNIX);
				}
			}
		}
	}

	//
	function addOptions(inputName, data) {
		let cEl = matchAddForm.se_formEl(inputName);
		for ( let cData of data ) {
			let option = document.createElement('option');
			option.value = cData.id;
			option.text = cData.title;
			cEl.add(option);
		}
	}

	//
	function matchCreate(e) {
		e.preventDefault();

		let elT1 = matchAddForm.se_formEl('tId_1'),
			elT2 = matchAddForm.se_formEl('tId_2'),
			elField = matchAddForm.se_formEl('fId'),
			t1Id = intVal(elT1.value),
			t2Id = intVal(elT2.value),
			//
			monthIndex = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
			timeStartUnix = matchAddFormTime.calendarInput.getUnix(),
			timeStart = matchAddFormTime.calendarInput.getDate();


		// Validación
		if ( t1Id === t2Id ) {
			alert("Mismo equipo en ambas opciones");
			return;
		}

		//
		let matchProperties = {
			id:0,
			gId:matchCreateInfo.gId,
			mOrder:0,
			mStatus:1,
			gTimeDay:`${ ('0'+timeStart.getDate()).slice(-2) } ${monthIndex[timeStart.getMonth()]}`,
			gTimeHour:`${ ('0'+timeStart.getHours()).slice(-2) }:${ ('0'+timeStart.getMinutes()).slice(-2) }`,
			gTimeUnix:timeStartUnix,
			gTimeEst:null,
			t1_id:t1Id,
			t1_name:elT1.options[elT1.selectedIndex].text,
			t1_side:0,
			t1_status:0,
			t1_points:0,
			t2_id:t2Id,
			t2_name:elT2.options[elT2.selectedIndex].text,
			t2_side:0,
			t2_status:0,
			t2_points:0,
			mPoints:[],
			mEvents:[],
			fId:intVal(elField.options[elField.selectedIndex].value),
			fTitle:elField.options[elField.selectedIndex].text,
			ggId:matchCreateInfo.ggId,
			ggTitle:matchCreateInfo.ggTitle,
			gsId:matchCreateInfo.gsId,
			gsTitle:matchCreateInfo.gsTitle,
			gTitle:matchCreateInfo.gTitle,
			teams:[t1Id, t2Id],
			sync:1
		};

		//
		// console.log("match", matchProperties);

		//
		app.idb.add('game_matches', matchProperties, {
			onSuccess:(e) => {
				//
				elT1.value = '';
				elT2.value = '';
				//
				matchProperties.id = e.target.result;
				gameContent.se_append(se.struct.stringPopulate(gameTemplate.se_html(), results.game_matches));
			}
		});
	}

	//
	init();
	return {};
};
