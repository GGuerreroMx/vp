﻿"use strict";
se.plugin.uwp_page_games_matches = function ( plugElem, plugOptions ) {
	//
	const
		gameTemplate = plugElem.querySelector('template'),
		gameContent = plugElem.querySelector('[se-elem="content"]'),
		//
		searchTeamInput = plugElem.querySelector('input[se-elem="searchTeam"]'),
		//
		statusDialog = plugElem.querySelector('dialog[se-elem="status"]'),
		statusForm = statusDialog.querySelector('form'),
		//
		sideDialog = plugElem.querySelector('dialog[se-elem="side"]'),
		sideForm = sideDialog.querySelector('form');

	//
	let temp2 = '';

	//
	function init() {
		let params = se.url.str_parse(window.location.search),
			navigationElement,
			navigationInfo = {},
			filter = {},
			headerQuery = [
				{
					rName:'games',
					table:'games',
					params: {
						get:intVal(params.gId)
					}
				}
			];


		console.log("PAGE: GAME MATCHES (POSSIBLE FILTERING).", params);

		// Determinar la forma (si no es default)
		if ( params.hasOwnProperty('tIndex') ) {
			console.log("filter by team");

			//
			filter.index = 'tId';
			filter.keyRange = IDBKeyRange.only(intVal(params.tIndex));

			//
			headerQuery.push({
				rName:'game_teams',
				table:'game_teams',
				params: {
					get:intVal(params.tIndex)
				}
			});

			//
			navigationElement = plugElem.querySelector('div[se-elem="navigation"][data-mode="team_single"]');
			navigationElement.se_attr('aria-hidden', 'false');

			// Generar json data
			app.idb.queryBatch(headerQuery, {
				onSuccess:(results) => {
					console.log("HEADER RESULTS", results);

					//
					se.uwp.elementTemplateProc(navigationElement, {
						gId:params.gId,
						tIndex:params.tIndex,
						gTitle:results.games.title,
						tTitle:results.game_teams.title
					});
				}
			});
		}
		else if ( params.hasOwnProperty('ggId') ) {
			console.log("filter by group");

			//
			filter.index = 'ggId';
			filter.keyRange = IDBKeyRange.only(intVal(params.ggId));

			//
			headerQuery.push(
				{
					rName:'game_stages',
					table:'game_stages',
					params: {
						get:intVal(params.gsId)
					}
				},
				{
					rName:'game_groups',
					table:'game_groups',
					params: {
						get:intVal(params.ggId)
					}
				}
			);

			//
			navigationElement = plugElem.querySelector('div[se-elem="navigation"][data-mode="group_all"]');
			navigationElement.se_attr('aria-hidden', 'false');


			// Generar json data
			app.idb.queryBatch(headerQuery, {
				onSuccess:(results) => {
					console.log("HEADER RESULTS", results);

					//
					se.uwp.elementTemplateProc(navigationElement, {
						gId:params.gId,
						gsId:params.gsId,
						ggId:params.ggId,
						gTitle:results.games.title,
						cTitle:results.game_stages.title,
						ggTitle:results.game_groups.title
					});
				}
			});
		}
		else if ( params.hasOwnProperty('gsId') ) {
			console.log("filter by section");

			//
			filter.index = 'gsId';
			filter.keyRange = IDBKeyRange.only(intVal(params.gsId));

			//
			headerQuery.push(
				{
					rName:'game_stages',
					table:'game_stages',
					params: {
						get:intVal(params.gsId)
					}
				}
			);

			//
			navigationElement = plugElem.querySelector('div[se-elem="navigation"][data-mode="section_all"]');
			navigationElement.se_attr('aria-hidden', 'false');

			// Generar json data
			app.idb.queryBatch(headerQuery, {
				onSuccess:(results) => {
					console.log("HEADER RESULTS", results);

					//
					se.uwp.elementTemplateProc(navigationElement, {
						gId:params.gId,
						gsId:params.gsId,
						gTitle:results.games.title,
						cTitle:results.game_stages.title
					});
				}
			});
		}
		else if ( params.hasOwnProperty('gId') ) {
			console.log("filter by games");

			//
			filter.index = 'gId';
			filter.keyRange = IDBKeyRange.only(intVal(params.gId));

			//
			navigationElement = plugElem.querySelector('div[se-elem="navigation"][data-mode="games_all"]');
			navigationElement.se_attr('aria-hidden', 'false');

			//
			app.idb.query('games', {
				get:intVal(params.gId),
				onSuccess:(data) => {
					console.log("GAMES INFORMATION", data);

					//
					se.uwp.elementTemplateProc(navigationElement, {
						gId:params.gId,
						gTitle:data.title
					});
				}
			});
		} else {
			plugElem.querySelector('div[se-elem="navigation"][data-mode="matches_all"]').se_attr('aria-hidden', 'false');
		}

		//
		console.log("IDB GAME FILTER INFO:", filter);

		// Print matches
		gameContent.se_empty();

		//
		app.idb.query('game_matches', se.object.merge(filter, {
			onSuccess:(data) => {
				gameContent.se_append(se.struct.stringPopulateMany(gameTemplate.se_html(), data));
			}
		}));
				
		// Bindings
		plugElem.se_on('click', '[se-act]', btnActions);
		//
		searchTeamInput.se_on('change', searchTeamFieldAction);
		//
		statusForm.se_on('submit', statusUpdate);
		//
		sideForm.se_on('change', 'input', sideSelect);
		sideForm.se_on('submit', sideUpdate);
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		let id;
		//
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'statusUpd':
				id = cBtn.se_closest('tr').se_data('index');
				statusDialogOpen(id);
				break;
			//
			case 'statusUpdCancel':
				statusDialog.close();
				break;
			//
			case 'sideUpd':
				id = cBtn.se_closest('tr').se_data('index');
				sideDialogOpen(id);
				break;
			//
			case 'sideUpdCancel':
				sideDialog.close();
				break;

			//
			default:
				console.error("Boton no programado", cBtn);
				break;
		}
	}

	//
	function searchTeamFieldAction(e) {
		e.preventDefault();

		//
		gameContent.querySelectorAll('td.searched').se_classDel('searched');

		//
		let cSearch = searchTeamInput.value.trim();
		if ( cSearch ) {
			cSearch = cSearch.toLowerCase();
			gameContent.querySelectorAll('td[se-elem="teamName"]').forEach((el) => {
				if ( el.textContent.toLowerCase().includes(cSearch) ) {
					el.se_classAdd('searched');
				}
			});
		}
	}

	//<editor-fold desc="Status">
	//
	function statusDialogOpen(mIndex) {
		console.log("STATUS DIALOG OPEN. INDEX: ", mIndex);
		// Load SQL
		app.idb.query('game_matches', {
			get:parseInt(mIndex),
			onSuccess:(data) => {
				//
				se.form.loadData(statusForm,  data );
				console.log("DATOS CARGADOS", data);
				//
				statusDialog.showModal();
			}
		});
	}

	//
	function statusUpdate(e) {
		e.preventDefault();
		let eData = se.form.serializeToObj(statusForm, false, {'int':['index', 'mStatus']});

		//
		app.idb.update('game_matches', eData.index,
			{
				'mStatus':eData.mStatus
			}, {
				onSuccess:(data) => {
					// Actualizar tabla
					let cRow = gameContent.querySelector('tr[data-index="' + eData.index + '"');
					cRow.se_data("status", eData.mStatus);
					//
					statusDialog.close();
				}
			}
		);
	}
	//</editor-fold>

	//<editor-fold desc="Side">
	//
	function sideSelect(e, cEl) {
		let cName = cEl.name,
			cVal = intVal(cEl.value);
		console.log("side select operation", cName, cVal);
		//
		switch ( cName ) {
			//
			case 't1_side':
				sideForm.se_formElVal('t2_side', ( cVal === 2 ) ? 1 : 2);
				break;

			//
			case 't2_side':
				sideForm.se_formElVal('t1_side', ( cVal === 2 ) ? 1 : 2);
				break;

			//
			default:
				console.log("");
				break;
		}
	}

	//
	function sideDialogOpen(mIndex) {
		console.log("loading", mIndex);
		// Load SQL
		app.idb.query('game_matches', {
			get:parseInt(mIndex),
			onSuccess:(data) => {
				//
				se.form.loadData(sideForm,  data );
				console.log("DATOS CARGADOS", data, sideForm.querySelector('span[se-elem="t1_name"]'));
				//
				sideForm.querySelector('td[se-elem="t1_name"]').se_text(data.t1_name);
				sideForm.querySelector('td[se-elem="t2_name"]').se_text(data.t2_name);
				//
				sideDialog.showModal();
			}
		});
	}

	//
	function sideUpdate(e) {
		e.preventDefault();
		let eData = se.form.serializeToObj(sideForm, false, {'int':['index', 't1_side', 't2_side']});

		//
		app.idb.update('game_matches', eData.index, {
			't1_side':eData.t1_side,
			't2_side':eData.t2_side
		}, {
			onSuccess:(data) => {
				// Actualizar tabla
				let cRow = gameContent.querySelector('tr[data-index="' + eData.index + '"');
				cRow.querySelector('svg[data-mode="side"][data-side="1"]').se_data("status", eData.t1_side);
				cRow.querySelector('svg[data-mode="side"][data-side="2"]').se_data("status", eData.t2_side);
				//
				sideDialog.close();
			}
		});
	}
	//</editor-fold>

	//
	init();
	return {};
};
