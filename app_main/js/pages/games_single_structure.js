﻿"use strict";
se.plugin.uwp_page_games_single_structure = function ( plugElem, plugOptions ) {
	//
	const
		sectionTemplate = plugElem.querySelector('template[se-elem="sections"]'),
		groupTemplate = plugElem.querySelector('template[se-elem="groups"]'),
		//
		sectionContainer = plugElem.querySelector('div[se-elem="sections"]');

	//
	function init() {
		let params = se.url.str_parse(window.location.search),
			gId = intVal(params.gId);

		//
		let queries = [
			{
				rName: 'games',
				table: 'games',
				params: {
					get:gId,
				}
			},
			{
				rName: 'game_stages',
				table: 'game_stages',
				params: {
					index:'gId',
					keyRange:IDBKeyRange.only(gId)
				}
			},
			{
				rName: 'game_groups',
				table: 'game_groups',
				params: {
					index:'gId',
					keyRange:IDBKeyRange.only(gId)
				}
			},
		];

		// Generar json data
		app.idb.queryBatch(queries, {
			onSuccess: (results) => {
				console.log("PAGE: DATA LOADED", results);

				// Update Page
				let navigation = plugElem.querySelector('div[se-elem="navigation"]');
				se.uwp.elementTemplateProc(navigation, {
					gId:params.gId,
					gTitle:results['games'].title
				});

				// Sections
				sectionContainer.se_append(se.struct.stringPopulateMany(sectionTemplate.se_html(), results['game_stages']));

				// Groups
				for ( let cGroup of results['game_groups'] ) {
					sectionContainer.querySelector('div.section[data-id="' + cGroup.gsId + '"] div[se-elem="groups"]').se_append(se.struct.stringPopulate(groupTemplate.se_html(), cGroup));
				}
			}
		});
	}

	//
	init();
	return {};
};
