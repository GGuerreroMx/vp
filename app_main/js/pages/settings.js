﻿"use strict";
se.plugin.uwp_page_settings = function(plugElem, plugOptions) {
	let settingsForm = plugElem.querySelector( 'form[se-elem="settings"]' ),
		settingsFormResponse = settingsForm.querySelector('output[se-elem="response"]'),
		//
		iotForm = plugElem.querySelector('form[se-elem="iot"]'),
		//
		notifOption = plugElem.querySelector('input[name="notifications"]');

	//
    function init() {
		console.log( "PAGE - SETTINGS" );
		console.log( "settings", appState.settings );
		// Set program variables
	    // settingsForm.se_formElVal( 'id', appState.settings.id );
	    settingsForm.se_formElVal( 'online', appState.settings.online );
	    notifOption.value = appState.settings.notifications;

		// Load IoT Data
		let tempAppState = localStorage.getItem( 'IoT' );
		//
		if ( tempAppState ) {
		    se.form.loadData(iotForm, JSON.parse( tempAppState ));
		}

		// Callbacks
		plugElem.se_on( 'click', 'button[se-act]', btnActions );
		settingsForm.se_on( 'change', 'input', settingUpdate );
		notifOption.se_on('change', app.notifications.toggle);
		iotForm.se_on('submit', iotFormSubmit);
    }

    //
	function iotFormSubmit(e) {
    	e.preventDefault();
		localStorage.setItem('IoT', JSON.stringify(se.form.serializeToObj(iotForm)));
	}

	//
	function settingUpdate( e, cEl ) {
		e.preventDefault();
		if ( cEl.type !== 'checkbox' ) {
			appState.settings[cEl.name] = cEl.value;
		} else if ( cEl.type === 'checkbox' ) {
			appState.settings[cEl.name] = ( cEl.checked );
		}

		//
		app.stateUpdate();
		// Posible update
		if ( cEl.name === 'developing' ) {
			appVariables.server.active = ( cEl.checked ) ? appVariables.server.dev : appVariables.server.live;
			console.log( "server udpated", appVariables.server.active);
		}
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'connectionTest':
				connectionTest(cBtn);
				break;
			//
			case 'syncRegistrations':
				syncRegistrations();
				break;
			//
			case 'syndGamesDownload':
				syndGamesDownload();
				break;
			//
			default:
				console.log("Boton no programado", cBtn);
				break;
		}
	}

	//
	function connectionTest( cBtn) {
		console.log( "Realizar prueba conexión. Servidor: ", appVariables.server.active );
		
		//
		se.uwp.httpPost( appVariables.server.active + '/server/event_operations/test',
			{ 'objId': appState.settings.id, 'objPw': appState.settings.pw }, {
				onSuccess: function ( msg ) {
					se.struct.notifAdvanced( settingsFormResponse, 'notification success', 'Conexión aprobada', '', 'fa-check' );
				},
				onFail: function () {
					se.struct.notifAdvanced( settingsFormResponse, 'notification alert', 'Formato incorrecto', 'No parece contener el formato correcto para continuar.', 'fa-remove' );
				},
				onComplete: function () {
					cBtn.disabled = false;
				},
				onRequest: function () {
					se.struct.notifAdvanced( settingsFormResponse, 'notification', 'Enviando solicitud', '', 'fa-spinner', 'spin' );
					cBtn.disabled = true;
				},
				onProgress: function (x) {
					console.log( "progress", x);
				}
			} );
	}

	//
    function syncRegistrations() {
    	//
	    app.request( '/registration/readAll', {}, {
			onSuccess: ( msg ) => {
				// Seleccionar tabla
				console.log("elementos", msg.d);
				app.idb.rebuild( 'registrations', msg.d, {
					onComplete: function () {
						console.log( "regs synced" );
					}
				} );
			}
        } );
	}

	//
	function syndGamesDownload() {
		//
		app.request('/games/syncDownload', {}, {
			onSuccess: (msg) => {
				let dbTXN, dbStore, name, j, cData;
				// Modifiar entorno

				// Procesar tablas

				// Seleccionar tabla
				for ( name in msg.d.db ) {
					if ( !msg.d.db.hasOwnProperty(name) ) { continue; }
					cData = msg.d.db[name];
					console.log("IDB: ", name, cData, cData.length);
					// Elemento de grabación
					dbTXN = app.idb.obj.transaction(name, "readwrite");
					dbStore = dbTXN.objectStore(name);
					dbStore.clear();
					// Agregar elementos
					for ( j in cData ) {
						if ( cData.hasOwnProperty(j) ) {
							dbStore.add(cData[j]);
						}
					}

					//
					dbTXN.onerror = (e) => {
						let error = e.target.error;
						//
						if ( error.name === 'AbortError' ) { return false; }
						//
						console.log("Error en la transacción...", error);
					};
					}

				// Notificar si hay exito
				console.log("App: All data reload.");
			}
		});
	}

    //
    init();
    return {};
};
