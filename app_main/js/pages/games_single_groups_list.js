﻿"use strict";
se.plugin.uwp_page_games_single_groups_list = function ( plugElem, plugOptions ) {
	let gameTemplate = plugElem.querySelector('template'),
		gameContent = plugElem.querySelector('[se-elem="content"]'),
		//
		temp2 = '';

	//
	function init() {
		let params = se.url.str_parse(window.location.search),
			gId = intVal(params.gId);

		//
		app.idb.query('games', {
			get:gId,
			onSuccess:(data) => {
				let navigation = plugElem.querySelector('div[se-elem="navigation"]');
				se.uwp.elementTemplateProc(navigation, {
					gId:params.gId,
					gTitle:data.title
				});
			}
		});

		//
		gameContent.se_empty();
		app.idb.query('game_groups', {
			index:'gId',
			keyRange:IDBKeyRange.only(gId),
			onSuccess:(data) => {
				gameContent.se_append(se.struct.stringPopulateMany(gameTemplate.se_html(), data));
			}
		});
	}

	//
	init();
	return {};
};
