﻿"use strict";

//
app.event_user_updateId = function(userIndex, newEventId, actions) {
	// Revisar que no exista un usuario en el evento con este número
	app.idb.query('event_players', {
		index: 'eventId',
		keyRange: IDBKeyRange.only( newEventId ),
		onSuccess:(result) => {
			if ( typeof actions.onIDUsed === 'function' ) {
				actions.onIDUsed(result);
			}
			console.log("Número no disponible", result);
		},
		onEmpty:() => {
			app.idb.update('event_players', userIndex, { sync:1, eventId:newEventId }, {
				onSuccess:(data) => {
					console.log("Event player updated");
					//
					app.idb.query('game_teams', {
						index: 'players_index',
						keyRange: IDBKeyRange.only( userIndex ),
						onSuccess:(teams) => {
							for ( let tIndex in teams ) {
								if ( !teams.hasOwnProperty(tIndex) ) { continue; }
								//
								for ( let pIndex in teams[tIndex].players_list ) {
									if ( !teams[tIndex].players_list.hasOwnProperty(pIndex) ) { continue; }
									if ( teams[tIndex].players_list[pIndex].index === userIndex ) {
										teams[tIndex].players_list[pIndex].eventId = newEventId;
										break;
									}
								}
								teams[tIndex]['sync'] = 1;
								if ( typeof actions.onSuccess === 'function' ) {
									actions.onSuccess();
								}
							}

							// Actualizar DB de nuevo de los equipos
							app.idb.updateMany('game_teams', 'id', teams, {
								onSuccess: () => {
									console.log("Equipos actualizados");
								},
								onError: (err) => {
									console.log("Equipos no actualizados", err);
								},
							});
						}
					});
				}
			});
		}
	});
	//
};

// Check if in NW.JS context, else behave like website
if ( ("require" in window ) ) {
	// Load native UI library
	var gui = require('nw.gui'),
		win = gui.Window.get(),
		tray = new gui.Tray({title: 'Tray', icon: 'images/StoreLogo.png'}),
		guiMenu = new gui.Menu();

	//
	guiMenu.append(new gui.MenuItem({
		type: 'normal', label: 'Abrir Ventana', 'click': () => {
			//
			win.show();
		}
	}));
	guiMenu.append(new gui.MenuItem({type: 'separator'}));
	guiMenu.append(new gui.MenuItem({
		type: 'normal', label: 'X Cerrar aplicación', 'click': () => {
			//
			if ( confirm("Esta por cerrar el servidor de VP.\n¿Continuar?")) {
				win.close(true);
			}
		}
	}));

	//
	tray.menu = guiMenu;

	// Al cerrar, esconder
	win.on('close', () => {
		win.hide();
	});
}