﻿"use strict";

//
var
	appState = {
	    loaded:false,
	    settings:{
	        setup:false,
			online:false,
			developing:false,
		    connected:false,
		    notifications:false,
		    key:''
		},
		eventData:{
	        id:0,
			title:'',
			pTitle:'',
			dtStart:'',
			dtEnd:'',
			dtStartUnix:'',
			dtEndUnix:''
		}
	},
	appVariables = {
		bluetooth:null,
		server: {
			live: 'https://www.vivepaintball.com.mx',
			dev: 'https://vivepaintball.cora.snako.dev',
			active: ''
		}
	};

//
if ( typeof site === 'undefined' ) {
	var site = {
		idb:null
	};
}

//
site.idb = {
	version:1,
	name: 'vp',
	status: {
		init: false
	},
	reset: false,
	structure: {
		//
		'season_players': {
			build: { keyPath: "id" },
			indexes: {
				'oType': {
					keyPath: 'oType',
					params: { unique: false }
				}
			}
		},
		'event_players': {
			build: { keyPath: "index", autoIncrement:true },
			indexes: {
				'id': {
					keyPath: 'id',
					params: { unique: false }
				},
				'eurIdReg': {
					keyPath: 'eurIdReg',
					params: { unique: false }
				},
				'lregId': {
					keyPath: 'lregId',
					params: { unique: false }
				},
				'shortId': {
					keyPath: 'shortId',
					params: { unique: false }
				},
				'eventId': {
					keyPath: 'eventId',
					params: { unique: false }
				},
				'uName': {
					keyPath: 'uName',
					params: { unique: false }
				},
				'tName': {
					keyPath: 'tName',
					params: { unique: false }
				},
				'sync': {
					keyPath: 'sync',
					params: { unique: false }
				}
			}
		},
		'event_fields': {
			build: { keyPath: "id" },
			indexes: {}
		},
		//
		'gametypes': {
			build: { keyPath: "id" },
			indexes: {}
		},
		'server_fields': {
			build: { keyPath: "id", autoIncrement:true },
			indexes: {
				'gameFilter': {
					keyPath:["grId", "fId"],
					params: { unique: false }
				},
			}
		},
		'server_devices': {
			build: { keyPath: "id", autoIncrement:true },
			indexes: {}
		},
		'games': {
			build: { keyPath: "id" },
			indexes: {}
		},
		'game_stages': {
			build: { keyPath: "id" },
			indexes: {
				'gId': {
					keyPath: 'gId',
					params: { unique: false }
				}
			}
		},
		'game_groups': {
			build: { keyPath: "id" },
			indexes: {
				'gId': {
					keyPath: 'gId',
					params: { unique: false }
				},
				'gsId': {
					keyPath: 'gsId',
					params: { unique: false }
				}
			}
		},
		'game_matches': {
			build: { keyPath: "index", autoIncrement:true },
			indexes: {
				'id': {
					keyPath: 'id',
					params: { unique: false }
				},
				'mOrder': {
					keyPath: 'mOrder',
					params: { unique: false }
				},
				'cOrder': {
					keyPath: ["ggId", "fId", "mOrder"],
					params: { unique: true }
				},
				'gameFilter': {
					keyPath: ["gsId", "fId"],
					params: { unique: false }
				},
				'gId': {
					keyPath: 'gId',
					params: { unique: false }
				},
				'ggId': {
					keyPath: 'ggId',
					params: { unique: false }
				},
				'gsId': {
					keyPath: 'gsId',
					params: { unique: false }
				},
				'fId': {
					keyPath: 'fId',
					params: { unique: false }
				},
				'tId': {
					keyPath: 'tId',
					params: { unique: false, multiEntry:true }
				},
				'sync': {
					keyPath: 'sync',
					params: { unique: false }
				}
			}
		},
		'game_teams': {
			build: { keyPath: "index", autoIncrement:true },
			indexes: {
				'id': {
					keyPath: 'id',
					params: { unique: false }
				},
				'gId': {
					keyPath: 'gId',
					params: { unique: false }
				},
				'players_index': {
					keyPath: 'players_index',
					params: { unique: false, multiEntry:true }
				},
				'sync': {
					keyPath: 'sync',
					params: { unique: false }
				}
			}
		}
	}
};

//
if ( typeof module === 'undefined' ) {
	var module = {};
}

//
module.exports = {
	siteIDB:site.idb
};