"use strict";

//
se.plugin.userSiteLogIn = function(plugElem, plugOptions) {
	let defaults = {},
		settings = se.object.merge(defaults, plugOptions),
		//
		inputPassword = plugElem.querySelector('input[name="current-password"]'),
		inputShowPass = plugElem.querySelector('input[se-elem="showPass"]');

	//
	function init() {
		//
		if ( inputShowPass ) {
			inputShowPass.se_on('change', passWordShowToggle);
		}


		//
		plugElem.se_plugin('simpleForm', {
			save_url: '/ajax/user/log/in/email',
			onSuccess: (msg) => {
				se.user.logIn(msg, plugElem);
			}
		});
	}

	//
	function passWordShowToggle(e) {
		e.preventDefault();
		if ( inputShowPass.checked ) {
			inputPassword.se_attr('type', 'text');
		} else {
			inputPassword.se_attr('type', 'password');
		}
	}

	//
	init();
	return {};
};
//
se.plugin.userSiteRegistration = function(element, options) {
	element.se_plugin('simpleForm', {
		save_url:'/ajax/user/register',
		onSuccess:(msg) => {
			se.user.logIn(msg, element);
		}
	});
};
//
se.plugin.shareAddon = function(element, options) {
	let defaults = {
			socnets:'facebook,twitter,whatsapp,email,linkedin,reddit,pinterest'
		},
		settings = se.object.merge(defaults, options),
		//
		plugin = this;

	//
	function init() {
		element.se_on('click', sharePage);
	}

	//
	async function sharePage() {
		let shareData = getData();

		if ( !navigator.canShare ) {
			console.error('navigator.canShare() not supported.');
		}
		else if ( navigator.canShare(shareData) ) {
			try {
				await navigator.share(shareData)
			} catch(err) {
				console.error('SHARING ERROR: ' + err );
			}
		}
		else {
			shareWindowCreate();
		}
	}

	//
	function getData(urlEncode = false) {
		let getData = [
				{ name:'title', query:'meta[property="og:title"]', attribute:'content' },
				{ name:'text', query:'meta[property="og:description"]', attribute:'content' },
				{ name:'url', query:'meta[property="og:url"]', attribute:'content' },
				{ name:'siteName', query:'meta[property="og:site_name"]', attribute:'content' },
				{ name:'image', query:'meta[property="og:image"]', attribute:'content' },
				{ name:'twName', query:'meta[name="twitter:site:name"]', attribute:'content' },
			],
			results = {},
			tempVal,
			tempQry;

		// Lectura del sitio
		for ( let cElem of getData ) {
			tempVal = '';
			if ( ( tempQry = document.head.querySelector(cElem.query)) ) {
				tempVal = tempQry.se_attr(cElem.attribute);
				if ( urlEncode ) {
					tempVal = encodeURIComponent(tempVal);
				}
			}
			results[cElem.name] = tempVal;
		}

		console.warn("current page information", results);

		return results;
	}

	//
	function shareWindowCreate() {
		let shareWindow = $('#se_shareWindow'),
			targets = settings.socnets.split(","),
			isMobileDevice = isMobile.any(),
			//
			url, valid, direct, isMain, icon, width, height,
			btns = '',
			//
			shareData = getData(true);
		//
		if ( !shareWindow ) {
			shareWindow = document.body.se_prepend('<dialog class="socialShareWindow" open><div class="socialShareLinks"></div><button class="btn wide blue"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg>Cancelar</button></dialog>');
		}

		let shareLinks = shareWindow.querySelector('div.socialShareLinks'),
			shareClose = shareWindow.querySelector('button');

		// Crear botones
		for ( let cTarget of targets ) {
			url = '';
			width = 500;
			height = 375;
			valid = true;
			direct = false;
			isMain = false;
			icon = cTarget;
			switch ( cTarget ) {
				case 'facebook':
					url = 'https://www.facebook.com/sharer/sharer.php?u=' + shareData.url;
					isMain = true;
					icon = cTarget + '-f';
					break;
				case 'twitter':
					url = 'https://twitter.com/intent/tweet/?text=' + shareData.title + '&url=' + shareData.url + '&via=' + shareData.twName;
					isMain = true;
					break;
				case 'linkedin':
					url = 'https://www.linkedin.com/shareArticle?mini=true&url=' + shareData.url + '&title=' + shareData.title + '&source=' + shareData.siteName + '&summary=' + shareData.text;
					width = 520;
					height = 570;
					break;
				case 'pinterest':
					url = 'https://www.pinterest.com/pin/create/button/?url=' + shareData.url + '&media=' + shareData.image + '&description=' + shareData.text;
					break;
				case 'reddit':
					url = 'http://www.reddit.com/submit/?url=' + shareData.url;
					icon = cTarget + '-alien';
					break;
				case 'whatsapp':
					if ( isMobileDevice ) {
						url = 'whatsapp://send?text=' + shareData.title + ' ' + shareData.url;
						direct = true;
						isMain = true;
					} else {
						valid = false;
					}
					break;
				case 'email':
					icon = cTarget = 'envelope';
					url = 'mailto:?subject=' + shareData.title + ' ' + shareData.siteName + '&body=' + shareData.text + '%0A' + shareData.url;
					direct = true;
					break;
				default:
					console.log("Socnet no definido", cTarget);
					valid = false;
					break;
			}
			if ( valid ) {
				btns+= '<a class="snb_'+ cTarget;
				btns+= ( isMain ) ? ' main"' : '"';
				btns+= ( direct ) ? ' ' : 'target="_blank" data-width="' + width + '" data-height="' + height + '" ';
				btns += 'href="'+url+'" rel="noopener"><svg class="icon"><use xlink:href="#fa-' + icon + '"></use></svg></a>';
			}
		}

		// Agregar
		shareLinks.innerHTML = btns;

		// Acción
		shareLinks.se_on('click', '[target="_blank"]', openWindow);
		shareClose.se_on('click', () => {
			shareWindow.se_attr('open', '');
		});
	}

	//
	function openWindow(event, cElem) {
		event.preventDefault();
		event.stopPropagation();
		let width = parseInt(cElem.se_data('width')),
			height = parseInt(cElem.se_data('height')),
			left = (screen.width / 2) - (width / 2),
			top = (screen.height / 2) - (height / 2);
		//
		window.open(cElem.href, "",
			"menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height + ",top=" + top + ",left=" + left
		);
	}
	//
	init();
	return {};
};

//