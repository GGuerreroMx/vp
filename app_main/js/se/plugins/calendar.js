/**
 * Actuall calendar element for not supported system calendars
 * @param (plugElem) DOM element to apply function to, a button that must be next to an input
 * @param (plugOptions) Modifiers of operation
 */
se.plugin.calendar = function(plugElem, plugOptions) {
	let defaults = {
			class:'se_calendar',
			icon:'fa-calendar-o',
			time:true,
			date:true
		},
		pSettings = se.object.merge(defaults, plugOptions),
		pProperties = {
			calendar:null,
			timer:null,
			input:null,
			sDate:null,
			tDate:null,
			showing:false,
			exist:false,
			valid:false
		};
	//
	function init() {
		//
		pProperties.input = plugElem.se_sibling('input');
		if ( pProperties.input ) {
			pProperties.valid = true;
			plugElem.se_on('click', calendarCheck);
		}
	}
	//
	function calendarBuild(date) {
		let cCal = pProperties.calendar = document.body.se_append('<div class="'+pSettings.class+'"></div>'),
			box = plugElem.getBoundingClientRect(),
			xPos = box.x + box.width - 200,
			yPos = box.y + box.height;
		cCal.se_attr('style', 'top:'+yPos+'px;left:'+xPos+'px');
		// Main
		cCal.se_append('<div class="main_control"><div class="date full"></div><button type="button" class="cBut" data-action="close"><svg class="icon"><use xlink:href="#fa-remove"></use></svg></button></div>');

		// Calendario
		if ( pSettings.date ) {
			cCal.se_append('<div class="month_nav"><button type="button" class="cBut" data-action="year_less">«</button><button type="button" class="cBut" data-action="month_less">‹</button><button type="button" class="cBut full" data-action="today">Hoy</button><button type="button" class="cBut" data-action="month_more">›</button><button type="button" class="cBut" data-action="year_more">»</button></div>');
			cCal.se_append('<table class="calendar"><thead><tr><th>D</th><th>L</th><th>M</th><th>M</th><th>J</th><th>V</th><th>S</th></tr></thead><tbody></tbody></table>');
		}

		// Reloj
		if ( pSettings.time ) {
			pProperties.timer = cCal.se_append('<div class="time"></div>');
			pProperties.timer.se_append('<div><input type="number" name="hours" min="0" max="23" value="'+date.getHours()+'" /> : <input type="number"  min="0" max="59" name="minutes" value="'+date.getMinutes()+'" /> : <input type="number" name="seconds"  min="0" max="59" value="'+date.getSeconds()+'" /></div>');
		}
		//
		cCal.se_append('<div class="last_menu"><button type="button" data-action="accept"><svg class="icon inline"><use xlink:href="#fa-check"></use></svg> Aceptar</button></div>');

		if ( pSettings.date ) {
			calendarBuildMonth(date.getFullYear(), date.getMonth());
			pProperties.showing = true;
			pProperties.exist = true;
		}

		// Callbacks
		cCal.se_on('click', 'button', butActions);
		cCal.se_on('click', 'td', pickDate);
		if ( pSettings.time ) {
			cCal.se_on('change', 'input', updTime);
		}
	}
	//
	function butActions(e, plugElem) {
		e.preventDefault();
		switch ( plugElem.se_data('action') ) {
			case 'accept':
				let dTime;
				if ( pSettings.date ) {
					if ( pSettings.time ) {
						dTime = mysqlDate(pProperties.sDate, true);
					} else {
						dTime = mysqlDate(pProperties.sDate, false);
					}
				} else {
					dTime = getTime().hhmmss;
				}
				pProperties.input.se_val(dTime);
				calendarClose();
				break;
			case 'close':
				calendarClose();
				break;
			case 'today':
				pProperties.tDate = new Date();
				pProperties.sDate = new Date(pProperties.tDate.getTime());
				calendarBuildMonth(pProperties.tDate.getFullYear(), pProperties.tDate.getMonth());
				calendarSelectDate(mysqlDate(pProperties.tDate, false));
				if ( pSettings.time ) {
					pProperties.timer.querySelector('input[name=hours]').se_val(pProperties.tDate.getHours());
					pProperties.timer.querySelector('input[name=minutes]').se_val(pProperties.tDate.getMinutes());
					pProperties.timer.querySelector('input[name=seconds]').se_val(pProperties.tDate.getSeconds());
				}
				break;
			case 'year_less':
				pProperties.tDate.setFullYear(pProperties.tDate.getFullYear() - 1);
				calendarBuildMonth(pProperties.tDate.getFullYear(), pProperties.tDate.getMonth());
				break;
			case 'year_more':
				pProperties.tDate.setFullYear(pProperties.tDate.getFullYear() + 1);
				calendarBuildMonth(pProperties.tDate.getFullYear(), pProperties.tDate.getMonth());
				break;
			case 'month_less':
				pProperties.tDate.setMonth(pProperties.tDate.getMonth() - 1);
				calendarBuildMonth(pProperties.tDate.getFullYear(), pProperties.tDate.getMonth());
				break;
			case 'month_more':
				pProperties.tDate.setMonth(pProperties.tDate.getMonth() + 1);
				calendarBuildMonth(pProperties.tDate.getFullYear(), pProperties.tDate.getMonth());
				break;
		}
	}
	//
	function mysqlDate(date, time) {
		let tDate = date.getFullYear() + '-' +
			('00' + (date.getMonth()+1)).slice(-2) + '-' +
			('00' + date.getDate()).slice(-2);
		if ( time ) {
			tDate+= ' ' +
				('00' + date.getHours()).slice(-2) + ':' +
				('00' + date.getMinutes()).slice(-2) + ':' +
				('00' + date.getSeconds()).slice(-2);
		}
		return tDate;
	}
	//
	function getTime() {
		let time = {};
		time.hours = pProperties.timer.querySelector('input[name=hours]').se_val();
		time.minutes = pProperties.timer.querySelector('input[name=minutes]').se_val();
		time.seconds = pProperties.timer.querySelector('input[name=seconds]').se_val();
		// Process
		time.hhmmss =
			('00' + time.hours).slice(-2) + ':' +
			('00' + time.minutes).slice(-2) + ':' +
			('00' + time.seconds).slice(-2);
		return time;
	}
	//
	function updTime() {
		let time = {};
		time.hours = pProperties.timer.querySelector('input[name=hours]').se_val();
		time.minutes = pProperties.timer.querySelector('input[name=minutes]').se_val();
		time.seconds = pProperties.timer.querySelector('input[name=seconds]').se_val();
		pProperties.tDate.setHours(time.hours);
		pProperties.tDate.setMinutes(time.minutes);
		pProperties.tDate.setSeconds(time.seconds);
		pProperties.sDate.setHours(time.hours);
		pProperties.sDate.setMinutes(time.minutes);
		pProperties.sDate.setSeconds(time.seconds);
		console.log("time updated");
	}
	//
	function pickDate(e, plugElem) {
		e.preventDefault();
		let date = plugElem.se_data('date').split(/[- :]/),
			time,
			tDate;
		if ( pSettings.time ) {
			time = getTime();
		} else {
			time = { hours:0, minutes:0, seconds:0 };
		}
		tDate = new Date(date[0], date[1]-1, date[2], time.hours, time.minutes, time.seconds);
		pProperties.tDate = new Date(tDate.getTime());
		pProperties.sDate = new Date(tDate.getTime());
		calendarSelectDate(plugElem.se_data('date'));
	}
	//
	function calendarSelectDate(date) {
		let tBody = pProperties.calendar.querySelector('tbody');
		tBody.querySelectorAll('.selected').se_classDel('selected');
		tBody.querySelector('[data-date="'+date+'"]').se_classAdd('selected');
	}
	//
	function calendarCheck() {
		if ( !pProperties.showing ) {
			if ( !pProperties.exist ) {
				// Split timestamp into [ Y, M, D, h, m, s ]
				let t = pProperties.input.se_val().split(/[- :]/);
				// Apply each element to the Date function
				let date = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
				if ( isNaN(date) ) {
					date = new Date();
				}
				pProperties.sDate = new Date(date.getTime());
				pProperties.tDate = new Date(date.getTime());
				calendarBuild(date);
			} else {
				pProperties.showing = true;
				pProperties.calendar.se_css('display', 'block');
			}
		}
	}
	//
	function calendarClose() {
		pProperties.showing = false;
		pProperties.calendar.se_css('display', 'none');
	}
	//
	function calendarBuildMonth(year, month) {
		let controlDate = new Date(year, month + 1, 0),
			currDate = new Date(year, month, 1),
			todayDate = new Date(),
			todayDateTime,
			iter = 0,
			ready = true,
			tBody = pProperties.calendar.querySelector('tbody'),
			tr,
			selectedDate, selectedDateTime,
			monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
				"Julio", "Agosto", "Septiembre", "Octobre", "Noviembre", "Diciembre"
			];
		// Today date fix
		todayDate.setHours(0, 0, 0, 0);
		todayDateTime = todayDate.getTime();

		// Selected date fix
		if ( pProperties.sDate ) {
			selectedDate = pProperties.sDate;
			selectedDate.setHours(0, 0, 0, 0);
			selectedDateTime = selectedDate.getTime();
		}

		// Month first row fix
		if ( currDate.getDay() !== 0 ) {
			iter = 0 - currDate.getDay()
		}
		tBody.se_empty();

		while ( ready ) {
			if ( currDate.getDay() === 6 ) {
				if (tr) { tBody.appendChild(tr); }
				tr = null;
			}
			if ( !tr ) { tr = document.createElement('tr'); }
			currDate = new Date(year, month, ++iter);
			tr.appendChild(newDayCell(currDate, iter < 1 || +currDate > +controlDate, todayDateTime, selectedDateTime));
			if ( +controlDate < +currDate && currDate.getDay() === 0 ) {
				ready = false;
			}
		}
		// Ajustar texto
		pProperties.calendar.querySelector('.main_control .date').se_text(monthNames[month] + ' ' + year);
	}
	//
	function newDayCell(dateObj, isOffset, todayDateTime, selectedDateTime) {
		let td = document.createElement('td'),
			isoDate = dateObj.toISOString(),
			dateObjTime = dateObj.getTime(),
			cClass = 'day';
		isoDate = isoDate.slice(0, isoDate.indexOf('T') );
		cClass+= ( dateObjTime < todayDateTime ) ? ' past' : '';
		cClass+= ( dateObjTime === todayDateTime ) ? ' today' : '';
		cClass+= ( dateObjTime === selectedDateTime ) ? ' selected' : '';
		cClass+= ( isOffset ) ? ' adj-month' : '';
		td.className = cClass;
		td.se_attr('data-date', isoDate);
		td.se_text(dateObj.getDate());
		return td
	}
	//
	init();
	return {};
};
