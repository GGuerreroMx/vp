"use strict";
se.plugin.se_uAdminScript = function (plugElem, options) {
	let ajaxPage = plugElem.se_data('ajaxpage'),
		formScript = plugElem.querySelector('form[se-elem="ua_scripts"]'),
		response = plugElem.querySelector('output[se-elem="response"]');

	//
	function init() {
		formScript.se_on('submit', formSubmit);
	}

	//
	function formSubmit(e) {
		e.preventDefault();
		//
		response.se_text("Solicitando información...");
		se.ajax.json(ajaxPage, se.form.serializeToObj(formScript), {
			onSuccess:(msg) => {
				response.se_text(msg.d);
			},
			onFail:(msg) => {
				console.log(msg);
				response.se_text(msg.s.e);
			},
			onError:(error) => {
				response.se_text(error);
			}
		});
	}

	init();
	return {};
};