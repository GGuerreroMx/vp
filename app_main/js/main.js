﻿"use strict";

//
function returnMessage(msg) {
	let page_server_field = $('#page_server_field');

	//
	console.log("MAIN - SW MESSAGE RECIEVED:", msg);

	//
	switch ( msg.op ) {
		//
		case 'adv':
			pages.match.admin.timer.advanceWork(msg);
			if ( msg.notif ) {
				se_app.notifications.add('Match', {
					body:msg.notif,
					// icon:'icons/chrome_36.png',
					vibrate:[500, 300, 500, 300, 500]
				});
			}
			break;
		//
		case 'field':
			if ( page_server_field ) {
				page_server_field.uwp_page_server_field.serverMessageRecieve(msg.id, msg.data);
			}
			break;
		//
		default:
			console.error("Server worker unknown message", e.data);
			break;
	}
}

//
window.addEventListener('DOMContentLoaded', function firstLoad() {
    // Single load remove
    document.removeEventListener("DOMContentLoaded", firstLoad, false);
    //
	win.showDevTools();
    //
	window.onpopstate = se.uwp.page.popState;

	window.nodeMessenger = require('./messenger');
	window.nodeMessenger.setCallback(returnMessage);

	//
	appVariables.bluetooth = new bluetooth();

	// CB
	function jsInitialize() {
		console.log( "MAIN - JS LOADED." );

		// initialize service worker (after first idb update, so idb is identicall without redefining)

		// Sistema de navegación
		document.body.se_on( 'click', '[se-nav-uwp]', se.uwp.page.navigation );

		// Crear appstates
		let tempAppState = localStorage.getItem( 'appState' );
		//
		if ( tempAppState ) {
			appState = JSON.parse( tempAppState );
			appVariables.server.active = ( appState.settings.developing ) ? appVariables.server.dev : appVariables.server.live;
			console.log( "server udpated", appVariables.server.active );
		}

		//
		app.loraUsb = new vp_loraUsb();

		// Autostart condition

		// Load images (icons)
		fetch( 'images/se-icons.svg' )
			.then(
				( response ) => {
					if ( response.status !== 200 ) {
						console.log( 'Looks like there was a problem. Status Code: ' +
							response.status );
						return;
					}
					//
					response.text().then( function ( data ) {
						document.body.se_append(data);
					} );
				}
			)
			.catch( ( err ) => {
				console.log( 'Icons not loaded: ', err );
            } );

        // Initialize page
		se.uwp.page.urlParse(window.location.search, false);
	}

	// Indexed DB
	console.log( "MAIN - IDB initialize CALL" );
	app.idb = new se_indexedDataBase(site.idb, jsInitialize );
});
