﻿"use strict";
var app = {},
	appState;

//
app.request = function ( url, postData, conditions ) {
	console.log("APPSTATE CHECK", appState);

	//
	if ( !appState.settings.key ) {
		alert("sistema no configurado correctamente (no hay llave)");
		return;
	}

	//
	url = appState.settings.server + '/server/event_operations' + url;

	// Agregar key
	if ( !('headers' in conditions) ) {
		conditions.headers = {};
	}
	conditions.headers['Authorization'] = 'Basic ' + appState.settings.key;
	conditions.headers['X-Requested-With'] = 'VPAPP';

	// Hacer solicitud
	if ( typeof Windows === "undefined" ) {
		// entorno web
		se.ajax.json(url, postData, conditions);
	} else {
		// entorno UWP
		se.uwp.httpPost(url, postData, conditions);
	}
};

app.stateUpdate = function() {
	// Put the object into storage
	console.log("actualizando appstae", appState);
	localStorage.setItem( 'appState', JSON.stringify( appState));
};

//
function uwpNavLoad(e, cBtn) {
	e.preventDefault();
	let cTarget = $('#' + cBtn.se_attr('se-nav-uwp')),
		cUrl = cBtn.se_data('src');
	uwp_pageLoad(cTarget, cUrl);
}

// Notificaicones
app.notifications = {
	toggle:function(ev) {
		console.log("change attemp");
		if ( ev.target.checked ) {
			if ( !("Notification" in window) ) {
				// Soporte
				console.log("Notifications: Not supported");
			} else if ( Notification.permission === "granted" ) {
				// Activadas, iniciar stream
				appState.settings.notifications = true;
				//
				let notification = new Notification("Notificaciones activadas.");
				setTimeout(function() {
					notification.close(); //closes the notification
				}, 3000);
			} else if ( Notification.permission !== 'denied' ) {
				// Permisos
				Notification.requestPermission(function(permission) {
					// Activadas, iniciar stream
					appState.settings.notifications = true;
					// If the user is okay, let's create a notification
					if ( permission === "granted" ) {
						let notification = new Notification("Notificaciones activadas.");
						setTimeout(function() {
							notification.close(); //closes the notification
						}, 3000);
					}
				});
			}
		} else {
			appState.settings.notifications = false;
		}
		// Guardar
		app.stateUpdate();
	},
	add:function(title, params, timeOut, actions) {
		if ( !appState.settings.notifications ) { console.log("No notifications.", title, params); return; }
		//
		let notification = new Notification(title, params);
		if ( params.vibrate ) {
			window.navigator.vibrate(params.vibrate);
		}
		if ( typeof timeOut !== 'undefined' && timeOut !== 0 ) {
			setTimeout(function() { notification.close(); }, timeOut);
		}
	}
};