# Certification authority
Son dos pasos, crear CA y crear para sitios.

## Certificado para CA
```
# we use 'ca' as the default section because we're usign the ca command
[req]
default_bits        = 2048
default_md          = sha256
default_days        = 3000
default_crl_days    = 300
dir                 = ./
database            = $dir/db.txt
encrypt_key         = no # Change to encrypt the private key using des3 or similar
prompt              = no
utf8                = yes
email_in_dn         = no 
# Speify the DN here so we aren't prompted (along with prompt = no above).
distinguished_name = req_distinguished_name
# Be sure to update the subject to match your organization.
[req_distinguished_name]
C  = MX
ST = Mexico
L  = Mexico
O  = Vive Paintball CA
OU = DEVELOPMENT

# Extensions
[v3_ca]
subjectKeyIdentifier    = hash
basicConstraints        = critical, CA:TRUE
keyUsage                = critical, digitalSignature, keyEncipherment, keyCertSign
extendedKeyUsage        = critical, serverAuth
subjectAltName          = @alt_names

[v3_req]
subjectKeyIdentifier    = hash
basicConstraints        = critical, CA:FALSE
keyUsage                = critical, digitalSignature, keyEncipherment
extendedKeyUsage        = critical, serverAuth
subjectAltName          = @alt_names

[alt_names]
IP.1  = 127.0.0.1
IP.2  = 192.168.1.100
IP.3  = 192.168.2.100
DNS.1 = localhost
DNS.2 = vivepaintball
DNS.3 = vivepaintball.local
```

Comando a ejecutar

```
# Crear certificados

# CA (Self sign (x509), get key and crt directly)
openssl req \
  -new \
  -newkey rsa:2048 \
  -x509 \
  -config "ca.cnf" \
  -extensions v3_ca \
  -keyout "ca.key" \
  -out "ca.crt"

# DOMAIN

# Request and key (single command)
openssl req \
    -new \
    -newkey rsa:2048 \
    -config "ca.cnf" \
    -extensions v3_req \
    -keyout "server.key" \
    -out "server.csr"
  
# Sign CSR -> CRT
openssl x509 \
    -req \
    -in "server.csr" \
    -CA "ca.crt" \
    -CAkey "ca.key" \
    -CAcreateserial \
    -extfile "ca.cnf" \
    -extensions v3_req \
    -out "server.crt"
    
# check cert
```