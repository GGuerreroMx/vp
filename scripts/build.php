<?php

//
$dirs = [
	'files' => __DIR__ . '/../',
	'engine' => 'C:\nwjs\sdk',
	'target' => 'D:\vp_app',
	'zipFolder' => 'D:\vp_app\content',
	'zipName' => 'D:\vp_app\content',
];

//
function recurse_copy($src, $dst) {
	$dir = opendir($src);
	@mkdir($dst);
	while ( false !== ( $file = readdir($dir) ) ) {
		// Skip folders
		if ( $file == '.'  || $file == '..' ) {
			continue;
		}

		//
		if ( is_dir($src . '/' . $file) ) {
			// Skip special folders
			if ( $file[0] == '.' ) {
				continue;
			}
			recurse_copy($src . '/' . $file,$dst . '/' . $file);
		} else {
			copy($src . '/' . $file,$dst . '/' . $file);
		}
	}

	//
	closedir($dir);
}


//
function recurse_zip($zip, $src, $cut) {
	$dir = opendir($src);

	echo "SRC: {$src}\n";

	while ( false !== ( $file = readdir($dir) ) ) {
		// Skip folders
		if ( $file == '.'  || $file == '..' ) {
			continue;
		}

		$cFile = $src.DIRECTORY_SEPARATOR.$file;

		//
		if ( is_dir($src . '/' . $file) ) {
			// Skip special folders
			if ( $file[0] == '.' ) {
				continue;
			}
			recurse_zip($zip, $cFile, $cut);
		} else {
			$shortLoc = substr($cFile, $cut);
			echo "FILE: {$cFile} - {$shortLoc} \n";
			$zip->addFile($cFile, $shortLoc);
		}
	}

	//
	closedir($dir);
}

//
echo "--\nCOPY FILES\n\n";

recurse_copy($dirs['files'], $dirs['target'].'/content');
// recurse_copy($dirs['engine'], $dirs['target'].'/content');

echo "--\nZIP FILES\n\n";

//
$zipFile = $dirs['target'].'\package.nw';
$zipArchive = new ZipArchive();

$opened = $zipArchive->open( $zipFile, ZipArchive::CREATE || ZIPARCHIVE::OVERWRITE );
if ( $opened !== true ) {
	$ZIP_ERROR = [
		ZipArchive::ER_EXISTS => 'File already exists.',
		ZipArchive::ER_INCONS => 'Zip archive inconsistent.',
		ZipArchive::ER_INVAL => 'Invalid argument.',
		ZipArchive::ER_MEMORY => 'Malloc failure.',
		ZipArchive::ER_NOENT => 'No such file.',
		ZipArchive::ER_NOZIP => 'Not a zip archive.',
		ZipArchive::ER_OPEN => "Can't open file.",
		ZipArchive::ER_READ => 'Read error.',
		ZipArchive::ER_SEEK => 'Seek error.',
	];
	$msg = isset($ZIP_ERROR[$opened])? $ZIP_ERROR[$opened] : 'Unknown error.';
	die("ZIP wont open.". $msg);
}

//
recurse_zip($zipArchive, $dirs['target'].'\content', strlen($dirs['target'].'\content') + 1);

$zipArchive->close();
//
echo "DONE";