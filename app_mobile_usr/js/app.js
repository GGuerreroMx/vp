﻿"use strict";
var se_app = {};

//
se_app.request = function ( url, postData, conditions ) {
	// entorno web
	se.ajax.json(app.state.settings.serverMain + url, postData, conditions);
};

//
se_app.stateUpdate = function() {
	// Put the object into storage
	console.log("app.state: updating. ", app.state);
	localStorage.setItem( 'app.state', JSON.stringify( app.state));
};

//
function uwpNavLoad(e, cBtn) {
	e.preventDefault();
	let cTarget = $('#' + cBtn.se_attr('se-nav-uwp')),
		cUrl = cBtn.se_data('src');
	uwp_pageLoad(cTarget, cUrl);
}

//
se_app.server_connect = function() {

	if ( app.server ) {
		app.server.disconnect();
	}

	//
	app.server = null;

	//
	switch ( app.state.connection.method ) {
		//
		case 'mqtt':
			app.server = new mqtt_server({
				'op_group':'appChange',
				'op_current':'connect',
				'op_data':{
					'url':window.location.hostname,
					'logged':app.state.logged,
					'user':app.state.connection.mqtt.user,
					'pass':app.state.connection.mqtt.pass,
				}
			});
			break;
		//
		case 'websocket':
			//
			app.server = new ws_server({
				'op_group':'appChange',
				'op_current':'connect',
				'op_data':{
					'url':'wss://' + window.location.hostname,
				}
			});
			break;
		//
		default:
			console.error("server connection method not defined.");
			break;
	}
};


// Notificaicones
se_app.notifications = {
	toggle:(ev) => {
		console.log("change attemp");
		if ( ev.target.checked ) {
			if ( !("Notification" in window) ) {
				// Soporte
				console.log("Notifications: Not supported");
			} else if ( Notification.permission === "granted" ) {
				// Activadas, iniciar stream
				app.state.settings.notifications = true;
				//
				let notification = new Notification("Notificaciones activadas.");
				setTimeout(() => {
					notification.close(); //closes the notification
				}, 3000);
			} else if ( Notification.permission !== 'denied' ) {
				// Permisos
				Notification.requestPermission(function(permission) {
					// Activadas, iniciar stream
					app.state.settings.notifications = true;
					// If the user is okay, let's create a notification
					if ( permission === "granted" ) {
						let notification = new Notification("Notificaciones activadas.");
						setTimeout(() => {
							notification.close(); //closes the notification
						}, 3000);
					}
				});
			}
		} else {
			app.state.settings.notifications = false;
		}
		// Guardar
		se_app.stateUpdate();
	},
	add:(title, params, timeOut, actions) => {
		if ( !app.state.settings.notifications ) { console.log("No notifications.", title, params); return; }
		//
		let notification = new Notification(title, params);
		if ( params.vibrate ) {
			window.navigator.vibrate(params.vibrate);
		}
		if ( typeof timeOut !== 'undefined' && timeOut !== 0 ) {
			setTimeout(function() { notification.close(); }, timeOut);
		}
	}
};