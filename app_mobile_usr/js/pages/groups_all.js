﻿"use strict";
//
se.plugin.uwp_page_groups_all = function (plugElem, plugOptions) {
	let serverFields = plugElem.querySelector('[se-elem="server_fields"]'),
		serverFieldsTemplate = serverFields.querySelector('template'),
		serverFieldsContent = serverFields.querySelector('div[se-elem="content"]')
	;

	//
    function init() {
	    console.log( "PAGE LOAD - GROUPS", app.state.settings );
	    //

	    //
	    printServerFields();

	    //
	    plugElem.se_on( 'click', 'button[se-act]', btnActions );
    }

	//
	function printServerFields() {
		serverFieldsContent.se_empty();
		app.idb.query('games', {
			onSuccess:(data) => {

				for ( let cData of data ) {
					let gameTitle = cData.title;
					// Get categories
					for ( let cCategory of cData.structure ) {
						let catTitle = gameTitle + ' - ' + cCategory.data.title;
						// get groups
						for ( let cGroup of cCategory.game_groups ) {
							serverFieldsContent.se_append( se.struct.stringPopulate( serverFieldsTemplate.se_html(), {
								id:cGroup.id,
								title:catTitle + ' - ' + cGroup.title
							}) );
						}
					}
				}
				//
			}
		});
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		//
		const cAction = cBtn.se_attr( 'se-act' );
		switch ( cAction ) {
			//
			default:
				console.log("Boton no programado: ", cAction, cBtn);
				break;
		}
	}

	//
	init();
	return {};
};
//
