﻿"use strict";
//
se.plugin.uwp_page_default = function (plugElem, plugOptions) {

	//
    function init() {
		console.log( "Default page", app.state.settings );

	    //
	    if ( !app.state.loaded ) {
	    	appSync();
	    }

	    //
	    plugElem.se_on( 'click', 'button[se-act]', btnActions );
	}


	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		//
		const cAction = cBtn.se_attr( 'se-act' );
		switch ( cAction ) {
			//
			case 'fullscreen_on':
				if ( document.documentElement.requestFullscreen ) {
					document.documentElement.requestFullscreen();
				} else if ( document.documentElement.mozRequestFullScreen ) {
					document.documentElement.mozRequestFullScreen();
				} else if ( document.documentElement.webkitRequestFullscreen ) {
					document.documentElement.webkitRequestFullscreen();
				} else if ( document.documentElement.msRequestFullscreen ) {
					document.documentElement.msRequestFullscreen();
				}
				break;
			//
			case 'fullscreen_off':
				if ( document.exitFullscreen ) {
					document.exitFullscreen();
				} else if ( document.mozCancelFullScreen ) {
					document.mozCancelFullScreen();
				} else if ( document.webkitExitFullscreen ) {
					document.webkitExitFullscreen();
				}
				break;

			//
			case 'sync':
				appSync();
				break;

			//
			case 'unload':
				if ( confirm("¿Desconectar la aplicación?\nTodos los datos serán eliminados.") ) {
					unloadApp();
				}
				break;

			//
			default:
				console.log("Boton no programado: ", cAction, cBtn);
				break;
		}
	}

	//
	function appSync() {
		let url =  '/ajax/sync',
			postData = {},
			params = {
				headers: {
					'X-Requested-With':'VPAPP'
				},
				onSuccess: ( msg ) => {
					//
					app.state.loaded = true;
					//
					console.log(msg);

					// Actualizar aplicación
					se_app.stateUpdate();

					// Actualizar elementos de la base de datos
					// Reiniciar DB
					app.idb.clearTables();

					// Guardar los nuevos elementos
					app.idb.syncData(msg.d.db);

					// Notificar si hay exito
					console.log("App: All data reload.");
				},
				onFail: () => {
					alert("No fue posible conectarse al servidor.");
				},
				onComplete: () => {
					// cBtn.disabled = false;
				},
				onRequest: () => {
					// cBtn.disabled = true;
				},
				onProgress: (x) => {
					console.log("progress", x);
				}
			};

		// request
		se.ajax.json(url, postData, params);
	}

	//
    init();
    return {};
};
//
