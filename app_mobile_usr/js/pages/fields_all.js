﻿"use strict";
//
se.plugin.uwp_page_fields_all = function (plugElem, plugOptions) {
	let serverFields = plugElem.querySelector('[se-elem="server_fields"]'),
		serverFieldsTemplate = serverFields.querySelector('template'),
		serverFieldsContent = serverFields.querySelector('div[se-elem="content"]')
	;

	//
    function init() {
		console.log( "PAGE LOAD - FIELDS", app.state.settings );

		//
	    printServerFields();

	    //
	    plugElem.se_on( 'click', 'button[se-act]', btnActions );
	}

	//
	function printServerFields() {
		serverFieldsContent.se_empty();
		app.idb.query('server_fields', {
			onSuccess:(data) => {
				serverFieldsContent.se_append( se.struct.stringPopulateMany( serverFieldsTemplate.se_html(), data) );
			}
		});
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		//
		const cAction = cBtn.se_attr( 'se-act' );
		switch ( cAction ) {
			//
			default:
				console.log("Boton no programado: ", cAction, cBtn);
				break;
		}
	}

	//
    init();
    return {};
};
//
