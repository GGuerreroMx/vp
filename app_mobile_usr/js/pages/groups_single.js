﻿"use strict";
//
se.plugin.uwp_page_groups_single = function (plugElem, plugOptions) {
	let serverFields = plugElem.querySelector('[se-elem="game_elements"]'),
		serverFieldsTemplate = serverFields.querySelector('template'),
		serverFieldsContent = serverFields.querySelector('div[se-elem="content"]'),
		//
		ggId = 0
	;

	//
    function init() {
	    console.log( "PAGE LOAD - GROUPS", app.state.settings );

	    //
	    let params = se.url.str_parse(window.location.search);
	    ggId = intVal(params.ggId);

	    //
	    printServerFields();

	    //
	    plugElem.se_on( 'click', 'button[se-act]', btnActions );
    }

	//
	function printServerFields() {
		serverFieldsContent.se_empty();
		app.idb.query('game_matches', {
			index:'ggId',
			keyRange:IDBKeyRange.only(ggId),
			onSuccess:(data) => {
				serverFieldsContent.se_append( se.struct.stringPopulateMany( serverFieldsTemplate.se_html(), data) );
			}
		});
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		//
		const cAction = cBtn.se_attr( 'se-act' );
		switch ( cAction ) {
			//
			default:
				console.log("Boton no programado: ", cAction, cBtn);
				break;
		}
	}

	//
	init();
	return {};
};
//
