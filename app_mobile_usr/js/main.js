﻿"use strict";

//
se_app.initialization = function() {
	//
	window.onpopstate = se.uwp.page.popState;

	// CB
	function jsInitialize() {
		// initialize background task
		console.log("MAIN - IDB INITIALIZED. FIRST JS CALL");

		// Crear appstates
		let tempAppState = localStorage.getItem( 'app.state' );
		//
		if ( tempAppState ) {
			app.state = JSON.parse( tempAppState );

			//
			if ( app.state.connection.method ) {
				se_app.server_connect();
			}
		}

		// Sistema de navegación
		document.body.se_on( 'click', '[se-nav-uwp]', se.uwp.page.navigation );

		// Load images (icons)
		fetch( 'img/se-icons.svg' )
			.then(
				( response ) => {
					if ( response.status !== 200 ) {
						console.log( 'Looks like there was a problem. Status Code: ' + response.status );
						return;
					}
					//
					response.text().then( ( data ) => {
						document.body.se_append(data);
					} );
				}
			)
			.catch( ( err ) => {
				console.log( 'Icons not loaded: ', err );
			});
		//

		// Initialize page
		se.uwp.page.urlParse(window.location.search, false);
	}

	// Indexed DB
	console.log( "MAIN - IDB START.", app_idb_struct);
	app.idb = new se_indexedDataBase(app_idb_struct, jsInitialize);

	// Service Worker
	if ( 'serviceWorker' in navigator ) {
		navigator.serviceWorker.register('/snkeng_main_sw.js')
			.then((reg) => {
				console.log("SW Registration:");
				if ( reg.installing ) {
					console.log('SW installing');
				} else if ( reg.waiting ) {
					console.log('SW installed');
				} else if ( reg.active ) {
					console.log('SW active');
				}
			})
			.catch((error) => {
				console.log("SW REGISTER FAILURE", error);
			});
	} else {
		console.log('Service workers aren\'t supported in this browser.');
	}
};

//
window.addEventListener('DOMContentLoaded', se_app.initialization);
