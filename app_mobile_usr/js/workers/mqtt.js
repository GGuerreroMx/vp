"use_strict";

//
var mqtt = {
	ready:false,
	obj:null,
	ops:{},
	subs:[],
	pendingSub:[]
};

//
mqtt.ops.start = function(domain, user, password, useSSL = true) {
	if ( mqtt.obj !== null ) {
		mqtt.obj.disconnect();
	}

	// Start MQTT
	console.log("SW - MQTT: Start called.");

	//
	mqtt.obj = new Paho.Client(domain, 8883, '', 'vp_phone_' + parseInt(Math.random() * 100, 10));

	// set callback handlers
	mqtt.obj.onConnectionLost = mqtt.ops.onDisconnect;
	mqtt.obj.onMessageArrived = mqtt.ops.onMessage;

	let connectionSettings = {
		mqttVersion:4,
		reconnect:true,
		onSuccess:mqtt.ops.onConnectionSuccess,
		onFailure:mqtt.ops.onConnectionFail,
		useSSL:useSSL
	};

	// If user exists
	if ( user !== '' ) {
		connectionSettings.userName = user;
		connectionSettings.password = password;
	}

	//
	mqtt.obj.connect(connectionSettings);
};

//
mqtt.ops.stop = function() {
	//

};


//
mqtt.ops.sendMessage = function(destination, message, qos = 2, retain = false) {
	console.log("SW - MQTT: SEND TO SERVER:\nQOS: %s. Retain: %s.\nT: %s\nC: %s\n", qos, retain, destination, message);
	//
	if ( typeof message === 'object' ) {
		message = JSON.stringify(message);
	} else if ( typeof message === 'number' ) {
		message = message.toString();
	}

	//
	let mqttMsg = new Paho.Message(message);
	mqttMsg.destinationName = destination;
	mqttMsg.qos = qos;
	mqttMsg.retain = retain;

	//
	mqtt.obj.send(mqttMsg);
};
//
mqtt.ops.onConnectionSuccess = function() {
	console.log("SW - MQTT: Connected");
	//
	if ( !mqtt.ready ) {
		mqtt.ready = true;
		if ( mqtt.pendingSub ) {
			mqtt.ops.subscribe(mqtt.pendingSub);
			mqtt.pendingSub = [];
		}
	}
	//
	mqtt.obj.subscribe("notifications", { qos:2 });
};

//
mqtt.ops.subscribe = function(subArray, purge = false) {
	//
	if ( !mqtt.ready ) {
		mqtt.pendingSub = subArray;
		return;
	}
	//
	if ( purge ) {
		for ( let cSub of mqtt.subs ) {
			mqtt.obj.unsubscribe(cSub);
		}
		mqtt.subs = [];
	}
	//
	for( let cSub of subArray ) {
		mqtt.subs.push(cSub[1]);
		mqtt.obj.subscribe(cSub[1], { qos:cSub[0] });
	}
};

//
mqtt.ops.onConnectionFail = function() {
	console.log("SW - MQTT: Fail to connect");
};

//
mqtt.ops.onDisconnect = function(responseObject) {
	if ( responseObject.errorCode !== 0 ) {
		console.log("SW - MQTT: Connection Lost:", responseObject.errorMessage);
	}
};

//
mqtt.ops.onMessage = function(message) {
	console.log("SW - MQTT: MSG RECIEVE FROM SERVER:\nQOS:%s. Retain:%s.\nT: %s\nC: %s\n", message.qos, message.retained, message.destinationName, message.payloadString);
	// Send message to the interpreter.
	messageInterpreter(message.destinationName, message.payloadString);
};

//
function setFieldAction(actData) {
	postMessage({
		op:'field',
		id:server_field_id,
		data:actData
	});
}