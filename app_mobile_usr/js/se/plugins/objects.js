"use strict";

//
se.plugin.object_image = function(plugElem, plugOptions) {
	let defaults = {},
		settings = se.object.merge(defaults, plugOptions),
		//
		parameters = {
			type:plugElem.se_data('type'),
			url:plugElem.se_data('url'),
			image:plugElem.se_data('image'),
		};


	//
	const
		imageUploadForm = plugElem.querySelector('form[se-elem="imageUpload"]'),
		imageUploadFormFile = imageUploadForm.querySelector('input[type="file"]'),
		photoCanvas = plugElem.querySelector('canvas'),
		mainImage = plugElem.querySelector('img[se-elem="mainPicture"]'),
		imageCropForm = plugElem.querySelector('form[se-elem="imageCrop"]'),
		dialog = plugElem.querySelector('dialog');


	//
	function init() {
		// Plugin
		photoCanvas.se_plugin('cropper', {
			onNewSel:(data) => {
				imageCropForm.se_formElVal('img_x', data.x);
				imageCropForm.se_formElVal('img_y', data.y);
				imageCropForm.se_formElVal('img_w', data.w);
				imageCropForm.se_formElVal('img_h', data.h);
			},
			ratio: ( parameters.type === 'profile' ) ? '1:1' : '3:1',
			min:( parameters.type === 'profile' ) ? '160x160' : '450x150'
		});

		se.form.appendHiddenChildren(imageUploadForm, [
			['oId', intVal(plugElem.se_data('id'))],
			['oKey', plugElem.se_data('eKey')],
		]);

		se.form.appendHiddenChildren(imageCropForm, [
			['oId', intVal(plugElem.se_data('id'))],
			['oKey', plugElem.se_data('eKey')],
		])


		// Actualización de la imagen
		imageUploadFormFile.se_on('change', () => {
			let event = new Event('submit', {
				'bubbles': true,
				'cancelable': true
			});
			imageUploadForm.dispatchEvent(event);
		});
		//
		imageUploadForm.se_plugin('simpleForm', {
			save_url: parameters.url + '/' + parameters.type + '_upload',
			onSuccess:(msg) => {
				updateImages(msg.d.img_crop);
			}
		});
		imageCropForm.se_plugin('simpleForm', {
			save_url: parameters.url + '/' + parameters.type + '_crop',
			onSuccess:(msg) => {
				updateImages(msg.d.img_crop);
			}
		});
		//
		dialog.se_data('mode', parameters.type);
		//
		plugElem.se_on('click', 'button[se-act]', btnAction);
	}

	//
	function btnAction(e, cBtn) {
		e.preventDefault();

		//
		switch ( cBtn.se_attr('se-act') ) {
			//
			case 'photoCrop':
				se.dialog.openModal(dialog);
				photoCanvas.cropper.canvasReDraw();
				break;
			//
			case 'cropClose':
				se.dialog.close(dialog);
				break;
			//
			case 'refresh':
				break;
			//
			default:
				console.error("engine windows action method not recognized", cBtn);
				break;
		}
	}

	//
	function updateImages(imgName) {
		let nUrl = '/server/image/objects',
			imageEnd = imgName + '?' + uniqId(),
			usrImg = $('#se_user img');

		//
		mainImage.src = nUrl + '/w_160/' + imageEnd;

		//
		if ( parameters.type === 'profile' && usrImg ) {
			usrImg.src = nUrl + '/w_35/' + imageEnd;
		}
	}

	//
	init();
	return {};
};
//

se.plugin.object_links = function(plugElem, plugOptions) {
	let defaults = {},
		settings = se.object.merge(defaults, plugOptions)
		;


	//
	const
		linkContainer = plugElem.querySelector('div.links'),
		linkTemplate = linkContainer.querySelector('template'),
		linkContent = linkContainer.querySelector('[se-elem="content"]')
		;


	//
	function init() {
		//
		plugElem.se_on('click', 'button[se-act]', btnAction);
	}

	//
	function btnAction(e, cBtn) {
		e.preventDefault();

		//
		switch ( cBtn.se_attr('se-act') ) {
			//
			case 'linkAdd':
				linkAdd();
				break;
			//
			case 'linkDel':
				linkDel(cBtn);
				break;
			//
			default:
				console.error("engine windows action method not recognized", cBtn);
				break;
		}
	}

	//
	function linkSave() {
		let obj = plugElem,
			objs = [];
		console.log("Save attemp", obj);
		obj.querySelectorAll('div.link').se_each(function(i, el) {
			console.log("pushing element", i, el);
			objs.push({type:el.se_data('type'), icon:el.se_data('icon'), url:el.se_data('url')});
		});
		console.log("elements", objs);
		se.ajax.json(
			apps.sn.url_gen + '/admin_obj/linksSave/',
			apps.sn.getCurObject({ data:JSON.stringify(objs) }),
			{
					'onSuccess':(msg) => {
						console.log("Saved");
					},
					'onError':(msg) => {
						console.log("FAIL");
					}
				}
			);
	}

	//
	function linkAdd() {
		let obj = plugElem,
			templateBody = linkTemplate.se_html(),
			input = obj.querySelector('input[name="url"]'),
			valid = false, i, len, cLink,
			links = [
				{url:'twitter.com', type:'twitter', icon:'fa-twitter'},
				{url:'facebook.com', type:'facebook', icon:'fa-facebook-f'},
				{url:'m.me', type:'fb-messenger', icon:'fa-facebook-messenger'},
				{url:'youtube.com', type:'youtube', icon:'fa-youtube'},
				{url:'youtu.be', type:'youtube', icon:'fa-youtube'}
			],
			sLink = {
				type:'',
				url:input.value,
				icon:'fa-globe'
			};

		//
		if ( !input.validity.valid ) {
			console.warn("link no válido.");
			return;
		}

		//
		sLink.url = input.value;
		for ( i = 0, len = links.length; i < len; i++ )
		{
			if ( sLink.url.includes(links[i].url) ) {
				sLink.icon = links[i].icon;
				sLink.type = links[i].type;
				break;
			}
		}

		//
		linkContent.se_append(se.struct.stringPopulate(templateBody, sLink));
		input.se_val('');
		linkSave();
	}

	//
	function linkDel(el) {
		el.se_closest('.link').se_remove();
		console.log(el);
		linkSave();
	}

	//
	init();
	return {};
};