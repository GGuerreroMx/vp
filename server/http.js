"use_strict";
// Adicionalmente usar websockets de: https://github.com/websockets/ws

let idb = require('./idb.js'),
	gs = require('./gameServer.js'),
	wsClients = new Map(),
	wss;
const {WebSocketServer} = require("ws");

// CB de inicio
function init_http() {
	//
	const
		http = require('https'),
		node_url = require('url'),
		qs = require('querystring'),
		fs = require('fs'),
		fPath = require('path'),
		{ WebSocket, WebSocketServer } = require('ws'),
		port = 443,
		wsPort = 9000,
		http_options = {
			ca: fs.readFileSync('ssl/ca.crt'),
			cert: fs.readFileSync('ssl/server.crt'),
			key: fs.readFileSync('ssl/server.key')
		};

	//<editor-fold desc="Simplificadores de respuestas">

	//
	function returnSimple(res, code, data) {
		//
		res.writeHead(code, {"Content-Type": "text/plain; charset=utf-8"});
		//
		res.end(data);
	}

	//
	function returnJSONSuccess(res, data) {
		console.log("HTTP REQUEST SUCCESS (JSON)", data);

		//
		res.writeHead(200, {"Content-Type": "text/json; charset=utf-8"});
		//
		res.end(JSON.stringify({
			d:data,
			s:{
				t:1,
				e:0
			}
		}));
	}

	//
	function returnJSONError(res, errNumber, errData) {
		console.log("HTTP REQUEST FAIL", errData);

		//
		res.writeHead(errNumber, {"Content-Type": "text/json; charset=utf-8"});
		//
		res.end(JSON.stringify({
			s:{
				t:0,
				e:errNumber,
				ex:errData
			}
		}));
	}

	//</editor-fold>

	//
	function processRequest(request, res) {
		//
		let urlParse = node_url.parse(request.url, true);
		//
		request.path = request.url.match('^[^?]*')[0].split('/').slice(1);
		request.query = urlParse.query;
		request.post = {};

		//
		if ( request.method === 'POST' ) {
			let body = "";
			request.on('error', (err) => {
				console.error(err);
			}).on('data', (chunk) => {
				body+= chunk;
			}).on('end', () => {
				request.post = qs.parse(body);
				// At this point, we have the headers, method, url and body, and can now
				// do whatever we need to in order to respond to this request.
				requestHandler(request, res);
			});
		} else {
			requestHandler(request, res);
		}
	}

	//
	function getEntriesFromCookie(cookieString = '') {
		return cookieString.split(';').map((pair) => {
			const indexOfEquals = pair.indexOf('=');
			let name;
			let value;
			if (indexOfEquals === -1) {
				name = '';
				value = pair.trim();
			} else {
				name = pair.substr(0, indexOfEquals).trim();
				value = pair.substr(indexOfEquals + 1).trim();
			}
			const firstQuote = value.indexOf('"');
			const lastQuote = value.lastIndexOf('"');
			if (firstQuote !== -1 && lastQuote !== -1) {
				value = value.substring(firstQuote + 1, lastQuote);
			}
			return [name, value];
		});
	}

	/**
	 * @param {Object} options
	 * @param {string} [options.name='']
	 * @param {string} [options.value='']
	 * @param {Date} [options.expires]
	 * @param {number} [options.maxAge]
	 * @param {string} [options.domain]
	 * @param {string} [options.path]
	 * @param {boolean} [options.secure]
	 * @param {boolean} [options.httpOnly]
	 * @param {'Strict'|'Lax'|'None'} [options.sameSite]
	 * @return {string}
	 */
	function createSetCookie(options) {
		return (`${options.name || ''}=${options.value || ''}`)
			+ (options.expires != null ? `; Expires=${options.expires.toUTCString()}` : '')
			+ (options.maxAge != null ? `; Max-Age=${options.maxAge}` : '')
			+ (options.domain != null ? `; Domain=${options.domain}` : '')
			+ (options.path != null ? `; Path=${options.path}` : '')
			+ (options.secure ? '; Secure' : '')
			+ (options.httpOnly ? '; HttpOnly' : '')
			+ (options.sameSite != null ? `; SameSite=${options.sameSite}` : '');
	}

	//
	function serveSimpleFile(res, dir, parsedUrl) {
		// parse URL

		// extract URL path
		let pathname = './' + dir + parsedUrl.pathname,
			fileExt = fPath.parse(pathname).ext;
		// maps file extention to MIME typere
		const map = {
			'.ico': 'image/x-icon',
			'.html': 'text/html',
			'.js': 'text/javascript',
			'.mjs': 'text/javascript',
			'.jsm': 'text/javascript',
			'.json': 'application/json',
			'.css': 'text/css',
			'.png': 'image/png',
			'.jpg': 'image/jpeg',
			'.wav': 'audio/wav',
			'.mp3': 'audio/mpeg',
			'.svg': 'image/svg+xml',
			'.svgz': 'image/svg+xml',
			'.ttf': 'application/x-font-truetype',
			'.otf': 'application/x-font-opentype',
			'.woff': 'application/font-woff',
			'.woff2': 'application/font-woff2',
			'.pdf': 'application/pdf',
			'.doc': 'application/msword',
			'.webmanifest': 'application/manifest+json'
		};

		// console.log("file ext", fileExt, pathname);

		//
		fs.exists(pathname, (exist) => {
			if ( !exist ) {
				returnSimple(res, 404, "Archivo no encontrado: " + pathname);
				return;
			}

			// console.log("FILE EXISTS, SENDING: ", fileExt, pathname);

			// if is a directory search for index file matching the extention
			if ( fs.statSync(pathname).isDirectory() ) {
				fileExt = '.html';
				pathname += '/index' + fileExt;
			}

			//
			res.setHeader('Cache-Control', 'public, max-age=3600');

			// read file from file system
			fs.readFile(pathname, (err, data) => {
				if ( err ) {
					returnSimple(res, 500, "Problemas con el servidor: " + pathname + ". Código: " + err);
				} else {
					// if the file is found, set Content-type and send data
					res.setHeader('Content-type', map[fileExt] || 'text/plain');
					res.end(data);
				}
			});
		});
	}

	//
	function requestHandler ( req, res ) {
		//
		const { headers, method, url, path, query, post } = req;

		// Website you wish to allow to connect
		res.setHeader('Access-Control-Allow-Origin', '*');
		// Request methods you wish to allow
		res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
		// Request headers you wish to allow
		res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
		// Set to true if you need the website to include cookies in the requests sent
		// to the API (e.g. in case you use sessions)
		res.setHeader('Access-Control-Allow-Credentials', true);

		// Read cookies
		const cookieEntries = getEntriesFromCookie(headers.cookie);
		const cookieList = {};
		cookieEntries.forEach((cEl) => { cookieList[cEl[0]] = cEl[1] });


		//
		switch ( path[0] ) {
			//
			case 'cert':
				let file = './ssl/ca.crt';
				fs.readFile(file, (err, data) => {
					if ( err ) {
						returnSimple(res, 500, "Problemas con el servidor: " + pathname + ". Código: " + err);
					} else {
						// if the file is found, set Content-type and send data
						res.setHeader('Content-type', 'application/x-x509-ca-cert');
						res.setHeader('Content-Disposition', 'attachment; filename="ca.cert"');
						res.end(data);
					}
				});
				break;

			//
			case 'iot':
				switch ( path[1] ) {
					//
					case 'device':
						//
						switch ( path[2] ) {
							//
							case 'verify':
								//
								if ( !post.hasOwnProperty('id') || !post.hasOwnProperty('key') ) {
									console.log("Object verification failure. no id and/or key", post);
									return;
								}
								//
								idb.idb.ops.query('server_devices', {
									get: (intVal(params.id)),
									onSuccess: (data) => {
										res.writeHead(200, { 'Content-Type': 'text/plain' });

										//
										if ( data.key !== post.key ) {
											res.end('VP Server Running.');
										} else {
											res.end('VP Server Running.');
										}
									},
									onFail:() => {
										res.writeHead(500, { 'Content-Type': 'text/plain' });
										res.end('Error en obtención de la información.');
									}
								});
								break;

							//
							case 'register':
								let deviceData = {
									'id':0,
									'sf_id':0,
									'gt_id':0,
									'gto_id':0,
									'title':'',
									'key':makeid(8),
								};
								//
								idb.idb.ops.add('server_devices', deviceData, {
									onSuccess:(e) => {
										// Update newGameData
										deviceData.id = intVal(e.target.result);
										//
										console.log("Dispositivo agregado:", deviceData);

										//
										res.writeHead(200, {'Content-Type': 'text/plain'});
										res.end('VP Server Running.');
									}
								});
								break;

							//
							default:
								returnJSONError(res, 404, "No encontrado");
								break;
						}

						break;

					//
					default:
						returnSimple(res, 404, "Not valid");
						break;
				}
				break;

			//
			case 'admin':
				// Check for cookie or params
				// console.log("QUERY DATA IS: ", query);
				// console.log("COOKIES: ", cookieEntries, cookieList, headers);

				let cookieData = [];

				if ( query && 'sfId' in query && 'sfKey' in query ) {
					// Set cookie
					const newCookie = createSetCookie({
						name: 'access',
						value: "adm:" + query.sfId + ":" + query.sfKey,
						path:'/',
					});
					res.setHeader('Set-Cookie', newCookie);

					res.writeHead(301, {
						Location: `/admin/`
					}).end();
				}
				else if ( 'access' in cookieList ) {
					// Validate cookie
					cookieData = cookieList['access'].split(':');

					//
					if ( cookieData[0] !== 'adm' ) {
						returnSimple(res, 403, "No autorizado.");
					}

					// console.log("COOKIE VALIDATION: ", cookieList.adminAccess);
				}
				else {
					returnSimple(res, 403, "No autorizado");
				}

				//
				cookieData[1] = parseInt(cookieData[1]);


				//
				switch ( path[1] ) {
					//
					case 'ajax':
						//
						switch ( path[2] ) {
							//
							case 'loadInit':
								//
								idb.idb.ops.query('server_fields', {
									get: cookieData[1],
									onSuccess:(cFieldData) => {
										console.log("cFieldData", cFieldData);

										// Next query with actual information
										let queries = [
											{
												rName:'gametypes',
												table:'gametypes',
												params:{
													get:cFieldData.gtId
												}
											}
										]

										//
										if ( cFieldData.confMatches === 'tournament' ) {
											queries.push({
												rName: 'game_matches',
												table: 'game_matches',
												params: {
													keyRange:IDBKeyRange.only([cFieldData.gsId, cFieldData.fId]),
												}
											});
										}

										// Procesar tipo de juego y jugadores
										idb.idb.ops.queryBatch(queries, {
											onSuccess:(results) => {
												returnJSONSuccess(res, {'server_fields':[cFieldData], 'gametypes': [results.gametypes], 'game_matches': results.game_matches});
											},
											onFail:() => {
												returnJSONError(res, 500, 'Información adicional no pudo ser obtenida.' + cField);
											}
										});
									},
									onFail:() => {
										returnJSONError(res, 500, 'Campo no válido o no encontrado en la base de datos.' + cField);
									},
									onEmpty:() => {
										returnJSONError(res, 500, 'Campo no válido o no encontrado en la base de datos.' + cField);
									}
								});
								break;
							//
							case 'gametypes':
								idb.idb.ops.query('gametypes', {
									onSuccess: (gametypes) => {
										console.log("Current Game Types:", gametypes);
										returnJSONSuccess(res, {gametypes:{gametypes}});
									}
								});
								break;

							//
							case 'serverfields':
								idb.idb.ops.query('server_fields', {
									onSuccess: (server_fields) => {
										console.log("Current Servers:", server_fields);
										returnJSONSuccess(res, {server_fields:{server_fields}});
									}
								});
								break;

							//
							case 'score_mobile':
								//
								switch ( path[3] ) {
									//
									case 'matchesUpload':
										//
										if ( !('data' in post) ) {
											returnJSONError(res, 500, 'Operación no válida. Datos incompletos');
											return;
										}

										let dataJson = JSON.parse(post.data);

										idb.idb.ops.updateMany('game_matches', 'index', dataJson, {
											onSuccess: (data) => {
												returnJSONSuccess(res, '');
											},
											onFail: () => {
												returnJSONError(res, 500, 'NO fue posible guardar la información.');
											}
										});
										break;

									//
									default:
										returnJSONError(res, 500, 'Operación no válida.' + path[3]);
										break;
								}
								break;

							//
							case 'score_server':
								let cField = parseInt(path[3]);
								if ( !cField ) {
									returnJSONError(res, 500, 'Campo no válido.')
								}

								//
								switch ( path[4] ) {
									//
									case 'get':
										//
										idb.idb.ops.query('server_fields', {
											get: cField,
											onSuccess: (cFieldData) => {
												returnJSONSuccess(res, cFieldData);
											},
											onFail:() => {
												returnJSONError(res, 500, 'Campo no válido o no encontrado en la base de datos.' + cField)
											}
										});
										break;

									//
									case 'scores':
										console.log("START SCORE UPDATE.");
										if ( !( 'mIndex' in post ) || !( 'points' in post ) ) {
											returnJSONError(res, 500, 'Operación no válida. Datos incompletos');
											return;
										}

										//
										idb.idb.ops.query('server_fields', {
											get: cField,
											onSuccess:(cFieldData) => {
												if ( cFieldData.cMatchIndex !== parseInt(post.mIndex) ) {
													returnJSONError(res, 500, 'Operación no válida. El campo ya no se encuentra en esa partida. ' + cFieldData.cMatchIndex + ':' + post.mIndex);
												}

												console.log("cFieldData", cFieldData);

												// Next query with actual information
												let queries = [
													{
														rName:'gameType',
														table:'gametypes',
														params:{
															get:cFieldData.gtId
														}
													},
													{
														rName:'match',
														table:'game_matches',
														params:{
															get:cFieldData.cMatchIndex
														}
													}
												];

												// Procesar tipo de juego y jugadores
												idb.idb.ops.queryBatch(queries, {
													onSuccess:(results) => {
														let scoreValues = JSON.parse(post['points']),
															matchData = results['match'],
															gameTypeData = results['gameType'];

														console.log("scoreValues", scoreValues);
														console.log("matchData", matchData);

														//
														matchData.mPoints = {
															t:{
																'1':{
																	'p':{},
																	't':0
																},
																'2':{
																	'p':{},
																	't':0
																}
															},
															p:{}
														};
														let totalPoints = {
															'1':0,
															'2':0
														};

														//
														for ( let cPointsId in gameTypeData.struct.points ) {
															if ( !gameTypeData.struct.points.hasOwnProperty(cPointsId) ) { continue; }

															//
															let cPointData = gameTypeData.struct.points[cPointsId],
																tCount, tTotal;

															// Skip user points
															if ( cPointData.target !== 1 ) { continue; }

															// Hacer doble
															for ( let i = 1; i < 3; i++ ) {
																let teamIndex = i.toString();


																//
																tCount = scoreValues['point'][teamIndex][cPointsId];
																tCount = ( tCount <= cPointData.pointsMax ) ? tCount : cPointData.pointsMax;
																//
																tTotal = tCount * cPointData.points;
																//
																matchData.mPoints.t[teamIndex].p[cPointsId] = {
																	'c':tCount,
																	't':tTotal
																};

																//
																totalPoints[teamIndex]+= tTotal;
															}
														}

														//
														for ( let i = 1; i < 3; i++ ) {
															let teamIndex = i.toString();
															totalPoints[teamIndex] = ( totalPoints[teamIndex] > gameTypeData.struct.settings.maxPointsGame ) ? gameTypeData.struct.settings.maxPointsGame : totalPoints[teamIndex];
															//
															matchData.mPoints.t[teamIndex].t = totalPoints[teamIndex];
														}

														//
														let saveData = {
															mPoints:matchData.mPoints,
															sync:1,
															mStatus:4
														};

														//
														if ( matchData.t1_side !== 0 && matchData.t2_side !== 0 ) {
															saveData.t1_points = totalPoints[matchData.t1_side.toString()];
															saveData.t2_points = totalPoints[matchData.t2_side.toString()];

															//
															if ( saveData.t1_points === saveData.t2_points ) {
																saveData.t1_status = 2;
																saveData.t2_status = 2;
															} else {
																saveData.t1_status = ( saveData.t1_points > saveData.t2_points ) ? 3 : 1;
																saveData.t2_status = ( saveData.t1_points < saveData.t2_points ) ? 3 : 1;
															}

															//
															idb.idb.ops.update('game_matches', matchData.index, saveData, {
																onSuccess:(data) => {
																	returnJSONSuccess(res, saveData);
																},
																onFail:() => {
																	returnJSONError(res, 500, 'Información adicional no pudo ser obtenida.' + cField);
																}
															});
														}
													},
													onFail:() => {
														returnJSONError(res, 500, 'Información adicional no pudo ser obtenida.' + cField);
													}
												});
											},
											onFail:() => {
												returnJSONError(res, 500, 'Campo no válido o no encontrado en la base de datos.' + cField);
											},
											onEmpty:() => {
												returnJSONError(res, 500, 'Campo no válido o no encontrado en la base de datos.' + cField);
											}
										});
										break;

									//
									case 'advance':
										console.log("START SCORE UPDATE.");
										if ( !( 'mIndex' in post ) ) {
											returnJSONError(res, 500, 'Operación no válida. Datos incompletos');
											return;
										}

										console.log("HTTP REQUEST START FIELD ADVANCING");

										//
										idb.idb.ops.query('server_fields', {
											get: cField,
											onSuccess:(cFieldData) => {
												if ( cFieldData.cMatchIndex !== parseInt(post.mIndex) ) {
													returnJSONError(res, 500, 'Operación no válida. El campo ya no se encuentra en esa partida. ' + cFieldData.cMatchIndex + ':' + post.mIndex);
												}

												console.log("cFieldData", cFieldData);

												// Determinar siguiente match disponible
												idb.idb.ops.query('game_matches', {
													index:'gameFilter',
													keyRange:IDBKeyRange.only([cFieldData.gsId, cFieldData.fId]),
													onSuccess:(data) => {
														let matchAvailable = false,
															selMatch;

														//
														for ( let cMatch of data ) {
															if ( cMatch.mStatus === 1 ) {
																selMatch = cMatch;
																matchAvailable = true;
																break;
															}
														}

														//
														if ( !matchAvailable ) {
															console.error("Ya no hay partidas pendientes.");
															this.send_postMessage({
																type:'alert',
																data:"Ya no hay partidas pendientes."
															});
															return;
														}

														//
														if ( selMatch.t1_side === 0 || selMatch.t2_side === 0 ) {
															returnJSONError(res, 500, 'La siguiente partida no tiene lados asignados. Favor de asignarlos.');
														}

														//
														if ( selMatch.index === cFieldData.cMatchIndex ) {
															returnJSONError(res, 500, 'La partida actual todavía aparece como pendiente. Actualizar puntuación.');
														}

														//
														console.log("NEXT MATCH...", selMatch);



														//
														cFieldData.cMatchIndex = selMatch.index;
														cFieldData.cMatchOrder = selMatch.mOrder;

														cFieldData.t1_name = ( selMatch.t1_side === 1 ) ? selMatch.t1_name : selMatch.t2_name;
														cFieldData.t2_name = ( selMatch.t2_side === 2 ) ? selMatch.t2_name : selMatch.t1_name;

														//
														idb.idb.ops.query('gametypes', {
															get:cFieldData.gtId,
															onSuccess:(data) => {
																console.log("gameTypeData", data);
																cFieldData.data = data.struct.field;

																//
																idb.idb.ops.update('server_fields', cField, cFieldData, {
																	onSuccess:(data) => {
																		returnJSONSuccess(res, cFieldData);
																	},
																	onFail:() => {
																		returnJSONError(res, 500, 'Información adicional no pudo ser obtenida.' + cField);
																	}
																});
															},
															onEmpty:() => {
																returnJSONError(res, 500, 'Ya no hay mas partidas que cumplan las condiciones de la base de datos..');
															}
														});
													},
													onEmpty:() => {
														returnJSONError(res, 500, 'Ya no hay mas partidas que cumplan las condiciones de la base de datos..');
													}
												});
											},
											onFail:() => {
												returnJSONError(res, 500, 'Campo no válido o no encontrado en la base de datos.' + cField);
											},
											onEmpty:() => {
												returnJSONError(res, 500, 'Campo no válido o no encontrado en la base de datos.' + cField);
											}
										});



										break;

									//
									default:
										returnJSONError(res, 500, 'Operación no válida.' + path[3]);
										break;
								}
								break;

							// Only send information about the field according to current info
							case 'deviceSetup':
								//
								idb.idb.ops.query('server_fields', {
									get: sFieldId,
									onSuccess:(cFieldData) => {
										// Array to actual object
										cFieldData = cFieldData[0];

										// Next query with actual information
										let queries = [
											{
												rName:'gametypes',
												table:'gametypes',
												params:{
													get:cFieldData.gtId
												}
											},
											{
												rName:'game_matches',
												table:'game_matches',
												params:{
													index:'gameFilter',
													keyRange:IDBKeyRange.only([cFieldData.gsId, cFieldData.fId])
												}
											}
										];

										// Procesar tipo de juego y jugadores
										idb.idb.ops.queryBatch(queries, {
											onSuccess:(results) => {
												//
												returnJSONSuccess(res, {
													server_fields:[cFieldData],
													gametypes:[results['gametypes']],
													game_matches:results['game_matches']
												});
											},
											onFail:() => {
												returnJSONError(res, 500, 'Información adicional no pudo ser obtenida.' + cField);
											}
										});
									},
									onFail:() => {
										returnJSONError(res, 500, 'Campo no válido o no encontrado en la base de datos.' + cField);
									},
									onEmpty:() => {
										returnJSONError(res, 500, 'Campo no válido o no encontrado en la base de datos.' + cField);
									}
								});
								break;

							//
							case 'sync':
								let queries = [
									{
										rName:'game_matches',
										table:'game_matches',
										params: {}
									},
									{
										rName:'gametypes',
										table:'gametypes',
										params: {}
									},
									{
										rName:'server_fields',
										table:'server_fields',
										params: {}
									}
								];

								// Generar json data
								idb.idb.ops.queryBatch(queries, {
									onSuccess:(results) => {
										//
										returnJSONSuccess(res, {db:results});
									},
									onFail:() => {
										returnJSONError(res, 500, "No fue posible leer la base de datos");
									}
								});
								break;

							//
							default:
								returnJSONError(res, 404, "Operación no reconocida.");
								break;
						}
						break;

					//
					default:
						// parse URL
						let parsedUrl = node_url.parse(req.url);

						// Remove the "admin" in order to match file directory
						parsedUrl.pathname = parsedUrl.pathname.substring(6);

						//
						serveSimpleFile(res,'app_mobile_adm', parsedUrl);
						break;
				}
				break;

			// User app

			//
			case 'ajax':
				switch ( path[1] ) {
					//
					case 'gametypes':
						idb.idb.ops.query('gametypes', {
							onSuccess: (gametypes) => {
								console.log("Current Game Types:", gametypes);
								returnJSONSuccess(res, {gametypes:{gametypes}});
							}
						});
						break;

					//
					case 'serverfields':
						idb.idb.ops.query('server_fields', {
							onSuccess: (server_fields) => {
								console.log("Current Servers:", server_fields);
								returnJSONSuccess(res, {server_fields:{server_fields}});
							}
						});
						break;

					//
					case 'sync':
						let queries = [
							{
								rName:'game_matches',
								table:'game_matches',
								params: {}
							},
							{
								rName:'gametypes',
								table:'gametypes',
								params: {}
							},
							{
								rName:'server_fields',
								table:'server_fields',
								params: {}
							}
						];

						// Generar json data
						idb.idb.ops.queryBatch(queries, {
							onSuccess:(results) => {
								//
								returnJSONSuccess(res, {db:results});
							},
							onFail:() => {
								returnJSONError(res, 500, "No fue posible leer la base de datos");
							}
						});
						break;

					//
					case 'posttest':
						res.writeHead(200, { 'Content-Type': 'text/json' });
						res.end(JSON.stringify({headers:headers, method:method, url:url, path:path, post:post, query:query}, null, true));
						break;

					//
					default:
						returnJSONError(res, 404, "Operación no reconocida.");
						break;
				}
				break;

			//
			default:
				// parse URL
				const parsedUrl = node_url.parse(req.url);
				serveSimpleFile(res,'app_mobile_usr', parsedUrl);
				break;
		}
		//
	}


	// Create server with all previous conditions
	const server = http.createServer(http_options, processRequest);

	// Upgrade to SSL
	server.on('upgrade', function upgrade(request, socket, head) {
		//
		const { headers } = request;
		const cookieEntries = getEntriesFromCookie(headers.cookie);
		const cookieList = {};
		cookieEntries.forEach((cEl) => { cookieList[cEl[0]] = cEl[1] });

		// console.warn("CONNECTING?", head, socket, request);

		let props = {
			type:null,
			fId:null,
		};
		//
		switch ( request.url ) {
			//
			case '/admin/ws/':
				// is admin?, validate cookie
				if ( !('access' in cookieList) ) {
					console.log("SERVER - WS - DENIED: missing cookie?.");
					socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
					socket.destroy();
					return;
				}
				let cData = cookieList['access'].split(':');

				if ( cData[0] !== 'adm' ) {
					console.log("SERVER - WS - DENIED: cookie is not admin.");
					socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
					socket.destroy();
					return;
				}

				console.log("SERVER - WS - COOKIE INFORMATION: ", cookieList['adminAccess'], cData);

				// TODSO: VALIDATE COOKIE?

				props.fId = cData[1];
				props.type = 'adm';
				break;
			//
			case '/iot/ws/':
				props.fId = 0;
				props.type = 'iot';
				console.warn("iot devie attempt to connect");
				break;
			//
			case '/ws/':
				props.type = 'usr';
				break;
			//
			default:
				console.log("SERVER - WS - DENIED: invalid path.", request.url);
				socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
				socket.destroy();
				return;
				break;
		}

		//
		// console.log("SERVER - WS - attempt upgrade", request, props);

		//
		wss.handleUpgrade(request, socket, head, function done (ws) {
			//
			wss.emit('connection', ws, request, props, props);
		});
	});






	// Websockets

	function uuidv4() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		});
	}


	// App devices (single related server)
	wss = new WebSocketServer({ noServer: true });
	// IOT devices (wss may not work, then use unsecure port)
	const uws = new WebSocketServer({ port: 8080 });
	const nws = new WebSocketServer({ port: 444, ca: fs.readFileSync('ssl/ca.crt'), cert: fs.readFileSync('ssl/server.crt'), key: fs.readFileSync('ssl/server.key') });

	//
	wss.binaryType = "arraybuffer";

	//
	uws.binaryType = "arraybuffer";
	nws.binaryType = "arraybuffer";

	//
	wss.on('connection', (ws, request, client, props) => {
		//
		console.log("SERVER - WS - NEW CLIENT CONNECTION", ws, request, client, props);

		//
		const id = uuidv4();
		const fId = parseInt(props.fId);
		const type = props.type;
		const metadata = { id, type, fId };

		// Save information
		wsClients.set(ws, metadata);

		// Message incomming
		ws.on('message', (msg) => {
			ws_incommingMessage(msg, ws);
		});
	});

	// IOT Connections
	uws.on('connection', iotWSConnection);
	nws.on('connection', iotWSConnection);

	// Disconnection
	wss.on("close", clientDisconnect);
	//
	nws.on('close', clientDisconnect);
	nws.on('close', clientDisconnect);

	function clientDisconnect(ws) {
		console.log("SERVER - WS - Disconnection ->", wsClients.get(ws));
		wsClients.delete(ws);
	}

	// Add new element and report back
	function registerDevice(ws, id, type, odId, odPw) {
		//
		let deviceInfo = {
			pw:Math.floor(Math.random() * 65535) + 1,
			devMode:0,
			devType:0,
			fieldId:0,
			fieldDevId:0
		};

		//
		app.idb.add('server_devices', deviceInfo, {
			onSuccess:(e) => {
				let dId = e.target.result,
					fId = 0;

				// Add element
				const metadata = { id, type, fId, dId };

				// Save information
				wsClients.set(ws, metadata);

				// Convert string
				let cData = [
					100, // letter d
					byteHigh(odId),
					byteLow(odId),
					byteHigh(odPw),
					byteLow(odPw),
					116, // Letter s
					byteHigh(dId),
					byteLow(dId),
					byteHigh(deviceInfo.pw),
					byteLow(deviceInfo.pw),
				];

				//



				// Report new device Id and field to element
				ws.send(cData);
			}
		});
	}

	//
	function iotWSConnection(ws, req) {
		console.warn("SERVER - WS - NEW IOT CONNECTION", ws, req.headers);

		//
		const id = uuidv4();
		const fId = parseInt(req.headers['fid']);
		const type = 'iot';
		const dId = parseInt(req.headers['did']);
		const dPw = parseInt(req.headers['dpw']);

		// Assign callback
		ws.on('message', function message(msg) {
			ws_incommingMessage(msg, ws);
		});

		// Save information
		const metadata = { id, type, fId, dId };
		wsClients.set(ws, metadata);

		/*
		// Validate device
		idb.idb.ops.query('server_devices', {
			get:dId,
			onSuccess: (data) => {
				//
				if ( data.key !== dPw ) {
					// get new id
					registerDevice(ws, id, 'iot', dId, dPw);
				} else {
					// do nothing, element is up to date add to list
					const metadata = { id, type, fId, dId };


				}
			},
			onFail:() => {
				// get new id
				registerDevice(ws, id, 'iot', dId, dPw);
			}
		});

		 */
	}


	//
	server.listen(port,
		(err) => {
			if (err) { return console.log('something bad happened', err); }
			console.log(`HTTP Server ready. Using port:${port}`);
		}
	);


	// Debugging WS
	// setInterval(sendTestMessages, 5000);

}

let testMessages = [
	[102, 1, 115, 129, 0, 0, 0, 0, 0, 9, 6, 133, 134, 132, 132, 132, 0, 0, 0, 98, 1, 115, 1, 110, 77, 65, 84, 67, 72, 32, 83, 84, 65, 82, 84, 32, 116, 6, 0],
	[102, 1, 115, 129, 0, 0, 0, 0, 0, 9, 6, 133, 134, 132, 132, 132, 0, 0, 0, 98, 9, 115, 5, 110, 83, 99, 111, 117, 116, 32, 32, 32, 32, 32, 32, 32],
	[102, 1, 115, 130, 0, 0, 0, 0, 0, 9, 6, 133, 134, 132, 132, 132, 0, 0, 0, 115, 4, 98, 4, 110, 80, 114, 105, 109, 101, 114, 97, 32, 32, 32, 32, 32, 116, 30, 0],
	[102, 1, 115, 130, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 132, 132, 0, 0, 0],
	[102, 1, 115, 130, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 132, 132, 0, 0, 0, 115, 6],
	[102, 1, 115, 130, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 133, 132, 0, 0, 0],
	[102, 1, 115, 130, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 133, 132, 0, 0, 0, 115, 4, 110, 83, 101, 103, 117, 110, 100, 97, 32, 32, 32, 32, 32, 98, 4, 116, 30, 0],
	[102, 1, 115, 4, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 133, 132, 0, 0, 0, 98, 3, 115, 3, 110, 77, 65, 84, 67, 72, 32, 69, 78, 68, 32, 32, 32],
	[102, 1, 115, 0, 0, 0, 0, 0, 0, 9, 6, 133, 134, 132, 132, 132, 0, 0, 0, 98, 0, 110, 70, 73, 69, 76, 68, 32, 82, 69, 65, 68, 89, 32],
],
	testCurrent = 0;

//
function sendTestMessages() {
	if ( testCurrent >= testMessages.length ) { testCurrent = 0; }

	console.warn("SENDING TEST MESSAGE TO FIELD 1.");

	wss_sendField(1, testMessages[testCurrent]);
	testCurrent++;
}

//
function ws_incommingMessage(origMsg, ws) {
	//
	const senderData = wsClients.get(ws);

	//
	let message = [...origMsg],
		cChar;

	console.log('WSS - MSG - IN:', message, senderData);

	//
	switch ( senderData.type ) {
		//
		case 'adm':
		case 'iot':
			cChar = String.fromCharCode(message.shift());
			switch ( cChar ) {
				// Field
				case 'f':
					let fieldId = parseInt(message.shift());

					//
					if ( !fieldId ) {
						console.error("Invalid field: not in msg.");
						return;
					} else if ( !gs.gameData.fields.hasOwnProperty(fieldId) ) {
						console.error("Campo no disponible", fieldId);
						return;
					}
					//
					cChar = String.fromCharCode(message.shift());
					switch ( cChar ) {
						// Command
						case 'c':
							// Command building
							let cComHead = [];

							//
							cChar = String.fromCharCode(message.shift());
							switch ( cChar ) {
								// Setup
								case 's':
									cComHead.push('setup');

									//
									cChar = String.fromCharCode(message.shift());
									switch ( cChar ) {
										// Command
										case 'i':
											cComHead.push('start');
											break;
										// Command
										case 's':
											cComHead.push('stop');
											break;
										// Command
										case 'r':
											cComHead.push('reset');
											break;
										// Command
										case 'n':
											cComHead.push('next');
											break;
										//
										default:
											console.warn("WS Message not recognized.", cChar, message);
											return;
									}
									break;
								// Game Status
								case 'g':
									// Validate if possible
									cComHead.push('gameStatus');

									// Copy gsIndex, nValue
									cComHead.push(message.shift());
									cComHead.push(message.shift());
									break;
								// Device
								case 'd':
									// Validate if possible
									cComHead.push('device');

									// Copy dIndex, nSide, nStatus
									cComHead.push(message.shift());
									cComHead.push(message.shift());
									cComHead.push(message.shift());
									break;
								// Action (NOT PROGRAMMED)
								case 'e':
									// Validate if possible
									cComHead.push('event_update');

									// Copy eIndex
									cComHead.push(message.shift());
									cComHead.push(message.shift());
									cComHead.push(message.shift());
									break;
								//
								default:
									console.warn("WS Message not recognized.", cChar, message);
									break;
							}

							//
							gs.gameData.fields[fieldId].command(cComHead);
							break;
						// Send status back
						case 's':
							break;
						//
						default:
							console.warn("WS Message not recognized.", cChar, message);
							break;
					}

					break;

				// Device
				case 'd':

					break;

				// Message
				case 'm':
					break;

				//
				case 's':
					// Validate if field still is the one
					if ( senderData.type === 'iot' ) {
						//
					}
					console.warn("SEND FIELD INFORMATION");
					break;

				default:
					console.warn("WS Message not recognized.", cChar, message);
					break;
			}
			break;
		//
		case 'usr':
			console.warn("ignoring revieced messages from user.");
			break;
		//
		default:
			console.error("unknown operator, great developing XD", senderData.type);
			return;
			break;
	}
	//
}

//
function wss_sendField(targetField, content) {
	console.log("WSS - MSG - OUT Field: fId:%s. Size: %s. Message:\n%s", targetField, content.length, content);

	// TODO: is this required? Convert array to buffer
	// let cMsg = new Uint8Array(content).buffer

	// Check who to send message to
	wsClients.forEach((value, wsc) => {
		// All devices with field id
		if ( targetField === value.fId ) {
			wsc.send(content);
		}
	});
}

// Send message to specific device
function wss_sendDevice(deviceId, content) {
	console.log("WSS - MSG - OUT: Target:%s. Size: %s.\n%s", target, content.length, content);
	wss.clients.forEach(function each(client) {
		client.send(content);
	});
}
//
function wss_sendAll(targetField, content) {
	console.log("WSS - MSG - OUT ALL: Target:%s. Size: %s.\n%s", target, content.length, content);

	//
	wsClients.forEach((value, wsc) => {
		wsc.send(content);
	});
}

//
function makeid(sSize) {
	let text = "",
		possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for ( let i = 0; i < sSize; i++ ) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}

	return text;
}

//
function byteHigh(smallInt) {
	return (smallInt >> 8) & 0xFF;
}

//
function byteLow(smallInt) {
	return smallInt & 0xFF;
}

//
function bytesToSmallInt(byteHigh, byteLow) {
	return (byteHigh << 8) | byteLow;
}


//
module.exports.start = init_http;
module.exports.wss_sendField = wss_sendField;
