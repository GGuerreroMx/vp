"use_strict";

//
let gameServer = require('./gameServer.js'),
	messageCB;

//
function callBackSet(fnc) {
	messageCB = fnc;
}

//
function messageSend(data) {
	console.log("SW - MSG TO APP.", data);
	messageCB(data);
}

//
function messageIncomming(msg) {
	//
	console.log("SW - MSG FROM APP.", msg);

	//
	switch ( msg.op_type[0] ) {
		//
		case 'server_field':
			//
			switch ( msg.op_type[1] ) {
				//
				case 'add':
					gameServer.field.add(msg);
					break;
				//
				case 'del':
					gameServer.field.del(msg);
					break;
				//
				default:
					console.error("GS: Invalid server field operation");
					break;
			}
			break;

		//
		case 'field':
			//
			switch ( msg.op_type[1] ) {
				//
				case 'command':
					console.log("sending command to field", gameServer);
					gameServer.field.command(msg.op_data);
					break;
				//
				default:
					console.error("GS: Invalid server field/match operation");
					break;
			}
			break;

		//
		default:
			console.error("GS: Invalid general operation");
			break;
	}
}

//
module.exports.msgToNode = messageIncomming;
module.exports.setCallback = callBackSet;
module.exports.msgToWeb = messageSend;
//
