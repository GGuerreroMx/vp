"use_strict";

// IDB SETUP
let siteIDB = require('../app_main/js/config.js'),
	idb = require('./idb.js'),
	//
	cHTTP = require('./http.js'),
	gameServer = require('./gameServer.js');

//
function nodeInit() {
	gameServer.start();
	cHTTP.start();
}

// Iniciar IDB
idb.idb.ops.init(siteIDB.siteIDB, nodeInit);
