"use_strict";

var idbObj = require('./idb.js'),
	http = require('./http.js'),
	messenger = require('./messenger.js'),
	translator = require('./translator');
	//
	gameData = {
		fields:{},
		gameTypes:{},
		isActive:false
	},
	timeObject = null;

//
class field {
	//
	constructor(fieldId, fieldData, gameType) {
		// Define current
		this.fieldId = parseInt(fieldId);
		this.cField = fieldData;
		this.cGameType = gameType;
		//
		this.gTime = 0;
		this.rTime = 0;
		this.sTime = 0;
	}

	//
	get active() {
		return this.cField.data.status.active;
	}

	//
	command(cmd) {
		console.log("SW - GS - F:%s. COMMAND Recieved.", this.fieldId, cmd);

		//
		let fieldActions = {d:[]},
			hasUpdate = false,
			cCommand = cmd.shift();

		//
		switch ( cCommand ) {
			//
			case 'setup':
				cCommand = cmd.shift();
				//
				switch ( cCommand ) {
					// start / pause mechanism
					case 'start':
						//
						if ( this.cField.data.status.active || this.cField.data.status.fase !== 0 ) {
							// Game has started, check for pause

							let cTime = (new Date()).getTime();

							// PAUSA (toggle)
							if ( this.cField.data.status.pause ) {
								// Is paused, unpause

								//
								let timeAdd = cTime - this.cField.data.status.timePauseStart;
								this.cField.data.status.timeStart+= timeAdd;
								this.cField.data.status.timeCur+= timeAdd;
								this.sTime+= timeAdd;

								//
								this.cField.data.status.pause = 0;
								this.cField.data.status.timePauseStart = 0;

								//
								fieldActions.d = [
									{
										type:'buzzer',
										buzzSide:0,
										buzzType:4
									},
									{
										type:'sound',
										soundFileSide:0,
										soundFileNumber:4
									},
									{
										type:'note',
										note:'CONTINUE'
									},
								];
							}
							else {
								// Is running, pause

								//
								this.cField.data.status.pause = 1;
								this.cField.data.status.timePauseStart = cTime;
								//
								fieldActions.d = [
									{
										type:'buzzer',
										buzzSide:0,
										buzzType:2
									},
									{
										type:'sound',
										soundFileSide:0,
										soundFileNumber:2
									},
									{
										type:'note',
										note:'PAUSE'
									},
								];
							}
						}
						else {
							// Game not started, start

							//
							this.cField.data.status.active = 1;
							this.cField.data.status.fase = 1;
							this.cField.data.status.pause = 0;
							this.cField.data.status.sectionC = 0;
							this.cField.data.status.timeStart = (new Date()).getTime();
							this.cField.data.status.timeCur = 0;

							console.log("this.cGameType.struct", this.cGameType.struct);

							//
							fieldActions.d = [
								{
									type: 'buzzer',
									buzzSide: 0,
									buzzType: 1
								},
								{
									type: 'sound',
									soundFileSide: 0,
									soundFileNumber: 1
								},
								{
									type: 'note',
									note: 'MATCH START'
								},
								{
									type: 'timer',
									timeInt:this.cGameType.struct.settings.timeBreakInt
								},
							];

							//
							timeCheck();
						}

						// reportar cambios
						hasUpdate = true;
						this.report('field_status', fieldActions);
						break;

					//
					case 'stop':
						if ( !this.cField.data.status.active ) {
							console.warn("Field attemp to stop ignored. (game not running)", this.cField.data.status);
							return;
						}

						//
						this.cField.data.status.active = 0;
						this.cField.data.status.pause = 0;
						this.cField.data.status.fase = 3;

						//
						fieldActions.d = [
							{
								type:'buzzer',
								buzzSide:0,
								buzzType:3
							},
							{
								type:'sound',
								soundFileSide:0,
								soundFileNumber:3
							},
							{
								type:'note',
								note:'MATCH END'
							},
						];

						//
						this.triggerCheck(fieldActions);

						//
						this.cField.data.status.fase = 4; // avoid double end game triggers if called by device action

						//
						timeCheck();

						//
						hasUpdate = true;

						//
						this.report('field_status', fieldActions);
						break;

					//
					case 'reset':
						if ( this.cField.data.status.active ) {
							console.warn("Field attemp to restart ignored. (game running)", this.cField.data.status);
							return;
						}

						//
						hasUpdate = true;

						//
						this.fieldReset();
						break;

					//
					case 'next':
						//
						if ( this.cField.data.status.active || this.cField.type === 'direct' ) {
							console.log("Field attempt to next ignored. (no tourney type)");
							return;
						}

						// Save match


						//
						console.log("--- NEXT MATCH --- \n");
						console.log("Current stored data.", this.cField);


						// Actualizar información del match a la base de datos, cargar datos del siguiente match, reiniciar elementos

						// Determinar siguiente match disponible
						idbObj.idb.ops.query('game_matches', {
							index:'gameFilter',
							keyRange:IDBKeyRange.only([this.cField.gsId, this.cField.fId]),
							onSuccess:(data) => {
								let matchAvailable = false,
									selMatch;

								//
								for ( let cMatch of data ) {
									if ( cMatch.mStatus === 1 ) {
										selMatch = cMatch;
										matchAvailable = true;
										break;
									}
								}

								//
								if ( !matchAvailable ) {
									console.error("Ya no hay partidas pendientes.");
									this.send_postMessage({
										type:'alert',
										data:"Ya no hay partidas pendientes."
									});
									return;
								}

								//
								if ( selMatch.t1_side === 0 || selMatch.t2_side === 0 ) {
									console.error("La siguiente partida no tiene lados asignados.");
									this.send_postMessage({
										type:'alert',
										data:"La siguiente partida no tiene lados asignados."
									});
									return;
								}

								//
								console.log("NEXT MATCH...", selMatch);

								//
								this.cField.cMatchIndex = selMatch.index;
								this.cField.cMatchOrder = selMatch.mOrder;

								this.cField.t1_name = ( selMatch.t1_side === 1 ) ? selMatch.t1_name : selMatch.t2_name;
								this.cField.t2_name = ( selMatch.t2_side === 2 ) ? selMatch.t2_name : selMatch.t1_name;

								//
								// this.report('field_info');
								// Send information
								this.send_postMessage({
									type:'info',
									data:{
										index:selMatch.index,
										mOrder:selMatch.mOrder,
										t1_name:this.cField.t1_name,
										t2_name:this.cField.t2_name
									}
								});

								//
								this.fieldReset();

								//
								this.saveField();
							},
							onEmpty:() => {
								console.error("No hay partidas en estas condiciones.");
							}
						});

						//
						break;

					//
					default:
						console.error("SETUP: FAIL OP NOT RECOGNIZED", cCommand);
						break;
				}
				break;

			//
			case 'gameStatus':
				//
				if ( !( this.cField.data.status.active && this.cField.data.status.fase === 2) ) {
					console.log("Match status update ignored.(inactive)");
					return;
				}
				if ( this.cField.data.status.pause ) {
					console.log("Device operation ignored. (paused)");
					return;
				}

				//
				if ( cmd.length !== 2 ) {
					console.error("new game status not viable", cmd);
				}

				//
				this.gameStatusUpdate(cmd.shift(), cmd.shift());
				break;

			//
			case 'device':
				//
				if ( !this.cField.data.status.active || this.cField.data.status.fase !== 2 ) {
					console.log("Device operation ignored. (inactive)");
					return;
				}
				if ( this.cField.data.status.pause ) {
					console.log("Device operation ignored. (paused)");
					return;
				}

				//
				if ( cmd.length !== 3 ) {
					console.error("new devices status not viable", cmd);
				}

				//
				if ( !this.deviceUpdate(cmd.shift(), cmd.shift(), cmd.shift(), fieldActions) ) {
					return;
				}

				//
				hasUpdate = true;

				// Trigger check
				this.triggerCheck(fieldActions);

				// Notificar cambios
				this.report('field_status', fieldActions);
				break;

			//
			case 'event_update':
				// hasUpdate = true;
				// this.cField.data.eventsUser[cmd.data.index].devUser = cmd.data.devUser;
				// this.report('field_event_update', cmd.data);
				break;

			//
			default:
				console.trace("Operación no reconocida.", cCommand);
				break;
		}

		//
		if ( hasUpdate ) {
			this.saveField();
		}
	}

	//
	game_set_section(secNumber, cTime, fieldActions) {
		let cSection = this.cGameType.struct.properties.sections[secNumber];

		// Get current actions
		fieldActions.d = fieldActions.d.concat(cSection.actions);

		// Start section timer
		this.sTime = cTime + ( ( cSection.data.durationInt ) * 1000 );
		this.cField.data.status.sectionNumber = secNumber;
		this.cField.data.status.sectionTimeStart = cTime;

		//
		let timeText = SecondsTohhmmss(this.rTime);

		//
		console.log("SW - F:%s. - SET SECTION. Time:%s. Name:%s", this.fieldId, timeText, cSection.data.title);

		// Add to records and send mqtt
		let eventData = {
			type:'automatic',
			time:this.rTime,
			subtype:'sections',
			id:secNumber
		};
		// Add to event list
		this.cField.data.eventsUser.push(eventData);
		// Notify new event
		this.report('field_event', eventData);

		// Triggers check
		this.triggerCheck(fieldActions);
	}

	//
	advance() {
		let hasUpdate = false,
			//
			cTime = (new Date()).getTime(),
			fieldActions = {d:[]},
			timeText;

		// Saltar cuando pausa
		if ( !this.cField.data.status.active || this.cField.data.status.pause ) {
			return;
		}

		//
		this.gTime = Math.round((cTime - this.cField.data.status.timeStart) / 1000);
		this.rTime = this.gTime - this.cGameType.struct.settings.timeBreakInt;

		// console.log("SW - F:%s. - TIME ADV. G:%s. E:%s.", this.fieldId, this.gTime, this.rTime);

		// Cambiar fase del juego
		if ( this.cField.data.status.fase === 1 && this.rTime >= 0 ) {
			hasUpdate = true;
			//
			this.cField.data.status.fase = 2;
			console.log("SW - F:%s. - SCORE SYSTEM START", this.fieldId);

			// Allways start on index
			this.game_set_section(0, cTime, fieldActions);
		}

		// Eventos timed list (son removidos según el avance del juego)
		if ( this.cField.data.eventsTimed.list.length && this.cField.data.eventsTimed.list[0].rtime <= this.gTime ) {
			let cEvent = this.cField.data.eventsTimed.list.shift(),
				tempProperty = this.cGameType.struct.properties[cEvent.type][cEvent.index],
				// Event information
				eventData;

			console.log("SW - F:%s. - TIMED EVENT - Type: %s. Name: %s.", this.fieldId, cEvent.type, tempProperty.data.title);
			console.log("SW - CONT", cEvent, tempProperty);

			//
			timeText = SecondsTohhmmss(this.rTime);

			//
			switch ( cEvent.type ) {
				//
				case 'sections':
					console.error("ya no debería haber este tipo de sección.", this.cField);
					break;
				//
				case 'eTimed':
					hasUpdate = true;

					console.log("SW - Field %s:Match Event. Time:%s. Name:%s.", this.fieldId, timeText, tempProperty.data.title);

					// Add event
					eventData = {
						type:'automatic',
						time:this.rTime,
						subtype:cEvent.type,
						id:cEvent.index
					};

					// Add event to list
					this.cField.data.eventsUser.push(eventData);

					// report new field event
					this.report('field_event', eventData);

					// Process operations
					this.actionOperations(tempProperty.actions, null, fieldActions);

					// Revisar triggers
					this.triggerCheck(fieldActions);
					break;
				//
				default:
					console.error("Incorrect event timed.");
					break;
			}
			//
		}

		// Revisión dentro del juego
		if ( this.cField.data.status.fase === 2 ) {
			// Secciones
			if ( cTime > this.sTime ) {
				console.log("SW - F:%s. - SECTION NEW - Time: %s.", this.fieldId, this.sTime);

				//
				let cSection = this.cGameType.struct.properties.sections[this.cField.data.status.sectionNumber];
				if ( cSection.data.endSection === 1 ) {
					// End game with ending section
					this.command(['setup', 'stop']);
				}
				else {
					let valid = false;
					// Look for next section
					for ( let i = 0; i < this.cGameType.struct.properties.sections.length; i++ ) {
						//
						let cSection = this.cGameType.struct.properties.sections[i];
						if (
							this.cField.data.status.sectionNumber !== i &&
							this.cField.data.status.sectionNumber === cSection.data.prevSection &&
							typeof cSection.conditions === 'object' &&
							this.conditionCheck(cSection.conditions)
						) {
							this.game_set_section(i, cTime, fieldActions);
							hasUpdate = true;
							valid = true;
							break;
						}
					}

					//
					if ( !valid ) {
						console.error("juego mal hecho?, no hay posible siguiente sección.");
					}
				}
			}

			// Puntos (se tiene que volver a corroborar que el juego siga activo)
			if ( this.cField.data.status.fase === 2 ) {
				for ( let period in this.cField.data.eventsTimed.cyclic ) {
					if ( !this.cField.data.eventsTimed.cyclic.hasOwnProperty(period) ) {
						continue;
					}
					//
					if ( this.rTime % period === 0 ) {
						//
						let cPData = this.cField.data.eventsTimed.cyclic[period];
						timeText = SecondsTohhmmss(this.rTime);

						//
						console.log("SW - F:%s. - POINT CYCLE - Count:%s. Time:%s.", this.fieldId, cPData.cycleCount, timeText);

						//
						for ( let cycleDeviceIndex in cPData.devices ) {
							let cycleDevice = cPData.devices[cycleDeviceIndex],
								cDeviceProps = this.cGameType.struct.properties.devices[cycleDevice.deviceId],
								cDevice = this.cField.data.devices[cycleDevice.deviceId],
								cPointId = cycleDevice.pointId.toString(),
								teoPoint = this.cGameType.struct.points.find((cP) => { return cP.id === cycleDevice.pointId });

							console.log("checking", cycleDevice, cDevice, cPointId, teoPoint);

							// Ignore inactive devices
							if ( !cDevice.active ) {
								continue;
							}

							// Point not found?
							if ( !teoPoint ) {
								console.error("PUNTO TEÓRICO NO ENCONTRADO.", teoPoint, cPointId, this.cGameType.struct.points);
								continue;
							}

							//
							switch ( cDeviceProps.data.devType ) {
								//
								case 'swflag':
									// Ignore if no side
									if ( cDevice.side === 0 ) {
										continue;
									}

									// Validate condition
									if ( !cycleDevice.cycleObj.hasOwnProperty(cPData.cycleCount) ) {
										console.warn("Cycle point checking error", cycleDevice.cycleObj, cPData.cycleCount, cDevice.side);
										continue;
									}

									// Mark point interval
									cycleDevice.cycleObj[cPData.cycleCount][cDevice.side] = 1;

									// Score Update
									this.cField.data.points.t[cDevice.side].p[cPointId].c++;
									this.cField.data.points.t[cDevice.side].p[cPointId].t += teoPoint.points;
									this.cField.data.points.t[cDevice.side].t += teoPoint.points;
									//
									hasUpdate = true;
									break;

								//
								case 'swopflag':
									// Ignore if same side
									if ( cDevice.side === cDevice.defSide ) {
										continue;
									}

									// Mark point interval
									cycleDevice.cycleObj[cPData.cycleCount][cDevice.side] = 1;

									// Score Update
									this.cField.data.points.t[cDevice.side].p[cPointId].c++;
									this.cField.data.points.t[cDevice.side].p[cPointId].t += teoPoint.points;
									this.cField.data.points.t[cDevice.side].t += teoPoint.points;
									//
									hasUpdate = true;
									break;

								//
								default:
									console.error("DEVICES INCORRECTLY DEFINED", cDeviceProps);
									break;
							}
						}

						// Sumar a la cuenta
						cPData.cycleCount++;
					}
				}
			}
		}

		// reportes

		//
		if ( hasUpdate ) {
			this.saveField();
			this.report('field_status', fieldActions);
		}
	}

	//
	fieldReset() {
		// Deep copy method
		this.cField.data = JSON.parse(JSON.stringify(this.cGameType.struct.field));

		//
		this.report('field_status', [
			{
				type:'buzzer',
				buzzSide:0,
				buzzType:0
			},
			{
				type:'note',
				note:'FIELD READY'
			},
		]);

		this.saveField();
	}

	//
	gameStatusUpdate(gsIndex, nValue) {
		let cStatusProperties = this.cGameType.struct.properties.gameStatus[gsIndex],
			cStatusField = this.cField.data.gameStatus[gsIndex];

		//
		cStatusField.value = nValue;

		// Ejecutar cambios
		if ( typeof cStatusProperties.actions === 'object' ) {
			this.actionOperations(cStatusProperties.actions);
		}

		//
		this.report('field_gameStatus', gsIndex);

		// Check triggers
		this.triggerCheck();
		// Save changes
		this.saveField();
	}

	//
	deviceUpdate(devIndex, nSide, nStatus, fieldActions) {
		let cDevProperties = this.cGameType.struct.properties.devices[devIndex],
			cDevField = this.cField.data.devices[devIndex];

		//
		console.log("device update", cDevProperties, cDevField);

		//
		console.log("SW - F:%s. - DEV UPDATE TRY . D:%s. D:%s. D:%s. D:%s.", this.fieldId, nSide, nStatus, fieldActions);

		//
		switch ( this.cField.data.status.fase ) {
			// Waiting
			case 0:
				//
				switch ( cDevProperties.data.devType ) {
					//
					case 'base':
						switch ( nStatus ) {
							//
							case 1:
								cDevField.status = 1;
								let gameReady = true;
								for ( let cDev of this.cField.data.devices ) {
									if ( cDev.type === 'base' && !cDev.status ) {
										gameReady = false;
										return false;
									}
								}
								//
								if ( gameReady ) {
									this.command(['setup', 'start']);
								} else {
									console.log("Not all teams are ready. Waiting.");
								}
								break;
							//
							case 0:
								cDevField.status = 0;
								break;
							//
							default:
								break;
						}
						break;
					//
					case 'swflag':
					case 'opflag':
					case 'opswflag':
					case 'ctflag':
					case 'bomb_loc':
					case 'bomb_bomb':
						console.warn("Device ignored during preparation.");
						break;
					//
					default:
						console.error("Device not recognized.");
						break;
				}
				break;

			// Pre
			case 1:
				//
				switch ( cDevProperties.data.devType ) {
					// ¿Should cancel start?
					case 'base':
						console.log("should cancel?.");
						break;
					//
					case 'swflag':
					case 'opflag':
					case 'opswflag':
					case 'ctflag':
					case 'bomb_loc':
					case 'bomb_bomb':
						console.warn("Devices ignored during preparation.");
						break;
					//
					default:
						console.error("Device not recognized.");
						break;
				}
				break;

			// Running
			case 2:
				//
				console.warn("what is going on", cDevField);
				if ( !cDevField.active ) {
					console.warn("FIELD IS NOT ACTIVE, IGNORING REQUESTS.", cDevField);
					return false;
				}

				//
				switch ( cDevProperties.data.devType ) {
					//
					case 'base':
						console.error("not programed, no operations available");
						return false;
						break;
					//
					case 'swflag':
						// Validation
						if ( cDevField.side === nSide ) {
							console.warn("Swing flag: Request denied. Same side.");
							return false;
						}

						//
						cDevField.side = nSide;
						break;
					//
					case 'opflag':
						// Validation
						if ( cDevField.side === nSide || cDevProperties.data.devSideDef === nSide ) {
							console.log("Opposing flag: Request denied. Same side or trying to return.");
							return false;
						}

						//
						cDevField.side = nSide;
						break;
					//
					case 'opswflag':
						// Validation
						if ( cDevField.side === nSide ) {
							console.warn("Opposing swing flag: Request denied. Same side.");
							return false;
						}

						//
						cDevField.side = nSide;
						break;
					//
					case 'endflag':
						// Validation
						if ( cDevField.side === nSide ) {
							console.warn("End flag: Request denied. Same side.");
							return false;
						}

						//
						cDevField.side = nSide;
						break;
					//
					case 'ctflag':
						// Validation
						if ( cDevField.side === nSide ) {
							console.warn("Capture the flag: Request denied. Same side.");
							return false;
						}

						//
						cDevField.side = nSide;
						break;
					//
					case 'ctspot':
						// Validation
						if ( cDevField.side === nSide || nStatus !== 2 ) {
							console.warn("Capture the spot: Request denied. Same side or current status is invalid (not 2)");
							return false;
						}

						//
						cDevField.side = nSide;
						break;
					//
					case 'bomb_loc':
					case 'bomb_bomb':
						console.warn("Bomb ops not developed.");
						return false;
						break;
					//
					default:
						console.error("UKNOWN DEVICE FOR OPERATION", cDevField);
						return false;
						break;
				}

				//
				let cEvent = {
					type:'device',
					time:this.rTime,
					devId:devIndex,
					devSide:nSide,
					devUser:0
				};
				// Add actual event to system
				this.cField.data.eventsUser.push(cEvent);

				// Report to external
				this.report('field_event', cEvent);
				//
				this.actionOperations(cDevProperties.actions, devIndex, fieldActions);
				// Trigger checks and savings are outside this function after true return
				return true
				break;

			// Waiting reset
			case 3:
				console.warn("GAME END - WAITING RESET");
				break;

			//
			default:
				console.error("GAME ERROR - UNKNOWN STATE.");
				return;
				break;
		}

		//
		return false;
	}

	//
	actionOperations(actions, devId = null, fieldActions) {
		let saveField = false,
			pointUpdate = false;

		//
		console.log("SW - FIELD DYNAMIC - ACTION OPERATION.", actions, devId);

		//
		for ( let cAction of actions ) {
			//
			switch ( cAction.type ) {
				// Enviar a notificaciones directamente
				case 'buzzer':
				case 'sound':
				case 'timer':
				case 'note':
					fieldActions.d.push(cAction);
					break;
				//
				case 'device':
					saveField = true;
					break;
				//
				case 'devMod':
					let cDevField = this.cField.data.devices[cAction.devNumber];
					cDevField.side = cAction.devSide;
					cDevField.status = cAction.devStatus;
					cDevField.active = cAction.devActive;
					break;
				//
				case 'statusMod':
					let cFieldStatus = this.cField.data.gameStatus[cAction.statusNumber];
					cFieldStatus.value = cAction.statusValue;
					break;
				//
				case 'gameEnd':
					// End game with ending section
					this.command(['setup', 'stop']);
					break;
				//
				case 'point':
					let cPoint = this.cGameType.struct.points.find((obj) => { return obj.id === cAction.pointId; }),
						deviceStatus = this.cField.data.devices[devId];

					//
					if ( !cPoint ) {
						console.error("GS - Action operation - Point. Punto no encontrado.", cAction.pointId, this.cGameType.struct.points, actions);
						continue;
					}

					//
					if ( !deviceStatus ) {
						console.error("Dispositivo no encontrado.", actions, cAction);
						continue;
					}

					// USER TYPE POINT CURRENTLY IGNORED
					if ( cPoint.target !== 1 ) { continue; }

					//
					switch ( cAction.pointType ) {
						//
						case 'simple':
							//
							this.cField.data.points.t[deviceStatus.side].p[cPoint.id].c++;
							this.cField.data.points.t[deviceStatus.side].p[cPoint.id].t += cPoint.points;
							this.cField.data.points.t[deviceStatus.side].t += cPoint.points;
							console.log("simple point executed");
							break;
						//
						case 'cyclic':
							let missing = true;

							//
							outer_loop:
								for ( let cIndex in this.cField.data.eventsTimed.cyclic ) {
									if ( !this.cField.data.eventsTimed.cyclic.hasOwnProperty(cIndex) ) { continue; }
									let content = this.cField.data.eventsTimed.cyclic[cIndex];
									for ( let cDev of content.devices ) {
										if ( cDev.deviceId === devId && cDev.cycleObj[content.cycleCount][deviceStatus.side.toString()] ) {
											missing = false;
											break outer_loop;
										}
									}
								}

							//
							if ( missing ) {
								//
								saveField = true;
								pointUpdate = true;
								//
								this.cField.data.points.t[deviceStatus.side.toString()].p[cAction.pointId.toString()].c++;
								this.cField.data.points.t[deviceStatus.side.toString()].p[cAction.pointId.toString()].t += cPoint.points;
								this.cField.data.points.t[deviceStatus.side.toString()].t += cPoint.points;
							}
							break;
						//
						case 'cyclic_exclusive':
							break;
						//
						default:
							break;
					}
					break;
				//
				case 'pointSet':
					let cPointSet = this.cGameType.struct.points.find((obj) => { return obj.id === cAction.pointSetId; });
					if ( !cPointSet ) {
						console.error("Action operation, punto no válido.", actions);
						continue;
					}

					//
					// For now, only points for teams
					if ( cPointSet.target !== 1 ) { continue; }

					//
					switch ( cAction.pointSetType ) {
						//
						case 'simple':
							//
							saveField = true;
							pointUpdate = true;
							//
							this.cField.data.points.t[cAction.pointSetSide].p[cAction.pointSetId.toString()].c++;
							this.cField.data.points.t[cAction.pointSetSide].p[cAction.pointSetId.toString()].t += cPointSet.points;
							this.cField.data.points.t[cAction.pointSetSide].t += cPointSet.points;
							break;
						//
						case 'cyclic':
							console.log("for now not biatch");
							break;
						//
						case 'cyclic_exclusive':
							console.log("for now not biatch");
							break;
						//
						default:
							console.error("pointset invalid pointtype");
							break;
					}
					break;
				//
				default:
					console.error("SW - Field - Action operation not defined.", cAction.type, cAction);
					break;
			}
		}
	}

	//
	triggerCheck(fieldActions) {
		//
		for ( let cTrigger of this.cGameType.struct.properties.eTriggered ) {
			//
			if ( this.conditionCheck(cTrigger.conditions) ) {
				this.actionOperations(cTrigger.actions, null, fieldActions);
				break;
			}
		}
	}

	//
	conditionCheck(conditionList) {
		// console.log("SW - F:%s. - CONDITION CHECK", this.fieldId);
		for ( let cCondition of conditionList ) {
			//
			switch ( cCondition.type ) {
				//
				case 'section':
					if ( this.cField.data.status.sectionNumber !== cCondition.sectionNumber ) {
						return false;
					}
					break;

				//
				case 'gameStatus':
					let cStatusValue = this.cField.data.gameStatus[cCondition.gameStatusNumber].value;
					switch ( cCondition.gameStatusComp ) {
						case '==':
							if ( !( cStatusValue === cCondition.gameStatusValue ) )
							{
								return false;
							}
							break;
						case '!=':
							if ( !( cStatusValue !== cCondition.gameStatusValue ) )
							{
								return false;
							}
							break;
						case '<=':
							if ( !( cStatusValue <= cCondition.gameStatusValue ) )
							{
								return false;
							}
							break;
						case '<':
							if ( !( cStatusValue < cCondition.gameStatusValue ) )
							{
								return false;
							}
							break;
						case '>=':
							if ( !( cStatusValue >= cCondition.gameStatusValue ) )
							{
								return false;
							}
							break;
						case '>':
							if ( !( cStatusValue > cCondition.gameStatusValue ) )
							{
								return false;
							}
							break;
						default:
							console.error("incorrect comparison value");
							break;
					}
					break;

				//
				case 'gameEnd':
					if ( this.cField.data.status.fase !== 3 ) {
						return false;
					}
					break;

				//
				case 'devStatus':
					let cDev = this.cField.data.devices[cCondition.devNumber];
					// console.log("SW - F:%s. - SAME SIDE CHECK", this.fieldId);
					// console.log("SW - CONT", cDev, cCondition);
					if (
						cDev.side !== cCondition.devSide ||
						cDev.status !== cCondition.devStatus ||
						cDev.active !== cCondition.devActive
					) {
						return false;
					}
					break;

				//
				case 'sameSideCheck':

					for ( let cDevId of cCondition.deviceNumberSide ) {
						if ( this.cField.data.devices[cDevId].side !== cCondition.sameSideValue ) {
							return false;
						}
					}
					break;

				//
				default:
					console.error("SERVER trigger check type invalid.");
					return false;
					break;
			}
		}
		//
		return true;
	}

	//
	saveField() {
		idbObj.idb.ops.put('server_fields', this.cField, {
			onSuccess:() => {
				console.log("SW - F:%s. - FIELD SAVED", this.fieldId);
				//
				if ( this.cField.type !== 'tournament' ) {
					return;
				}

				//
				idbObj.idb.ops.query('game_matches', {
					get:this.cField.cMatchIndex,
					onSuccess:(matchDataOrig) => {
						console.log(matchDataOrig);
						let mData = {
							mStatus:1,
							mPoints:this.cField.data.points,
							mEvents:this.cField.data.eventsUser,
							t1_points:this.cField.data.points.t[matchDataOrig.t1_side].t,
							t1_status:0,
							t2_points:this.cField.data.points.t[matchDataOrig.t2_side].t,
							t2_status:0,
						};

						//
						switch ( this.cField.data.status.fase ) {
							//
							case 0:
								mData.mStatus = 1;
								break;
							//
							case 1:
							case 2:
								mData.mStatus = 2;
								break;
							// Ending time
							case 3:
							case 4:
								mData.mStatus = 3;
								mData.t1_points = ( mData.t1_points > this.cGameType.struct.settings.maxPointsGame ) ? this.cGameType.struct.settings.maxPointsGame : mData.t1_points;
								mData.t2_points = ( mData.t2_points > this.cGameType.struct.settings.maxPointsGame ) ? this.cGameType.struct.settings.maxPointsGame : mData.t2_points;
								if ( mData.t1_points > mData.t2_points ) {
									mData.t1_status = 3;
									mData.t2_status = 1;
								} else if ( mData.t1_points < mData.t2_points ) {
									mData.t1_status = 1;
									mData.t2_status = 3;
								} else {
									mData.t1_status = 2;
									mData.t2_status = 2;
								}
								break;
							//
							default:
								console.error("");
								break;
						}

						// Actualizar match
						idbObj.idb.ops.update('game_matches', this.cField.cMatchIndex, mData, {
							onSuccess:(data) => {
								console.log("SW - MATCH DATA SAVED");
							},
							onError:() => {
								console.log("match not updated...");
							}
						});

						//
					}
				});
				//
				// END IF GAME
			}
		});
	}

	//
	report(type, data = []) {
		//
		switch ( type ) {
			//
			case 'field_status':
				let structure = {
					field: {
						active: this.cField.data.status.active,
						fase: this.cField.data.status.fase,
						pause: this.cField.data.status.pause,
						sectionC: this.cField.data.status.section,
					},
					points: {
						'1':{t: this.cField.data.points.t['1'].t},
						'2':{t: this.cField.data.points.t['2'].t}
					},
					devices: this.cField.data.devices.map((cObj) => { delete cObj.type; return cObj; }),
					gameStatus: this.cField.data.gameStatus.map((cObj) => { return cObj.value; }),
					actions: data.d
				};
				//
				this.send_postMessage({
					type:'status',
					data: structure
				});

				// Send websocket update
				console.log("FIELD STATUS (%d)\n\n", this.fieldId);
				let cMsg = translator.jsonToMsg(this.fieldId, structure);
				let newStruct = translator.msgToJson(cMsg.slice(3), this.cField.data.devices.length, this.cField.data.gameStatus.length);
				console.log("\n\n");
				this.send_ws_update('field', cMsg);
				break;
			//
			case 'field_event':
				//
				this.send_postMessage({
					type:'event',
					data: data
				});
				break;
			//
			case 'field_event_update':
				//
				this.send_postMessage({
					type:'event_update',
					data: data
				});
				break;
			//
			default:
				console.error("reporting type not defined", type, data);
				break;
		}
	}

	//
	send_ws_update(type, cData = null) {
		//
		switch ( type ) {
			//
			case 'field':
				console.log("FIELD STATUS:", this.cField);
				break;
			//
			case 'actions':
				break;
		}

		// Llamada de envio
		http.wss_sendField(this.fieldId, cData);
	}

	//
	send_postMessage(data) {
		messenger.msgToWeb({
			op:'field',
			id:this.fieldId,
			data:data
		});
	}
}
//

//
function SecondsTohhmmss(totalSeconds) {
	let minutes = Math.floor(totalSeconds / 60),
		seconds = totalSeconds - (minutes * 60);

	// round seconds
	seconds = Math.round(seconds * 100) / 100;

	let result = ( minutes < 10 ? "0" + minutes :minutes );
	result += ":" + ( seconds  < 10 ? "0" + seconds :seconds );

	return result;
}
//

//
function timeCheck() {
	let activeGame = false;

	//
	if ( gameData.fields ) {
		//
		for ( let cField in gameData.fields ) {
			if ( !gameData.fields.hasOwnProperty(cField) ) { continue; }
			if ( gameData.fields[cField].active ) {
				activeGame = true;
				break;
			}
		}
	}

	//
	if ( activeGame ) {
		console.log("GS - GENERAL TIMER CHECK");
		if ( timeObject === null ) {
			console.log("GS - GENERAL TIMER START");
			timeObject = setInterval(fieldProgress, 1000);
		}
	} else {
		console.log("GS - GENERAL TIMER STOP");
		clearInterval(timeObject);
		timeObject = null;
	}
}

//
function fieldProgress() {
	//
	for ( let cField in gameData.fields ) {
		if ( !gameData.fields.hasOwnProperty(cField) || !gameData.fields[cField].active ) { continue; }
		gameData.fields[cField].advance();
	}
}

//
function init() {
	//
	console.log("GAMESERVER - Start.");

	// Load field status
	let queries = [
		{
			rName:'gametypes',
			table:'gametypes',
			params: {}
		},
		{
			rName:'server_fields',
			table:'server_fields',
			params: {}
		}
	];

	// Generar json data
	idbObj.idb.ops.queryBatch(queries, {
		onSuccess:(results) => {
			console.log("GS - GAMES LOADED", results);

			//
			if ( results.gametypes ) {
				for ( let cGameType of results.gametypes ) {
					let id = cGameType.id.toString();
					gameData.gameTypes[id] = cGameType;
				}
			}

			//
			if ( results.server_fields ) {
				for ( let cField of results.server_fields ) {
					let id = cField.id.toString();
					// Create object
					gameData.fields[id] = new field(id, cField, gameData.gameTypes[cField.gtId.toString()]);

					// Debug only
					// setTimeout(() => { gameData.fields[id].report('field_status'); }, 2000);
				}
				console.log("INI - SERVER FIELDS REGISTERED:", gameData.fields);
			}

			// Autostart?
			timeCheck();
		}
	});

	// Bind messages
	// self.addEventListener('message', worker.message, false);
}

//
function field_getString(cField) {
	let fData = '';

	// Extract field data
	fData+= 's:';
	fData+= cField.status.active.toString();
	fData+= ',';
	fData+= cField.status.fase.toString();
	fData+= ',';
	fData+= cField.status.section.toString();
	fData+= ',';
	fData+= cField.status.pause.toString();
	fData+= ';';

	// Points
	fData+= 'p:';
	fData+= cField.points.t['1'].t.toString();
	fData+= ',';
	fData+= cField.points.t['2'].t.toString();
	fData+= ';';

	// Device status
	fData+= 'd:';
	let dArray = [];
	cField.devices.forEach((cEl) => {
		dArray.push(cEl.active.toString() + cEl.status.toString() + cEl.side.toString())
	});
	fData+= dArray.join(',');
	fData+= ';';

	// gameStatus
	fData+= 'g:'
	let gSArray = [];
	cField.gameStatus.forEach((cEl) => {
		gSArray.push(cEl.value.toString())
	});
	fData+= gSArray.join(',');

	//
	return fData;
}

//
function field_add(msg) {
	let id = msg.fId.toString();

	//
	gameData.fields[id] = new field(id, msg.data);
	console.log("SW - Campo agregado, actuales: ", id, gameData.fields);
}

//
function field_del(msg) {
	let id = msg.fId.toString();
	delete gameData.fields[id];
}

//
function field_command(msg) {
	let fieldId = msg.shift().toString();
	//
	if ( !gameData.fields.hasOwnProperty(fieldId) ) {
		console.error("Campo no disponible", fieldId, gameData.fields);
		return;
	}
	//
	gameData.fields[fieldId].command(msg);
}

//
module.exports.start = init;
module.exports.gameData = gameData;
module.exports.field = {
	add:field_add,
	del:field_del,
	command:field_command,
	getField:field_getString
};
//
