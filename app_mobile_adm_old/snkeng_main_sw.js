'use strict';


var sw_config = {
	version:'02',
	staticCacheItems:[
	],
	cacheIgnorePattern:/^\/(ajax|iot)(.+)?$/,
	cacheStaticPattern:/^(.+).(css\.gz|js\.gz|svgz|svg|jpe?g|gif|png)$/,
	offlineImage:'<svg role="img" aria-labelledby="offline-title"'
	+ ' viewBox="0 0 400 300" xmlns="http://www.w3.org/2000/svg">'
	+ '<title id="offline-title">Offline</title>'
	+ '<g fill="none" fill-rule="evenodd"><path fill="#D8D8D8" d="M0 0h400v300H0z"/>'
	+ '<text fill="#9B9B9B" font-family="Times New Roman,Times,serif" font-size="72" font-weight="bold">'
	+ '<tspan x="93" y="172">offline</tspan></text></g></svg>',
	offlinePage:'/offline/'
};

function cacheName(key, opts) {
	return `${opts.version}-${key}`;
}

function addToCache(cacheKey, request, response) {
	if ( response.ok ) {
		let copy = response.clone();
		caches.open(cacheKey).then(cache => {
			cache.put(request, copy);
		});
	}
	return response;
}

function fetchFromCache(event) {
	return caches.match(event.request).then(response => {
		if ( !response ) {
			throw Error(`${event.request.url} not found in cache`);
		}
		return response;
	});
}

function offlineResponse(resourceType, opts) {
	if ( resourceType === 'image' ) {
		return new Response(opts.offlineImage,
			{headers:{'Content-Type':'image/svg+xml'}}
		);
	} else if ( resourceType === 'content' ) {
		return caches.match(opts.offlinePage);
	}
	return undefined;
}
//
self.addEventListener('install', event => {
	function onInstall(event, opts) {
		let cacheKey = cacheName('static', opts);
		return caches.open(cacheKey).then(cache => cache.addAll(opts.staticCacheItems));
	}

	event.waitUntil(
		onInstall(event, sw_config).then(() => self.skipWaiting())
	);
});
//
self.addEventListener('activate', event => {
	function onActivate(event, opts) {
		return caches.keys()
			.then(cacheKeys => {
				let oldCacheKeys = cacheKeys.filter(key => key.indexOf(opts.version) !== 0),
					deletePromises = oldCacheKeys.map(oldKey => caches.delete(oldKey));
				return Promise.all(deletePromises);
			});
	}

	event.waitUntil(
		onActivate(event, sw_config).then(() => self.clients.claim())
	);
});
//
self.addEventListener('fetch', event => {
	function shouldHandleFetch(event, opts) {
		let request = event.request,
			url = new URL(request.url),
			criteria = {
				matchesPathPattern:!opts.cacheIgnorePattern.test(url.pathname),
				isGETRequest:request.method === 'GET',
				isFromMyOrigin:url.origin === self.location.origin
			},
			failingCriteria = Object.keys(criteria).filter(criteriaKey => !criteria[criteriaKey]);
		return !failingCriteria.length;
	}

	function onFetch(event, opts) {
		let request = event.request,
			resourceType,
			cacheKey;

		resourceType = ( opts.cacheStaticPattern.test(request.url) ) ? 'resource' : 'content';
		cacheKey = cacheName(resourceType, opts);

		if ( resourceType === 'content' ) {
			event.respondWith(
				fetch(request)
					.then(response => addToCache(cacheKey, request, response))
					.catch(() => fetchFromCache(event))
					.catch(() => offlineResponse(resourceType, opts))
			);
		} else {
			event.respondWith(
				fetchFromCache(event)
					.catch(() => fetch(request))
					.then(response => addToCache(cacheKey, request, response))
					.catch(() => offlineResponse(resourceType, opts))
			);
		}
	}

	//
	if ( shouldHandleFetch(event, sw_config) ) {
		onFetch(event, sw_config);
	}
});
//