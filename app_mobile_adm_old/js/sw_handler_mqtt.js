﻿"use strict";

//
class mqtt_server {
	status = false;
	sw_object = undefined;
	//
	constructor(connData) {
		//
		if ( !window.Worker ) {
			alert("NO Webworker support... Kill Self");
			return;
		}

		//
		if ( typeof(this.sw_object) !== "undefined" ) {
			console.error("Objeto ya inicializado");
			return;
		}

		//
		this.sw_object = new Worker("/js/workers/server_mqtt.js");
		console.log("SW - MQTT: Init called");

		// CB Assign
		this.sw_object.onmessage = this.sw_onMessage;

		// Send connect message
		this.sw_postMessage(connData);
	}

	//
	disconnect() {
		this.sw_object.terminate();
		this.sw_object = undefined;
	}

	//
	sw_onMessage(e) {
		let msg = e.data,
			page_field_advanced = $('#page_field_advanced');

		//
		console.log("MAIN - SW MESSAGE RECIEVED:", msg);

		//
		switch ( msg.op ) {
			//
			case 'adv':
				pages.match.admin.timer.advanceWork(msg);
				if ( msg.notif ) {
					se_app.notifications.add('Match', {
						body:msg.notif,
						// icon:'icons/chrome_36.png',
						vibrate:[500, 300, 500, 300, 500]
					});
				}
				break;
			//
			case 'field':
				if ( page_field_advanced ) {
					page_field_advanced.uwp_page_field_advanced.serverMessageRecieve(msg.id, msg.data);
				}
				break;
			//
			default:
				console.error("Server worker unknown message", e.data);
				break;
		}
	}

	//
	sw_postMessage(data) {
		if ( !this.sw_object ) {
			console.error("no service workder object");
			return;
		}

		//
		this.sw_object.postMessage(data);
	}
}
//
