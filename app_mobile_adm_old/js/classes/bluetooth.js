class bluetooth {
	//
	btDevice = null;
	btGattServer = null;
	btService = null;
	//
	btChar_fieldUpdate = null;
	btChar_msgHwIn = null;

	constructor() {
	}

	async connect() {
		//
		let DEVICE_NAME = 'VP_DEVICE',
			SERVICE_UUID = "4fafc201-1fb5-459e-8fcc-c5c9c331914b";

		//
		let options = {
			filters:[{name:DEVICE_NAME}],
			optionalServices:[SERVICE_UUID]
		};
		
		try {
			console.log('Requesting Bluetooth Device...');
			console.log('with ' + JSON.stringify(options));
			this.btDevice = await navigator.bluetooth.requestDevice(options);

			console.log('> Name:             ' + this.btDevice.name);
			console.log('> Id:               ' + this.btDevice.id);
			console.log('> Connected:        ' + this.btDevice.gatt.connected);

			// Auto reconnect
			this.btDevice.addEventListener('gattserverdisconnected', await this.onDisconnect);

			// Actual connect operation
			await this.connectReal();
		} catch(error)  {
			console.error("BLE CONNECT ERROR!\n", error);
		}
	}

	//
	async onDisconnect() {
		console.log('> Bluetooth Device disconnected');
		//
		this.btGattServer = null;
		this.btService = null;
		//
		await this.connectReal();
	}

	//
	async connectReal() {
		//
		let SERVICE_UUID = "4fafc201-1fb5-459e-8fcc-c5c9c331914b",
			UUID_FIELD_UPDATE = "beb5483e-36e1-4688-b7f5-ea07361b32a8",
			UUID_DIRECT_COMS = "beb5483e-36e1-4688-b7f5-ea07361b42a8",
			context = this;


		this.exponentialBackoff(10 /* max retries */, 1 /* seconds delay */,
			async function toTry() {
				console.log('Connecting to Bluetooth Device... ', context);

				console.log('Connecting to GATT Server...');
				context.btGattServer = await context.btDevice.gatt.connect();

				console.log('Getting Service...');
				context.btService = await context.btGattServer.getPrimaryService(SERVICE_UUID);

				console.log('Getting Characteristic... (DIRECT_COMS)');
				context.btChar_msgHwIn = await context.btService.getCharacteristic(UUID_DIRECT_COMS);


				// Read current value
				await context.btChar_msgHwIn.readValue().then(
					(value) => {
						console.log('msg in Current Value: ' + value, value.byteLength);

						let nValue = context.convertData(value);
						console.log('msg in Currented Value: ' + nValue);
					}
				);

				//
				await context.btChar_msgHwIn.startNotifications();
				context.btChar_msgHwIn.addEventListener('characteristicvaluechanged', context.hardwareMessageIn);
			},
			function success() {
				console.log('BT connected');
			},
			function fail() {
				console.log('Failed to reconnect.');
			},
		);
	}

	//
	async exponentialBackoff(max, delay, toTry, success, fail, context) {
		try {
			const result = await toTry();
			success(result);
		} catch(error) {
			if (max === 0) {
				return fail();
			}
			console.log('Retrying in ' + delay + 's... (' + max + ' tries left)');
			setTimeout(
				() => this.exponentialBackoff(--max, delay + 1, toTry, success, fail, context),
				delay * 1000
			);
		}
	}

	//
	convertData(dataView) {
		let nValue = [];

		//
		for ( let i = 0; i < dataView.byteLength; i++ ) {
			nValue.push(this.padHex(dataView.getUint8(i)));
		}
		//
		return nValue;
	}

	//
	padHex(value) {
		return ('00' + value.toString(16).toUpperCase()).slice(-2);
	}

	//
	fieldUpdateChange(event) {
		let fieldData = event.target.value,
			nValue = this.convertData(fieldData);
		console.log('UPDATED VALUE (Length: ' + fieldData.byteLength + ') - ' + nValue);
	}

	//
	hardwareMessageIn(event) {
		let fieldData = event.target.value;
		// 	nValue = this.convertData(fieldData);
		console.log('UPDATED VALUE (Length: ' + fieldData.byteLength + ') - ', fieldData);

		// General ops


		// Process for logging
		let logPage = $('page_device_bluetooth');
		if ( logPage ) {
			logPage.uwp_page_device_bluetooth.addToLog('in', fieldData);
		}

		/*
		// Online mode
		let gamePage = $('page_device_bluetooth');
		if ( logPage ) {
			logPage.uwp_page_device_bluetooth.addToLog('in', fieldData);
		}

		 */
	}
	
	//
	sendMessageString(value) {
		if ( !this.btChar_msgHwIn ) { return; }

		//
		let enc = new TextEncoder(), // always utf-8
			cArray = enc.encode(value);

		console.log("MAIN - BT - Send message string: ", value, cArray);

		//
		this.btChar_msgHwIn.writeValueWithResponse(cArray).then(
			(e) => {
				console.log("MAIN - BT - Sent message: ", value, cArray);
			}
		)
	}

	//
	sendMessageDirect(value) {
		if ( !this.btChar_msgHwIn ) { return; }

		console.log("MAIN - BT - Send message: ", value);

		//
		this.btChar_msgHwIn.writeValueWithResponse(value).then(
			(e) => {
				console.log("MAIN - BT - Sent message: ", value);
			}
		)
	}
}
