class webserial {
	messageRecieveFunc = null;
	usbPort = null

	constructor() {
	}

	//
	async connect() {
		// Prompt user to select any serial port. vid=1A86 pid=7523
		this.usbPort = await navigator.serial.requestPort({ usbVendorId: 0x1A86, usbProductId: 0x7523 });
		await this.usbPort.open({ baudRate: 57600, dataBits:8, stopBits:1, parity:'none' });

		//
		while ( this.usbPort.readable ) {
			const reader = this.usbPort.readable.getReader();

			try {
				while (true) {
					const { value, done } = await reader.read();
					if (done) {
						// Allow the serial port to be closed later.
						reader.releaseLock();
						break;
					}
					if ( value ) {
						console.log("RAW INCOMMING VALUE", value);
						if ( typeof this.messageRecieveFunc === 'function' ) {
							this.messageRecieveFunc(value);
						}
					}
				}
			} catch (error) {
				// TODO: Handle non-fatal read error.
			}
		}


		// With transform streams.

		const textDecoder = new TextDecoderStream();
		const readableStreamClosed = this.usbPort.readable.pipeTo(textDecoder.writable);
		const reader = textDecoder.readable.getReader();

		// Listen to data coming from the serial device.
		while ( true ) {
			const { value, done } = await reader.read();
			if ( done ) {
				reader.releaseLock();
				break;
			}
			// value is a string.
			if ( typeof this.messageRecieveFunc === 'function' ) {
				this.messageRecieveFunc(value);
			}
		}

		const textEncoder = new TextEncoderStream();
		const writableStreamClosed = textEncoder.readable.pipeTo(this.usbPort.writable);

		reader.cancel();
		await readableStreamClosed.catch(() => { /* Ignore the error */ });

		await this.usbPort.close();

		/*
		navigator.serial.addEventListener("connect", (event) => {
			// TODO: Automatically open event.target or warn user a port is available.
		});

		navigator.serial.addEventListener("disconnect", (event) => {
			// TODO: Remove |event.target| from the UI.
			// If the serial port was opened, a stream error would be observed as well.
		});

		 */
	}

	//
	messageRecieveBind(funcName) {
		if ( typeof funcName !== 'function' ) {
			console.error("esta no es una función", funcName);
		}
	}

	//
	messageRecieveUnbind() {
		this.messageRecieveFunc = null;
	}

	//
	async messageSendBytes(content) {
		const writer = this.usbPort.writable.getWriter();

		const data = new Uint8Array([104, 101, 108, 108, 111]); // hello
		await writer.write(data);


		// Allow the serial port to be closed later.
		writer.releaseLock();
	}
	//
	async messageSendText(content) {
		const textEncoder = new TextEncoderStream();
		const writableStreamClosed = textEncoder.readable.pipeTo(this.usbPort.writable);

		const writer = textEncoder.writable.getWriter();

		await writer.write(content);

		writer.releaseLock();
	}
}