

//
var translator = {
	//
	jsonToMsg(messsageJSON) {
		let byteData = [],
			cByte = '',
			codedString;

		// Field

		// Properties
		cByte = ( messsageJSON.field.active ) ? '1' : '0';
		cByte+= ( messsageJSON.field.pause ) ? '1' : '0';
		cByte+= translator.numberToCuttedBinary(messsageJSON.field.fase, 6);

		byteData.push(translator.binaryStringToChar(cByte));

		// Current section
		byteData.push(translator.numberToChar(messsageJSON.field.sectionC));

		// Points
		byteData = byteData.concat(translator.smallIntToCharArray(messsageJSON.points["1"]));
		byteData = byteData.concat(translator.smallIntToCharArray(messsageJSON.points["2"]));

		// Devices
		for ( let cDev of messsageJSON.devices ) {
			cByte = ( cDev.active ) ? '1' : '0';
			cByte+= translator.numberToCuttedBinary(cDev.status, 5);
			cByte+= translator.numberToCuttedBinary(cDev.side, 2);
			byteData.push(translator.binaryStringToChar(cByte));
		}

		// Conditions
		for ( let cCondition of messsageJSON.gameStatus ) {
			byteData.push(translator.numberToChar(cCondition));
		}

		// Actions
		if ( messsageJSON.actions ) {
			for ( let cAction of messsageJSON.actions ) {

				//
				switch ( cAction.type ) {
					//
					case 'buzzer':
						byteData.push('b');
						cByte = translator.numberToCuttedBinary(cAction.buzzSide, 2);
						cByte += translator.numberToCuttedBinary(cAction.buzzType, 6);
						//
						byteData.push(translator.binaryStringToChar(cByte));
						break;
					//
					case 'sound':
						byteData.push('s');
						cByte = translator.numberToCuttedBinary(cAction.soundFileSide, 2);
						cByte += translator.numberToCuttedBinary(cAction.soundFileNumber, 6);
						//
						byteData.push(translator.binaryStringToChar(cByte));
						break;
					//
					case 'timer':
						byteData.push('t');
						byteData = byteData.concat(translator.smallIntToCharArray(cAction.timeInt));
						break;
					//
					case 'note':
						byteData.push('n');
						byteData = byteData.concat(cAction.note.substring(0, 12).padEnd(12, ' ').split(''));
						break;
					//
					default:
						console.error("but how? action type not defined.");
						break;
				}
			}
		}

		//
		let byteData2 = byteData.map((cEl) => { return translator.charToNumber(cEl); }),
			strData = byteData2.join(',');
		//
		// console.error("OPERATION (length:%s):\n{%s},\n", byteData2.length, strData);

		return byteData.join('');
	},

	//
	msgToJson(messageString, deviceLength, conditionLength) {
		let jsonStruct = {
				"field": {
					"active": 0,
					"pause": 0,
					"fase": 0,
					"sectionC": 0
				},
				"points": {
					"1": 0,
					"2": 0
				},
				"devices": [],
				"gameStatus": [],
				"actions": []
			},
			messageArray = messageString.split(''),
			cChar, cBinary;

		//
		console.log("INCOMMING MESSAGE", messageArray);

		// Field

		// Properties
		cChar = messageArray.shift();

		cBinary = translator.charToBinaryString(cChar);

		jsonStruct.field.active = translator.cuttedBinaryToNumber(cBinary, 0, 1);
		jsonStruct.field.pause = translator.cuttedBinaryToNumber(cBinary, 1, 2);
		jsonStruct.field.fase = translator.cuttedBinaryToNumber(cBinary, 3, 8);

		// Section
		jsonStruct.field.sectionC = translator.charToNumber(messageArray.shift());

		// Points
		jsonStruct.points['1'] = translator.charArrayToSmallInt(messageArray.splice(0, 2));
		jsonStruct.points['2'] = translator.charArrayToSmallInt(messageArray.splice(0, 2));

		// Devices
		for ( let i = 0; i < deviceLength; i++ ) {
			let cBinary = translator.charToBinaryString(messageArray.shift());

			jsonStruct.devices.push({
				active:translator.cuttedBinaryToNumber(cBinary, 0, 1),
				status:translator.cuttedBinaryToNumber(cBinary, 1, 6),
				side:translator.cuttedBinaryToNumber(cBinary, 6, 8)
			});
		}

		// Condition
		for ( let i = 0; i < conditionLength; i++ ) {
			jsonStruct.gameStatus.push(translator.charToNumber(messageArray.shift()));
		}

		//
		while ( messageArray.length )
		{
			let cBinary,
				cAction;
			switch ( messageArray.shift() ) {
				//
				case 'b':
					cBinary = translator.charToBinaryString(messageArray.shift());
					cAction = {
						type:'buzzer',
						buzzSide:translator.cuttedBinaryToNumber(cBinary, 0, 2),
						buzzType:translator.cuttedBinaryToNumber(cBinary, 3, 8),
					};
					break;
				//
				case 's':
					cBinary = translator.charToBinaryString(messageArray.shift());
					cAction = {
						type:'sound',
						soundFileSide:translator.cuttedBinaryToNumber(cBinary, 0, 2),
						soundFileNumber:translator.cuttedBinaryToNumber(cBinary, 3, 8),
					};
					break;
				//
				case 't':
					cAction = {
						type:'timer',
						timeInt:translator.charArrayToSmallInt(messageArray.splice(0, 2)),
					};
					break;
				//
				case 'n':
					cAction = {
						type:'note',
						note:messageArray.splice(0, 12).join('').trimRight(),
					};
					break;
				//
				default:
					break;
			}

			//
			if ( cAction ) {
				jsonStruct.actions.push(cAction);
			}
		}


		return jsonStruct;
	},

	//
	numberToCuttedBinary(value, maxLength) {
		let binaryNumber = ( value >>> 0 ).toString(2),
			binaryText = binaryNumber.substring(-maxLength).padStart(maxLength, '0');

		return binaryText;
	},

	//
	smallIntToCharArray(/*long*/long) {
		// we want to represent the input as a 8-bytes char array
		let byteArray = [0, 0];

		for ( let index = 0; index < byteArray.length; index ++ ) {
			let byte = long & 0xff;
			byteArray [ index ] = String.fromCharCode(byte);
			long = (long - byte) / 256 ;
		}

		return byteArray;
	},

	//
	charArrayToSmallInt(/*byte[]*/byteArray) {
		let value = 0;
		for ( let i = byteArray.length - 1; i >= 0; i--) {
			value = ( value * 256 ) + byteArray[i].charCodeAt(0);
		}

		return value;
	},

	//
	binaryStringToChar(binaryString) {
		let intValue = parseInt(binaryString, 2),
			charValue = String.fromCharCode(intValue);
		//
		return charValue;
	},

	//
	charToBinaryString(char) {
		let intValue = char.charCodeAt(0),
			binaryString = ( intValue >>> 0 ).toString(2).padStart(8, '0');
		//
		return binaryString;
	},

	//
	numberToChar(number) {
		return String.fromCharCode(number);
	},

	//
	charToNumber(char) {
		return char.charCodeAt(0);
	},

	//
	cuttedBinaryToNumber(binary, ini, end) {
		if ( binary.length > 8 ) {
			console.error("SIZE IS BIGGER THAN EXPECTED FOR CHAR TO BINARY.... =(", binary);
		}

		let resultingBinary = binary.substring(ini, end).padStart(8, '0'),
			number = parseInt(resultingBinary, 2);

		return number;
	}
}
