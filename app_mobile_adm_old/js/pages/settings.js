﻿"use strict";
se.plugin.uwp_page_settings = function(plugElem, plugOptions) {
	let settingsForm = plugElem.querySelector( 'form[se-elem="settings"]' ),
		settingsFormResponse = settingsForm.querySelector('output[se-elem="response"]'),
		//
		userLogged = plugElem.querySelector('div[se-elem="logged"]'),
		userNotLogged = plugElem.querySelector('div[se-elem="notlogged"]'),
		userLoginForm = plugElem.querySelector('form[se-elem="userLogin"]'),
		//
		notifOption = plugElem.querySelector('input[name="notifications"]');

	//
    function init() {
		console.log( "settings started", app.state.settings );
		// Set program variables
	    // settingsForm.se_formElVal( 'id', app.state.settings.id );
	    notifOption.value = app.state.settings.notifications;

	    settingsStatusUpdate();

	    // Callbacks
	    plugElem.se_on( 'click', 'button[se-act]', btnActions );
	    settingsForm.se_on( 'change', 'input', settingUpdate );
	    notifOption.se_on('change', se_app.notifications.toggle);
	    userLoginForm.se_on('submit', userLogin);
    }

    //
	function userLogin(e) {
    	e.preventDefault();
    	se.ajax.json(
    		'/ajax/login',
		    {user:userLoginForm.se_formElVal('user'), pass:userLoginForm.se_formElVal('pass')},
		    {
		    	response:userLoginForm.querySelector('output[se-elem="response"]'),
			    onSuccess:userLoginSuccess
		    }
		);
	}

	//
	function settingUpdate( e, cEl ) {
		e.preventDefault();
		if ( cEl.type !== 'checkbox' ) {
			app.state.settings[cEl.name] = cEl.value;
		} else if ( cEl.type === 'checkbox' ) {
			app.state.settings[cEl.name] = ( cEl.checked );
		}

		//
		se_app.stateUpdate();
	}

	//
	function settingsStatusUpdate() {
    	let bodyObj = $('body');
    	if ( app.state.logged ) {
		    userLogged.se_show();
		    userNotLogged.se_hide();
		    bodyObj.se_data('admin', 1);
	    } else {
		    userLogged.se_hide();
		    userNotLogged.se_show();
		    bodyObj.se_data('admin', 0);
	    }
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'appReset':
				if ( confirm("¿Desconectar la aplicación?\nTodos los datos serán eliminados.") ) {
					appReset();
				}
				break;
			//
			case 'userDisconnect':
				if ( confirm("¿Dejar de ser administrador?") ) {
					userDisconnect();
				}
				break;
			//
			default:
				console.log("Boton no programado", cBtn);
				break;
		}
	}

	//
	function userDisconnect() {
		//
		app.state.logged = false;
		app.state.mqtt.user = '';
		app.state.mqtt.pass = '';
		//
		se_app.stateUpdate();

		server.sw_postMessage({
			'op_type':['appChange', 'connect'],
			'op_data':{
				'url':window.location.hostname,
				'logged':app.state.logged,
				'user':app.state.mqtt.user,
				'pass':app.state.mqtt.pass,
			}
		});
		//
		settingsStatusUpdate();
	}

	//
	function appReset() {
		// Reset appstate
		app.state = {
			logged:false,
			loaded:false,
			settings:{
			},
			mqtt:{
				user:'',
				pass:''
			},
			status:{}
		};

		// Reiniciar DB
		app.idb.clearTables();

		// Cambiar estilo
		se_app.stateUpdate();
	}

    //
    init();
    return {};
};
