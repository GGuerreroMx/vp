﻿"use strict";
se.plugin.uwp_page_field_dynamic_server = function ( plugElem, plugOptions ) {
	let matchData, gameData, playerList = {},
		team_1_data, team_2_data,
		matchEvents = [],
		matchTimeTotal,
		//
		//
		timerObj = null, timerTotalObj = null,
		timeStart = 0,
		timerTargetTime, timerTargetTimeTotal,
		//
		fieldData, gameTypeData,
		fieldId,
		fieldStatus = {
			active:0,
			fase:0,
			pause:0
		},
		//
		audioContext = AudioContext && new AudioContext(),
		audioBeeps = [
			// Defualts
			[100, 50, 100, 50, 100],                        // 00 - Field Reset
			[300, 150, 300, 150, 300],                      // 01 - Game Start
			[500, 500, 500, 500, 500, 500, 500, 500, 500],  // 02 - Pause
			[500, 500, 500, 500, 3000],                     // 03 - Game Stop
			[2000],                                         // 04 - Insertion
			//
			[450, 100, 450],                                // 05 - ?
			[500, 500, 500],                                // 06 - ?
			[200, 50, 200],                                 // 07 - No
			[2000],                                         // 08 - Single Long
			[500],                                          // 09 - Single short
		],
		//
		// Speech recognition
		sr_ops = ['game', 'start', 'stop', 'pause', 'device', 'status', 'side', ''],
		sr_grammar = '#JSGF V1.0; grammar colors; public <color> = ' + sr_ops.join(' | ') + ' ;',
		//
		sr_recognition,
		sr_recognitionList;

	//
	const
		actionsForm = plugElem.querySelector('form[se-elem="actions"]'),
		actionsFormSubmitButton = actionsForm.querySelector('button[type="submit"]'),
		actionsSide = actionsForm.querySelector('div[se-elem="side"]'),
		//
		statusSection = plugElem.querySelector('div[se-elem="gameStatus"]'),
		statusSectionContent = statusSection.querySelector('div[se-elem="content"]'),
		//
		actionsDevices = actionsForm.querySelector('div[se-elem="devices"]'),
		actionsDevicesTemplate = actionsDevices.querySelector('template'),
		actionsDevicesContent = actionsDevices.querySelector('div[se-elem="content"]'),
		//
		actionChronicle = plugElem.querySelector('div[se-elem="chronicle"]'),
		actionChronicleTemplate = actionChronicle.querySelector('template'),
		actionChronicleContent = actionChronicle.querySelector('div[se-elem="content"]'),
		actionsList = plugElem.querySelector('div[se-elem="ev_list"]'),
		actionsListTemplate = actionsList.querySelector('template'),
		actionsListContent = actionsList.querySelector('tbody[se-elem="content"]'),
		//
		eventEditDiag = plugElem.querySelector('dialog[se-elem="eventEdit"]'),
		eventEditForm = eventEditDiag.querySelector('form'),
		//
		gameMap = plugElem.querySelector('div[se-elem="map"]'),
		gameMapTempalte = gameMap.querySelector('template'),
		gameMapContent = gameMap.querySelector('div[se-elem="content"]'),
		//
		game_status = plugElem.querySelector('div[se-elem="status"]'),
		game_status_section_c = game_status.querySelector('span[se-elem="section_current"]'),
		game_timer = plugElem.querySelector('div[se-elem="timer"]'),
		game_timer_progress = game_timer.querySelector('div.bar'),
		game_timer_text = game_timer.querySelector('div.timeText'),
		game_score = plugElem.querySelector('div[se-elem="scoreboard"]'),
		game_text = plugElem.querySelector('div[se-elem="text"]'),
		game_timertotal_text = game_status.querySelector('span[se-elem="timeText_total"]'),
		//
		game_action_buttons = plugElem.querySelector('div[se-elem="timerAct"]'),
		//
		voice_button = plugElem.querySelector('button[se-act="voice"]');


	//
	function init() {
		let params = se.url.str_parse(window.location.search);
		//
		fieldId = intVal(params.id);

		// Guardar información del campo actual en localStorage
		app.state.fieldId = fieldId;

		// Información del juego
		app.idb.query('server_fields', {
			get: fieldId,
			onSuccess:(fieldData) => {
				console.log("MAIN - PAGE:field_dynamic_server - Query:field data", fieldData);

				// Status inicial
				plugElem.se_data('type', fieldData.type);

				//
				se.uwp.sectionSetValues(plugElem, fieldData);

				//
				let queries = [
					{
						rName:'gametypes',
						table:'gametypes',
						params: {
							get:fieldData.gtId,
						}
					}
				];

				// Procesar tipo de juego y jugadores
				app.idb.queryBatch(queries, {
					onSuccess:(results) => {
						//
						console.log("MAIN - PAGE:field_dynamic_server - Query:gametypedata information", results);
						gameTypeData = results['gametypes'][0];

						// Llenar status
						for ( let i = 0; i < gameTypeData.struct.properties.gameStatus.length; i++ ) {
							let cStatus = gameTypeData.struct.properties.gameStatus[i].data;

							//
							if ( cStatus.statusValueType === 'bool' ) {
								//
								statusSectionContent.se_append(se.struct.stringPopulate(statusSection.querySelector('template[se-elem="boolean"]').se_html(), {
									index: i,
									title: cStatus.title
								}));
							} else {
								//
								statusSectionContent.se_append(se.struct.stringPopulate(statusSection.querySelector('template[se-elem="number"]').se_html(), {
									index: i,
									title: cStatus.title
								}));
							}
						}

						// Llenar dispositivos (mapa y lista)
						for ( let i = 0; i < gameTypeData.struct.properties.devices.length; i++ ) {
							let cDev = gameTypeData.struct.properties.devices[i].data;
							// Mapa
							gameMapContent.se_append(se.struct.stringPopulate(gameMapTempalte.se_html(), {
								id: i,
								title: cDev.title,
								type: cDev.devType,
								mapicon: cDev.mapicon,
								mapx: cDev.mapx,
								mapy: cDev.mapy,
								active: cDev.devActiveDef,
								side: cDev.devSideDef,
								status: cDev.devStatusDef,
							}));

							// Acciones
							// Saltar en lista la base
							if ( cDev.devType === 'base' ) { continue; }
							//
							actionsDevicesContent.se_append(se.struct.stringPopulate(actionsDevicesTemplate.se_html(), {
								id: i,
								title: cDev.title,
								type: cDev.devType,
								icon: cDev.mapicon,
								active: cDev.devActiveDef,
								side: cDev.devSideDef,
								status: cDev.devStatusDef,
							}));
						}

						//
						// ajuste variables
						matchTimeTotal = gameTypeData.struct.settings.timeGameInt;
						actionChronicle.querySelector('.end').se_text(secondsToTime(matchTimeTotal));

						// Status
						if ( fieldData.data ) {
							// Poner eventos
							matchEvents = fieldData.data.eventsUser;
							reDrawEvents();

							// Puntos
							game_score.querySelector('[se-score="1"]').se_text(fieldData.data.points.t['1'].t);
							game_score.querySelector('[se-score="2"]').se_text(fieldData.data.points.t['2'].t);


							for ( let cGameStatusIndex in fieldData.data.gameStatus ) {
								if ( !fieldData.data.gameStatus.hasOwnProperty(cGameStatusIndex) ) {
									continue;
								}
								//
								let cGameStatusObj = fieldData.data.gameStatus[cGameStatusIndex],
									cGameStatusInput = statusSection.querySelector('label[data-index="' + cGameStatusIndex + '"] input');

								//
								switch ( cGameStatusInput.type ) {
									case 'checkbox':
										cGameStatusInput.checked = (cGameStatusObj.value);
										break;
									case 'number':
										cGameStatusInput.value = cGameStatusObj.value;
										break;
									default:
										console.error("Valor incorrecto", cGameStatusInput);
										break;
								}
							}

							// Dispositivos
							for ( let cDeviceId in fieldData.data.devices ) {
								if ( !fieldData.data.devices.hasOwnProperty(cDeviceId) ) {
									continue;
								}
								let cDev = fieldData.data.devices[cDeviceId];
								//
								gameMapContent.querySelector('svg[data-oid="' + cDeviceId + '"]').se_data('active', cDev.active).se_data('side', cDev.side).se_data('status', cDev.status);
							}

							// Vista general
							updatePage(fieldData.data.status.active, fieldData.data.status.fase, fieldData.data.section, gameTypeData.struct.settings.sectionsTotal);
						}

					},
					onFail:() => {}
				});

			}
		});
		//

		// Speech recognition
		//
		if ( "SpeechRecognition" in window || "webkitSpeechRecognition" in window ) {
			var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition,
				SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList,
				SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;

			//
			sr_recognition = new SpeechRecognition();
			sr_recognitionList = new SpeechGrammarList();

			//
			sr_recognitionList.addFromString(sr_grammar, 1);
			sr_recognition.grammars = sr_recognitionList;
			//recognition.continuous = false;
			sr_recognition.lang = 'en-US';
			sr_recognition.interimResults = false;
			sr_recognition.maxAlternatives = 1;

			//
			sr_recognition.onresult = voiceCommandRead;
			//
			sr_recognition.onspeechend = voiceCommandOff;
			//
			sr_recognition.onnomatch = (event) => {
				console.log("MP - VOICE - NO MATCH", event);
			};
			//
			sr_recognition.onerror = (event) => {
				console.log("MP - VOICE - ERROR", event);
			};

			//
			voice_button.parentElement.se_show();
		}

		// Bindings
		plugElem.se_on('click', '[se-act]', btnActions);

		// Actions
		actionsDevicesContent.se_on('click', 'div.object', actionDeviceSelect);
		actionsSide.se_on('click', 'div.object', actionSideSelect);
		actionsForm.se_on('submit', actionFormSubmitAction);
		//
		eventEditForm.se_on('submit', actionUpdate);
		statusSection.se_on('change', 'input', gameStatusUpdate);
	}

	//<editor-fold desc="Beeper">

	//amp:0..100, freq in Hz, ms
	function beep(amp, freq, ms) {
		if ( !audioContext ) return;
		let osc = audioContext.createOscillator(),
			gain = audioContext.createGain();
		osc.connect(gain);
		osc.frequency.value = freq;
		gain.connect(audioContext.destination);
		gain.gain.value = amp/100;
		osc.start(audioContext.currentTime);
		osc.stop(audioContext.currentTime + ms / 1000);
	}

	//
	function doBeep(beepNum) {
		if ( !audioBeeps.hasOwnProperty(beepNum) ) {
			console.error("BEEP ERROR. Requested beep does not exists.", beepNum);
		}

		//
		let sound = true,
			tTime = 0;
		for ( let cTime of audioBeeps[beepNum] ) {
			//
			if ( sound ) {
				setTimeout(() => { beep(10, 900, cTime) }, tTime);
			}
			sound = !sound;
			tTime+= cTime;
		}
	}

	//</editor-fold>

	//<editor-fold desc="Voice recognition">

	//
	function voiceCommandRead(event) {
		// The SpeechRecognitionEvent results property returns a SpeechRecognitionResultList object
		// The SpeechRecognitionResultList object contains SpeechRecognitionResult objects.
		// It has a getter so it can be accessed like an array
		// The [last] returns the SpeechRecognitionResult at the last position.
		// Each SpeechRecognitionResult object contains SpeechRecognitionAlternative objects that contain individual results.
		// These also have getters so they can be accessed like arrays.
		// The [0] returns the SpeechRecognitionAlternative at position 0.
		// We then return the transcript property of the SpeechRecognitionAlternative object

		let result = event.results[event.results.length - 1][0],
			command = result.transcript.toLowerCase(),
			confidence = result.confidence,
			cmdList = command.split(' ');
		//
		console.log('MP - VOICE - Result. Received: "%s". Confidence: %s.', command, confidence);

		//
		switch ( cmdList[0] ) {
			//
			case 'game':
				//
				switch ( cmdList[1] ) {
					//
					case 'start':
						serverMessageSend('setup', 'start');
						break;
					//
					case 'stop':
						serverMessageSend('setup', 'stop');
						break;
					//
					case 'reset':
						serverMessageSend('setup', 'reset');
						break;
					//
					case 'next':
						serverMessageSend('setup', 'next');
						break;
					//
					default:
						console.error("comando no reconocido (game)", cmdList);
						break;
				}
				break;
			//
			case 'device':
				let valid = true;
				break;
			//
			case 'status':
				break;
			//
			default:
				console.error("Command not recognized", cmdList);
				break;
		}
		//
	}

	//
	function voiceCommandOff() {
		console.log('MP - VOICE - STOP');
		recognition.stop();
		voice_button.se_classSwitch('red', 'blue');
	}

	//
	function voiceCommandOn() {
		console.log('MP - VOICE - START');
		recognition.start();
		voice_button.se_classSwitch('blue', 'red');
	}

	//</editor-fold>

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		let id;
		// Read match status

		//
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'voice':
				voiceCommandOn();
				break;

			//
			case 'm_start':
				serverMessageSend('setup', 'start');
				break;
			//
			case 'm_stop':
				serverMessageSend('setup', 'stop');
				break;
			//
			case 'm_reset':
				serverMessageSend('setup', 'reset');
				break;
			//
			case 'm_next':
				serverMessageSend('setup', 'next');
				break;

			//
			case 'act_add_init':
				actionsForm.querySelectorAll('div[data-step]').se_attr('aria-hidden', 'true');
				actionsForm.querySelectorAll('div[data-step="0"]').se_attr('aria-hidden', 'false');
				window.scrollTo({
					'behavior': 'smooth',
					'left': 0,
					'top': actionsForm.offsetTop - 80
				});
				break;
			//
			case 'act_device_sel':
				actionsForm.querySelectorAll('div[data-step]').se_attr('aria-hidden', 'true');
				actionsForm.querySelectorAll('div[data-step="0"]').se_attr('aria-hidden', 'false');
				break;
			//
			case 'act_add_cancel':
				actionFormReset();
				break;

			//
			case 'actionEdit':
				id = cBtn.se_closest('tr').se_index();
				actionDel(id);
				break;

			//
			case 'actionDel':
				id = cBtn.se_closest('tr').se_index();
				actionDel(id);
				break;

			//
			default:
				console.log("Boton no programado", cBtn);
				break;
		}
	}

	//
	function gameStatusUpdate(e, cEl) {
		e.preventDefault();
		let value;

		//
		switch ( cEl.type ) {
			case 'checkbox':
				value = ( cEl.checked ) ? 1 : 0;
				break;
			case 'number':
				value = intVal(cEl.value);
				break;
			default:
				console.error("Game status update invalid...", cType);
				return;
				break;
		}

		//
		serverMessageSend('gameStatus', {
				index:intVal(cEl.se_data('index')),
				value:value
		});
	}

	//<editor-fold desc="Action Add Form">

	//
	function actionDeviceSelect(e, cEl) {
		let devId = cEl.se_data('id');
		actionsDevicesContent.querySelectorAll('div.object').se_attr("aria-selected", "false");
		cEl.se_attr("aria-selected", "true");
		//
		actionsForm.querySelectorAll('div[data-step]').se_attr('aria-hidden', 'true');
		actionsForm.querySelectorAll('div[data-step="1"]').se_attr('aria-hidden', 'false');
		//
		actionsForm.querySelector('div[data-step="1"] div.object.device')
			.se_data('id', cEl.se_data('id'))
			.se_data('type', cEl.se_data('type'))
			.se_data('side', cEl.se_data('side'))
			.se_data('active', cEl.se_data('active'))
			.se_data('status', cEl.se_data('status'))
			.se_html(cEl.se_html());
		actionsForm.querySelector('div[data-step="2"] div.object.device')
			.se_data('id', cEl.se_data('id'))
			.se_data('type', cEl.se_data('type'))
			.se_data('side', cEl.se_data('side'))
			.se_data('active', cEl.se_data('active'))
			.se_data('status', cEl.se_data('status'))
			.se_html(cEl.se_html());
		//
		actionsForm.se_formElVal('deviceId', devId);
	}

	//
	function actionSideSelect(e, cEl) {
		let side = cEl.se_data('side');
		actionsSide.querySelectorAll('div.object').se_attr("aria-selected", "false");
		cEl.se_attr("aria-selected", "true");
		//
		actionsForm.querySelectorAll('div[data-step]').se_attr('aria-hidden', 'true');
		actionsForm.querySelectorAll('div[data-step="2"]').se_attr('aria-hidden', 'false');
		//
		actionsForm.querySelector('div[data-step="2"] div.object.action').se_data('side', cEl.se_data('side')).se_text(cEl.se_text());
		//
		actionsForm.se_formElVal('side', side);
	}

	//
	function actionFormReset() {
		actionsForm.querySelectorAll('div[data-step]').se_attr('aria-hidden', 'true');
		actionsDevicesContent.querySelectorAll('div.object').se_attr("aria-selected", "false");
		actionsSide.querySelectorAll('div.object').se_attr("aria-selected", "false");
		actionsForm.reset();
	}

	//
	function actionFormSubmitAction(e) {
		// Bloquear si no esta corriendo juego
		if ( !fieldStatus.active || fieldStatus.fase === 0 ) {
			console.log("No debería poderse hacer submit ja");
		}

		e.preventDefault();
		let actionData = se.form.serializeToObj(actionsForm, false, {'int':['deviceId', 'side']});

		// Proper conversion
		actionData.status = 1;
		actionData.userId = 0;

		//
		serverMessageSend('device', actionData);

		// Reset form
		actionFormReset();
	}

	//</editor-fold>

	//
	function actionUpdate(e) {
		e.preventDefault();
		let eData = se.form.serializeToObj(editEventForm, false, {'int':['id', 'devUser']});

		// Send information to server (update in server), return success
		//
		serverMessageSend('event_update', {
			index:eData.id,
			devUser:eData.devUser
		});

		/*
		console.log("PRE\n\n\n", matchEvents[eData.id], eData);
		//
		matchEvents[eData.id].devUser = parseInt(eData.devUser);
		console.log("post", matchEvents[eData.id]);
		*/
		//
		eventEditDiag.close();
	}

	//
	function actionDel(actId) {

	}

	//
	function updatePage(sActive, sFase, sSectionC, sPaused) {
		//
		plugElem.se_data('active', sActive);
		plugElem.se_data('fase', sFase);
		plugElem.se_data('sectionC', sSectionC);
		plugElem.se_data('paused', sPaused);

		//
		fieldStatus.active = parseInt(sActive);
		fieldStatus.fase = parseInt(sFase);
		fieldStatus.pause = parseInt(sFase);

		// if reset
		if ( !fieldStatus.active && !fieldStatus.fase ) {
			actionChronicleContent.se_empty();
			actionsListContent.se_empty();
			//
			game_score.querySelector('[se-score="1"]').se_text(0);
			game_score.querySelector('[se-score="2"]').se_text(0);
			//
			matchEvents = [];

			for ( let i = 0; i < gameTypeData.struct.properties.devices.length; i++ ) {
				let cDev = gameTypeData.struct.properties.devices[i].data;
				gameMapContent.querySelector('svg[data-oid="'+i+'"]').se_data('active', cDev.devActiveDef).se_data('side', cDev.devSideDef).se_data('status', cDev.devStatusDef);
			}
		}

		//
		game_status_section_c.se_text(sSectionC);

		// Action buttons
		game_action_buttons.querySelectorAll('button').forEach((element) => {
			element.disabled = true;
		});

		actionsFormSubmitButton.disabled = true;

		// Operations
		if ( fieldStatus.active ) {
			game_action_buttons.querySelector('button[se-act="m_stop"]').disabled = false;
			if ( fieldStatus.fase === 1 ) {

			} else {
				actionsFormSubmitButton.disabled = false;
			}
		} else {
			timer_stop();
			if ( fieldStatus.fase === 0 ) {
				timerText_set(0);
				game_text.se_text('');
				game_action_buttons.querySelector('button[se-act="m_start"]').disabled = false;
			} else {
				game_action_buttons.querySelector('button[se-act="m_reset"]').disabled = false;
				game_action_buttons.querySelector('button[se-act="m_next"]').disabled = false;
			}
		}

	}

	//
	function serverMessageRecieve(rFieldId, msg) {
		console.log("MAIN - SERVER LIVE - SW- MSG - IN:", msg);
		// Analize current field
		if ( fieldId !== rFieldId ) {
			console.error("Ignoring: Not this field. %s - %s", fieldId, rFieldId, typeof fieldId, typeof rFieldId);
			return;
		}

		//
		switch ( msg.type ) {
			//
			case 'status':
				console.log("PAGE UPDATE - Status", msg.data);
				// Status
				updatePage(msg.data.status.active, msg.data.status.fase, msg.data.status.section, msg.data.status.pause);

				// Points
				game_score.querySelector('[se-score="1"]').se_text(msg.data.points[0].t);
				game_score.querySelector('[se-score="2"]').se_text(msg.data.points[1].t);

				// Devices
				for ( let cDevId in msg.data.devices ) {
					if ( !msg.data.devices.hasOwnProperty(cDevId) ) { return; }
					let cDevProp = msg.data.devices[cDevId];

					gameMapContent.querySelector('svg[data-oid="'+cDevId+'"]').se_data('active', cDevProp.active).se_data('side', cDevProp.side).se_data('status', cDevProp.status);
				}

				// gameStatus
				for ( let cGameStatusId in msg.data.gameStatus ) {
					if ( !msg.data.gameStatus.hasOwnProperty(cGameStatusId) ) { break; }
					let cGameStatus = msg.data.gameStatus[cGameStatusId];

					//
					let cGameStatusInput = statusSection.querySelector('label[data-index="'+cGameStatusId+'"] input');

					//
					switch ( cGameStatusInput.type ) {
						case 'checkbox':
							cGameStatusInput.checked = ( msg.data.value );
							break;
						case 'number':
							cGameStatusInput.value = msg.data.value;
							break;
						default:
							console.error("Valor incorrecto", cGameStatusInput);
							break;
					}
				}

				// Actions
				for ( let cAction of msg.data.actions ) {
					switch ( cAction.type ) {
						// Ignorar sonidos
						case 'buzzer':
							if ( cAction.buzzSide === 0 ) {
								doBeep(parseInt(cAction.buzzType));
							}
							break;
						//
						case 'sound':
							break;
						//
						case 'timer':
							timer_set(parseInt(cAction.timeInt));
							break;
						//
						case 'note':
							game_text.se_text(cAction.note);
							break;
						//
						default:
							console.error("Field actions, action not valid", cAction);
							break;
					}
				}
				break;

			//
			case 'event':
				console.log("PAGE UPDATE - Event", msg);
				// Agregar a la lista
				matchEvents.push(msg.data);
				// Recrear listas
				reDrawEvents();
				break;
			//
			default:
				console.log("error, not defined operation in main server app");
				break;
		}
	}

	//
	function reDrawEvents() {
		let chronTemplate = actionChronicleTemplate.se_html(),
			listTemplate = actionsListTemplate.se_html();
		//
		actionChronicleContent.se_empty();
		actionsListContent.se_empty();

		//
		for ( let cEventId in matchEvents ) {
			//
			if ( !matchEvents.hasOwnProperty(cEventId) ) { continue; }
			//
			let cEvent = matchEvents[cEventId],
				cSide = 0,
				dataContent;

			// No va a mostrar eventos pre inicio de juego
			if ( cEvent.time < 0 ) { continue; }

			console.log("cEvent", cEvent);

			//
			switch ( cEvent.type ) {
				//
				case 'automatic':
					let cEventTimedInfo = gameTypeData.struct.properties[cEvent.subtype][cEvent.id].data;
					console.log(cEventTimedInfo);
					dataContent = `${cEventTimedInfo.title}`;
					break;
				//
				case 'device':
					let gtDevice = gameTypeData.struct.properties.devices[cEvent.devId].data;

					//
					dataContent = `
<svg class="device icon inline mr"><use xlink:href="#fa-user" /></svg> ${cEvent.devUser}<br />
<svg class="device icon inline mr" data-side="${cEvent.devSide}"><use xlink:href="#${gtDevice.mapicon}" /></svg> ${gtDevice.title}
`;
					//
					cSide = cEvent.devSide;
					break;
			}


			//
			actionChronicleContent.se_append(se.struct.stringPopulate(chronTemplate, {
				id:cEventId,
				type:cEvent.type,
				side:cSide,
				relTime:cEvent.time / matchTimeTotal * 100
			}));
			//
			actionsListContent.se_append(se.struct.stringPopulate(listTemplate, {
				id:cEventId,
				type:cEvent.type,
				timeText:secondsToTime(cEvent.time),
				dataContent:dataContent
			}));
		}
	}

	//<editor-fold desc="Timer stuff">

	//
	function timer_set(timeInt) {
		let timeCur = Math.round((new Date()).getTime() / 1000);
		timerTargetTime = timeCur + timeInt;
		timerTargetTimeTotal = timeInt;
		if ( timeStart === 0 ) {
			timeStart = timeCur;
			timerTotalObj = setInterval(timerTotal_advance, 1000);
		}
		// Reset section timer (since it may not be exact seconds from other.
		if ( timerObj !== null ) {
			clearInterval(timerObj);
			timerObj = null;
		}
		timerObj = setInterval(timer_advance, 1000);
		//
		timerText_set(timeInt);
		timerTotalText_set(- gameTypeData.struct.settings.timeBreakInt);
	}

	//
	function timer_advance() {
		let timeCur = Math.round((new Date()).getTime() / 1000),
			timeDiff = timerTargetTime - timeCur;
		// Ejecutar tiempo
		timerText_set(timeDiff);
	}

	//
	function timerTotal_advance() {
		let timeCur = Math.round((new Date()).getTime() / 1000),
			timeTotal = timeCur - timeStart - gameTypeData.struct.settings.timeBreakInt;
		// Ejecutar tiempo
		timerTotalText_set(timeTotal);
	}

	// Actualizar tiempos
	function timerText_set(mTime) {
		let perAdvance = 0,
			cClass;
		//
		if ( mTime > 0 ) {
			// game_timer_progress.se_classDel('end').se_classAdd('countdown');
			perAdvance = 100 - ( mTime / timerTargetTimeTotal * 100);
			cClass = 'coundown'; // could be 'normal'
		} else {
			cClass = 'end';
		}
		//
		game_timer_text.se_text(secondsToTime(mTime));
		game_timer_progress.se_css('width', perAdvance + '%').se_attr('class', cClass);
	}

	//
	function timerTotalText_set(mTime) {
		// console.log("time total update...?", mTime);
		game_timertotal_text.se_text(secondsToTime(mTime));
	}

	//
	function timer_stop() {
		timeStart = 0;
		clearInterval(timerObj);
		timerObj = null;
		clearInterval(timerTotalObj);
		timerTotalObj = null;
	}

	//
	function secondsToTime(seconds) {
		// we will use this function to convert seconds in normal time format
		let secs = Math.abs(seconds),
			hr = Math.floor(secs / 3600),
			min = Math.floor((secs - (hr * 3600)) / 60),
			sec = Math.floor(secs - (hr * 3600) - (min * 60)),
			time = '';
		if ( hr !== 0 ) {
			if ( hr < 10 ) {
				time += '0';
			}
			time += hr + ':';
		}
		if ( min !== 0 ) {
			if ( min < 10 ) {
				time += '0';
			}
			time += min + ':';
		} else {
			time += '00:';
		}
		if ( sec < 10 ) {
			time += '0';
		}
		time += sec;
		if ( seconds < 0 ) {
			time = '-' + time;
		}
		return time;
	}

	//</editor-fold>

	//
	function serverMessageSend(op, data = {}) {
		console.log("MAIN - SERVER LIVE - SWSEND MESSAGE: ", fieldId, op, data);
		// Mensaje al worker
		app.server.sw_postMessage({
			'op_type':['field', 'command'],
			'op_data':{
				id:fieldId,
				op:op,
				data:data
			}
		});
	}

	//
	init();
	return {
		serverMessageRecieve:serverMessageRecieve
	};
};
