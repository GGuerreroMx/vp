﻿"use strict";
se.plugin.uwp_page_field_basic = function ( plugElem, plugOptions ) {
	let gameScore = plugElem.querySelector('div[se-elem="gameScore"]'),
		scoreForm = gameScore.querySelector('form[se-elem="scoreForm"]'),
		scoreFormTemplate = gameScore.querySelector('template'),
		scoreFormBody = gameScore.querySelector('tbody'),
		scoreFormFoot = gameScore.querySelector('tfoot'),
		//
		infoSection = plugElem.querySelector('div[se-elem="info"]'),
		scoreBoard = infoSection.querySelector('div[se-elem="scoreboard"]'),
		nextGameButton = plugElem.querySelector('button[se-elem="nextButton"]'),
		//
		/*
		game_status = plugElem.querySelector('div[se-elem="status"]'),
		game_status_section_c = game_status.querySelector('span[se-elem="section_current"]'),
		game_timer = plugElem.querySelector('div[se-elem="timer"]'),
		game_timer_progress = game_timer.querySelector('div.bar'),
		game_timer_text = game_timer.querySelector('div.timeText'),
		game_score = plugElem.querySelector('div[se-elem="scoreboard"]'),
		game_text = plugElem.querySelector('div[se-elem="text"]'),
		game_timertotal_text = game_status.querySelector('span[se-elem="timeText_total"]'),
		*/
		//
		timerObj = null, timerTotalObj = null,
		timeStart = 0,
		timerTargetTime, timerTargetTimeTotal,
		//
		fieldData, gameTypeData,
		matchIndex, fieldId;

	//
	function init() {
		let params = se.url.str_parse(window.location.search);
		//
		fieldId = intVal(params.id);

		//
		field_current();

		// Bindings
		plugElem.se_on('click', '[se-act]', btnActions);

		// Actions
		scoreForm.se_on('change', 'input', pointFormReCount);
		scoreForm.se_on('submit', pointsFormSubmit);
	}


	//<editor-fold desc="Points">

	//
	function pointFormReCount() {
		console.log("recount");
		//
		let points = {
			'1': 0,
			'2': 0
		};

		//
		scoreFormBody.querySelectorAll('input').forEach((cInput) => {
			let cPointVal = intVal(cInput.se_data('pointval')),
				cSide = cInput.se_data('side'),
				cPointAmmount = intVal(cInput.value);
			//
			points[cSide] += cPointAmmount * cPointVal;
		});

		console.log("point update", points, scoreFormFoot.querySelector('td[data-side="1"]'), points['1'], scoreFormFoot.querySelector('td[data-side="2"]'));
		//
		scoreFormFoot.querySelector('td[data-side="1"]').se_text(points['1']);
		scoreFormFoot.querySelector('td[data-side="2"]').se_text(points['2']);
	}

	//
	function pointsFormSubmit(e) {
		e.preventDefault();

		let cFormJSON = se.form.serializeObject(scoreForm);

		se.ajax.json('/ajax/field/' + fieldId + '/scores', {mIndex: matchIndex, points: JSON.stringify(cFormJSON)}, {
			response: scoreForm.querySelector('output[se-elem="response"]'),
			onSuccess: () => {
				nextGameButton.se_show();
			}
		});
	}

	//</editor-fold>

	//<editor-fold desc="Admin">

	//
	function field_current() {
		//
		se.ajax.json('/ajax/field/'+fieldId + '/get', {}, {
			onSuccess:field_proc,
			onFail:(msg) => {
				alert(msg.s.e + "\n" + msg.s.ex);
			}
		});
	}

	//
	function field_next() {
		//
		se.ajax.json('/ajax/field/'+fieldId + '/advance', {mIndex:matchIndex}, {
			onSuccess:field_proc,
			onFail:(msg) => {
				alert(msg.s.e + "\n" + msg.s.ex);
			}
		});
	}

	//
	function field_proc(data) {
		fieldData = data.d;
		console.log("FIELD RECIEVED DATA: ", fieldData);

		//
		matchIndex = fieldData.cMatchIndex;

		//
		scoreBoard.querySelector('div[data-side="1"] [se-elem="title"]').se_text(fieldData.t1_name);
		scoreBoard.querySelector('div[data-side="2"] [se-elem="title"]').se_text(fieldData.t2_name);

		//
		scoreForm.querySelector('th[data-side="1"]').se_text(fieldData.t1_name);
		scoreForm.querySelector('th[data-side="2"]').se_text(fieldData.t2_name);

		//
		infoSection.querySelector('td[se-elem="id"]').se_text(fieldData.cMatchOrder);
		infoSection.querySelector('td[se-elem="gtTitle"]').se_text(fieldData.gtTitle);
		infoSection.querySelector('td[se-elem="gTitle"]').se_text(fieldData.gTitle);
		infoSection.querySelector('td[se-elem="cMatchIndex"]').se_text(fieldData.cMatchIndex);
		infoSection.querySelector('td[se-elem="fTitle"]').se_text(fieldData.fTitle);

		//
		nextGameButton.se_hide();

		//
		app.idb.query('gametypes', {
			get:fieldData.gtId,
			onSuccess:(data) => {

				// Manual score board update
				scoreFormBody.se_empty();
				let template = scoreFormTemplate.se_html();
				//
				console.log("query data", data);

				for ( let cPointId in data.struct.points ) {
					if ( !data.struct.points.hasOwnProperty(cPointId) ) { continue; }

					let cPointData = data.struct.points[cPointId];

					// Only team points
					if ( cPointData.target !== 1 ) { continue; }

					console.log("c points", cPointId, cPointData);
					scoreFormBody.se_append(se.struct.stringPopulate(template, {
						'pointId':cPointId,
						'pointTitle':cPointData['title'],
						'pointMax':cPointData['pointsMax'],
						'pointVal':cPointData['points'],
					}));
				}
			}
		});
	}

	//</editor-fold>

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		let id;
		// Read match status

		//
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'm_start':
				break;
			//
			case 'm_stop':
				break;
			//
			case 'm_reset':
				break;
			//
			case 'm_next':
				if ( confirm('¿Avanzar a la siguiente partida? (no reversible)') ) {
					field_next();
				}
				break;

			//
			case 'n_up':
				manualFormInputMod(cBtn, +1);
				break;
			//
			case 'n_down':
				manualFormInputMod(cBtn, -1);
				break;

			//
			default:
				console.log("Boton no programado", cBtn);
				break;
		}
	}

	//
	function manualFormInputMod(cBtn, act) {
		let cInput = cBtn.parentElement.parentElement.querySelector('input'),
			cValue = intVal(cInput.value),
			nValue = cValue + act,
			minValue = intVal(cInput.se_attr('min')),
			maxValue = intVal(cInput.se_attr('max'))
			;

		//
		if ( nValue >= minValue && nValue <= maxValue ) {
			cInput.value = nValue;
			pointFormReCount();
		}
	}

	//
	init();
	return {};
};
//
