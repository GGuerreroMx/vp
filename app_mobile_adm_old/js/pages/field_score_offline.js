﻿"use strict";
se.plugin.uwp_page_field_score_mobile = function ( plugElem, plugOptions ) {
	let //
		/*
		game_status = plugElem.querySelector('div[se-elem="status"]'),
		game_status_section_c = game_status.querySelector('span[se-elem="section_current"]'),
		game_timer = plugElem.querySelector('div[se-elem="timer"]'),
		game_timer_progress = game_timer.querySelector('div.bar'),
		game_timer_text = game_timer.querySelector('div.timeText'),
		game_score = plugElem.querySelector('div[se-elem="scoreboard"]'),
		game_text = plugElem.querySelector('div[se-elem="text"]'),
		game_timertotal_text = game_status.querySelector('span[se-elem="timeText_total"]'),
		*/
		//
		timerObj = null, timerTotalObj = null,
		timeStart = 0,
		timerTargetTime, timerTargetTimeTotal,
		//
		serverFieldData,
		matchData, matchIndex,
		gameTypeData;

	//
	const
		//
		infoSection = plugElem.querySelector('div[se-elem="info"]'),
		matchOps = plugElem.querySelector('div[se-elem="matchOps"]'),
		matchesListObj = plugElem.querySelector('div[se-elem="matchesList"]'),
		//
		nextGameButton = plugElem.querySelector('button[se-elem="nextButton"]'),
		// Info section
		scoreBoard = infoSection.querySelector('div[se-elem="scoreboard"]'),
		// Game section
		pickSideDetails = matchOps.querySelector('details[se-elem="sides"]'),
		timersDetails = matchOps.querySelector('details[se-elem="timers"]'),
		scoresDetails = matchOps.querySelector('details[se-elem="scores"]'),
		//
		pickSideForm = pickSideDetails.querySelector('form[se-elem="pickSide"]'),
		scoreForm = scoresDetails.querySelector('form[se-elem="scoreForm"]'),
		scoreFormTemplate = scoreForm.querySelector('template'),
		scoreFormBody = scoreForm.querySelector('tbody'),
		// Matches section
		matchTemplate = matchesListObj.querySelector('template[se-elem="template"]'),
		matchListCont = matchesListObj.querySelector('div[se-elem="content"]')
		;
	//

	//
	function init() {
		let params = se.url.str_parse(window.location.search);

		// Get field related matches
		app.idb.query('server_fields', {
			get:app.state.status.serverFieldId,
			onSuccess:(data) => {
				console.log("CURRENT FIELD: ", data);

				serverFieldData = data;

				// Get all matches
				app.idb.query('game_matches', {
					index: 'gameFilter',
					keyRange: IDBKeyRange.only([app.state.status.gameStageId, app.state.status.eventFieldId]),
					sort:{
						name:'mOrder',
						direction:'ASC'
					},
					onSuccess:(data) => {
						// Add to list
						matchListCont.se_append( se.struct.stringPopulateMany( matchTemplate.se_html(), data) );

						// Loop matches searching for current
						for ( let cMatch of data ) {
							if ( cMatch.index === serverFieldData.cMatchIndex ) {
								matchIndex = cMatch.index;
								matchData = cMatch;

								// Get gametypedata
								app.idb.query('gametypes', {
									get:cMatch.gtId,
									onSuccess:(gameType) => {
										gameTypeData = gameType;
										field_proc();
									}
								});
								break;
							}
						}
					}
				});
			}
		});

		// Bindings
		plugElem.se_on('click', '[se-act]', btnActions);

		// Actions
		pickSideForm.se_on('change', 'input', pickSideAuto);
		pickSideForm.se_on('submit', pickSideSubmit);
		scoreForm.se_on('submit', pointsFormSubmit);
	}

	//<editor-fold desc="Admin">

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		let id;
		// Read match status

		//
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'm_start':
				break;
			//
			case 'm_stop':
				break;
			//
			case 'm_reset':
				break;
			//
			case 'm_next':
				if ( confirm('¿Avanzar a la siguiente partida? (no reversible)') ) {
					field_next();
				}
				break;
			//
			case 'n_up':
				inputMod(cBtn, +1);
				break;
			//
			case 'n_down':
				inputMod(cBtn, -1);
				break;
			//
			case 'uploadScores':
				uploadScores();
				break;
			//
			default:
				console.log("Boton no programado", cBtn);
				break;
		}
	}

	//
	function pickSideAuto(e, cEl) {
		e.preventDefault();
		let cName = cEl.name,
			cVal = intVal(cEl.value);
		console.log("side select operation", cName, cVal);
		//
		switch ( cName ) {
			//
			case 't1_side':
				pickSideForm.se_formElVal('t2_side', ( cVal === 2 ) ? 1 : 2);
				break;

			//
			case 't2_side':
				pickSideForm.se_formElVal('t1_side', ( cVal === 2 ) ? 1 : 2);
				break;

			//
			default:
				console.error("what, obviously not defined");
				break;
		}
	}

	//
	function pickSideSubmit(e) {
		e.preventDefault();
		let eData = se.form.serializeToObj(pickSideForm, false, {'int':['t1_side', 't2_side']});

		//
		console.log("COLOR SUBMITED", eData);

		// All content is updated after saving information
		updateMatch({
			't1_side':eData.t1_side,
			't2_side':eData.t2_side
		});
	}

	//
	function pointsFormSubmit(e) {
		e.preventDefault();

		console.log("\nPOINTS SUBMIT\n");

		//
		if ( matchData.t1_side === 0 || matchData.t2_side === 0 ) {
			console.warn("sides not set...? ignoring");
			return;
		}

		//
		let scoreValues = se.form.serializeObject(scoreForm),
			saveData = {},
			cPointsList = {
				// Teams
				t:{
					'1':{
						'p':{},
						't':0
					},
					'2':{
						'p':{},
						't':0
					}
				},
				// Persons, not available
				p:{}
			};

		console.log("SUBMITED:", scoreValues);

		//
		let totalPoints = {
			'1':0,
			'2':0
		};

		// Game points loop, apply max limits and sum
		for ( let cPointsId in gameTypeData.struct.points ) {
			if ( !gameTypeData.struct.points.hasOwnProperty(cPointsId) ) { continue; }

			//
			let cPointData = gameTypeData.struct.points[cPointsId],
				tCount, tTotal;

			// Skip user points
			if ( cPointData.target !== 1 ) { continue; }

			// Do per team
			for ( let i = 1; i < 3; i++ ) {
				let teamIndex = i.toString();

				//
				tCount = scoreValues['point'][teamIndex][cPointsId];
				tCount = ( tCount <= cPointData.pointsMax ) ? tCount : cPointData.pointsMax;
				//
				tTotal = tCount * cPointData.points;
				//
				cPointsList.t[teamIndex].p[cPointsId] = {
					'c':tCount,
					't':tTotal
				};

				//
				totalPoints[teamIndex]+= tTotal;
			}
		}

		// Is there any max limit for points? (as set in game)
		for ( let i = 1; i < 3; i++ ) {
			let teamIndex = i.toString();
			totalPoints[teamIndex] = ( totalPoints[teamIndex] > gameTypeData.struct.settings.maxPointsGame ) ? gameTypeData.struct.settings.maxPointsGame : totalPoints[teamIndex];
			//
			cPointsList.t[teamIndex].t = totalPoints[teamIndex];
		}

		// Set "points side"
		saveData.t1_points = totalPoints[matchData.t1_side.toString()];
		saveData.t2_points = totalPoints[matchData.t2_side.toString()];

		// Set victory side (or draw)
		if ( saveData.t1_points === saveData.t2_points ) {
			saveData.t1_status = 2;
			saveData.t2_status = 2;
		} else {
			saveData.t1_status = ( saveData.t1_points > saveData.t2_points ) ? 3 : 1;
			saveData.t2_status = ( saveData.t1_points < saveData.t2_points ) ? 3 : 1;
		}

		// Add points to save and set match as ended
		saveData.mPoints = cPointsList;
		saveData.mStatus = 4;

		// Match in list update
		let cMatchObj = matchListCont.querySelector('.matchObj[data-index="' + matchIndex + '"]');
		//
		cMatchObj.se_data('status', 4);
		//
		se.element.childrenUpdate(
			cMatchObj,
			{
				'.team.home .score':[
					['text', saveData.t1_points ]
				],
				'.team.home svg[data-mode="win"]':[
					['data', 'status', saveData.t1_status ]
				],
				'.team.visitor .score':[
					['text', saveData.t2_points ]
				],
				'.team.visitor svg[data-mode="win"]':[
					['data', 'status', saveData.t2_status ]
				],
			}
		);

		// Save content
		updateMatch(saveData, false);

		//
		nextGameButton.se_show();
	}

	//
	function uploadScores() {
		// Get all matches
		app.idb.query('game_matches', {
			index:'sync',
			keyRange:IDBKeyRange.only(0),
			onSuccess:(matches) => {
				let cMatchesUpd = [],
					cMatchesPost = [];

				//
				for ( let tIndex in matches ) {
					if ( !matches.hasOwnProperty(tIndex) ) { continue; }

					let cMatch = matches[tIndex];

					//
					cMatchesUpd.push({index:cMatch.index, sync:1});

					//
					cMatchesPost.push({
						index:cMatch.index,
						t1_side:cMatch.t1_side,
						t1_points:cMatch.t1_points,
						t1_status:cMatch.t1_status,
						t2_side:cMatch.t2_side,
						t2_points:cMatch.t2_points,
						t2_status:cMatch.t2_status,
						mStatus:cMatch.mStatus,
						mPoints:cMatch.mPoints,
						mEvents:cMatch.mEvents,
					});
				}

				//
				se.ajax.json('/admin/ajax/score_mobile/matchesUpload',
					{
						data:JSON.stringify(cMatchesPost)
					},
					{
						onSuccess:() => {
							// Actualizar DB de nuevo de los equipos
							app.idb.updateMany('game_matches', 'index', cMatchesUpd, {
								onSuccess: () => {
									console.log("Matches sincronizados");
								},
								onError: (err) => {
									console.log("Matches no sincronizados", err);
								},
							});

						}
					}
				);
			}
		});
	}

	//
	function field_next() {
		// Unsynced matches
		app.idb.query('game_matches', {
			index: 'gameFilter',
			keyRange: IDBKeyRange.only([app.state.status.gameStageId, app.state.status.eventFieldId]),
			sort:{
				name:'mOrder',
				direction:'ASC'
			},
			onSuccess:(data) => {
				// Loop matches searching for next available
				for ( let cMatch of data ) {
					if ( cMatch.mStatus !== 1 ) { continue; }

					//
					matchIndex = cMatch.index;
					matchData = cMatch;

					// Update field
					updateServerField({
						cMatchIndex:matchData.index,
						cMatchOrder:matchData.mOrder,
						gtId:matchData.gtId,
						t1_name:matchData.t1_name,
						t2_name:matchData.t2_name,
					});

					// Check if diferent gamemode, update if needed (probably never applies...?)
					if ( gameTypeData.id !== matchData.gtId ) {
						// Get gametypedata
						app.idb.query('gametypes', {
							get: cMatch.gtId,
							onSuccess: (gameType) => {
								gameTypeData = gameType;
								field_proc();
							}
						});
					} else {
						field_proc();
					}
					break;
				}
			}
		});
	}

	//
	function field_proc() {
		console.debug("FIELD PROC DATA: ", serverFieldData, matchData, gameTypeData);

		// Reset
		scoreFormBody.se_empty();
		nextGameButton.se_hide();

		//
		se.element.childrenUpdate(scoreBoard, {
			'.team.home [se-elem="title"]':[
				['text', matchData.t1_name]
			],
			'.team.visitor [se-elem="title"]':[
				['text', matchData.t2_name]
			],
			'.team.home':[
				['data', 'side', 0]
			],
			'.team.visitor':[
				['data', 'side', 0]
			],
			'.team.home [se-elem="score"]':[
				['text', '0']
			],
			'.team.visitor [se-elem="score"]':[
				['text', '0']
			],
		});

		//
		se.element.childrenUpdate(scoreForm, {
			'th[data-side="1"]':[
				['text', 'no definido']
			],
			'th[data-side="2"]':[
				['text', 'no definido']
			],
			'tfoot td[data-side="1"]':[
				['text', '0']
			],
			'tfoot td[data-side="2"]':[
				['text', '0']
			],
		});

		//
		se.element.childrenUpdate(infoSection, {
			'td[se-elem="gtTitle"]':[
				['text', serverFieldData.gtTitle]
			],
			'td[se-elem="gsTitle"]':[
				['text', serverFieldData.gsTitle]
			],
			'td[se-elem="fTitle"]':[
				['text', serverFieldData.fTitle]
			],
			'td[se-elem="id"]':[
				['text', matchData.index]
			],
			'td[se-elem="cMatchIndex"]':[
				['text', matchData.mOrder]
			],
		});

		//
		se.element.childrenUpdate(pickSideForm, {
			'td[se-elem="t1_name"]':[
				['text', matchData.t1_name]
			],
			'td[se-elem="t2_name"]':[
				['text', matchData.t2_name]
			],
		});

		// Form selector
		pickSideForm.reset();

		// Lados definidos?
		if ( matchData.t1_side === 0 ) {
			pickSideDetails.open = true;
			scoresDetails.open = false;
			return;
		}

		//
		pickSideDetails.open = false;
		scoresDetails.open = true;

		//
		se.element.childrenUpdate(scoreBoard, {
			'.team.home [se-elem="title"]':[
				['text', matchData.t1_name]
			],
			'.team.visitor [se-elem="title"]':[
				['text', matchData.t2_name]
			],
			'.team.home':[
				['data', 'side', matchData.t1_side]
			],
			'.team.visitor':[
				['data', 'side', matchData.t2_side]
			],
		});

		// Score form
		se.element.childrenUpdate(scoreForm, {
			'.elementSide[data-side="1"]':[
				['text', ( matchData.t1_side === 1 ) ? matchData.t1_name : matchData.t2_name ]
			],
			'.elementSide[data-side="2"]':[
				['text', ( matchData.t1_side === 2 ) ? matchData.t1_name : matchData.t2_name ]
			],
		});

		// Match in list
		se.element.childrenUpdate(
			matchListCont.querySelector('.matchObj[data-index="' + matchIndex + '"]'),
			{
				'.team.home':[
					['data', 'side', matchData.t1_side ]
				],
				'.team.visitor':[
					['data', 'side', matchData.t2_side ]
				],
			}
		);

		//
		let template = scoreFormTemplate.se_html();

		// Print points
		for ( let cPointId in gameTypeData.struct.points ) {
			let cPointData = gameTypeData.struct.points[cPointId];

			// Skip no team points
			if ( cPointData.target !== 1 ) { continue; }

			//
			scoreFormBody.se_append(se.struct.stringPopulate(template, {
				'pointId':cPointId,
				'pointTitle':cPointData['title'],
				'pointValue':cPointData['points'],
				'pointMax':cPointData['pointsMax'],
			}));
		}

		//
		if ( matchData.mStatus === 4 ) {
			nextGameButton.se_show();
		}
	}

	//
	function updateServerField(params) {
		//
		// serverFieldData = se.object.merge(serverFieldData, params);

		//
		app.idb.update('server_fields', app.state.status.serverFieldId,
			params,
			{
				onSuccess:(data) => {}
			}
		);
	}

	//
	function updateMatch(params, updateAll = true) {
		//
		matchData = se.object.merge(matchData, { sync:0 }, params);

		console.log("new match information... (local)", matchData);

		//
		app.idb.update('game_matches', matchIndex,
			se.object.merge({ sync:0 }, params),
			{
				onSuccess:(data) => {
					console.log("success data=?", data);
					if ( updateAll ) {
						field_proc();
					}
				}
			}
		);
	}

	//</editor-fold>

	//
	function inputMod(cBtn, act) {
		let cInput = cBtn.se_closest('td').querySelector('input:not(disabled)'),
			cInputDummy = cBtn.se_closest('td').querySelector('input[disabled]'),
			cSide = intVal(cInputDummy.se_data('side')),
			cPointsValue = intVal(cInput.se_data('value')),
			cValue = intVal(cInput.value),
			nValue = cValue + act,
			minValue = intVal(cInput.se_attr('min')),
			maxValue = intVal(cInput.se_attr('max'))
			;

		//
		if ( nValue >= minValue && nValue <= maxValue ) {
			cInput.value = nValue;
			cInputDummy.value = nValue * cPointsValue;

			// Get side total
			let cTotal = 0;
			scoreForm.querySelectorAll('input[data-side="' + cSide + '"]').forEach((cEl) => {
				cTotal+= intVal(cEl.value);
			});
			scoreForm.querySelector('tfoot td[data-side="' + cSide + '"]').se_text(cTotal);
		}
	}

	//
	init();
	return {};
};
//
