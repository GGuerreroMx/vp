﻿"use strict";
//
se.plugin.uwp_page_device_bluetooth = function (plugElem, plugOptions) {
	const terminalObj = plugElem.querySelector('div[se-elem="terminal"]'),
		terminalTemplate = terminalObj.querySelector('template'),
		terminalContent = terminalObj.querySelector('div[se-elem="content"]'),
		terminalForm = terminalObj.querySelector('form');

	//
	let emulationTimer,
		emulationCounter = 0,
		emulationMessages = [
			new Uint8Array([102, 48, 115, 129, 0, 0, 0, 0, 0, 9, 6, 133, 134, 132, 132, 132, 0, 0, 0, 98, 1, 115, 1, 110, 77, 65, 84, 67, 72, 32, 83, 84, 65, 82, 84, 32, 116, 6, 0]),
			new Uint8Array([102, 48, 115, 129, 0, 0, 0, 0, 0, 9, 6, 133, 134, 132, 132, 132, 0, 0, 0, 98, 9, 115, 5, 110, 83, 99, 111, 117, 116, 32, 32, 32, 32, 32, 32, 32]),
			new Uint8Array([102, 48, 115, 130, 0, 0, 0, 0, 0, 9, 6, 133, 134, 132, 132, 132, 0, 0, 0, 115, 4, 98, 4, 110, 80, 114, 105, 109, 101, 114, 97, 32, 32, 32, 32, 32, 116, 30, 0]),
			new Uint8Array([102, 48, 115, 130, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 132, 132, 0, 0, 0]),
			new Uint8Array([102, 48, 115, 130, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 132, 132, 0, 0, 0, 115, 6]),
			new Uint8Array([102, 48, 115, 130, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 133, 132, 0, 0, 0]),
			new Uint8Array([102, 48, 115, 130, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 133, 132, 0, 0, 0, 115, 4, 110, 83, 101, 103, 117, 110, 100, 97, 32, 32, 32, 32, 32, 98, 4, 116, 30, 0]),
			new Uint8Array([102, 48, 115, 4, 0, 4, 0, 0, 0, 9, 6, 133, 134, 133, 133, 132, 0, 0, 0, 98, 3, 115, 3, 110, 77, 65, 84, 67, 72, 32, 69, 78, 68, 32, 32, 32]),
			new Uint8Array([102, 48, 115, 0, 0, 0, 0, 0, 0, 9, 6, 133, 134, 132, 132, 132, 0, 0, 0, 98, 0, 110, 70, 73, 69, 76, 68, 32, 82, 69, 65, 68, 89, 32]),
		];

	//
    function init() {
	    //
	    plugElem.se_on( 'click', 'button[se-act]', btnActions );
		terminalForm.se_on('submit', terminalFormSubmit);
	}

	//
	function destroy() {
	}

	//
	function terminalFormSubmit(e) {
		e.preventDefault();
		//
		let cValue = terminalForm.se_formElVal('content');
		addToLog('out', cValue);
		se_app.bluetooth.sendMessageString(cValue);
		terminalForm.reset();
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		//
		switch ( cBtn.se_attr( 'se-act' ) ) {

			//
			case 'connect':
				se_app.bluetooth.connect();
				break;
			//
			case 'sendConfig':
				sendConfig();
				break;
			//
			case 'emulationStart':
				emulationStart();
				break;
			//
			case 'emulationStop':
				emulationStop();
				break;
			//
			default:
				console.error("Boton no programado: ", cAction, cBtn);
				break;
		}
	}

	//
	function addToLog(type, content) {
		terminalContent.se_append(se.struct.stringPopulate(terminalTemplate.se_html(), {
			type:type, content:content, time:new Date().toLocaleTimeString()
		}));
	}

	//
	function sendConfig() {

		//
		// Get gametypedata
		app.idb.query('gametypes', {
			get:app.state.status.gameTypeId,
			onSuccess:(gameType) => {

				console.log("this is sent", gameType);

				//
				let fieldMode = 0x00;
				switch ( app.state.status.serverMode )
				{
					//
					case 'dynamic_mobile':
						fieldMode = 0x01;
						break;
					//
					case 'dynamic_server':
						fieldMode = 0x02;
						break;
				}

				// Properties to send
				const value = new Uint8Array([
					0x64,                                           // Message start ( d: should be 0x64 )
					app.state.status.serverFieldId,                 // Field id
					fieldMode,                                      // Field mode
					gameType.struct.properties.devices.length,      // Field total devices
					gameType.struct.properties.gameStatus.length,   // Field total status
					0x00,                                           // Field device number
					0x00,                                           // Field device type
					0x00,                                           // Field device type
				]);

				console.log("this is sent", value);

				//
				se_app.bluetooth.sendMessageDirect(value);
			}
		});
	}

	//
	function emulationStart() {
		// First send
		emulationSend();
		emulationTimer = setInterval(emulationSend, 5000);
	}

	//
	function emulationStop() {
		clearTimeout(emulationTimer);
	}

	//
	function emulationSend() {
		console.log("this is loopy loop", emulationCounter, emulationMessages.length);

		//
		if ( emulationCounter >= emulationMessages.length ) {
			emulationCounter = 0;
		}

		//
		se_app.bluetooth.sendMessageDirect(emulationMessages[emulationCounter]);

		//
		emulationCounter++;
	}

	//
    init();
    return {
		logAdd:addToLog,
		_destroy:destroy
	};
};
//
