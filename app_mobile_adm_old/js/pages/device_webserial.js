﻿"use strict";
//
se.plugin.uwp_page_device_webserial = function (plugElem, plugOptions) {
	const terminalObj = plugElem.querySelector('div[se-elem="terminal"]'),
		terminalTemplate = terminalObj.querySelector('template'),
		terminalContent = terminalObj.querySelector('div[se-elem="content"]'),
		terminalForm = terminalObj.querySelector('form');

	//
	function init() {
		//
		plugElem.se_on( 'click', 'button[se-act]', btnActions );
		terminalForm.se_on('submit', terminalFormSubmit);

		se_app.webserial.messageRecieveBind(msgCallBack);


	}

	//
	function destroy() {
		se_app.webserial.messageRecieveUnbind();
	}

	//
	function terminalFormSubmit(e) {
		e.preventDefault();
		//
		let cValue = terminalForm.se_formElVal('content');
		addToLog('upload', cValue);
		terminalForm.reset();
		terminalContent.scrollTop = terminalContent.scrollHeight;
		se_app.webserial.messageSendText(cValue);
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		//
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'connect':
				se_app.webserial.connect();
				break;
			//
			default:
				console.error("Boton no programado: ", cBtn);
				break;
		}
	}

	//
	function addToLog(type, content) {
		terminalContent.se_append(se.struct.stringPopulate(terminalTemplate.se_html(), {
			type:type, content:content, time:new Date().toLocaleTimeString()
		}));
	}

	//
	function msgCallBack(message) {
		console.log("function callback biatch", message);
	}

	//
	init();
	return {
		_destroy:destroy
	};
};
//
