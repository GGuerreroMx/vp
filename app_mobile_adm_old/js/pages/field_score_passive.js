﻿"use strict";

//
se.plugin.uwp_page_field_score_server = function( plugElem, plugOptions ) {
	//
	let timerObj = null, timerTotalObj = null,
		timeStart = 0,
		timerTargetTime, timerTargetTimeTotal,
		//
		fieldData, gameTypeData,
		matchIndex, fieldId,
		ajaxUrl = '/admin/ajax/score_server/' + app.state.status.fieldId
	;
	//
	const gameScore = plugElem.querySelector('div[se-elem="gameScore"]'),
		scoreForm = gameScore.querySelector('form[se-elem="scoreForm"]'),
		scoreFormTemplate = gameScore.querySelector('template'),
		scoreFormBody = gameScore.querySelector('tbody'),
		//
		infoSection = plugElem.querySelector('div[se-elem="info"]'),
		scoreBoard = infoSection.querySelector('div[se-elem="scoreboard"]'),
		nextGameButton = plugElem.querySelector('button[se-elem="nextButton"]');
		//
		/*
		game_status = plugElem.querySelector('div[se-elem="status"]'),
		game_status_section_c = game_status.querySelector('span[se-elem="section_current"]'),
		game_timer = plugElem.querySelector('div[se-elem="timer"]'),
		game_timer_progress = game_timer.querySelector('div.bar'),
		game_timer_text = game_timer.querySelector('div.timeText'),
		game_score = plugElem.querySelector('div[se-elem="scoreboard"]'),
		game_text = plugElem.querySelector('div[se-elem="text"]'),
		game_timertotal_text = game_status.querySelector('span[se-elem="timeText_total"]'),
		*/

	//
	function init() {
		let params = se.url.str_parse(window.location.search);
		//
		fieldId = intVal(params.id);

		//
		field_current();

		// Bindings
		plugElem.se_on('click', '[se-act]', btnActions);

		// Actions
		scoreForm.se_on('submit', pointsFormSubmit);
	}

	//<editor-fold desc="Admin">

	//
	function pointsFormSubmit(e) {
		e.preventDefault();

		let cFormJSON = se.form.serializeObject(scoreForm);

		//
		se.ajax.json(ajaxUrl + '/scores', {mIndex:matchIndex, points:JSON.stringify(cFormJSON)}, {
			response:scoreForm.querySelector('output[se-elem="response"]'),
			onSuccess:() => {
				nextGameButton.se_show();
			}
		});
	}

	//
	function field_current() {
		//
		se.ajax.json(ajaxUrl + '/get', {}, {
			onSuccess:field_proc,
			onFail:(msg) => {
				alert(msg.s.e + "\n" + msg.s.ex);
			}
		});
	}

	//
	function field_next() {
		//
		se.ajax.json(ajaxUrl + '/advance', {mIndex:matchIndex}, {
			onSuccess:field_proc,
			onFail:(msg) => {
				alert(msg.s.e + "\n" + msg.s.ex);
			}
		});
	}

	//
	function field_proc(data) {
		fieldData = data.d;
		console.log("FIELD RECIEVED DATA: ", fieldData);

		//
		matchIndex = fieldData.cMatchIndex;

		//
		scoreBoard.querySelector('div[data-side="1"] [se-elem="title"]').se_text(fieldData.t1_name);
		scoreBoard.querySelector('div[data-side="2"] [se-elem="title"]').se_text(fieldData.t2_name);

		//
		scoreForm.querySelector('th[data-side="1"]').se_text(fieldData.t1_name);
		scoreForm.querySelector('th[data-side="2"]').se_text(fieldData.t2_name);

		//
		infoSection.querySelector('td[se-elem="id"]').se_text(fieldData.cMatchOrder);
		infoSection.querySelector('td[se-elem="gtTitle"]').se_text(fieldData.gtTitle);
		infoSection.querySelector('td[se-elem="gTitle"]').se_text(fieldData.gTitle);
		infoSection.querySelector('td[se-elem="cMatchIndex"]').se_text(fieldData.cMatchIndex);
		infoSection.querySelector('td[se-elem="fTitle"]').se_text(fieldData.fTitle);

		//
		nextGameButton.se_hide();

		//
		app.idb.query('gametypes', {
			get:fieldData.gtId,
			onSuccess:(data) => {
				scoreFormBody.se_empty();
				let template = scoreFormTemplate.se_html();
				//
				console.log("query data", data);

				for ( let cPointId in data.struct.points ) {
					let cPointData = data.struct.points[cPointId];

					// Skip no team points
					if ( cPointData.target !== 1 ) { continue; }

					//
					scoreFormBody.se_append(se.struct.stringPopulate(template, {
						'pointId':cPointId,
						'pointTitle':cPointData['title'],
						'pointValue':cPointData['points'],
						'pointMax':cPointData['pointsMax'],
					}));
				}
			}
		});
	}

	//</editor-fold>

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		let id;
		// Read match status

		//
		switch ( cBtn.se_attr( 'se-act' ) ) {
			//
			case 'm_start':
				break;
			//
			case 'm_stop':
				break;
			//
			case 'm_reset':
				break;
			//
			case 'm_next':
				if ( confirm('¿Avanzar a la siguiente partida? (no reversible)') ) {
					field_next();
				}
				break;

			//
			case 'n_up':
				inputMod(cBtn, +1);
				break;
			//
			case 'n_down':
				inputMod(cBtn, -1);
				break;

			//
			default:
				console.log("Boton no programado", cBtn);
				break;
		}
	}

	//
	function inputMod(cBtn, act) {
		let cInput = cBtn.se_closest('td').querySelector('input:not(disabled)'),
			cInputDummy = cBtn.se_closest('td').querySelector('input[disabled]'),
			cSide = intVal(cInputDummy.se_data('side')),
			cPointsValue = intVal(cInput.se_data('value')),
			cValue = intVal(cInput.value),
			nValue = cValue + act,
			minValue = intVal(cInput.se_attr('min')),
			maxValue = intVal(cInput.se_attr('max'))
		;

		//
		if ( nValue >= minValue && nValue <= maxValue ) {
			cInput.value = nValue;
			cInputDummy.value = nValue * cPointsValue;

			// Get side total
			let cTotal = 0;
			scoreForm.querySelectorAll('input[data-side="' + cSide + '"]').forEach((cEl) => {
				cTotal+= intVal(cEl.value);
			});
			scoreForm.querySelector('tfoot td[data-side="' + cSide + '"]').se_text(cTotal);
		}
	}

	//
	init();
	return {};
};
//
