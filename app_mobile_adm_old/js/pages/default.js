﻿"use strict";
//
se.plugin.uwp_page_default = function (plugElem, plugOptions) {
	//
	const deviceInfo = plugElem.querySelector('div[se-elem="deviceInfo"]'),
		// Tournament
		serverFields = plugElem.querySelector('div[se-elem="game_elements"]'),
		serverFieldsTemplate = serverFields.querySelector('template'),
		serverFieldsContent = serverFields.querySelector('div[se-elem="content"]');

	//
    function init() {
		console.log( "Default page", app.state );

	    //
	    if ( app.state.loaded ) {
			appLoad(app.state.status.fieldId);
	    } else {
	    	downloadInformation();
	    }

	    //
	    plugElem.se_on( 'click', 'button[se-act]', btnActions );
	}


	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		//
		const cAction = cBtn.se_attr( 'se-act' );
		switch ( cAction ) {
			//
			case 'fullscreen_on':
				if ( document.documentElement.requestFullscreen ) {
					document.documentElement.requestFullscreen();
				} else if ( document.documentElement.mozRequestFullScreen ) {
					document.documentElement.mozRequestFullScreen();
				} else if ( document.documentElement.webkitRequestFullscreen ) {
					document.documentElement.webkitRequestFullscreen();
				} else if ( document.documentElement.msRequestFullscreen ) {
					document.documentElement.msRequestFullscreen();
				}
				break;
			//
			case 'fullscreen_off':
				if ( document.exitFullscreen ) {
					document.exitFullscreen();
				} else if ( document.mozCancelFullScreen ) {
					document.mozCancelFullScreen();
				} else if ( document.webkitExitFullscreen ) {
					document.webkitExitFullscreen();
				}
				break;

			//
			case 'startTournament':
				startTournament();
				break;

			//
			case 'uploadInfo':
				uploadInformation();
				break;

			//
			case 'downloadInfo':
				downloadInformation();
				break;

			//
			case 'unload':
				if ( confirm("¿Desconectar la aplicación?\nTodos los datos serán eliminados.") ) {
					unloadApp();
				}
				break;

			//
			default:
				console.log("Boton no programado: ", cAction, cBtn);
				break;
		}
	}
	
	//
	function updateDeviceInfo(fieldInformation) {
		console.warn("field information", fieldInformation);

		// Update page
		se.element.childrenUpdate(deviceInfo,  {
			'td[se-elem="id"]':[
				['text', fieldInformation.id]
			],
			'td[se-elem="gsTitle"]':[
				['text', fieldInformation.gsTitle]
			],
			'td[se-elem="fTitle"]':[
				['text', fieldInformation.fTitle]
			],
			'td[se-elem="gtTitle"]':[
				['text', fieldInformation.gtTitle]
			],
			'td[se-elem="type"]':[
				['text', fieldInformation.type]
			],
			'td[se-elem="serverMode"]':[
				['text', fieldInformation.serverMode]
			],
		});

		// Set conditions
		switch ( app.state.status.type ) {
			case 'tournament':
				plugElem.querySelectorAll('[data-type="isTournament"]').se_attr('aria-hidden', 'false');
				plugElem.querySelectorAll('[data-type="isGame"]').se_attr('aria-hidden', 'true');
				break;
			case 'game':
				plugElem.querySelectorAll('[data-type="isTournament"]').se_attr('aria-hidden', 'true');
				plugElem.querySelectorAll('[data-type="isGame"]').se_attr('aria-hidden', 'false');
				break;
		}
	}

	//
	function updateMatches(matches) {
		serverFieldsContent.se_empty().se_append( se.struct.stringPopulateMany( serverFieldsTemplate.se_html(), matches) );
	}

	//
	function appLoad(fieldId) {
		//
		app.idb.query('server_fields', {
			get:fieldId,
			onSuccess:(fieldData) => {
				// Update information
				updateDeviceInfo(fieldData[0]);

				// Load matches
				app.idb.query('game_matches', {
					index:'gsId',
					keyRange:IDBKeyRange.only(app.state.status.gameStageId),
					onSuccess:(matches) => {
						updateMatches(matches);
					}
				});
			}
		});
	}

	//
	function uploadInformation() {

	}

	//
	function downloadInformation() {
		let url =  '/admin/ajax/deviceSetup',
			postData = {},
			params = {
				headers: {
					'X-Requested-With':'VPAPP'
				},
				onSuccess: ( msg ) => {
					//
					app.state.loaded = true;
					//
					app.state.status.serverFieldId = msg.d['server_fields'][0].id;
					app.state.status.gameStageId = msg.d['server_fields'][0].gsId;
					app.state.status.eventFieldId = msg.d['server_fields'][0].fId;
					app.state.status.gameTypeId = msg.d['server_fields'][0].gtId;
					app.state.status.type = msg.d['server_fields'][0].type;
					app.state.status.serverMode = msg.d['server_fields'][0].serverMode;
					app.state.status.cMatchIndex = msg.d['server_fields'][0].cMatchIndex;

					// Update status
					se_app.stateUpdate();

					// Save all information

					// Clear
					app.idb.clearTables();

					// Copy downloaded information
					app.idb.syncData(msg.d);

					// Display new information

					// General information
					updateDeviceInfo(msg.d['server_fields'][0]);

					// Print games
					updateMatches(msg.d['game_matches']);

					// Initial connect
					se_app.server_connect();
				},
				onFail: () => {
					alert("No fue posible conectarse al servidor.");
				},
				onComplete: () => {
					// cBtn.disabled = false;
				},
				onRequest: () => {
					// cBtn.disabled = true;
				},
				onProgress: (x) => {
					console.log("progress", x);
				}
			};

		// request
		se.ajax.json(url, postData, params);
	}

	//
	function startTournament() {
		let urlTarget = '?page=';

		//
		switch ( app.state.status.type ) {
			//
			case 'tournament':
				//
				switch ( app.state.status.serverMode ) {
					//
					case 'score_server':
						urlTarget+= 'field_score_server';
						break;
					//
					case 'score_mobile':
						urlTarget+= 'field_score_mobile';
						break;
					//
					case 'dynamic_mobile':
						urlTarget+= 'field_dynamic_mobile';
						break;
					//
					case 'dynamic_server':
						urlTarget+= 'field_dynamic_server';
						break;
					//
					default:
						console.error("estado del servidor no definido", app.state.status);
						return;
						break;
				}
				break;
			//
			case 'free_mode':
				break;
			//
			default:
				console.error("estado del servidor no definido", app.state.status);
				return;
				break;
		}

		//
		se.uwp.page.urlParse(urlTarget, true);
	}


	//
    init();
    return {};
};
//
