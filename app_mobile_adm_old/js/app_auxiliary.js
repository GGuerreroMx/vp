﻿"use strict";

//
se_app.event_user_updateId = function(userIndex, newEventId, actions) {
	// Revisar que no exista un usuario en el evento con este número
	app.idb.query('event_players', {
		index: 'eventId',
		keyRange: IDBKeyRange.only( newEventId ),
		onSuccess:(result) => {
			if ( typeof actions.onIDUsed === 'function' ) {
				actions.onIDUsed(result);
			}
			console.log("Número no disponible", result);
		},
		onEmpty:() => {
			app.idb.update('event_players', userIndex, { sync:1, eventId:newEventId }, {
				onSuccess:(data) => {
					console.log("Event player updated");
					//
					app.idb.query('game_teams', {
						index: 'players_index',
						keyRange: IDBKeyRange.only( userIndex ),
						onSuccess:(teams) => {
							for ( let tIndex in teams ) {
								if ( !teams.hasOwnProperty(tIndex) ) { continue; }
								//
								for ( let pIndex in teams[tIndex].players_list ) {
									if ( !teams[tIndex].players_list.hasOwnProperty(pIndex) ) { continue; }
									if ( teams[tIndex].players_list[pIndex].index === userIndex ) {
										teams[tIndex].players_list[pIndex].eventId = newEventId;
										break;
									}
								}
								teams[tIndex]['sync'] = 1;
								if ( typeof actions.onSuccess === 'function' ) {
									actions.onSuccess();
								}
							}

							// Actualizar DB de nuevo de los equipos
							app.idb.updateMany('game_teams', 'id', teams, {
								onSuccess: () => {
									console.log("Equipos actualizados");
								},
								onError: (err) => {
									console.log("Equipos no actualizados", err);
								},
							});
						}
					});
				}
			});
		}
	});
	//
};


// Plugin menu
se.plugin.navMenu = function (element, options) {
	//
	let navigation = element.querySelector('.navigation'),
		tMenu = navigation.querySelector('div.mainMenu');
	// Funciones
	function init() {
		// Binds
		element.se_on('click', '.navigation .background', closeMenus);
		element.se_on('click', 'a[se-m-nav]', navigate);
		element.se_on('click', 'button[data-menu]', menuOp);
		element.se_on('transitionend', 'div.mainMenu', checkOp);
	}
	function navigate(e, cEl) {
		e.preventDefault();
		// Cambió de página
		se.uwp.page.urlParse(cEl.getAttribute('se-m-nav'), true);
		//
		closeMenus();
	}
	function closeMenus() {
		element.querySelectorAll('.navigation > .sel').se_classDel('sel');
	}
	function menuOp(e) {
		e.stopPropagation();
		if ( tMenu ) {
			if ( tMenu.se_classHas('sel') ) {
				//
				tMenu.se_classDel('sel');
			} else {
				element.se_classAdd('full');
				// Cerrar el resto
				element.querySelectorAll('.navigation > .sel').se_classDel('sel');
				//
				tMenu.se_classAdd('sel');
			}
		} else {
			console.log("No esta definido el menu.", tMenu);
		}
	}
	function checkOp(e, cObj) {
		let openMenus = element.querySelectorAll('.navigation > .sel');
		if ( openMenus.length === 0 ) {
			element.se_classDel('full');
		}
	}
	//
	init();

	//
	return {
		closeMenus:closeMenus
	};
};
//</editor-fold>
