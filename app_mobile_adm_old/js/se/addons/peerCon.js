'use_strict';

//
se.addon.peerConnection = function(cOptions) {
	let pDefaults = {
			'configuration':null,
			'autoStart':false, 
			'onAddStream':null,
			'onTrackAdd':null,
			'onIceCandidate':null,
			'onOpen':null,
			'onError':null,
			'onClose':null
		},
		pSettings = se.object.merge(pDefaults, cOptions),
		//
		peerCon = null,
		debugging = false,
		//
		streamLocal,
		streamExternal,
		//
		status = {
			connected:false
		};

	//
	function init() {
		if ( pSettings.autoStart ) {
			pc_start();
		}
	}

	//
	function pc_start() {
		if ( !pSettings.configuration ) {
			pc_log('log', 'Error: configuration not set', pSettings);
			return;
		}

		// RTC CONECTION
		peerCon = new RTCPeerConnection(pSettings.configuration);

		// Callbacks
		// peerCon.onaddstream = pc_addStream;
		peerCon.ontrack = pc_onTrackAdd;
		//
		peerCon.onicecandidate = pc_onicecandidate;
		peerCon.onopen = pc_open;
		peerCon.onerror = pc_error;
	}

	//<editor-fold desc="Conection">

	//
	function pc_offerCreate(onSuccess, onError) {
		// create an offer
		peerCon.createOffer(
			(offer) => {
				pc_log('log', 'Offer create success:', offer);
				peerCon.setLocalDescription(offer);
				//
				if ( typeof onSuccess === "function" ) {
					onSuccess(offer);
				}
			},
			(error) => {
				pc_log('log', 'Offer create error:', error);
				//
				if ( typeof onError === "function" ) {
					onError(error);
				}
			},
			{
				offerToReceiveAudio:1,
				offerToReceiveVideo:1
			}
		);
	}

	//
	function pc_offerHandle(offer, onSuccess, onError) {
		// Incomming call, accept and set remote
		peerCon.setRemoteDescription(new RTCSessionDescription(offer));

		//create an answer to an offer
		peerCon.createAnswer(
			(answer) => {
				pc_log('log', 'Offer handle success:', answer);
				peerCon.setLocalDescription(answer);
				//
				if ( typeof onSuccess === "function" ) {
					onSuccess(answer);
				}
			},
			(error) => {
				pc_log('error', 'Offer handle error:', error);
				//
				if ( typeof onError === "function" ) {
					onError(error);
				}
			}
		);
	}

	//
	function pc_answerHandle(answer) {
		peerCon.setRemoteDescription(new RTCSessionDescription(answer));
	}

	//
	function pc_candidateHandle(candidate) {
		peerCon.addIceCandidate(new RTCIceCandidate(candidate));
	}

	//
	function pc_open(e) {
		pc_log('log', 'Open:', e);
		status.connected = true;
		if ( typeof pSettings.onOpen === "function" ) {
			pSettings.onOpen(e);
		}
	}

	//
	function pc_close() {
		pc_log('log', 'Close');
		//
		if ( peerCon ) {
			peerCon.close();
			peerCon = null;
		}
		//
		if ( typeof pSettings.onClose === "function" ) {
			pSettings.onClose();
		}
	}

	//
	function pc_error(e) {
		pc_log('error', 'ERROR', e);
		if ( typeof pSettings.onError === "function" ) {
			pSettings.onError(e);
		}
	}

	//
	function pc_onicecandidate(e) {
		pc_log('log', 'Ice Candidate', e);
		if ( typeof pSettings.onIceCandidate === "function" ) {
			pSettings.onIceCandidate(e);
		}
	}

	//</editor-fold>

	//
	function pc_addStream(e) {
		pc_log('log', 'onAddStream', e);
		if ( typeof pSettings.onAddStream === "function" ) {
			pSettings.onAddStream(e);
		}
	}

	//
	function pc_streamAdd(trackInfo, streamInfo = null) {
		peerCon.addStream(trackInfo, streamInfo);
	}

	//
	function pc_streamDel() {

	}


	//
	function pc_onTrackAdd(e) {
		pc_log('log', 'onTrackAdd', e);
		if ( typeof pSettings.onTrackAdd === "function" ) {
			pSettings.onTrackAdd(e);
		}
	}

	//
	function pc_trackAdd(track) {
		peerCon.addTrack(track);
	}

	//
	function pc_trackDel() {

	}

	//
	function pc_createStream(constrains, onSuccess, onError = null) {
		//
		navigator.mediaDevices.getUserMedia(constrains)
		.then(
			(mediaStream) => {
				streamLocal = mediaStream;
				if ( typeof onSuccess === "function" ) {ºº
					onSuccess(streamLocal);
				}
			}
		)
		.catch(
			(e) => {
				pc_log('error', 'createStreamError', e);
				if ( typeof onError === "function" ) {
					onError(e);
				}
			}
		);
	}

	//
	function pc_log(type, title) {
		if ( !debugging ) {
			return;
		}

		let cTitle = 'PC - ' + title,
			argList = [];

		for ( let i = 2; i < arguments.length; i++ ) {
			argList.push(arguments[i]);
		}

		//
		switch (type) {
			//
			case 'log':
				console.log(cTitle, argList);
				break;
			//
			case 'error':
				console.error(cTitle, argList);
				break;
			//
			default:
				console.error('WS - INCORRECT DATA')
				break;
		}
	}

	//
	init();
	return {
		'start':pc_start,
		'close':pc_close,
		'offerCreate':pc_offerCreate,
		'offerHandle':pc_offerHandle,
		'answerHandle':pc_answerHandle,
		'candidateHandle':pc_candidateHandle,
		'createStream':pc_createStream,
		'streamAdd':pc_streamAdd,
		'streamDel':pc_streamDel,
		'trackAdd':pc_trackAdd,
		'trackDel':pc_trackDel,
	};
}
//
