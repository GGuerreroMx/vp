"use strict";

// jQuery selector aprox
window.$ = function (selector) {
	let many = true,
		selection;
	// Determinar tipo
	if ( typeof selector === 'string' ) {
		if ( selector.indexOf('#') === 0 ) {
			selector = selector.substr(1, selector.length);
			let index = selector.indexOf(' ');
			if ( index === -1 ) {
				selection = document.getElementById(selector);
				many = false;
			} else {
				let id = selector.substr(0, index),
					query = selector.substr(index + 1, selector.length);
				selection = document.getElementById(id);
				if ( selection !== null) {
					selection = selection.querySelectorAll(query);
				}
			}
		} else {
			selection = document.querySelectorAll(selector);
		}
		//
		if ( many && ( !selection || selection.length === 0 )) {
			console.trace('$ - Empty: ', selector);
			selection = document.createDocumentFragment().querySelectorAll('div');
		} else if ( many && selection.length === 1 ) {
			selection = selection[0];
		}
		return selection;
	} else if ( typeof selector === 'undefined' ) {
		console.log('$ - No selector');
		return document.createDocumentFragment().querySelectorAll('div');
	} else {
		console.log('$ - Not recognized', selector);
		return document.createDocumentFragment().querySelectorAll('div');
	}
};

//<editor-fold desc="DOM PROTOTYPE">

//<editor-fold desc="ELEMENT">
FormData.prototype.se_jsonAppend = function () {
	let el;
	for ( let i = 0, cArg; cArg = arguments[i]; ++i ) {
		for ( el in cArg ) {
			this.append(el, cArg[el]);
		}
	}
};

//
Node.prototype.se_formToObj = function() {
	let l = this.elements.length,
		i, cEl, data = {};

	//
	for ( i = 0; i < l; i++ ) {
		cEl = this.elements[i];
		if ( cEl.name ) {
			if ( cEl.type !== 'checkbox' ) {
				data[cEl.name] = cEl.value;
			} else if ( cEl.type === 'checkbox' ) {
				data[cEl.name] = cEl.checked ? cEl.value : false;
			}
		}
	}

	//
	return data;
};
//
Node.prototype.se_formEl = function(elName) {
	return this.elements.namedItem(elName);
};
/**
 * @param {string} elName Form element to search
 * @param {string|number} cVal optional assign value
 */
Node.prototype.se_formElVal = function(elName, cVal) {
	let cEl = this.elements.namedItem(elName);
	//
	if ( !cEl ) {
		console.trace("Form El Val, undefined element:", elName);
		return;
	}

	//
	if ( typeof cVal !== 'undefined' ) {
		cEl.value = cVal;
		//
		if ( cEl.length > 1 ) {
			for ( let i = 0; i < cEl.length; i++ ) {
				let curEl = cEl[i];
				if ( curEl.value === cVal ) {
					curEl.checked = true;
					break;
				}
			}
		} else if ( cEl.type.toLowerCase() === 'checkbox' ) {
			cEl.checked = ( cVal );
		}
		return this;
	} else {
		if ( cEl.type && cEl.type.toLowerCase() === 'checkbox' ) {
			return cEl.checked;
		}
		else {
			return this.elements.namedItem(elName).value;
		}
	}
};
Node.prototype.se_remove = function() {
	this.parentElement.removeChild(this);
};
Node.prototype.se_index = function() {
	return Array.prototype.indexOf.call(this.parentNode.children, this);
};
Node.prototype.se_empty = function() {
	this.innerHTML = '';
	return this;
};
Node.prototype.se_html = function(html) {
	if ( typeof html !== 'undefined' ) {
		this.innerHTML = html;
		return this;
	} else {
		return this.innerHTML;
	}
};
Node.prototype.se_text = function(text) {
	if ( typeof text !== 'undefined' ) {
		this.innerHTML = se.element.htmlentities(text);
		return this;
	} else {
		return se.element.htmlentities(this.innerHTML);
	}
};
//
Node.prototype.se_data = function (var1, var2) {
	// Check type
	if ( typeof var1 === 'string') {
		var1 = 'data-'+var1;
		if ( typeof var2 !== 'undefined' ) {
			if ( var2 !== null ) {
				this.setAttribute(var1, var2);
			} else {
				this.removeAttribute(var1);
			}
			return this;
		} else {
			return this.getAttribute(var1);
		}
	} else if ( Array.isArray(var1) )  {
		for ( let cRow of var1 ) {
			cRow[0] = 'data-'+cRow[0];
			this.setAttribute(cRow[0], cRow[1]);
		}
	} else {
		console.error("se_data can't be applyed. Recieved: ", var1, var2);
	}
};
Node.prototype.se_replaceWith = function(nEl) {
	this.insertAdjacentHTML('afterEnd', nEl);
	let newElem = this.nextSibling,
		cParent = this.parentElement;
	cParent.replaceChild(newElem, this);
	return newElem;
};
Node.prototype.se_wrap = function(elem) {
	this.insertAdjacentHTML('afterEnd', elem);
	let newParent = this.nextSibling;
	newParent.appendChild(this);
};
Node.prototype.se_closest = function(selector, limit) {
	let el = this,
		docEl = document.documentElement,
		matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector;
	//
	limit = ( typeof limit !== 'undefinded' ) ? limit : document.getElementsByTagName('body')[0];

	//
	do {
		if ( matches.call(el, selector) ) { return el; }
	} while ( (el = el.parentNode) && el.tagName !== 'HTML' && el !== limit );

	//
	return null;
};
Node.prototype.se_prev = function(sel) {
	let el = this,
		docEl = document.documentElement,
		matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector;
	if ( typeof sel === 'string' ) {
		while ( (el = el.previousSibling) ) {
			if ( matches.call(el, sel) ) { return el; }
		}
		return null;
	} else {
		return this.previousSibling;
	}
};
Node.prototype.se_prevUntil = function(uEl, sel) {
	let el = this,
		docEl = document.documentElement,
		matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector;
	while ( (el = el.previousSibling) && uEl !== el ) {
		if ( matches.call(el, sel) ) { return el; }
	}
	return null;
};
Node.prototype.se_next = function(sel) {
	let el = this,
		docEl = document.documentElement,
		matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector;
	if ( typeof sel === 'string' ) {
		while ( (el = el.nextSibling) ) {
			if ( el.nodeType === 1 && matches.call(el, sel) ) { return el; }
		}
		return null;
	} else {
		return this.nextSibling;
	}
};
Node.prototype.se_nextUntil = function (uEl, sel) {
	let el = this,
		docEl = document.documentElement,
		matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector;
	while ( (el = el.nextSibling) && uEl !== el ) {
		if ( el.nodeType === 1 && matches.call(el, sel) ) { return el; }
	}
	return null;
};
Node.prototype.se_hasParent = function (parent) {
	let el = this;
	do {
		if ( el === parent ) {
			return true;
		}
	} while ( (el = el.parentNode) );
	return false;
};
Node.prototype.se_searchParent = function(parent) {
	let el = this;
	do {
		if ( el.nodeType !== 1 ) {
			return false;
		}
		if ( el.matches(parent) ) {
			return el;
		}
	} while ( (el = el.parentNode) );
	return false;
};
Node.prototype.se_sibling = function (selector) {
	let el = this,
		docEl = document.documentElement,
		matches = docEl.matches || docEl.msMatchesSelector || docEl.oMatchesSelector,
		cEl = this.parentNode.firstElementChild;
	do {
		if ( el !== cEl && matches.call(cEl, selector) ) {
			return cEl;
		}
	} while ( cEl = cEl.nextSibling );
	return false;
};
Node.prototype.se_before = function(cEl) {
	if ( typeof cEl === 'string' ) {
		this.insertAdjacentHTML('beforebegin', cEl);
	} else if ( typeof cEl === 'object' ) {
		this.parentNode.insertBefore(cEl, this);
	} else {
		console.log("Append, caso no definido", typeof cEl, cEl);
	}
	return this.previousElementSibling;
};
Node.prototype.se_after = function (cEl) {
	if ( typeof cEl === 'string' ) {
		this.insertAdjacentHTML('afterend', cEl);
	} else if ( typeof cEl === 'object' ) {
		this.parentNode.insertBefore(cEl, this.nextSibling);
	} else {
		console.log("Append, caso no definido", typeof cEl, cEl);
	}
	return this.nextElementSibling;
};

Node.prototype.se_insertManyFromString = function (mode, structure, content) {
	// Cargar datos
	for ( let i = 0, len = content.length; i < len; i++ ) {
		if ( mode === 'append' ) {
			this.se_append(se.struct.stringPopulate(structure, content[i]));
		} else {
			this.se_prepend(se.struct.stringPopulate(structure, content[i]));
		}
	}
	return this;
};
Node.prototype.se_append = function (cEl) {
	if ( typeof cEl === 'string' ) {
		this.insertAdjacentHTML('beforeend', cEl);
	} else if ( typeof cEl === 'object' ) {
		this.appendChild(cEl);
	} else {
		console.log("Append, caso no definido", typeof cEl, cEl);
	}
	return this.lastElementChild;
};
Node.prototype.se_prepend = function (cEl) {
	if ( typeof cEl === 'string' ) {
		this.insertAdjacentHTML('afterbegin', cEl);
	} else if ( typeof cEl === 'object' ) {
		if ( this.children.length !== 0 ) {
			this.insertBefore(cEl, this.children[0]);
		} else {
			this.append(cEl);
		}
	} else {
		console.log("Append, caso no definido", typeof cEl, cEl);
	}
	return this.firstElementChild;
};
Node.prototype.se_offset = function() {
	let offset = {
		x:window.scrollX,
		y:window.scrollY
	};
	// Al elemento
	let box = this.getBoundingClientRect();
	offset.x+= box.left;
	offset.y+= box.top;
	return offset;
};
Node.prototype.se_on = function(var1, var2, var3) {
	let direct = function (cElem, cEvent, cFunc) {
			cElem.addEventListener(cEvent, cFunc);
		},
		delegate = function (cElem, cEvent, cDelegate, cFunc, subDelegate = false) {
			cElem.addEventListener(cEvent, (e) => {
				let rTarget = e.target.se_closest(cDelegate, cElem);
				if ( rTarget !== null ) {
					cFunc(e, rTarget);
				}
			}, subDelegate);
		},
		//
		typeVar1 = typeof var1,
		typeVar2 = typeof var2,
		typeVar3 = typeof var3,
		//
		eventList, cEvent,
		cSubEl;


	//
	if ( typeVar1 === 'object' ) {
		//
		if ( typeVar2 === 'object' ) {
			// Multiple events on multiple sub elements
			if ( typeVar3 !== 'function' ) {
				console.error('Element - se_on: Delegate . v1-object, v2-object, v3-not valid', typeVar3);
				return this;
			}

			//
			for ( cEvent of var1 ) {
				for ( cSubEl of var2 ) {
					delegate(this, cEvent, cSubEl, var3);
				}
			}
		} else if ( typeVar2 === 'string' ) {
			// Multiple events and funcs related with single sub element, posible subDelegation
			for ( cEvent in var1 ) {
				if ( !var1.hasOwnProperty(cEvent) ) { continue; }
				let cFunc = var1[cEvent];
				if ( typeof cFunc !== "function" ) { console.error("invalid function added"); }
				let subDelegation = ( var3 );
				delegate(this, cEvent, var2, cFunc, subDelegation);
			}
		} else if ( typeVar2 === 'undefined' ) {
			// Multiple events and funcs no sub element
			for ( cEvent in var1 ) {
				if ( !var1.hasOwnProperty(cEvent) ) { continue; }
				let cFunc = var1[cEvent];
				direct(this, cEvent, cFunc);
			}
		} else {
			console.error('Element - se_on: Delegate . v1-object, v2-object, v3-not valid', typeVar3);
		}
	} else if ( typeVar1 === 'string' ) {
		eventList = var1.split(' ');
		if ( typeVar2 === 'function' ) {
			// single/multiple event, single function
			for ( cEvent of eventList ) {
				direct(this, cEvent, var2);
			}
		} else if ( typeVar2 === 'object' ) {
			// single/multiple event, multiple subElements (compatibility with first function)

			// Ignorar si la tercera variable no es una función
			if ( typeVar3 !== 'function' ) {
				console.error('Element - se_on: Delegate . v1-string, v2-object, v3-not valid', typeVar3);
				return this;
			}

			//
			for ( cEvent of eventList ) {
				for ( cSubEl of var2 ) {
					delegate(this, cEvent, cSubEl, var3);
				}
			}
		} else if ( typeVar2 === 'string' ) {
			// single/multiple event, single subElement with function
			if ( typeVar3 !== 'function' ) {
				console.error('Element - se_on: Delegate . v1-string, v2-string, v3-not valid');
				return this;
			}

			//
			for ( cEvent of eventList ) {
				delegate(this, cEvent, var2, var3);
			}
		} else {
			console.log('Element - se_on: Delegate . v1-string, v2-not valid', var2);
		}
	} else {
		console.error('Element - se_on: Method . v1-not valid', var1);
	}
	return this;
};
Node.prototype.se_once = function (cMethod, action) {
	let tFunc = function(e) {
		e.target.removeEventListener(e.type, tFunc);
		return action(e);
	};
	//
	this.addEventListener(cMethod, tFunc);
	return this;
};
Node.prototype.se_classAdd = function(nClass) {
	let classes = nClass.split(' ');
	for ( let i = 0, len = classes.length; i < len; i++ ) {
		this.classList.add(classes[i]);
	}
	return this;
};
Node.prototype.se_classDel = function(nClass) {
	let classes = nClass.split(' ');
	for ( let i = 0, len = classes.length; i < len; i++ ) {
		this.classList.remove(classes[i]);
	}
	return this;
};
Node.prototype.se_classHas = function(nClass) {
	return this.classList.contains(nClass);
};
Node.prototype.se_classToggle = function(nClass) {
	let classes = nClass.split(' ');
	for ( let i = 0, len = classes.length; i < len; i++ ) {
		this.classList.toggle(classes[i]);
	}
	return this;
};
Node.prototype.se_classSwitch = function(rClass, nClass) {
	this.se_classDel(rClass);
	this.se_classAdd(nClass);
	return this;
};
Node.prototype.se_css = function(var1, var2) {
	switch ( typeof(var1) )
	{
		case 'object':
			for ( let cStyle in var1 ) {
				if ( var1.hasOwnProperty(cStyle) ) {
					let value = ( typeof var1[cStyle] !== 'undefined' ) ? var1[cStyle] : null;
					this.style[cStyle] = value;
				}
			}
			return this;
			break;
		case 'string':
			if ( typeof(var2) === 'undefined' ) {
				return this.style[var1];
			} else {
				this.style[var1] = var2;
				return this;
			}
			break;
		default:
			break;
	}
};
Node.prototype.se_plugin = function(plugName, data) {
	// Avoid duplication
	if ( this[plugName] !== undefined ) { console.warn("Plugin already loaded in element.", plugName, this); return; }
	// Check if plugin is actually defined
	if ( typeof se.plugin[plugName] !== 'function' ) { console.error("Plugin no válido", plugName, se.plugin[plugName]); return; }
	// Load plugin
	this[plugName] = new se.plugin[plugName](this, data);
	// Setup self destruct function (if available)
	let destroyFunction = ( typeof this[plugName]._destroy === 'function' ) ? this[plugName]._destroy : null;
	// Define plugin list
	se.pluginManager.pluginList.push({'el':this, 'name':plugName, 'func':destroyFunction});
};
Node.prototype.se_hide = function () {
	let cStyle = document.defaultView.getComputedStyle(this, null);
	if ( cStyle.display !== 'none' ) {
		this.setAttribute('se-display', cStyle.display);
	}
	this.style.display = 'none';
};
Node.prototype.se_show = function () {
	let cStyle = document.defaultView.getComputedStyle(this, null);
	if ( cStyle.display !== 'none' ) { return; }
	this.style.display = this.getAttribute('se-display') || 'block';
	this.removeAttribute('se-display');
};
Node.prototype.se_attr = function (var1, var2) {
	if ( typeof var2 !== 'undefined' ) {
		if ( var2 === '' || var2 === false ) {
			this.removeAttribute(var1);
		} else {
			this.setAttribute(var1, var2);
		}
		return this;
	} else {
		return this.getAttribute(var1);
	}
};
Node.prototype.se_val = NodeList.prototype.se_val = function (var1) {
	if ( typeof var1 !== 'undefined' ) {
		this.value = var1;
		return this;
	} else {
		return this.value;
	}
};
//
// Múltiples
NodeList.prototype.se_testSel = HTMLCollection.prototype.se_testSel = function () {
	if ( this.length ) {
		for ( let i = 0, len = this.length; i < len; i++ ) {
			console.log('testSel', i, this[i]);
		}
	} else {
		console.log('Selección vacía.');
	}
};
NodeList.prototype.se_text = HTMLCollection.prototype.se_text = function (cText) {
	for ( let i = 0, len = this.length; i < len; i++ ) {
		this[i].se_text(cText);
	}
	return this;
};
NodeList.prototype.se_html = HTMLCollection.prototype.se_html  = function (cText) {
	for ( let i = 0, len = this.length; i < len; i++ ) {
		this[i].se_html(cText);
	}
	return this;
};
NodeList.prototype.se_val = HTMLCollection.prototype.se_val = function (cVal) {
	for ( let i = 0, len = this.length; i < len; i++ ) {
		this[i].se_val(cVal);
	}
	return this;
};
NodeList.prototype.se_classAdd = HTMLCollection.prototype.se_classAdd = function (nClass) {
	for ( let i = 0, len = this.length; i < len; i++ ) {
		this[i].se_classAdd(nClass);
	}
	return this;
};
NodeList.prototype.se_classDel = HTMLCollection.prototype.se_classDel = function (nClass) {
	for ( let i = 0, len = this.length; i < len; i++ ) {
		this[i].se_classDel(nClass);
	}
	return this;
};
NodeList.prototype.se_classSwitch = HTMLCollection.prototype.se_classSwitch = function (rClass, aClass) {
	for ( let i = 0, len = this.length; i < len; i++ ) {
		this[i].se_classSwitch(rClass, aClass);
	}
	return this;
};
NodeList.prototype.se_each = HTMLCollection.prototype.se_each = Array.prototype.se_each = function (callback, scope) {
	for ( let i = 0, len = this.length; i < len; i++ ) {
		callback.call(scope, i, this[i]);
	}
	return this;
};
NodeList.prototype.se_css = HTMLCollection.prototype.se_css = function (var1, var2) {
	for ( let i = 0, len = this.length; i < len; i++ ) {
		this[i].se_css(var1, var2);
	}
	return this;
};
NodeList.prototype.se_hide = HTMLCollection.prototype.se_hide = function () {
	for ( let i = 0, len = this.length; i < len; i++ ) {
		this[i].se_hide();
	}
	return this;
};
NodeList.prototype.se_show = HTMLCollection.prototype.se_show = function () {
	for ( let i = 0, len = this.length; i < len; i++ ) {
		this[i].se_show();
	}
	return this;
};
NodeList.prototype.se_data = HTMLCollection.prototype.se_data = function (var1, var2) {
	for ( let i = 0, len = this.length; i < len; i++ ) {
		this[i].se_data(var1, var2);
	}
	return this;
};
NodeList.prototype.se_attr = HTMLCollection.prototype.se_attr = function (var1, var2) {
	for ( let i = 0, len = this.length; i < len; i++ ) {
		this[i].se_attr(var1, var2);
	}
	return this;
};
NodeList.prototype.se_remove = HTMLCollection.prototype.se_remove = function () {
	for ( let i = 0, len = this.length; i < len; i++ ) {
		if ( this[i] && this[i].parentElement ) {
			this[i].parentElement.removeChild(this[i]);
		}
	}
	return this;
};
//</editor-fold>

String.prototype.sprintf = function (obj) {
	let useArguments = false,
		_arguments = arguments,
		i = -1;
	if ( typeof _arguments[0] === "string" ) {
		useArguments = true;
	}
	if ( obj instanceof Array || useArguments ) {
		return this.replace(/\%s/g, function (a, b) {
			i++;
			if ( useArguments ) {
				if ( typeof _arguments[i] === 'string' ) {
					return _arguments[i];
				}
				else {
					throw new Error("Arguments element is an invalid type");
				}
			}
			return obj[i];
		});
	} else {
		return this.replace(/{([^{}]*)}/g, function (a, b) {
			let r = obj[b];
			return typeof r === 'string' || typeof r === 'number' ? r : a;
		});
	}
};

//<editor-fold desc="Animaciones">
Node.prototype.se_fadeIn = function() {
	let transEvent = transitionEndEventName();
	this.style.transition = 'opacity 0.5s';
	this.style.opacity=1;
	return this;
};
Node.prototype.se_fadeOut = function(destroy) {
	let transEvent = transitionEndEventName();
	this.style.transition = 'opacity 0.5s';
	this.style.opacity=0;
	if ( transEvent ) {
		this.addEventListener(transEvent, function(e){
			if (destroy){
				this.se_remove();
			}
		});
	} else {
		if (destroy){
			this.se_remove();
		}
	}
	return this;
};
//</editor-fold>

//</editor-fold>
