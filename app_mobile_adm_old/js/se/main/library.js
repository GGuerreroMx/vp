"use strict";
window.se = {
	plugin:{},
	addon:{}
};
window.site = {
	apps:{},
	properties:{},
	functions:{},
	user:{
		logged:false,
		data:{},
		as:{}
	}
};

//<editor-fold desc="Snk Eng">

//
se.pluginManager = {
	mutationObserver:new MutationObserver(function(mutations) {
		if ( se.pluginManager.pluginList.length === 0 ) { return; }

		// Loop backwards to delete with splice
		let i = se.pluginManager.pluginList.length;
		while ( i-- ) {
			let element = se.pluginManager.pluginList[i];
			if ( !document.body.contains(element.el) ) {
				se.pluginManager.purge(element.el);
				if ( typeof element.func === 'function' ) { element.func(); }
				element.el[element.name] = '';
				se.pluginManager.pluginList.splice(i, 1);
			}
		}
	}),
	pluginList:[],
	purge:function(d) {
		let a = d.attributes, i, l, n;
		if ( a ) {
			for ( i = a.length - 1; i >= 0; i -= 1 ) {
				n = a[i].name;
				if ( typeof d[n] === 'function' ) {
					d[n] = null;
				}
			}
		}
		a = d.childNodes;
		if ( a ) {
			l = a.length;
			for ( i = 0; i < l; i += 1 ) {
				se.pluginManager.purge(d.childNodes[i]);
			}
		}
	}
};

//
se.funcs = {
	after:function(times, func) {
		return function() {
			if ( --times < 1 ) {
				return func.apply(this, arguments);
			}
		};
	},
	before:function(times, func) {
		let memo;
		return function() {
			if ( --times > 0 ) { memo = func.apply(this, arguments); }
			if ( times <= 1 ) { func = null; }
			return memo;
		};
	},
	once:function(func, context) {
		let result;
		return function() {
			if( func ) {
				result = func.apply(context || this, arguments);
				func = null;
			}
			return result;
		};
	},
	delay:function(func, wait) {
		let args = slice.call(arguments, 2);
		return setTimeout(function() {
			return func.apply(null, args);
		}, wait);
	},
	throttle:function(func, threshhold, scope) {
		threshhold = threshhold || 250;
		let last,
			deferTimer;
		return function() {
			let context = scope || this,
				now = +new Date,
				args = arguments;
			if ( last && now < last + threshhold ) {
				// hold on to it
				clearTimeout(deferTimer);
				deferTimer = setTimeout(function() {
					last = now;
					func.apply(context, args);
				}, threshhold);
			} else {
				last = now;
				func.apply(context, args);
			}
		};
	},
	debounce:function(func, wait, immediate) {
		let timeout;
		return function() {
			let context = this,
				args = arguments,
				later = function() {
					timeout = null;
					if ( !immediate ) { func.apply(context, args); }
				},
				callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	}
};

//
se.windowEvent = function(element, action, func) {
	if ( typeof func === 'function' && element ) {
		let newFunc = function(event) {
			if ( document.contains(element) ) {
				func(event);
			} else {
				window.removeEventListener(action, newFunc);
			}
		};
		window.addEventListener(action, newFunc);
	} else { console.log('Función o elemento no válido', element, func); }
};

//
se.dialog = {
	openModal:(cEl) => {
		if ( typeof cEl.showModal === "function") {
			// Check if modal is opened and ignore
			if ( cEl.attributes.hasOwnProperty('open') ) {
				return;
			}

			//
			cEl.showModal();
		} else {
			cEl.se_attr('open', 'true');
		}
	},
	open:(cEl) => {
		if ( typeof cEl.show === "function") {
			cEl.show();
		} else {
			cEl.se_attr('open', 'true').se_classAdd('noModal');
		}
	},
	close:(cEl) => {
		if ( typeof cEl.showModal === "function") {
			cEl.close();
		} else {
			cEl.se_attr('open', '');
		}
	},
};

//
se.main = {
	init:function() {
		// Links
		document.body.se_on('click', 'a[se-nav]', (event, element) => {
			event.preventDefault();
			let post = element.se_data('post'),
				mode = element.se_attr('se-mode');
			if ( typeof post !== 'undefined' ) {
				try {
					post = JSON.parse(post);
				} catch(ex) {
					post = {};
					console.error("post data", post);
				}
			} else {
				post = {};
			}
			//
			if ( mode && mode === 'mobile' ) {
				let checkBoxElement = element.se_closest('[se-elem="mobileMenu"]').querySelector('input[type="checkbox"]');
				if ( checkBoxElement ) {
					checkBoxElement.checked = false;
				}
			}
			//
			se.ajax.pageLink(element.href, element.getAttribute('se-nav'), post);
		});

		// Soft-Scroll
		document.body.se_on('click', 'a[se-scroll]', (event, element) => {
			event.preventDefault();
			event.stopPropagation();
			let dataID = element.getAttribute('href'),
				dataTarget = document.querySelector(dataID),
				dataSpeed = element.getAttribute('data-speed');
			if ( dataTarget ) {
				se.navigation.smoothScroll(dataTarget, dataSpeed || 500);
			}
		});

		// Removed elements
		se.pluginManager.mutationObserver.observe(document.body, { childList:true, subtree:true });

		// History managment
		if ( typeof history.replaceState !== "undefined" ) {
			history.replaceState({'function':'se.ajax.pageInit', 'target':'se_middle', 'url':window.location.pathname + window.location.search }, null, null);
		}
		// History change
		window.onpopstate = function(event) {
			if ( event.state !== null ) {
				let stateObj = event.state;
				stateObj.target = ( $('#'+ stateObj.target) ) ? stateObj.target : 'se_middle';
				executeFunctionByName(stateObj.function, window, stateObj, false);
			}
		};
		// Load files if exist
		let fileList = document.head.querySelector('meta[name="se:files"]').se_attr('content');
		if ( fileList !== '' ) {
			let fileArray = fileList.split(';');
			se.ajax.filesLoadAsync(fileArray);
		}
		site.properties = document.head.querySelector('meta[name="se:properties"]').dataset;
		// Auto plugin load
		se.main.pluginLoad();
	},
	pluginLoad:function(elem) {
		elem = ( typeof elem !== 'undefined' ) ? elem : document.body;
		elem.querySelectorAll('[se-plugin]').se_each((i, el) => {
			let pluginName = el.getAttribute('se-plugin'),
				settings,
				defaults = {};
			//
			try {
				settings = el.querySelector(':scope > script[se-elem="defaults"]')
			} catch ( e ) {
				// Posible errors
				settings = el.querySelector('script[se-elem="defaults"]')
			}

			//
			if ( settings ) {
				try {
					let tDefaults = Function('"use strict";return (' + settings.se_html() + ')')();
					if ( typeof tDefaults === 'object' ) {
						defaults = tDefaults;
					} else {
						console.error("SE PLUGIN - Improper object", tDefaults);
						return;
					}
				} catch(e) {
					console.error('Plugin Load Error: Defaults not valid.', e, settings);
					return;
				}
			}
			el.se_plugin(pluginName, defaults);
		});
	}
};

//
se.element = {
	htmlentities:function(str) {
		return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	},
	toggle:function(id) {
		let el = $(id);
		if ( el !== null ) {
			el.style.display = el.style.display === 'none' ? '' : 'none';
		}
	},
	fixValues:function(cEl) {
		let fixers = cEl.querySelectorAll('[se-fix]'),
			fixVal;
		if ( fixers ) {
			fixers.se_each(function(i, cEl) {
				fixVal = cEl.se_attr('se-fix');
				switch ( cEl.tagName.toLowerCase() ) {
					case 'select':
						cEl.value = fixVal;
						break;
					case 'input':
						switch ( cEl.type) {
							case 'checkbox':
								fixVal = parseInt(fixVal);
								cEl.checked = ( fixVal === 1 );
								break;
							case 'radio':
								let cForm = cEl.se_closest('form');
								cForm.se_formElVal(cEl.name, fixVal);
								break;
							default:
								console.log("AJAXSIMPLE - ElementFix - Input no definido");
								break;
						}
						break;
					default:
						console.log("AJAXSIMPLE - ElementFix - caso no definido", cEl.tagName);
						break;
				}
			});
		}
	},
	childrenUpdate:function(cEl, updateObject) {
		for ( const [cSelector, cOperations] of Object.entries(updateObject) ) {
			let cElement = cEl.querySelector(cSelector);
			//
			if ( !cElement ) {
				console.error("Element not found", cSelector);
				continue;
			}

			//
			for ( let cOp of cOperations ) {
				//
				switch ( cOp[0] ) {
					case 'data':
						cElement.se_data(cOp[1], cOp[2]);
						break;
					case 'text':
						cElement.se_text(cOp[1]);
						break;
					case 'html':
						cElement.se_html(cOp[1]);
						break;
					default:
						console.error("not valid operation", cOp)
						break;
				}
			}
		}
	}
};

//
se.object = {
	merge:function() {
		let objectEnd = {},
			attrname;
		//
		for ( let i = 0; i < arguments.length; ++i ) {
			let cArg = arguments[i];
			if ( cArg ) {
				for ( attrname in cArg ) {
					if ( cArg.hasOwnProperty(attrname) ) {
						objectEnd[attrname] = cArg[attrname];
					}
				}
			}
		}
		//
		return objectEnd;
	},
	serialize:function(obj) {
		let str = [],
			p;
		for ( p in obj )
		{
			if ( obj.hasOwnProperty(p) ) {
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			}
		}
		return str.join("&");
	},
	each:function(obj, callback, scope) {
		for ( let id in obj ) {
			if ( obj.hasOwnProperty(id) ) {
				callback.call(scope, id, obj[id]);
			}
		}
	},
	sort:function(data, key, way = 'ASC') {
		return data.sort(function(a, b) {
			let x = a[key], y = b[key];
			if ( way === 'ASC' ) { return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
			else { return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
		});
	},
	copy:function(origObj) {
		return JSON.parse(JSON.stringify(origObj));
	}
};

//
se.struct = {
	stringPopulate:function(cString, cData) {
		let value, re;
		for ( let cName in cData ) {
			if ( !cData.hasOwnProperty(cName) ) { continue; }
			value = ( cData[cName] !== null ) ? cData[cName] : '';
			re = new RegExp('!' + cName + ';', "g");
			cString = cString.replace(re, value);
		}
		return cString;
	},
	stringPopulateMany:function(cString, cData, modFunction = null) {
		let rString = "", cMod = "",
			rowId, rowCur, cElId, cEl, re;
		for ( rowId in cData ) {
			if ( !cData.hasOwnProperty(rowId) ) { continue; }
			//
			rowCur = cData[rowId];
			cMod = cString;

			// Mod current row
			if ( typeof modFunction === 'function' ) {
				modFunction(rowCur);
			}
			// Convert all elements
			for ( cElId in rowCur ) {
				if ( !rowCur.hasOwnProperty(cElId) ) { continue; }
				cEl = ( rowCur[cElId] !== null ) ? rowCur[cElId] : '';
				re = new RegExp('!' + cElId + ';', "g");
				cMod = cMod.replace(re, cEl);
			}
			rString += cMod;
		}
		return rString;
	},
	notification:function(cEl, cOptions) {
		let defaults = {
				icon:null,
				iconMod:null,
				title:null,
				content:'',
				contentHTML:false,
				style:null,
				autoClear:null
			},
			settings = se.object.merge(defaults, cOptions),
			//
			html = '',
			clearInterval;

		//
		if ( settings.icon ) {
			html+= '<div class="icon">';
			if ( settings.icon.indexOf('.') === -1 )
			{
				settings.iconMod = ( typeof settings.iconMod === 'string' ) ? settings.iconMod : '';
				html+= '<svg class="' + settings.iconMod + '"><use xlink:href="#' + settings.icon + '" /></svg>';
			} else {
				console.log("NOTIFICACION, IMAGEN NO PROGRAMADA");
			}
			html+= '</div>';
		}
		// Contenido
		let htmlContent = ( settings.contentHTML ) ? ' html' : '';
		html+= '<div class="text"><div class="title">' + settings.title  + '</div><div class="content'+ htmlContent +'">' + settings.content + '</div></div>';

		// Apply
		cEl.className = settings.style;
		cEl.innerHTML = html;

		//
		if ( typeof settings.autoClear === 'number' && settings.autoClear !== 0 ) {
			clearInterval = setTimeout(() => {
				cEl.se_empty();
			}, settings.autoClear);
		} else {
			clearTimeout(clearInterval);
		}
	},
	notifAdvanced:function(element, style, title, content, icon, iconMod) {
		se.struct.notification(element, {
			icon:icon,
			iconMod:iconMod,
			title:title,
			content:content,
			style:style
		});
	},
	notifSimple:function(element, style, content) {
		if ( element ) {
			element.className = style;
			element.innerHTML = content;
		}
	},
	errorAlert:function(msg) {
		console.error("Mostrar mensaje de error.", msg);
		let eMsg = msg.s.d + "\n" + msg.s.ex;

		alert(eMsg);
	},
	htmlFromJson:function(cObj) {
		let cStruct = '';

		if ( typeof cObj !== 'object' ) {
			console.error("struct from json, error. type not valid", cObj);
		}

		//
		if ( cObj instanceof Array ) {
			for ( const cEl of cObj ) {
				cStruct+= se.struct.fromJson(cEl);
			}
		} else {
			let iniTag = cObj.tag;

			if ( cObj.hasOwnProperty('attr') ) {
				for ( const aName in cObj.attr ) {
					if ( !cObj.attr.hasOwnProperty(aName) ) { continue; }
					iniTag+= ' ' + aName + '="' + cObj.attr[aName] + '"';
				}
			}

			if ( cObj.hasOwnProperty('children') ) {
				let cBody = '';
				for ( const cEl of cObj.children) {
					cBody+= se.struct.htmlFromJson(cEl);
				}
				//
				cStruct+= `<${ iniTag }>${ cBody }<${ cObj.tag }>`
			} else if ( cObj.hasOwnProperty('content') ) {
				//
				cStruct+= `<${ iniTag }>${ cObj['content'] }<${ cObj.tag }>`
			} else {
				cStruct+= `<${ iniTag } />`
			}
		}

		//
		return cStruct;
	},
	insertDataFromArray:function(parentObject, data, cList) {
		for ( let cEl of cList ) {
			parentObject.querySelector(cEl[0]).innerHTML = data[cEl[1]];
		}
	}
};

//
se.ajax = {
	//
	core:function(url, configuration) {
		let xhr = new XMLHttpRequest(),
			defaults = {
				headers:{},
				method:'GET',
				variables:[],
				response:null,
				return:'text',
				timeOut:0,
				onRequest:null,
				onComplete:null,
				onSuccess:null,
				onFail:null,
				onError:null,
				onProgress:null,
				onAbort:null,
				onTimeOut:null,
			},
			settings = se.object.merge(defaults, configuration);

		//
		if ( settings.response !== null ) {
			//
			se.struct.notification(
				settings.response,
				{
					title:'Enviando información...',
					style:'notification',
					icon:'fa-spinner',
					iconMod:'spin',
					autoClear:false
				}
			);
		}
		if ( typeof settings.onRequest === 'function' ) { settings.onRequest(); }

		//
		xhr.onload = () => {
			let title, content, msg;

			//
			if ( settings.response ) { settings.response.innerHTML = ''; }

			//
			switch ( settings.return ) {
				//
				case 'text':
					// Processing
					msg = xhr.responseText;

					// Results
					if ( xhr.status === 200 ) {
						if ( settings.response !== null ) {
							//
							se.struct.notification(settings.response,
								{
									title:'Enviado',
									style:'notification success',
									icon:'fa-check'
								}
							);
						}

						//
						if ( typeof settings.onSuccess === 'function' ) { settings.onSuccess(msg); }
					}
					else {
						console.log("---\nASYNC CALL ERROR.\n HTTP Status:%s\nCONTENT:\n%s\n---", xhr.status, xhr.responseText);
						//
						if ( settings.response !== null ) {
							//
							se.struct.notification(settings.response,
								{
									title:'ERROR',
									content:msg.s.date,
									style:'notification alert',
									icon:'fa-remove'
								}
							);
						}

						//
						if ( typeof settings.onError === 'function' ) { settings.onError(xhr); }
					}
					break;

				//
				case 'json':
					//
					try {
						let fPost =  xhr.responseText.indexOf("{");
						if ( fPost === -1 ) {
							throw "No json";
						}
						else if ( fPost !== 0 ) {
							console.error("AJAX JSON WITH ERRORS: \n\n", xhr.responseText.trim().substr(0, fPost));
							msg = JSON.parse(xhr.responseText.substr(fPost));
						} else {
							msg = JSON.parse(xhr.responseText);
						}
					}
					catch ( err ) {
						//
						console.error("AJAX JSON ERROR\n\nURL: %s\nStatus: %s\nJSON PARSE ERR:%s.\nRESPONSE:\n---\n", xhr.responseURL, xhr.status, err, xhr.response);
						console.trace();

						//
						if ( settings.response !== null ) {
							//
							se.struct.notification(settings.response,
								{
									title:'ERROR (AJAX)',
									content:'No pudo ser procesada la respuesta. Formato no válido.',
									style:'notification alert',
									icon:'fa-remove'
								}
							);
						}

						//
						if ( typeof settings.onError === 'function' ) { settings.onError(xhr.response); }

						//
						return;
					}

					// Add debug information to console
					if ( msg.hasOwnProperty('debug') ) {
						console.debug("DEBUG:\n\n", msg.debug,  JSON.stringify(msg.debug, null, 2));
					}

					//
					if ( msg.s.t === 1 ) {
						//
						if ( settings.response !== null ) {
							//
							se.struct.notification(settings.response,
								{
									title:( 'd' in msg.s ) ? msg.s.d : 'Enviado',
									content:( 'ex' in msg.s ) ? msg.s.ex : ( 'date' in msg.s ) ? msg.s.date : '',
									style:'notification success',
									icon:'fa-check',
									autoClear:10000
								}
							);

						}

						//
						if ( typeof settings.onSuccess === 'function' ) {
							settings.onSuccess(msg);
						}
					}
					else {
						console.log("AJAX FAIL, t=0:\n---\nE:\t%s \n---\nEX:\t%s", msg.s.d, msg.s.ex);
						//
						if ( settings.response !== null ) {
							//
							se.struct.notification(settings.response,
								{
									title:( 'd' in msg.s ) ? 'ERROR: ' + msg.s.d : 'ERROR: Caso no definido',
									content:( 'ex' in msg.s ) ? msg.s.ex : ( 'date' in msg.s ) ? msg.s.date : '',
									style:'notification alert',
									icon:'fa-remove'
								}
							);
						}

						//
						if ( typeof settings.onFail === 'function' ) {
							settings.onFail(msg);
						}
					}
					//
					break;
				//
				default:
					console.error("xhr caso de retorno de información no definido");
					break;
			}
			//
		};

		//
		xhr.onerror = (ev) => {
			//
			if ( typeof settings.onError === 'function' ) { settings.onError(ev); }
		};

		//
		xhr.onabort = (ev) => {
			//
			if ( typeof settings.onAbort === 'function' ) { settings.onAbort(ev); }
		};

		//
		xhr.onloadend = (ev) => {
			//
			if ( typeof settings.onComplete === 'function' ) { settings.onComplete(ev); }
		};

		// on progress
		if ( typeof settings.onProgress === 'function' ) {
			xhr.upload.addEventListener('progress', settings.onProgress, false);
		}

		// Timeout
		if ( settings.timeOut ) {
			xhr.timeout = settings.timeOut;

			//
			if ( typeof settings.onTimeOut === 'function' ) { xhr.ontimeout = settings.onTimeOut; }
		}

		//
		xhr.open(settings.method, url, true);

		// Headers
		settings.headers['X-Requested-With'] = settings.headers['X-Requested-With'] || 'XMLHttpRequest';
		for ( const name in settings.headers ) {
			if ( !settings.headers.hasOwnProperty(name) ) { continue; }
			xhr.setRequestHeader(name, settings.headers[name]);
		}

		// Post modifier
		switch ( typeof settings.variables )
		{
			//
			case 'object':
				if ( !(settings.variables instanceof FormData) )
				{
					settings.variables = se.object.serialize(settings.variables);
					xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=UTF-8');
				}
				break;
			//
			case 'undefined':
				break;
			//
			default:
				console.trace("XHR: post, objeto no definido");
				break;
		}

		//
		xhr.send(settings.variables);
	},
	//
	json:function(url, post, configuration) {
		// mod to traditional information
		configuration.method = ( typeof post !== 'undefined' && post !== null ) ? 'POST' : 'GET';
		configuration.variables = post;
		configuration.return = 'json';

		//
		return se.ajax.core(url, configuration);
	},
	//
	text:function(url, post, configuration) {
		// mod to traditional information
		configuration.method = ( typeof post !== 'undefined' && post !== null ) ? 'POST' : 'GET';
		configuration.variables = post;
		configuration.return = 'text';

		//
		return se.ajax.core(url, configuration);
	},
	//
	pageLink:function(url, pTarget, post) {
		if ( typeof history.pushState === "undefined" ) { location.href = url; }
		if ( typeof post === "undefined" ) { post = {}; }
		se.ajax.pageInit({'url':url, 'target':pTarget, 'function':'se.ajax.pageInit', 'post':post }, true);
	},
	//
	pageInit:function(stateObj, saveHistory) {
		let cTarget = $('#' + stateObj.target),
			loadBar = cTarget.se_prepend('<div class="loadBar"><div class="loadBarStat"></div></div>'),
			parser = document.createElement('a');

		// Fallback for lacking page
		if ( !cTarget ) {
			document.location.href = parser.pathname;
			return;
		}

		// popular modificación
		parser.href = stateObj.url;
		// Mod
		parser.pathname = '/partial/' + stateObj.target + parser.pathname;
		// show new url
		stateObj.rUrl = parser.href;

		//
		se.ajax.json(stateObj.rUrl, stateObj.post, {
			// headers:[['X-Page-Async-Target', stateObj.target]],
			onProgress:(e) => {
				if ( e.lengthComputable ) {
					loadBar.querySelector('div.loadBarStat').style.width = e.loaded / e.total * 100 + '%';
				}
			},
			onComplete:() => {},
			onSuccess:(msg) => {
				se.ajax.pageLoad(msg.d, stateObj, saveHistory);
			},
			onFail:(msg) => {
				console.log('Page Load Fail.', stateObj, msg.s);
				if ( 'et' in msg.s ) { se.ajax.problemSolver(stateObj, msg); }
				else if ( 'e' in msg.s ) {
					alert('ERROR Sistema. No pudo cargarse la página.');
				}
			},
			onError:(msg) => {
				alert("No pudo ser cargada la página.\n\n", msg);
			}
		});
	},
	problemSolver:function(stateObj, response) {
		let problem = parseInt(response.s.et),
			eMsg = response.s.e + "\n\n" + response.s.ex;

		//
		switch ( problem ) {
			case 1:
				site.functions.user.askLogin();
				break;
			//
			case 307:
			case 308:
				se.ajax.pageInit({'url': response.s.d, 'target': stateObj.target, 'function':'se.ajax.pageInit' }, true);
				break;
			case 400:
				alert(eMsg);
				break;
			case 401:
				alert("Permisos insuficientes:\n\n" + eMsg);
				break;
			case 403:
				alert("Permisos insuficientes.");
				break;
			case 500:
				alert("ERROR en el servidor:\n\n" + eMsg);
				break;
			default:
				console.log("Caso no definido", response);
				alert(eMsg);
				break;
		}
	},
	//
	pageLoad:function(pData, stateObj, saveHistory) {
		// Init
		let head = document.getElementsByTagName('head')[0],
			pageTarget = document.getElementById(stateObj.target),
			cUrl = stateObj.url,
			lastHead = head.querySelector('meta[property="og:type"]'),
			rSimple = function(elem, array) {
				return ( elem in array && array[elem] !== '' ) ? array[elem] : '';
			},
			pluginLoad = function() {
				// Auto-plugins
				se.main.pluginLoad(pageTarget);
				// console.log("load Plugin", pageTarget);
			},
			cEl;
		// Set history
		if ( saveHistory ) { history.pushState(stateObj, pData.title, stateObj.url); }
		// Change content
		pageTarget.innerHTML = pData.body;
		// HEAD
		if ( 'url_path' in pData ) { cUrl = pData.url_path; }
		let cTitle = rSimple('title', pData.head),
			mDesc = rSimple('metaDesc', pData.head),
			mUrl = site.properties.url + rSimple('url', pData.head),
			metaElems = {
				'meta[name="keywords"]':{ content:rSimple('metaWord', pData.head) },
				'meta[name="description"]':{ content:mDesc },
				'link[rel="canonical"]':{ href:mUrl },
				//
				'meta[property="og:title"]':{ content:cTitle },
				'meta[property="og:url"]':{ content:mUrl },
				'meta[property="og:image"]':{ content:rSimple('img', pData.head.og) || site.properties.url + site.properties.image },
				'meta[property="og:description"]':{ content:mDesc },
				'meta[property="og:type"]':{ content:rSimple('type', pData.head.og) },
				//
				'meta[name="twitter:card"]':{ content:rSimple('twType', pData.head.og) },

			},
			cElem, i;
		//
		document.title = ( cTitle === '' ) ? site.properties.name : cTitle+' | '+site.properties.name;
		//
		for ( const [cTarget, cProps] of Object.entries(metaElems) ) {
			if ( ( cElem = head.querySelector(cTarget) ) ) {
				for ( const [cName, cValue] of Object.entries(cProps) ) {
					cElem.setAttribute(cName, cValue);
				}
			}
		}

		// Limpiar de página anterior
		head.querySelectorAll('[data-pageOnly="1"]').se_remove();
		// OG No básicos
		if ( 'data' in pData.head.og && pData.head.og.data.length !== 0 )
		{
			for ( i = 0; i < pData.head.og.data.length; i++ ) {
				cEl = pData.head.og.data[i];
				let propName = (cEl[0] === 'twitter') ? 'name' : 'property';
				lastHead.insertAdjacentHTML('afterend', '<meta data-ogcustom="1" '+propName+'="'+cEl[1]+'" content="'+cEl[2]+'"/>');
			}
		}
		// Extra head
		if ( 'headExtra' in pData && pData.head.extra.length !== 0 )
		{
			for ( i = 0; i < pData.head.extra.length; i++ ) {
				cEl = pData.head.extra[i];
				lastHead.insertAdjacentHTML('afterend', '<'+cEl.type+' data-ogcustom="1" '+cEl.content+'"/>');
			}
		}

		// External async
		let snLink = ( 'socnetlink' in site.properties ) ? site.properties.socnetlink : true;
		if ( snLink )
		{
			if ( typeof FB !== 'undefined' ) { FB.XFBML.parse(pageTarget); }
			if ( typeof twttr !== 'undefined' ) { twttr.widgets.load(); }
			if ( typeof _gaq !== 'undefined' ) { _gaq.push(['_trackPageview', cUrl]); }
			if ( typeof dataLayer !== 'undefined' ) { gtag('send', 'pageview'); }
		}

		// Files
		se.ajax.fileLoadAsync(pData, '#jsAjax', pluginLoad);

		// Scroll
		se.navigation.smoothScroll(pageTarget, 300, true);
	},
	//
	fileLoadAsync:function(pData, jsTarget, onLoad) {
		let jsElem = $(jsTarget),
			jsText = '',
			jsReady = function() {
				window.eval(jsText);
				if ( typeof onLoad === 'function' ) {
					onLoad();
				}
			};
		//
		if ( 'js' in pData ) {
			jsText+= pData.js;
		}
		//
		if ( 'mt' in pData ) {
			jsText += pData.mt;
		}

		//
		try {
			jsElem.text = jsText;
		}
		catch ( e ) {
			try {
				jsElem.innerHTML = jsText;
			}
			catch ( ee ) {
				console.log(ee);
			}
		}
		// Posible new bug
		if ( 'files' in pData ) {
			// Ext (appends to other files)
			if ( 'ext' in pData.files && pData.files.ext.length !== 0 ) {
				//
				for ( let i = 0; i < pData.files.ext.length; i++ ) {
					let cFile = pData.files.ext[i];
					if ( cFile.endsWith('.js') || cFile.endsWith('.js.gz') || cFile.includes('.js?') ) {
						pData.files.js.push(cFile);
					} else if ( cFile.endsWith('.css') || cFile.endsWith('.css.gz') || cFile.includes('.css?') ) {
						pData.files.css.push(cFile);
					} else if ( cFile.endsWith('.svg') || cFile.endsWith('.svgz') ) {
						pData.files.svg.push(cFile);
					} else if ( cFile.includes('//fonts.googleapis.com') ) {
						pData.files.css.push(cFile);
					} else {
						console.log("File async not supported (1): ", cFile);
					}
				}
			}

			// CSS
			if ( 'css' in pData.files && pData.files.css.length !== 0 ) {
				se.ajax.cssLoad(pData.files.css);
			}
			// JS
			if ( 'js' in pData.files && pData.files.js.length !== 0 ) {
				se.ajax.jsLoad(pData.files.js, jsReady);
			} else {
				jsReady();
			}
			// SVG
			if ( 'svg' in pData.files && pData.files.svg.length !== 0 ) {
				se.ajax.svgLoad(pData.files.svg);
			}
		} else {
			console.log("NO FILES");
			jsReady();
		}
	},
	jsFilesRemaining:0,
	jsLoad:function(file, callback) {
		let head = document.getElementsByTagName('head')[0],
			curJS = head.getElementsByTagName('script'),
			notExist, compare, cFile;
		// Create array
		if ( typeof file === 'string' ) { file = [file]; }
		// If not empty
		if ( file ) {
			for ( let i = 0, fLen = file.length; i < fLen; i++ ) {
				cFile = file[i].replace('&amp;', '&');
				notExist = true;
				compare = this.fileRealDir(cFile);
				for ( let j = 0, oLen = curJS.length; j < oLen; j++ ) {
					if ( compare === curJS[j].src ) {
						notExist = false;
					}
				}
				//
				if ( notExist ) {
					let js = document.createElement('script');
					js.async = true;
					js.src = cFile;
					head.appendChild(js);
					if ( typeof callback === 'function' ) {
						se.ajax.jsFilesRemaining++;
						js.se_on('load', function(event) {
							se.ajax.jsFilesRemaining--;
							if ( se.ajax.jsFilesRemaining <= 0 ) {
								callback(event);
								se.ajax.jsFilesRemaining = 0;
							}
						}, this);
					}
				}
			}
		}
		if ( se.ajax.jsFilesRemaining === 0 && typeof callback === 'function' ) {
			callback(null);
		}
	},
	cssLoad:function(file) {
		let head = document.getElementsByTagName('head')[0],
			curCSS = head.getElementsByTagName('link'),
			notExist, compare, cFile;
		// Hacer array
		if ( typeof file === 'string' ) {
			file = [file];
		}
		for ( let i = 0, fLen = file.length; i < fLen; i++ ) {
			cFile = file[i];
			notExist = true;
			compare = this.fileRealDir(cFile);
			for ( let j = 0, oLen = curCSS.length; j < oLen; j++ ) {
				if ( compare === curCSS[j].href ) {
					notExist = false;
				}
			}
			//
			if ( notExist ) {
				let css = document.createElement('link');
				css.rel = "stylesheet";
				css.href = cFile;
				head.appendChild(css);
			}
		}
	},
	svgLoad:function(file) {
		let ajax;
		if ( typeof file === 'string' ) {
			file = [file];
		}
		for ( let i = 0, fLen = file.length; i < fLen; i++ ) {
			ajax = new XMLHttpRequest();
			ajax.open("GET", file[i], true);
			ajax.onload = function(e) {
				document.body.se_append(e.target.response);
			};
			ajax.send();
		}
	},
	fileRealDir:function(file) {
		let compare = file;
		if ( file.indexOf("//") === 0 ) {
			compare = window.location.protocol + file;
		} else if ( file.indexOf("/") === 0 ) {
			let port = (intVal(window.location.port) !== 0) ? ':' + window.location.port : '';
			compare = window.location.protocol + '//' + window.location.hostname + port + file;
		}
		return compare;
	},
	filesLoadAsync:function(files) {
		let i, cFile,
			fjs = [],
			fcss = [],
			fsvg = [];
		//
		for ( i = 0; i < files.length; i++ ) {
			cFile = files[i];
			if ( cFile.endsWith('.js') || cFile.endsWith('.js.gz') || cFile.includes('.js?') ) {
				fjs.push(cFile);
			} else if ( cFile.endsWith('.css') || cFile.endsWith('.css.gz') || cFile.includes('.css?') ) {
				fcss.push(cFile);
			} else if ( cFile.endsWith('.svg') || cFile.endsWith('.svgz') ) {
				fsvg.push(cFile);
			} else if ( cFile.includes('//fonts.googleapis.com') ) {
				fcss.push(cFile);
			} else {
				console.log("File async not supported (2): ", cFile);
			}
		}

		// Enviar
		if ( fjs.length ) {
			se.ajax.jsLoad(fjs);
		}
		if ( fcss.length ) {
			se.ajax.cssLoad(fcss);
		}
		if ( fsvg.length ) {
			se.ajax.svgLoad(fsvg);
		}
	}
};

//
se.url = {
	str_parse:function(string) {
		let result = {},
			vSplit = [], pair,
			i, iLen,
			fChar;
		// Check if query
		if ( string && string.lastIndexOf('=') ) {
			// Remove first char?
			fChar = string.charAt(0);
			if ( fChar === '?' || fChar === '#' ) {
				string = string.substr(1);
			}

			if ( string.lastIndexOf('&') ) {
				vSplit = string.split('&');
			} else {
				vSplit.push(string);
			}
			iLen = vSplit.length;
			for ( i = 0; i < iLen; i++ ) {
				pair = vSplit[i].split('=');
				result[pair[0]] = pair[1];
			}
		}
		//
		return result;
	},
	parse:function() {
		let pathV = decodeURIComponent(window.location.pathname.substring(1)).split('/'),
			pathP = [];
		// Path
		for ( let i = 0; i < pathV.length; i++ ) {
			if ( pathV[i] ) {
				pathP.push(pathV[i]);
			}
		}
		// console.log("url parsing", result);
		return {
			path:pathP,
			search:se.url.str_parse(decodeURIComponent(window.location.search.substring(1))),
			hash:se.url.str_parse(decodeURIComponent(window.location.hash.substring(1)))
		};
	}
};

//
se.form = {
	appendHiddenChildren:(cForm, cData) => {
		if ( cForm.tagName.toLowerCase() !== 'form' ) { console.error('objeto no es un formulario', cForm); return; }
		if ( !Array.isArray(cData) ) { console.error('la información no es un array', cData); return; }

		//
		for ( let cIndex in cData ) {
			if ( !cData.hasOwnProperty(cIndex) ) { continue; }
			let cRow = cData[cIndex];
			//
			cForm.se_append(`<input type="hidden" name="${ cRow[0] }" value="${ cRow[1] }" />`);
		}
	},
	createFromObj:(options) => {
		let defaults = {
				'method':'post',
				'save_url':'',
				'actName':'Guardar',
				'class':'se_form',
				'enctype':'multipart/form-data',
				'elems':[]
			},
			settings = se.object.merge(defaults, options),
			formContent = '',
			xData = '',
			cElemsL, cElem, x, i, length;
		//
		if ( 'data' in settings ) {
			for ( i = 0, length = settings.data.length; i < length; i++ ) {
				xData+= ' ' + settings.data[i].name + '="' + settings.data[i].value + '"';
			}
		}
		//
		formContent+= '<form class="'+settings.class+'" method="'+settings.method+'" action="'+settings.save_url+'" enctype="'+settings.enctype+'"'+ xData +'>';
		cElemsL = settings.elems.length;
		for ( x=0; x<cElemsL; x++ ) {
			cElem = settings.elems[x];
			if ( cElem.ftype === 'hidden' )
			{
				formContent+= '<input type="hidden" name="'+cElem.vname+'" value="'+cElem.dval+'" />';
			}
			else
			{
				//
				cElem.fdesc = ( typeof cElem.fdesc === 'undefined' ) ? '' : cElem.fdesc;

				//
				let dVal = ( 'dval' in cElem ) ? cElem.dval : '',
					isReq = ( 'requiered' in cElem && cElem.requiered ) ? ' required' : '',
					dataBox = '<div class="cont"><span class="title">'+cElem.fname+'</span><span class="desc">'+cElem.fdesc+'</span></div>',
					placeholder = ( 'placeholder' in cElem && cElem.placeholder !== '' ) ? cElem.placeholder : '',
					reg = ( 'regexp' in cElem && cElem.regexp !== '' ) ? regexpsimple[cElem.regexp] : '',
					lMin = ( 'lMin' in cElem && cElem.lMin !== '' ) ? intVal(cElem.lMin) : 0,
					lMax = ( 'lMax' in cElem && cElem.lMax !== '' ) ? intVal(cElem.lMax) : 230,
					val, name, selected, cPat = '';

				//
				formContent += '<div class="separator' + isReq + '">';

				//
				switch ( cElem.ftype ) {
					case 'text':
					case 'search':
						// let cPat = ( reg !== '' ) ? ' pattern="^'+reg+'$"': '';
						cPat = ( reg !== '' ) ? ' pattern="'+reg+'*"': '';
						formContent+= '<label>'+dataBox;
						formContent+= '<input type="'+cElem.ftype+'" name="'+cElem.vname+'" value="'+dVal+'" minlength="'+lMin+'" maxlength="'+lMax+'"'+cPat+''+isReq+'/>';
						formContent+= '</label>';
						break;
					case 'password':
						formContent += '<label>' + dataBox;
						formContent += '<input type="password" name="' + cElem.vname + '" value="' + dVal + '" minlength="'+lMin+'" maxlength="'+lMax+'"' + isReq + ' />';
						formContent += '</label>';
						break;
					case 'tel':
						formContent+= '<label>'+dataBox;
						formContent+= '<input type="tel" name="'+cElem.vname+'" value="' + dVal + '" '+isReq+' />';
						formContent+= '</label>';
						break;
					case 'url':
						formContent+= '<label>'+dataBox;
						formContent+= '<input type="url" name="'+cElem.vname+'" value="' + dVal + '" '+isReq+' />';
						formContent+= '</label>';
						break;
					case 'email':
						formContent+= '<label>'+dataBox;
						formContent+= '<input type="email" name="'+cElem.vname+'" value="' + dVal + '" minlength="'+lMin+'" maxlength="'+lMax+'"'+isReq+' pattern="'+regexpsimple.email+'" />';
						formContent+= '</label>';
						break;
					case 'number':
						let parameters = '';
						parameters+= ( 'step' in cElem && cElem.step !== '' ) ? ' step="'+cElem.step+'"' : '';
						parameters+= ( 'vMin' in cElem && cElem.vMin !== '' ) ? ' min="'+cElem.vMin+'"' : '';
						parameters+= ( 'vMax' in cElem && cElem.vMax !== '' ) ? ' max="'+cElem.vMax+'"' : '';
						//
						formContent+= '<label>'+dataBox;
						formContent+= '<input type="number" name="'+cElem.vname+'" value="'+dVal+'"'+parameters+isReq+'' + cPat + ' />';
						formContent+= '</label>';
						break;
					case 'money':
						formContent+= '<label>'+dataBox;
						formContent+= '<input type="number" name="'+cElem.vname+'" value="'+dVal+'" min="0.00" max="100000.00" step="0.01" '+isReq+' />';
						formContent+= '</label>';
						break;
					case 'moneyInt':
						formContent+= '<label>'+dataBox;
						formContent+= '<input type="number" data-type="moneyInt" name="'+cElem.vname+'" value="'+dVal+'" min="0.00" max="100000.00" step="0.01" '+isReq+' />';
						formContent+= '</label>';
						break;
					case 'date':
						formContent+= '<label>'+dataBox;
						formContent+= '<input type="date" name="'+cElem.vname+'" value="'+dVal+'"'+isReq+''+cPat+' se-plugin="calendarInput" />';
						formContent+= '</label>';
						break;
					case 'time':
						formContent+= '<label>'+dataBox;
						formContent+= '<input type="time" name="'+cElem.vname+'" value="'+dVal+'"'+isReq+''+cPat+' se-plugin="calendarInput" />';
						formContent+= '</label>';
						break;
					case 'datetime-local':
						placeholder = ( placeholder !== '' ) ? placeholder : 'aaaa-mm-dd hh:mm:ss';
						formContent+= '<label>'+dataBox;
						formContent+= '<input type="datetime-local" name="'+cElem.vname+'" value="'+dVal+'"'+isReq+''+cPat+' se-plugin="calendarInput" />';
						formContent+= '</label>';
						break;
					case 'color':
						placeholder = ( placeholder !== '' ) ? placeholder : '#FFFFFF';
						formContent += '<label>' + dataBox;
						formContent += '<input type="color" name="' + cElem.vname + '" value="' + dVal + '"' + isReq + ' pattern="' + regexpsimple.color + '" />';
						formContent += '</label>';
						break;
					//
					case 'file':
						let accept = ( 'accept' in cElem && cElem.accept !== '' ) ? ' accept="'+cElem.accept+'"' : '';
						formContent+= '<label>'+dataBox;
						formContent+= '<input type="file" name="'+cElem.vname+'" '+isReq + accept+' />';
						formContent+= '</label>';
						break;
					//
					case 'textarea':
						let taRows = ( 'rows' in cElem && cElem.rows !== '' ) ? ' rows="'+cElem.rows+'"' : '',
							taCols = ( 'cols' in cElem && cElem.cols !== '' ) ? ' cols="'+cElem.cols+'"' : '';
						formContent+= '<label class="'+isReq+'">'+dataBox+'</label>';
						formContent+= '<textarea name="'+cElem.vname+'" minlength="'+lMin+'" maxlength="'+lMax+'" se-plugin="autoSize"'+taRows+taCols+isReq+'>'+dVal+'</textarea>';
						break;
					//
					case 'textareaesp':
						formContent+= '<label class="'+isReq+'">'+dataBox+'</label>';
						formContent+= '<textarea name="'+cElem.vname+'" minlength="'+lMin+'" maxlength="'+lMax+'" data-textwrap="'+cElem.textWrap+'" data-mode="'+cElem.taemode+'" se-plugin="wysiwyg"'+isReq+'>'+dVal+'</textarea>';
						break;
					//
					case 'checkbox':
						formContent+= '<label class="checkbox"><input type="checkbox" name="'+cElem.vname+'" value="1" />'+dataBox+'</label>';
						break;
					case 'radio':
						formContent+= '<div class="cont radioList">'+dataBox;
						for ( val in cElem.opts )
						{
							name = cElem.opts[val];
							selected = ( val === dVal ) ? 'checked data-check="asdf"' : '';
							formContent+= '<label><input type="radio" name="'+cElem.vname+'" value="'+val+'" '+isReq+' '+selected+'/>'+name+'</label>';
						}
						formContent+= '</div>';
						break;
					//
					case 'list':
					case 'menu':
						formContent+= '<label>'+dataBox;
						formContent+= '<select name="'+cElem.vname+'" '+isReq+'>';
						for ( val in cElem.opts )
						{
							name = cElem.opts[val];
							selected = ( val === dVal ) ? 'selected' : '';
							formContent+= '<option value="'+val+'" '+selected+'>'+name+'</option>';
						}
						formContent+= '</select></label>';
						break;
					//
					default:
						console.log(cElem, "Form type not defined");
						formContent+='ERROR TIPO DE ELEMENTO NO DEFINIDO';
						break;
				}

				//
				formContent+= '</div>';
			}
		}
		formContent+= '<button type="submit">'+settings.actName+'</button>';
		formContent+= '<output se-elem="response"></output>';
		formContent+= '</form>';
		return formContent;
	},
	loadData:(cForm, cData) => {
		let cName;
		//
		cForm.reset();

		//
		for ( cName in cData ) {
			if ( !cData.hasOwnProperty(cName) ) { continue; }
			//
			let cVal = cData[cName],
				elem = cForm.elements.namedItem(cName);

			//
			if ( elem !== null ) {
				if ( elem.tagName === 'INPUT' && elem.type === 'checkbox' ) {
					elem.checked = (intVal(cVal) === 1 || cVal === true);
				} else if ( elem.tagName === 'INPUT' && elem.type === 'hidden' && elem.se_attr('se-plugin-is') === 'calendarInput' ) {
					// Try to find actual calendar input element
					let realTarget = elem.previousElementSibling
					if ( realTarget.se_attr('se-plugin') !== 'calendarInput' ) {
						console.error("no target element found, skip");
						continue;
					}
					realTarget.calendarInput.setValue(cVal);
				} else if ( elem.tagName === 'INPUT' && elem.type === 'number' && elem.se_data('type') === 'moneyInt' ) {
					elem.value = intVal(cVal) / 100;
				} else if ( elem.tagName === 'TEXTAREA' && elem.se_data('mode') === 'textareaesp' ) {
					elem.value = cVal;
					let evt = document.createEvent("HTMLEvents");
					evt.initEvent("change", false, true);
					elem.dispatchEvent(evt);
				} else if ( elem.tagName === 'SELECT' && elem.multiple && cVal && cVal instanceof Array ) {
					let i, iLen = cVal.length,
						options = elem.options, cOption,
						j, jLen = options.length;
					//
					for ( i = 0; i < iLen; i++ ) {
						for ( j = 0; j < jLen; j++ ) {
							cOption = elem.options[j];
							if ( cOption.value === cVal[i] ) { cOption.selected = true; }
						}
					}
				} else {
					elem.value = cVal;
				}
			}
			//
		}
	},
	serializeToObj:(cForm, excludeDisabled, extraOps = null) => {
		let elements,
			data = {};

		//
		excludeDisabled = ( typeof excludeDisabled === 'undefined') ? false : excludeDisabled;
		if ( excludeDisabled ) {
			elements = cForm.querySelectorAll('input:enabled, select:enabled, textarea:enabled');
		} else {
			elements = cForm.elements;
		}

		//
		if ( !elements ) {
			console.error('form.serializeToObj - Formulario vació');
			return data;
		}

		//
		for ( let i = 0; i < elements.length; i++ ) {
			let cEl = elements[i];
			if ( !cEl.name ) {
				console.warn('form.serializeToObj - Elemento del formulario sin nombre. Saltar', cEl);
				continue;
			}

			// Type
			switch ( cEl.tagName ) {
				//
				case 'INPUT':
					switch ( cEl.type ) {
						//
						case 'number':
							data[cEl.name] = floatVal(cEl.value);
							break;
						//
						case 'radio':
							data[cEl.name] = cForm.elements.namedItem(cEl.name).value;
							break;
						//
						case 'checkbox':
							if ( cEl.value === '1' ) {
								data[cEl.name] = (cEl.checked) ? 1 : 0;
							} else {
								data[cEl.name] = cEl.value;
							}
							break;
						//
						default:
							data[cEl.name] = cEl.value;
							break;
					}
					break;
				//
				case 'SELECT':
					console.log("select", cEl);
					if ( cEl.multiple ) {
						console.log("multiple");
						let tResult = [],
							optList = cEl.options, cOpt, j,
							jLen = optList.length;
						//
						for ( j = 0; j < jLen; j++ ) {
							cOpt = optList[j];
							if ( cOpt.selected ) {
								tResult.push(cOpt.value || cOpt.text);
							}
						}
						data[cEl.name] = tResult;
					} else {
						data[cEl.name] = cEl.value;
					}
					break;
				//
				default:
					data[cEl.name] = cEl.value;
					break;
			}
		}

		//
		if ( extraOps ) {
			for ( let cOp in extraOps ) {
				if ( !extraOps.hasOwnProperty(cOp) ) { continue; }
				let cOpContent = extraOps[cOp];
				//
				switch ( cOp ) {
					case 'int':
						for ( let cIndex of cOpContent ) {
							data[cIndex] = parseInt(data[cIndex]);
						}
						break;
				}
			}
		}

		//
		return data;
	},
	serializeObject:(cForm) => {
		let json = {},
			patterns = {
				"validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
				"key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
				"push":     /^$/,
				"fixed":    /^\d+$/,
				"named":    /^[a-zA-Z0-9_]+$/
			},
			elements = cForm.elements,
			len = elements.length
		;

		//
		for ( let i = 0; i < len; i++ ) {
			let cEl = elements[i];
			// Only process valid elements
			if ( cEl.name && patterns.validate.test(cEl.name) ) {
				let k,
					keys = cEl.name.match(patterns.key),
					merge = cEl.value;

				//
				if ( cEl.tagName === 'INPUT' && cEl.type === 'number' ) {
					merge = floatVal(merge);
				}

				// Loop building
				let cPos =json;
				while ( (k = keys.shift()) !== undefined ) {
					if ( !cPos.hasOwnProperty(k) ) {
						cPos[k] = {};
					}

					// Check if last
					if ( keys.length === 0 ) {
						// Asign value
						cPos[k] = merge;
					} else {
						// Advance pointer
						cPos = cPos[k];
					}
				}
			}
		}

		//
		return json;
	}
};

//
se.user = {
	logIn:function(msgs, element) {
		if ( msgs.s.l === 1 ) {
			let redirectText = element.se_data('redirect'),
				targetUrl = ( redirectText ) ? redirectText : ( element.se_data('target') ) ? element.se_data('target') : '/',
				cTarget = $('#se_middle'),
				pTarget = ( cTarget ) ? 'se_middle' : 'se_template_root';

			//
			se.user.loginUpdateAttempt();

			//
			se.ajax.pageLink(targetUrl, pTarget);
		} else {
			if ( msgs.s.c === 1 ) {
				console.log("TURN ON CAPTCHA.");
			} else {
				//
				se.struct.notification(element.querySelector('[se-elem="response"]'),
					{
						title:'ERROR',
						content:'Usuario y/o contraseña no válido.',
						style:'notification alert',
						icon:'fa-remove'
					}
				);
			}
		}
	},
	logOut:function(url) {
		se.ajax.json('/ajax/user/log/out/', {}, {
			onSuccess:(msg) => {
				url = ( url !== undefined ) ? url : '/';
				console.log("LOGOUT", url);
				document.body.se_data('userlogin', 0);
				if ( msg.s.t === 1 ) {
					if ( msg.s.l === 0 ) {
						if ( $('#se_template_root') ) {
							se.ajax.pageLink(url, 'se_template_root');
						} else {
							window.location = url;
						}
					}
				} else {
					alert(msg.s.d);
				}
			}
		});
	},
	loginPageUpdate:() => {
		let bodyDiv = $('body');
		console.log("USER", site.user.as);
		if ( site.user ) {

			bodyDiv.se_data('userlogin', 1);
			bodyDiv.querySelectorAll('[se-site-user]').forEach((cEl, i) => {
				console.log("loop elements", cEl, i);

				switch ( cEl.se_attr('se-site-user') ) {
					//
					case 'name_full':
						cEl.se_text(site.user.as.name_full);
						break;
					//
					case 'image_square':
						let width = cEl.se_data('width'),
							imgWebp = cEl.querySelector('source[type="image/webp"]');
						// Fix posible prev fix of missing image
						if ( !imgWebp ) {
							imgWebp = cEl.se_append('<source type="image/webp"></source>');
						}
						//
						imgWebp.srcset = '/server/image/objects/w_' + width + '/' + site.user.as.image_square + '.webp';
						cEl.querySelector('img').src = '/server/image/objects/w_' + width + '/' + site.user.as.image_square + '.jpg';
						break;
					//
					default:
						console.error("Caso no definido.");
						break;
				}
			});
		} else {
			bodyDiv.se_data('userlogin', 0);
		}
	},
	loginUpdateAttempt:function () {
		se.ajax.json('/ajax/user/log/check/', {}, {
			onSuccess:(msg) => {
				if ( msg.d.s ) {
					site.user = msg.d;
				} else {
					site.user = null;
					se.cookie.del('rememberme');
				}
				se.user.loginPageUpdate();
			}
		});
	},
	logInitialCheck:function() {
		console.log("ATTEMPTING LOGIN CHECK");
		let loginCookie = se.cookie.get('logedin');
		if ( loginCookie ) {
			console.log("Cookie present, attempt reading user data.");
			se.user.loginUpdateAttempt();
		}
	},
	logCheck:function() {
		if ( site.user.data ) {
			return true;
		} else {
			page.user.askLogin();
			return false;
		}
	}
};

//
se.uniqueId = function() {
	let newDate = new Date;
	return newDate.getTime();
};

//
se.randomString = function(strLen) {
	let text = "",
		possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for ( let i=0; i < strLen; i++ ) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
};

//
se.array = {
	shuffle:(array) => {
		let currentIndex = array.length,  randomIndex;

		// While there remain elements to shuffle...
		while ( currentIndex !== 0 ) {
			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex--;

			// And swap it with the current element.
			[array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
		}

		return array;
	}
}

//
se.navigation = {
	smoothScroll:function(anchor, duration, visEnough) {
		// Calculate how far and how fast to scroll
		let startLocation = window.pageYOffset,
			navigation = document.body.querySelector('.navigation'),
			navHeigth = ( navigation ) ? navigation.clientHeight : 0,
			endLocation = anchor.se_offset().y - navHeigth,
			distance = endLocation - startLocation,
			increments = distance / (duration / 16),
			mCounts = Math.ceil(duration / 16) + 3,
			cCounts = 0,
			stopAnimation,
			runAnim = true,
			runAnimation,
		// Scroll the page by an increment, and check if it's time to stop
			animateScroll = function() {
				window.scrollBy(0, increments);
				cCounts++;
				stopAnimation();
			};
		visEnough = ( typeof visEnough !== 'undefined' ) ? visEnough : false;
		// Posibilidad de no animar dado que esta visible
		if ( visEnough && endLocation > startLocation && endLocation < startLocation+window.innerHeight ){
			runAnim = false;
		}
		// Correr animación
		if ( runAnim ) {
			if ( increments >= 0 ) {
				// DOWN
				stopAnimation = function() {
					let travelled = window.pageYOffset;
					if ( travelled >= (endLocation - increments) || (window.innerHeight + travelled) >= document.body.offsetHeight || cCounts > mCounts ) {
						clearInterval(runAnimation);
					}
				};
			} else {
				// UP
				stopAnimation = function() {
					let travelled = window.pageYOffset;
					if ( travelled <= endLocation || cCounts > mCounts || travelled <= 0 ) {
						clearInterval(runAnimation);
					}
				};
			}
			// Loop the animation function
			runAnimation = setInterval(animateScroll, 16);
		}
	},
	redirectPost:function(url, params) {
		let form = document.createElement("form");
		form.method = 'post';
		form.action = url;
		se.object.each(params, function(name, value) {
			console.log(name, value);
			let input = document.createElement('input');
			input.type = "hidden";
			input.name = name;
			input.value = value;
			form.appendChild(input);
		});
		//
		document.body.se_append(form);
		form.submit();
	}
};

//
se.number = {
	toMoney:function(n, c, d, t) {
		// Defaults
		c = isNaN(c = Math.abs(c)) ? 2 : c;
		d = d === undefined ? "." : d;
		t = t === undefined ? "," : t;
		// Variables
		let s = n < 0 ? "-" : "",
			i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
			j;
		//
		j = ( j = i.length ) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	},
	precisionRound:function(number, precision) {
		let factor = Math.pow(10, precision);
		return Math.round(number * factor) / factor;
	},
	randomBetween:function(minValue, maxValue) {
		return Math.floor(Math.random() * (maxValue - minValue + 1) + minValue);
	}
};

//
se.time = {
	secondsToTime:function(seconds) {
		// we will use this function to convert seconds in normal time format
		let secs = Math.abs(seconds),
			hr = Math.floor(secs / 3600),
			min = Math.floor((secs - (hr * 3600)) / 60),
			sec = Math.floor(secs - (hr * 3600) - (min * 60)),
			time = '';
		if ( hr !== 0 ) {
			if ( hr < 10 ) {
				time += '0';
			}
			time += hr + ':';
		}
		if ( min !== 0 ) {
			if ( min < 10 ) {
				time += '0';
			}
			time += min + ':';
		} else {
			time += '00:';
		}
		if ( sec < 10 ) {
			time += '0';
		}
		time += sec;
		if ( seconds < 0 ) {
			time = '-' + time;
		}
		return time;
	},
	timeToSecs:function(text) {
		let time,
			seconds = 0,
			negative;
		text = text.trim();
		negative = ( text.charAt(0) === '-' );
		time = text.split(':');
		switch ( time.length ) {
			case 1:
				seconds = intVal(time[0]);
				break;
			case 2:
				seconds = intVal(time[0]) * 60 + intVal(time[1]);
				break;
			case 3:
				seconds = intVal(time[0]) * 3600 + intVal(time[1]) * 60 + intVal(time[2]);
				break;
		}
		if ( negative ) { seconds = -seconds; }
		return seconds;
	},
	timePad:function(txt) {
		let pad = "00";
		txt = "" + txt;
		return pad.substring(0, pad.length - txt.length) + txt;
	},
	toHHMMSS:function(sec_numb, hoursHide) {
		let hours = Math.floor(sec_numb / 3600),
			minutes = Math.floor((sec_numb - (hours * 3600)) / 60),
			seconds = sec_numb - (hours * 3600) - (minutes * 60);
		if ( hours < 10 ) {
			hours = "0" + hours;
		}
		if ( minutes < 10 ) {
			minutes = "0" + minutes;
		}
		if ( seconds < 10 ) {
			seconds = "0" + seconds;
		}
		if ( hoursHide ) {
			return minutes + ':' + seconds;
		} else {
			return hours + ':' + minutes + ':' + seconds;
		}
	},

	stringToDateObject:function(dateString) {
		// MySQL String Date a DateObj
		let reggie = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/,
			dateArray = reggie.exec(dateString),
			dateObject = new Date(
				(+dateArray[1]),
				(+dateArray[2]) - 1, // Careful, month starts at 0!
				(+dateArray[3]),
				(+dateArray[4]),
				(+dateArray[5]),
				(+dateArray[6])
			);
		return dateObject;
	},
	dateObjectToString:function(dateObject) {
		let dtMonth = dateObject.getMonth() + 1;
		return dateObject.getFullYear().toString() + '-' +
				se.time.timePad(dtMonth.toString()) + '-' +
				se.time.timePad(dateObject.getDate().toString()) + ' ' +
				se.time.timePad(dateObject.getHours().toString()) + ':' +
				se.time.timePad(dateObject.getMinutes().toString()) + ':' +
				se.time.timePad(dateObject.getSeconds().toString());
	},
	prettyDate_esp:function(time){
		let date = new Date((time || "").replace(/-/g, "/").replace(/[TZ]/g, " ")),
			diff = (((new Date()).getTime() - date.getTime()) / 1000),
			day_diff = Math.floor(diff / 86400);

		if ( isNaN(day_diff) || day_diff < 0 || day_diff >= 365000 ) {
			return time;
		}

		return day_diff === 0 && (
			diff < 60 && "Justo ahora" ||
			diff < 120 && "Hace 1 minuto" ||
			diff < 3600 && "Hace " + Math.floor(diff / 60) + " minutos" ||
			diff < 7200 && "Hace 1 hora" ||
			diff < 86400 && "Hace " + Math.floor(diff / 3600) + " horas") ||
			day_diff === 1 && "Ayer" ||
			day_diff < 14 && "Hace " + day_diff + " días" ||
			day_diff < 60 && "Hace " + Math.floor(day_diff / 7) + " semanas" ||
			day_diff < 365 && "Hace " + Math.floor(day_diff / 30) + " meses" ||
			day_diff < 730 && "Hace 1 año" ||
			day_diff < 365000 && "Hace " + Math.floor(day_diff / 365) + " años";
	}
};

//
se.date = {
	addMonths:function(date, months) {
		date.setMonth(date.getMonth() + months);
		return date;
	},
	getWeek:function(d) {
		// Copy date so don't modify original
		d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
		// Set to nearest Thursday: current date + 4 - current day number
		// Make Sunday's day number 7
		d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
		// Get first day of year
		let yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
		// Calculate full weeks to nearest Thursday
		let weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
		// Return array of year and week number
		return [d.getUTCFullYear(), weekNo];
	},
};

//
se.string = {
	// Add links to non link available text
	linkify:function(inputText) {
		let replacedText, replacePattern1, replacePattern2, replacePattern3;

		//URLs starting with http://, https://, or ftp://
		replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
		replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

		//URLs starting with "www." (without // before it, or it'd re-link the ones done above).
		replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
		replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

		//Change email addresses to mailto:: links.
		replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
		replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

		return replacedText;
	}
};

//
se.cookie = {
	set:function(name, data, hours, path) {
		path = ( typeof path === 'undefined') ? '/' : path;
		hours = ( typeof hours === 'undefined') ? null : hours;
		//
		let cookie = name + "=" + data + ";";
		//
		if ( hours ) {
			let d = new Date(), expires;
			d.setTime(d.getTime() + (hours * 60 * 60 * 1000));
			cookie+= "expires=" + d.toUTCString() + ";";
		}
		if ( path ) {
			cookie+= "path=" + path + ";";
		}

		document.cookie = cookie;
	},
	get:function(cname) {
		let name = cname + "=",
			dcookie = decodeURIComponent(document.cookie),
			ca = dcookie.split(';');
		for ( let cCookie of ca ) {
			cCookie = cCookie.trim();
			if ( cCookie.startsWith(name) ) {
				return cCookie.substring(name.length);
			}
		}
		//
		return false;
	},
	del:function(name) {
		document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}
};
//</editor-fold>

//<editor-fold desc="Funciones">

function transitionEndEventName() {
	let i,
		undefined,
		el = document.createElement('div'),
		transitions = {
			'transition': 'transitionend',
			'OTransition': 'otransitionend',  // oTransitionEnd in very old Opera
			'MozTransition': 'transitionend',
			'WebkitTransition': 'webkitTransitionEnd'
		};

	for ( i in transitions ) {
		if ( transitions.hasOwnProperty(i) && el.style[i] !== 'undefined' ) {
			return transitions[i];
		}
	}
}
function animationEndEventName() {
	let i,
		undefined,
		el = document.createElement('div'),
		transitions = {
			'transition': 'animationend',
			'OTransition': 'oanimationend',  // oTransitionEnd in very old Opera
			'MozTransition': 'animationend',
			'WebkitTransition': 'webkitAnimationEnd'
		};

	for ( i in transitions ) {
		if ( transitions.hasOwnProperty(i) && el.style[i] !== 'undefined' ) {
			return transitions[i];
		}
	}
}
function executeFunctionByName(functionName, context /*, args */) {
	let args = [].slice.call(arguments).splice(2);
	let namespaces = functionName.split(".");
	let func = namespaces.pop();
	for ( let i = 0; i < namespaces.length; i++ ) {
		context = context[namespaces[i]];
	}
	return context[func].apply(this, args);
}
let regexpsimple = {
	'decimal':'[-+]?[0-9]+(\\.[0-9]+)?',
	'number':'[0-9]',
	'letter':'[A-z]',
	'names':'[A-z\\u00C0-\\u00ff\\ ]',
	'password':'[A-z0-9.,_\\-\\!\\¡\\?\\¿]',
	'urlnames':'[A-z0-9._\\-]',
	'simpleTitle':'[A-z\\u00C0-\\u00ff0-9\\-., _¡!¿?]',
	'simpleText':'[A-z\\u00C0-\\u00ff0-9\\.\\,\\¡\\!\\¿\\?\\(\\)\\[\\]\\-\\_\\s\\"\\\'\\%\\&\\$\\#\\\]',
	'urlTitle':'[A-z0-9\\-]',
	'urlSimple':'[A-z0-9/\\&\\_\\.\\\\-\\?]',
	'urlPath':'[A-z0-9-_/]+',
	'datetime-local':'[0-2][0-9]{3}\\-[0-1][0-9]\\-[0-3][0-9] [0-5][0-9]\\:[0-5][0-9]\\:[0-5][0-9]',
	'time':'[0-9]{1,2}:[0-9]{1,2}',
	'color':'#[a-fA-F0-9]{6}',
	'email':'[A-z0-9_\\-\\.]+[@][A-z0-9_-]+([.][A-z0-9_-]+)*[.][A-z]{2,4}'
};
function intVal(value) {
	value = parseInt(value);
	if ( isNaN(value) ) {
		value = 0;
	}
	return value;
}
function floatVal(value) {
	value = parseFloat(value);
	if ( isNaN(value) ) {
		value = 0;
	}
	return value;
}
function uniqId() {
	return (new Date()).getTime();
}
let isMobile = {
	Android:function() {
		return navigator.userAgent.match(/Android/i);
	},
	BlackBerry:function() {
		return navigator.userAgent.match(/BlackBerry/i);
	},
	iOS:function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	Opera:function() {
		return navigator.userAgent.match(/Opera Mini/i);
	},
	Windows:function() {
		return navigator.userAgent.match(/IEMobile/i);
	},
	any:function() {
		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	}
};
function isElementInViewport (el) {
	let rect = el.getBoundingClientRect();
	return (rect.top >= 0 && rect.left >= 0 && rect.bottom <= (window.innerHeight) && rect.right <= (window.innerWidth));
}
//</editor-fold>

if ( typeof String.prototype.endsWith !== 'function' ) {
	String.prototype.endsWith = function(suffix) {
		return this.indexOf(suffix, this.length - suffix.length) !== -1;
	};
}

//
String.prototype.sprintf = function(obj) {
	let useArguments = false,
		_arguments = arguments,
		i = -1;
	if ( typeof _arguments[0] === "string" ) {
		useArguments = true;
	}
	if ( obj instanceof Array || useArguments ) {
		return this.replace(/\%s/g, function(a, b) {
			i++;
			if ( useArguments ) {
				if ( typeof _arguments[i] === 'string' ) {
					return _arguments[i];
				}
				else {
					throw new Error("Arguments element is an invalid type");
				}
			}
			return obj[i];
		});
	} else {
		return this.replace(/{([^{}]*)}/g, function(a, b) {
			let r = obj[b];
			return typeof r === 'string' || typeof r === 'number' ? r : a;
		});
	}
};
