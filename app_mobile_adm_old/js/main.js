﻿"use strict";

//
se_app.initialization = function() {
	//
	window.onpopstate = se.uwp.page.popState;

	// CB
	function jsInitialize() {
		console.log( "MAIN - IDB LOADED, JS START");
		// initialize background task

		// Crear appstates
		let tempAppState = localStorage.getItem( 'app.state' );
		//
		if ( tempAppState ) {
			app.state = JSON.parse( tempAppState );

			// already has info about current status, connect
			se_app.server_connect();
		}

		// General bindings
		se_app.bluetooth = new bluetooth();

		// Navigation
		document.body.se_on( 'click', '[se-nav-uwp]', se.uwp.page.navigation );

		// Load images (icons)
		fetch( 'img/se-icons.svg' )
			.then(
				( response ) => {
					if ( response.status !== 200 ) {
						console.log( 'Looks like there was a problem. Status Code: ' + response.status );
						return;
					}
					//
					response.text().then( ( data ) => {
						document.body.se_append(data);
					} );
				}
			)
			.catch( ( err ) => {
				console.log( 'Icons not loaded: ', err );
			});
		//



		// Initialize page
		se.uwp.page.urlParse(window.location.search, false);
	}

	// Indexed DB
	console.log("MAIN - IDB INITIALIZE CALL", site.idb);
	app.idb = new se_indexedDataBase(site.idb, jsInitialize );

	// Service Worker
	if ( 'serviceWorker' in navigator ) {
		navigator.serviceWorker.register('/admin/snkeng_main_sw.js')
			.then((reg) => {
				console.log("SW Registration:");
				if ( reg.installing ) {
					console.log('SW installing');
				} else if ( reg.waiting ) {
					console.log('SW installed');
				} else if ( reg.active ) {
					console.log('SW active');
				}
			})
			.catch((error) => {
				console.log("SW REGISTER FAILURE", error);
			});
	} else {
		console.log('Service workers aren\'t supported in this browser.');
	}
};

//
window.addEventListener('DOMContentLoaded', se_app.initialization);
