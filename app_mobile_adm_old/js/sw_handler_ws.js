﻿"use strict";

//
class ws_server {
	status = false;
	sw_object = undefined;

	//
	constructor(hasOnline, hasLocal, data = null) {
		//
		if ( !window.Worker ) {
			alert("NO Webworker support... Kill Self");
			return;
		}

		//
		if ( typeof(this.sw_object) !== "undefined" ) {
			console.error("Objeto ya inicializado");
			return;
		}

		// Define what kind of server

		console.warn("ws_server constructor data", data);

		//
		if ( hasOnline ) {
			this.sw_object = new Worker("/admin/js/workers/server_ws.js");
		} else if ( hasLocal ) {
			this.sw_object = new Worker("/admin/js/workers/server_local.js");
		} else {
			console.error("incorrect options for new class");
			return;
		}

		console.log("SW WS - INIT");

		// CB Assign
		this.sw_object.onmessage = this.sw_onMessage;

		// First operation (connect mainly)
		if ( data ) {
			this.sw_postMessage(data);
		}
	}

	//
	disconnect() {
		this.sw_object.terminate();
		this.sw_object = undefined;
	}

	//
	sw_onMessage(e) {
		console.trace("what");
		let msg = e.data;

		//
		console.log("MAIN - SW MESSAGE RECIEVED:", msg);

		//
		switch ( msg.op ) {
			//
			case 'adv':
				pages.match.admin.timer.advanceWork(msg);
				if ( msg.notif ) {
					se_app.notifications.add('Match', {
						body:msg.notif,
						// icon:'icons/chrome_36.png',
						vibrate:[500, 300, 500, 300, 500]
					});
				}
				break;

			//
			case 'field':
				let page_dynamic_mobile = $('#page_field_dynamic_mobile'),
					page_dynamic_server = $('#page_field_dynamic_server');
				if ( page_dynamic_mobile ) {
					page_dynamic_mobile.uwp_page_field_dynamic_mobile.serverMessageRecieve(msg.id, msg.data);
				} else if ( page_dynamic_server ) {
					page_dynamic_mobile.uwp_page_field_dynamic_server.serverMessageRecieve(msg.id, msg.data);
				}
				break;

			//
			default:
				console.error("Server worker unknown message", e.data);
				break;
		}
	}

	//
	sw_postMessage(data) {
		if ( !this.sw_object ) {
			console.error("no service workder object");
			return;
		}

		//
		this.sw_object.postMessage(data);
	}
}
//
