"use_strict";

var ws_socket,
	server_field_id = null;

//
function init() {
	//
	self.addEventListener('message', ww_messageFromMain, false);
}

//
function ws_open() {
	console.log("WS - Open");
}

//
function ws_close(e) {
	console.log("WS - Closed");
}

//
function ws_message(ev) {
	console.log("WS - Msg - IN:", ev.data);

	console.log("SW - MQTT - Incomming: QOS:%s. Retain:%s.\nT: %s\nC: %s\n", message.qos, message.retained, message.destinationName, message.payloadString);

	// Operaciones
	let path = message.destinationName.split('/'),
		actList;

	//
	switch ( path[0] ) {
		//
		case 'server':
			break;
		//
		case 'notifications':
			break;
		//
		case 'fields':
			let fieldId = path[1];
			//
			if ( !fieldId ) {
				console.log("No hay campo.", fieldId);
				return;
			}
			//
			if ( server_field_id !== parseInt(fieldId) ) {
				console.error("Not current field.", msg.op_data.id, server_field_id, typeof msg.op_data.id, typeof server_field_id);
				return;
			}
			//
			switch ( path[2] ) {
				//
				case 'try':
					break;
				//
				case 'actions':
					let data = [];

					actList = message.payloadString.split(';');

					//
					for ( let cAct of actList ) {
						let cActList = cAct.split(':');
						//
						if ( cActList[0] === 'b' ) {
							data.push({
								type:'buzzer',
								buzzSide:parseInt(cActList[1]),
								buzzType:parseInt(cActList[2])
							});
						} else if ( cActList[0] === 's' ) {
							data.push({
								type:'sound',
								soundSide:parseInt(cActList[1]),
								soundFileName:parseInt(cActList[2])
							});
						} else if ( cActList[0] === 't' ) {
							data.push({
								type:'timer',
								timeInt:parseInt(cActList[1])
							});
						} else if ( cActList[0] === 'n' ) {
							data.push({
								type:'note',
								note:cActList[1]
							});
						} else {
							console.error("Action add attemp failed", cAct, cActList);
						}
					}

					setFieldAction({
						type:'actions',
						data:data
					});
					break;
				//
				case 'status':
					actList = message.payloadString.split(';');
					setFieldAction({
						type:'status',
						data:{
							active:actList[0],
							fase:actList[1],
							sectionC:actList[2]
						}
					});
					break;
				//
				case 'notif':
					//
					switch ( path[3] ) {
						//
						case 'points':
							actList = JSON.parse(message.payloadString);
							setFieldAction({
								type:'points',
								data:actList
							});
							break;
						//
						case 'event':
							actList = JSON.parse(message.payloadString);
							setFieldAction({
								type:'event',
								data:actList
							});
							break;
						//
						default:
							console.error("MQTT, CAMPO, Operación no reconocida de notificación", path[3]);
							break;
					}
					break;
				//
				case 'gameStatus':
					//
					switch ( path[4] ) {
						//
						case 'try':

							break;
						//
						case 'set':
							//
							setFieldAction({
								type:'gameStatus',
								data:{
									index:parseInt(path[3]),
									value:parseInt(message.payloadString)
								}
							});
							break;
						//
						default:
							console.error("MQTT, CAMPO, gameStatus. Operación no reconocida", path[4]);
							break;
					}
					break;
				//
				case 'device':
					//
					switch ( path[4] ) {
						//
						case 'try':
							break;
						//
						case 'set':
							actList = message.payloadString.split(';');
							setFieldAction({
								type:'device',
								data:{
									id:path[3],
									active:actList[0],
									side:actList[1],
									status:actList[2],
								}
							});
							break;
						//
						default:
							console.error("MQTT, CAMPO, Dispositivo. Operación no reconocida", path[4]);
							break;
					}
					break;
				//
				default:
					console.error("MQTT, CAMPO, Operación no reconocida", path[2]);
					break;
			}
			break;
		//
		default:
			break;
	}
}

//
function ws_send(data) {
	console.log("WS - Msg - OUT:", data);
	ws_socket.send(data);
}

//
function ws_error(error) {
	console.log("WS error", error);
}

//
function serverConnect(cData) {
	ws_socket = new WebSocket(cData.url);
	ws_socket.onopen = ws_open;
	ws_socket.onmessage = ws_message;
	ws_socket.onclose = ws_close;
	ws_socket.onerror = ws_error;
}

//
function ww_messageFromMain(e) {
	let msg = e.data,
		id;

	//
	console.log("SW_WS - MESSAGE FROM MAIN: ", msg);
	//
	switch ( msg.op_type[0] ) {
		//
		case 'field':
			//
			switch ( msg.op_type[1] ) {
				//
				case 'command':
					//
					if ( server_field_id !== msg.op_data.id ) {
						console.error("Not current field.", msg.op_data.id, server_field_id, typeof msg.op_data.id, typeof server_field_id);
						return;
					}

					//
					switch ( msg.op_data.op ) {
						//
						case 'setup':
							mqtt.ops.sendMessage("fields/"+server_field_id+"/try", msg.op_data.data);
							break;
						//
						case 'gameStatus':
							// Proper conversion
							mqtt.ops.sendMessage("fields/"+server_field_id+"/gameStatus/"+msg.op_data.data.index.toString()+"/try", msg.op_data.data.value.toString());
							break;
						//
						case 'device':
							// Proper conversion
							mqtt.ops.sendMessage("fields/"+server_field_id+"/device/"+msg.op_data.id.toString()+"/try", "" + msg.op_data.data.status.toString() + ";" + msg.op_data.data.side.toString() + ";");
							break;
						//
						case 'point':
							// Proper conversion
							mqtt.ops.sendMessage("fields/"+server_field_id+"/action/"+msg.op_data.actId+"/try", msg.op_data.userId.toString());
							break;
						//
						default:
							console.error("MQTT Send attempt failed. no data op valid", msg);
							break;
					}
					break;

				//
				case 'set':
					console.log("FIELD SET: ", msg.op_data.id);
					server_field_id = parseInt(msg.op_data.id);
					// MQTT Update
					mqtt.ops.subscribe([
						[2, 'notifications'],
						[2, 'fields/'+server_field_id+'/status'],
						[2, 'fields/'+server_field_id+'/gameStatus/set'],
						[2, 'fields/'+server_field_id+'/device/+/set'],
						[0, 'fields/'+server_field_id+'/actions'],
						[2, 'fields/'+server_field_id+'/notif/#']
					], true);
					break;

				//
				case 'resync':
					break;
				//
				default:
					console.error("WORKER: Invalid server field/match operation");
					break;
			}
			break;

		//
		case 'appChange':
			//
			switch ( msg.op_type[1] ) {
				//
				case 'connect':
					serverConnect(msg.op_data);
					break;
				//
				case 'disconnect':
					// mqtt.ops.stop();
					break;
				//
				default:
					console.error("WORKER: Invalid server field operation");
					break;
			}
			break;

		//
		default:
			console.error("SW Message, op invalid.");
			break;
	}
}

//
function setFieldAction(actData) {
	postMessage({
		op:'field',
		id:server_field_id,
		data:actData
	});
}

//
init();