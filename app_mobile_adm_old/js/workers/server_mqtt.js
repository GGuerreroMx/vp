"use_strict";

// Field operations
importScripts('idb.js', 'msg_interpreter.js', 'mqtt.js', 'paho-mqtt.js');

// SW Data
var server_field_id,
	worker;

//
function init() {
	// Load IDB
	idb.ops.initialize('vp_device', 1, {
		onSuccess:() => {
			// Bind
			console.log("SW - IDB: READY");
		},
		onFail:() => {
			console.error("SW - IDB: ERROR. Not initialized.");
		}
	});

	//
	self.addEventListener('message', ww_messageFromMain, false);
}

//
function serverConnect(cData) {
	mqtt.ops.start(cData.url, cData.user, cData.pass);
}

//
function ww_messageFromMain(e) {

	let msg = e.data,
		id;

	//
	console.log("SW: MESSAGE FROM MAIN RECIEVED.", msg);

	//
	switch ( msg.op_group ) {
		//
		case 'field':
			//
			switch ( msg.op_current ) {
				//
				case 'command':
					//
					if ( server_field_id !== msg.op_data.id ) {
						console.error("Not current field.", msg.op_data.id, server_field_id, typeof msg.op_data.id, typeof server_field_id);
						return;
					}

					//
					switch ( msg.op_data.op ) {
						//
						case 'setup':
							mqtt.ops.sendMessage("fields/"+server_field_id+"/try", msg.op_data.data);
							break;
						//
						case 'gameStatus':
							// Proper conversion
							mqtt.ops.sendMessage("fields/"+server_field_id+"/gameStatus/"+msg.op_data.data.index.toString()+"/try", msg.op_data.data.value.toString());
							break;
						//
						case 'device':
							// Proper conversion
							mqtt.ops.sendMessage("fields/"+server_field_id+"/device/"+msg.op_data.id.toString()+"/try", "" + msg.op_data.data.status.toString() + ";" + msg.op_data.data.side.toString() + ";");
							break;
						//
						case 'point':
							// Proper conversion
							mqtt.ops.sendMessage("fields/"+server_field_id+"/action/"+msg.op_data.actId+"/try", msg.op_data.userId.toString());
							break;
						//
						default:
							console.error("MQTT Send attempt failed. no data op valid", msg);
							break;
					}
					break;
				//
				case 'set':
					console.log("SW: FIELD SET: ", msg.op_data.id);
					server_field_id = parseInt(msg.op_data.id);
					// MQTT Update
					mqtt.ops.subscribe([
						[2, 'notifications'],
						[2, 'fields/'+server_field_id+'/status'],
						[2, 'fields/'+server_field_id+'/gameStatus/set'],
						[2, 'fields/'+server_field_id+'/device/+/set'],
						[0, 'fields/'+server_field_id+'/actions'],
						[2, 'fields/'+server_field_id+'/notif/#']
					], true);
					break;
				//
				case 'resync':
					break;
				//
				default:
					console.error("WORKER: Invalid server field/match operation");
					break;
			}
			break;

		//
		case 'appChange':
			//
			switch ( msg.op_current ) {
				//
				case 'connect':
					serverConnect(msg.op_data);
					break;
				//
				case 'disconnect':
					mqtt.ops.stop();
					break;
				//
				default:
					console.error("WORKER: Invalid server field operation");
					break;
			}
			break;

		//
		default:
			console.error("SW Message, op invalid.");
			break;
	}
}

//
init();