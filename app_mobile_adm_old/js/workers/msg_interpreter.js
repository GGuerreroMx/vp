"use_strict";

//
function messageInterpreter(msgHead, msgBody) {
	// Operaciones
	let path = msgHead.split('/'),
		actList;

	//
	switch ( path[0] ) {
		//
		case 'server':
			break;
		//
		case 'notifications':
			break;
		//
		case 'fields':
			let fieldId = path[1];
			//
			if ( !fieldId ) {
				console.log("No hay campo.", fieldId);
				return;
			}
			//
			if ( server_field_id !== parseInt(fieldId) ) {
				console.error("Not current field.", msg.op_data.id, server_field_id, typeof msg.op_data.id, typeof server_field_id);
				return;
			}
			//
			switch ( path[2] ) {
				//
				case 'try':
					break;
				//
				case 'actions':
					let data = [];

					actList = msgBody.split(';');

					//
					for ( let cAct of actList ) {
						let cActList = cAct.split(':');
						//
						if ( cActList[0] === 'b' ) {
							data.push({
								type:'buzzer',
								buzzSide:parseInt(cActList[1]),
								buzzType:parseInt(cActList[2])
							});
						} else if ( cActList[0] === 's' ) {
							data.push({
								type:'sound',
								soundSide:parseInt(cActList[1]),
								soundFileName:parseInt(cActList[2])
							});
						} else if ( cActList[0] === 't' ) {
							data.push({
								type:'timer',
								timeInt:parseInt(cActList[1])
							});
						} else if ( cActList[0] === 'n' ) {
							data.push({
								type:'note',
								note:cActList[1]
							});
						} else {
							console.error("Action add attemp failed", cAct, cActList);
						}
					}

					setFieldAction({
						type:'actions',
						data:data
					});
					break;
				//
				case 'status':
					actList = msgBody.split(';');
					setFieldAction({
						type:'status',
						data:{
							active:actList[0],
							fase:actList[1],
							sectionC:actList[2]
						}
					});
					break;
				//
				case 'notif':
					//
					switch ( path[3] ) {
						//
						case 'points':
							actList = JSON.parse(msgBody);
							setFieldAction({
								type:'points',
								data:actList
							});
							break;
						//
						case 'event':
							actList = JSON.parse(msgBody);
							setFieldAction({
								type:'event',
								data:actList
							});
							break;
						//
						default:
							console.error("MQTT, CAMPO, Operación no reconocida de notificación", path[3]);
							break;
					}
					break;
				//
				case 'gameStatus':
					//
					switch ( path[4] ) {
						//
						case 'try':

							break;
						//
						case 'set':
							//
							setFieldAction({
								type:'gameStatus',
								data:{
									index:parseInt(path[3]),
									value:parseInt(msgBody)
								}
							});
							break;
						//
						default:
							console.error("MQTT, CAMPO, gameStatus. Operación no reconocida", path[4]);
							break;
					}
					break;
				//
				case 'device':
					//
					switch ( path[4] ) {
						//
						case 'try':
							break;
						//
						case 'set':
							actList = msgBody.split(';');
							setFieldAction({
								type:'device',
								data:{
									id:path[3],
									active:actList[0],
									side:actList[1],
									status:actList[2],
								}
							});
							break;
						//
						default:
							console.error("MQTT, CAMPO, Dispositivo. Operación no reconocida", path[4]);
							break;
					}
					break;
				//
				default:
					console.error("MQTT, CAMPO, Operación no reconocida", path[2]);
					break;
			}
			break;
		//
		default:
			break;
	}
}