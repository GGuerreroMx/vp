﻿"use strict";
var se_app = {};

//
se_app.request = function ( url, postData, conditions ) {
	// entorno web
	se.ajax.json(app.state.settings.serverMain + url, postData, conditions);
};

//
se_app.stateUpdate = function() {
	// Put the object into storage
	console.log("app.state: updating. ", app.state);
	localStorage.setItem( 'app.state', JSON.stringify( app.state));
};

//
function uwpNavLoad(e, cBtn) {
	e.preventDefault();
	let cTarget = $('#' + cBtn.se_attr('se-nav-uwp')),
		cUrl = cBtn.se_data('src');
	uwp_pageLoad(cTarget, cUrl);
}

//
se_app.server_connect = () => {

	if ( app.server ) {
		return;
		// app.server.disconnect();
	}

	//
	app.server = null;

	console.log("app state for server");

	//
	switch ( app.state.status.serverMode ) {
		// No connection 100% offline
		case 'score_mobile':
			break;
		// Pseudo connection to local server
		case 'dynamic_mobile':
			//
			app.server = new ws_server(false, true);
			break;
		//
		case 'score_server':
			//
			app.server = new ws_server(true, false, {
				'op_type':['appChange', 'connect'],
				'op_data':{
					'url':'wss://' + window.location.hostname + '/admin/ws',
				}
			});
			break;
		//
		case 'dynamic_server':
			se_app.ws.connect('wss://' + window.location.hostname + '/admin/ws/');
			break;
		//
		default:
			console.error("estado del servidor no definido", app.state.status);
			return;
			break;
	}

	//
	switch ( app.state.connection.method ) {
		//
		case 'mqtt':
			app.server = new mqtt_server({
				'op_type':['appChange', 'connect'],
				'op_data':{
					'url':window.location.hostname,
					'logged':app.state.logged,
					'user':app.state.connection.mqtt.user,
					'pass':app.state.connection.mqtt.pass,
				}
			});
			break;
		//
		case 'websocket':

			break;
		//
		default:
			console.error("server connection method not defined.");
			break;
	}
};

//
se_app.ws = {
	object:null,
	status:{
		connected:false,
	},
	connect:(url) => {
		se_app.ws.object = new WebSocket(url);
		se_app.ws.object.onopen = se_app.ws.onConnect;
		se_app.ws.object.onclose = se_app.ws.onDisconnect;
		se_app.ws.object.onmessage = se_app.ws.onMessage;
		se_app.ws.object.onerror = se_app.ws.onError;
	},
	message:(message) => {
		console.log("WS - MESSAGE OUT", message);
		if ( !se_app.ws.status.connected ) {
			console.log("WS - NO CONNECTION.");
			return;
		}
		se_app.ws.object.send(message);
	},
	onConnect:(msg) => {
		se_app.ws.status.connected = true;
		console.log("WS - CONNECTED", msg);
	},
	onDisconnect:(msg) => {
		se_app.ws.status.connected = true;
		console.log("WS - DISCONNECTED", msg);
	},
	onMessage:(msg) => {
		console.log("WS - MESSAGE IN", msg);
		let msgData;
		//
		try {
			msgData = JSON.parse(msg.data);
		}
		catch ( err ) {
			console.log(err);
			return;
		}

		//
		switch ( msgData.type ) {
			//
			case 'connect':

				break;
			//
			case 'chat_msg':
				break;
			//
			default:
				console.error("tipo no soportado", msg);
				break;
		}
	},
	onError:(msg) => {
		console.log("WS - ERROR", msg);
	},
};

// Notificaicones
se_app.notifications = {
	toggle:(ev) => {
		console.log("change attemp");
		if ( ev.target.checked ) {
			if ( !("Notification" in window) ) {
				// Soporte
				console.log("Notifications: Not supported");
			} else if ( Notification.permission === "granted" ) {
				// Activadas, iniciar stream
				app.state.settings.notifications = true;
				//
				let notification = new Notification("Notificaciones activadas.");
				setTimeout(() => {
					notification.close(); //closes the notification
				}, 3000);
			} else if ( Notification.permission !== 'denied' ) {
				// Permisos
				Notification.requestPermission(function(permission) {
					// Activadas, iniciar stream
					app.state.settings.notifications = true;
					// If the user is okay, let's create a notification
					if ( permission === "granted" ) {
						let notification = new Notification("Notificaciones activadas.");
						setTimeout(() => {
							notification.close(); //closes the notification
						}, 3000);
					}
				});
			}
		} else {
			app.state.settings.notifications = false;
		}
		// Guardar
		se_app.stateUpdate();
	},
	add:(title, params, timeOut, actions) => {
		if ( !app.state.settings.notifications ) { console.log("No notifications.", title, params); return; }
		//
		let notification = new Notification(title, params);
		if ( params.vibrate ) {
			window.navigator.vibrate(params.vibrate);
		}
		if ( typeof timeOut !== 'undefined' && timeOut !== 0 ) {
			setTimeout(function() { notification.close(); }, timeOut);
		}
	}
};